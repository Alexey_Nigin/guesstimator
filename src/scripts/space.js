// Usage:
//     let space;
//     ...
//     function setup() {
//         ...
//         space = new Space();
//         bigContainer.addChild(space);
//         ...
//     }
//     ...
//     function tick() {
//         ...
//         space.update(dt);
//         ...
//     }

import {Container, Rectangle} from "pixi.js";
import {round} from "./utils.js";
import {Vector} from "./i.js"; // ./vector.js
import {Random} from "./i.js"; // ../random.js
import {logger} from "./i.js"; // ./logger.js
import {DEBUG, sq} from "./i.js"; // ./dump.js
import {CodeFactoryError} from "./i.js"; // ./code-factories.js
import {InvalidConfigError} from "./i.js"; // ./validators.js
import {Guesstimator} from "./i.js"; // ./guesstimator.js
import {Scanner, Grid} from "./i.js"; // ./scanner.js
import {Density} from "./i.js"; // ./density.js
import {Particles} from "./i.js"; // ./particles.js
import {Thing} from "./i.js"; // ./thing.js
import {Component, Item} from "./i.js"; // ./drop.js
import {Player} from "./i.js"; // ./player.js
import {Cossette} from "./i.js"; // ./cossette.js
import {Prankster} from "./i.js"; // ./prankster.js
import {Meteorus} from "./i.js"; // ./meteorus.js
import {Box} from "./i.js";
import {Station} from "./i.js"; // ./station.js
import {Axon} from "./i.js";
import {Damsel, Tower} from "./i.js";
import {Spiderdome} from "./i.js";
import {Interactor} from "./i.js"; // ./interactor.js
import {Background} from "./i.js";

// Debug only
import {Compass} from "./i.js";

const SAVE_CLASSNAMES = new Map([
	[Axon,      "AXON"     ],
	[Box,       "BOX"      ],
	[Damsel,    "DAMSEL"   ],
	[Player,    "PLAYER"   ],
	[Prankster, "PRANKSTER"],
	[Station,   "STATION"  ],
	[Tower,     "TOWER"    ]
]);

function getClassFromClassname(classname) {
	for (let entry of SAVE_CLASSNAMES.entries()) {
		if (entry[1] === classname) return entry[0];
	}

	logger.error("space.js", `unknown classname: ${classname}`);
	return null;
}



export class Space extends Container {
	// Create a new space manager
	// Only create one space manager per application; creating more results in
	// undefined behavior.
	//
	// Args:
	//   game     - reference to the game that created this space
	//   savefile - optional savefile
	constructor(game, savefile) {
		super();

		this.game = game;

		this.compassDirection = savefile?.compassDirection ?? Random.angle();

		this._spawnQueue = [];
		this._nextThingId = savefile?.nextThingId ?? 0;

		this._grid = new Grid();
		this.addChild(this._grid);

		this.scanner = new Scanner(this);

		// TODO: Possibly turn this into a map?
		this.things = [];

		this.background = new Background(this);
		this.addChild(this.background.display);

		this.particles = new Particles(this);
		this.addChild(this.particles.displayBack);

		// thingDisplays contain PIXI objects for displaying things on screen
		this.thingDisplays = new Container();
		this.thingDisplays.sortableChildren = true;
		this.addChild(this.thingDisplays);

		this.addChild(this.particles.displayFront);

		this.time = savefile?.time ?? 0;

		if (savefile?.th !== undefined) {
			// Things are already saved, spawn them from the savefile
			savefile.th.forEach(entry => {
				let thingClass = getClassFromClassname(entry[0]);
				if (thingClass === null) return;

				let config = entry[1];
				let thing = this.requestSpawn(thingClass, config);
				if (thingClass === Player) this.player = thing;
			});
			// The game REALLY doesn't like it if the player doesn't exist, so create a
			// player with default config if there was no player in the savefile
			if (this.player === undefined) this.player = this.requestSpawn(Player, {});
		} else {
			// No things saved

			this.player = this.requestSpawn(Player, {});
			let originStation = this.requestSpawn(Station, {s: "ORIGIN"});
			// Add an arrow to the origin station
			// TODO: This timeout is a little hacky, but without it the arrow would be deleted
			// at the start of the first tick.
			setTimeout(() => {this.game.unicenter.gps.arrows.add(originStation.id)}, 100);
			this.requestSpawn(Station, {s: "FORGE"});
			this.requestSpawn(Station, {s: "GRAND"});

			this.requestSpawn(Box, {holder: originStation.id});

			this.requestSpawn(Prankster, {});
			this.requestSpawn(Damsel, {});
			this.requestSpawn(Spiderdome, {});
		}
		this.requestSpawn(Meteorus, {
			posGenerator: `motion/circle/0/0/100/${30 * 60}/${- Math.PI / 2}`,
			time: this.time
		});
		this.requestSpawn(Meteorus, {
			posGenerator: `motion/circle/0/0/100/${30 * 60}/0`,
			time: this.time
		});
		this.requestSpawn(Meteorus, {
			posGenerator: `motion/circle/0/0/100/${30 * 60}/${Math.PI / 2}`,
			time: this.time
		});
		this.requestSpawn(Cossette, {
			sleepPosGenerator: `motion/circle/0/0/200/${60 * 60}/${- 2 * Math.PI / 5}`,
			time: this.time
		});
		if (DEBUG) {
			// Add a Meteorus close to the station for testing purposes
			this.requestSpawn(Meteorus, {
				posGenerator: `motion/circle/0/0/9/${30 * 60}/${Math.PI}`,
				time: this.time
			});
			for (let i = 0; i < 30; ++i) {
				this.requestSpawn(Compass, {pos: new Vector(3, 0).chooseNearby(7)});
			}
		}

		this.addChild(this.scanner);

		this.guesstimator = new Guesstimator(this);
		this.interactor = new Interactor(this);

		// Set tactical report stuff to reasonable empty values because density queries
		// the tactical report early
		this._tacticalReportFull = [];
		this._tacticalReportCache = new Map();

		this.density = new Density(this);

		// Whether the player can currently dock
		// false or a string describing what we're docking to.
		this.canDock = false;

		// Add event listeners
		this.interactive = true;
		this.hitArea = new Rectangle(0, 0, 1, 1);
		this.on("pointerdown", this._startFiring);
		this.on("pointerup", this._stopFiring);
		this.on("pointerupoutside", this._stopFiring);

		// Make sure various thing lookups return correct values right away
		this._handleSpawnRequests();
	}

	// Advance all things in space by one frame
	// dt is the time since last update, in seconds. screenSize is a vector
	// containing the screen size, in pixels.
	update(dt, screenSize) {
		// Increment time at the start of the method because this method runs
		// after dt has already passed.
		this.time += dt;

		let pixelsPerFlare = Math.sqrt((screenSize.x * screenSize.y) / (8 * 6));
		// Hack: update hit area
		this.hitArea.width = screenSize.x;
		this.hitArea.height = screenSize.y;

		this._handleSpawnRequests();
		this._guesstimate(dt, screenSize, pixelsPerFlare);
		this._generateTacticalReport();
		this._move(dt);
		this._collide(dt);
		this._kill();
		this._redraw(screenSize, pixelsPerFlare);

		this.background.update(this.time, pixelsPerFlare);

		this.particles.update(dt, screenSize, pixelsPerFlare);
	}

	// Request a conversion from pos to position
	// pos is the pos to be converted; pixelsPerFlare is a real number. This
	// function is only guaranteed to work if the player hasn't moved since the
	// player was last redrawn.
	requestPosToPosition(pos, pixelsPerFlare) {
		return Vector.diff(pos, this.player.pos).scale(pixelsPerFlare)
			.add(this.player.display.position);
	}

	// Request to spawn a thing
	// thingClass is a reference to a class, and config is a config object.
	// config is enriched and then used to construct an instance of thingClass.
	// The resulting thing is added to space at the start of the next tick.
	// This function adds new properties to config. This function also returns a
	// reference to the newly spawned thing. Keep in mind that the reference is
	// returned immediately, but the thing is still only added to space on the
	// next tick.
	requestSpawn(thingClass, config) {
		// Save a copy of the provided config for logging purposes
		let configToLog = JSON.stringify(config);

		// Enrich config
		if (config.id === undefined) config.id = this._nextThingId++;
		config.space = this;

		// Spawn the thing
		try {
			let thing = new thingClass(config);
			this._spawnQueue.push(thing);
			return thing;
		} catch(e) {
			if ((e instanceof InvalidConfigError)
							|| (e instanceof CodeFactoryError)) {
				logger.error(
					"space.js",
					// TODO: make this log message minimization-friendly
					`invalid config for spawning ${thingClass.name}:`
									+ ` ${configToLog} (reason: ${e.message})`
				);
				return {};
			} else {
				throw e;
			}
		}
	}

	// Request a tactical report
	// filter is a function used to filter down the tactical report; it takes in
	// a report entry, and outputs true if this entry should be included and
	// false if this entry shouldn't be included. cacheName is a string under
	// which the request may be cached; requests with the same cacheName must
	// have the same filter.
	requestTacticalReport(filter, cacheName) {
		if (!this._tacticalReportCache.has(cacheName)) {
			this._tacticalReportCache.set(
				cacheName,
				this._tacticalReportFull.filter(filter)
			);
		}
		return this._tacticalReportCache.get(cacheName);
	}

	// Request a reference to the thing with the specific id
	// id is an integer. If the thing with the provided id is not found, the
	// function returns null.
	requestThingById(id) {
		let thing = this.things.find(t => (t.id === id));
		if (thing === undefined) return null;
		return thing;
	}

	// Request to drop drops
	// pos/radius/vel refer to the parent thing. dropList is an instance of
	// DropList.
	requestDrop(pos, radius, vel, dropList) {
		for (let itemName of dropList.getItems()) {
			this.requestSpawn(Item, {
				itemName: itemName,
				parentPos: pos,
				parentRadius: radius,
				parentVel: vel
			});
		}
		for (let componentName of dropList.getComponents()) {
			this.requestSpawn(Component, {
				componentName: componentName,
				parentPos: pos,
				parentRadius: radius,
				parentVel: vel
			});
		}
	}

	// Save the state of space into an object
	save() {
		let state = {
			time: round(this.time),
			compassDirection: round(this.compassDirection),
			nextThingId: this._nextThingId
			// TODO: Possibly save this._spawnQueue in some way?
		};

		state.th = [];
		for (let thing of this.things) {
			// Not ready to save all things just yet
			let savedClasses = Array.from(SAVE_CLASSNAMES.keys());
			if (!savedClasses.some(c => thing instanceof c)) continue;

			let config = thing.save();

			state.th.push([
				SAVE_CLASSNAMES.get(thing.constructor),
				config
			]);
		}

		return state;
	}

	// Spawn requested things and clear the spawn queue
	_handleSpawnRequests() {
		if (this._spawnQueue.length > 0) {
			this.things.push(...this._spawnQueue)
			this.thingDisplays.addChild(
				...this._spawnQueue.map(thing => thing.display)
			);
			for (let spawn of this._spawnQueue) {
				this.interactor.addHitbox(spawn.hitbox);
			}
			this._spawnQueue = [];
		}
	}

	// Spawn and despawn enemies according to the guesstimation
	// dt is the time since last update, in seconds.
	_guesstimate(dt, screenSize, pixelsPerFlare) {
		this.guesstimator.updateBubble(Vector.scale(screenSize, 1 / pixelsPerFlare));

		// Despawn things
		for (let i = this.things.length; i--;) {
			let victim = this.things[i];
			if (!victim.CAN_DESPAWN) continue;
			if (victim.time_outside_bubble === null
							|| victim.DESPAWN_SAFETY_WINDOW === null) {
				logger.error(
					"space.js",
					"victim doesn't have required parameters for despawning"
				);
				continue;
			}
			if (this.guesstimator.isOutsideBubble(victim.pos)) {
				victim.timeOutsideBubble += dt;
			} else {
				victim.timeOutsideBubble = 0;
			}
			// Using <= to always continue when timeOutsideBubble === 0
			if (victim.timeOutsideBubble <= victim.DESPAWN_SAFETY_WINDOW) {
				continue;
			}
			// First false is to not drop items, second is to not drop particles
			victim.prepareToDie(false, false);
			this.things.splice(i, 1);
			this.thingDisplays.removeChild(victim.display);
		}

		// Calculate a weight for each spawn pool
		let currentWeights = new Map(Object.values(Density.POOLS).map(pool => [pool, 0]));
		for (let thing of this.things) {
			// Cheap check first
			if (thing.GUESSTIMATION_WEIGHT === 0) continue;
			if (this.guesstimator.isOutsideBubble(thing.pos)) continue;
			console.assert(thing.pool !== null);
			currentWeights.set(
				thing.pool,
				currentWeights.get(thing.pool) + thing.GUESSTIMATION_WEIGHT
			);
		}

		let targetWeights = this.density.getDensities(this.player.pos, this.time);
		if (this.game.debugger !== undefined) {
			this.game.debugger.reportDensities(targetWeights);
		}
		targetWeights.forEach((v, k, map) => {
			map.set(k, v * this.guesstimator.getOversize())
		});

		// Spawn new things
		targetWeights.forEach((targetWeight, pool) => {
			while (currentWeights.get(pool) < targetWeight) {
				let freshSpawn = this.requestSpawn(
					...this.density.selectThing(
						this.guesstimator.selectSpawn(),
						this.time,
						pool
					)
				);
				if (freshSpawn.GUESSTIMATION_WEIGHT <= 0) {
					logger.error(
						"space.js",
						"Attempting to spawn things with GUESSTIMATION_WEIGHT <= 0"
										+ " can crash the game, aborting spawning"
										+ " for the tick"
					);
					return;
				}
				currentWeights.set(
					pool,
					currentWeights.get(pool) + freshSpawn.GUESSTIMATION_WEIGHT
				);
			}
		});
	}

	// Generate a new tactical report
	_generateTacticalReport() {
		this._tacticalReportFull = this.things
			.filter(thing => (thing.state === Thing.STATES.ALIVE))
			.map(thing => {
				return {
					// Commonly needed properties
					id:                thing.id,
					identity:          thing.identity,
					TYPE:              thing.TYPE,
					CAN_PICK_UP_ITEMS: thing.CAN_PICK_UP_ITEMS,
					IS_BOSS:           (thing instanceof Meteorus),
					name:              thing.name,
					pos:               new Vector(thing.pos),
					isPlayer:          thing === this.player,
					// Reference to the thing - can be checked for more obscure properties
					thing:             thing
				}
			});
		this._tacticalReportCache = new Map();
	}

	// Move all things
	// dt is the time since last update, in seconds.
	_move(dt) {
		for (let thing of this.things) {
			thing.move(dt, this.time);
		}
	}

	// Collide all things with each other
	// dt is the time since last update, in seconds.
	_collide(dt) {
		// Assume we can't dock, interactor will correct this if necessary
		this.canDock = false;

		this.interactor.handleAllInteractions(dt, this.time);

		// If the player is already docking, then we really can't dock, whatever
		// the interactor says
		if (this.player.dockingTo !== null) this.canDock = false;
	}

	// Destroy all dead things
	_kill() {
		this.things.forEach(thing => {
			if (thing.state === Thing.STATES.DEAD) {
				this.thingDisplays.removeChild(thing.display);
			}
		});

		this.things = this.things.filter(thing => (thing.state !== Thing.STATES.DEAD));
	}

	// Redraw everything on the screen
	// screenSize is a vector containing the screen size, in pixels.
	// pixelsPerFlare is a real number.
	_redraw(screenSize, pixelsPerFlare) {
		// Redraw the player first
		// By redrawing the player first, we put an accurate value into
		// requestPosToPosition().
		this.player.redraw(screenSize, pixelsPerFlare);
		for (let thing of this.things) {
			if (thing !== this.player) thing.redraw(screenSize, pixelsPerFlare);
		}
		this._grid.redraw(
			screenSize,
			pixelsPerFlare,
			this.requestPosToPosition(new Vector(), pixelsPerFlare)
		);
		this.scanner.redraw(screenSize, pixelsPerFlare);
	}

	// Start firing the weapon
	_startFiring() {
		this.player.firingMouse = true;
	}

	// Stop firing the weapon
	_stopFiring() {
		this.player.firingMouse = false;
	}
}
