// Misc utils
//
// Import this directly, without i.js.

// Round x to the specified number of places after decimal point (defaults to 6)
export function round(x, places) {
	let tempMultiplier = Math.pow(10, places ?? 6);
	return Math.round(x * tempMultiplier) / tempMultiplier;
}



// TODO: Deduplicate with dump.js
function lerp(min, max, fraction) {
	return min + fraction * (max - min);
}

// Smoothly interpolate between two trajectories, matching value and slope at both ends
// Returns:
//   A function mapping time to value
export function createSmoothInterpolation(
	startTime, startValue, startSlope, endTime, endValue, endSlope
) {
	// (startTime, startValue, startSlope) defines a straight line.
	// (endTime, endValue, endSlope) defines another straight line.
	// That is what "straight lines" in the comments below refer to.

	// Total time the interpolation spans
	let timeDuration = (endTime - startTime);

	// Extend the straight line from the start to get a value at one-third time
	let oneThird = startValue + (timeDuration / 3) * startSlope;
	// Extend the straight line from the end to get a value at two-thirds time
	let twoThirds = endValue - (timeDuration / 3) * endSlope;

	return (time) => {
		// Outside the interpolated area, just return the straight lines
		if (time <= startTime) return startValue - (startTime - time) * startSlope;
		if (time >= endTime) return endValue + (time - endTime) * endSlope;

		// Fraction of the time from start to end that has passed so far
		let timeFraction = (time - startTime) / timeDuration;

		// Bezier curve time!
		// Use startValue, oneThird, twoThirds, and endValue to construct a Bezier curve.
		let a = lerp(startValue, oneThird, timeFraction);
		let b = lerp(oneThird, twoThirds, timeFraction);
		let c = lerp(twoThirds, endValue, timeFraction);
		let d = lerp(a, b, timeFraction);
		let e = lerp(b, c, timeFraction);
		return lerp(d, e, timeFraction);
	};
}



// Math-y smooth interpolation - use when minimizing max acceleration is really important
// AKA dead code I am too proud of to delete

function _optimizeMiddle(a, b, d, e) {
	// Ideal c for (a, b, c)
	let x = 2 * b - a;
	// Ideal c for (c, d, e)
	let y = 2 * d - e;
	// Ideal c for (b, c, d) - twice as important as x and y
	let z = (b + d) / 2;

	// Badness of choosing c based on pairs of (x, y, z)
	let xyHeight = Math.abs(x - y) / 2;
	let xzHeight = Math.abs(x - z) * 2 / 3;
	let yzHeight = Math.abs(y - z) * 2 / 3;

	// Use the biggest height to choose c
	if (xyHeight > xzHeight && xyHeight > yzHeight) {
		return (x + y) / 2;
	} else if (xzHeight > yzHeight) {
		return (x + 2 * z) / 3;
	}
	return (y + 2 * z) / 3;
}

export function createOptimizedSmoothInterpolation(
	startTime, startValue, startSlope, endTime, endValue, endSlope, iterations
) {
	if (iterations === undefined) iterations = 5;

	let timeDuration = endTime - startTime;

	let points = [
		startValue, endValue
	];

	for (let iteration = 0; iteration < iterations; ++iteration) {
		// Add a new point in the middle of each line segment
		let oldPoints = points;
		points = Array(2 * oldPoints.length - 1);
		for (let i = 0; i < points.length; ++i) {
			points[i] = (oldPoints[Math.floor(i / 2)] + oldPoints[Math.ceil(i / 2)]) / 2;
		}

		// Adjust the points to minimize max acceleration

		// Time between successive points, used for end conditions
		let intervalWidth = timeDuration / (points.length - 1);

		function adjustPoint(i) {
			let a = (i === 1)
				? (startValue - intervalWidth * startSlope)
				: points[i - 2];
			let b = points[i - 1];
			let d = points[i + 1];
			let e = (i === points.length - 2)
				? (endValue + intervalWidth * endSlope)
				: points[i + 2];
			points[i] = _optimizeMiddle(a, b, d, e);
		}

		for (let i = 1; i < points.length - 1; ++i) {
			adjustPoint(i);
		}
		for (let i = points.length - 2; i >= 1; --i) {
			adjustPoint(i);
		}
	}

	return (time) => {
		if (time <= startTime) return startValue - (startTime - time) * startSlope;
		if (time >= endTime) return endValue + (time - endTime) * endSlope;

		let timeFraction = (time - startTime) / (endTime - startTime);
		let indexBefore = Math.floor(timeFraction * (points.length - 1));
		let indexAfter = Math.ceil(timeFraction * (points.length - 1));

		let intervalWidth = (endTime - startTime) / (points.length - 1);
		let intervalFraction = (time - startTime) / intervalWidth - indexBefore;

		return (1 - intervalFraction) * points[indexBefore]
						+ intervalFraction * points[indexAfter];
	};
}
