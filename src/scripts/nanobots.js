import {Vector} from "./i.js"; // ./vector.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {AI} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js

class NanobotsAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Id of the parent thing, usually player
		new RequiredProperty("parentId", v => Number.isFinite(v)),
		// Direction of travel, relative to parent
		new RequiredProperty("relativeDir", v => Number.isFinite(v))
	]);

	// Lifetime of the nanobots swarm, in seconds
	static LIFETIME = 2;

	constructor(thing, config) {
		NanobotsAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._parentId = config.parentId;
		this._relativeDir = config.relativeDir;

		// Initialize pos, vel, rot
		// TODO: Don't do this when loading? Store age in AI?
		let parent = this.thing.space.requestThingById(this._parentId);
		if (parent === null) this.thing.prepareToDie();
		this.thing.pos.copyFrom(parent.pos);
		this.thing.vel.copyFrom(parent.vel);
		this.thing.rot = parent.rot + this._relativeDir;
	}

	move(dt, time) {
		let parent = this.thing.space.requestThingById(this._parentId);
		if (parent === null) {
			this.thing.prepareToDie();
			return;
		}

		this.thing.age += dt;

		if (this.thing.age >= NanobotsAI.LIFETIME) {
			this.thing.prepareToDie();
			return;
		}

		let oldPos = new Vector(this.thing.pos);

		this.thing.pos.copyFrom(Vector.sum(
			parent.pos,
			new Vector(1, 0)
				.rotateBy(parent.rot + this._relativeDir)
				.scale(0.75 * Math.pow(
					Math.sin(Math.PI * this.thing.age / NanobotsAI.LIFETIME),
					1 / 3
				))
		));

		this.thing.vel.copyFrom(Vector.diff(this.thing.pos, oldPos).scale(1 / dt));

		this.thing.rot = parent.rot + this._relativeDir;
	}
}



export class Nanobots extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Id of the parent thing, usually player
		new RequiredProperty("parentId", v => Number.isFinite(v)),
		// Direction of travel, relative to parent
		new RequiredProperty("relativeDir", v => Number.isFinite(v)),
		// pos of this swarm, a vector with values in flares
		new OptionalProperty("pos", v => (v instanceof Vector)),
		// vel of this swarm, a vector with values in flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// rot of this swarm, in radians
		new OptionalProperty("rot", v => Number.isFinite(v)),
		// age of this swarm, in seconds
		new OptionalProperty("age", v => (Number.isFinite(v) && v >= 0)),
	]);

	// Create a swarm of nanobots
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Nanobots.CONFIG_STRUCTURE.test(config);

		config.texture = "images/nanobots.svg";
		config.zIndex  = Z_INDICES.PLAYER_CHILD;
		config.len     = 0.05;
		config.type    = THING_TYPES.HEALING;
		config.faction = FACTIONS.PLAYER;
		config.canDespawn = false;

		// Dummy values, until AI is initialized
		if (config.pos === undefined) config.pos = new Vector();
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = 0;

		config.maxHealth = 1;

		super(config);

		this.ai = new NanobotsAI(this, {
			parentId: config.parentId,
			relativeDir: config.relativeDir
		});

		this.age = config.age ?? 0;
	}

	// Return true iff the swarm is ready to heal
	isReadyToHeal() {
		return this.age >= NanobotsAI.LIFETIME / 2;
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		this.prepareToDie(false);
	}

	// Save this swarm into a config object
	// See the comment on Thing.save() for more details.
	save() {
		// TODO
	}
}
