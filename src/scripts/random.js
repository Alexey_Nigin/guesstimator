// Randomizers

export class Random {
	// For now, there is no reason to construct instances of Random, because
	// this class only has static methods. If we ever decide to use a fancier
	// PRNG than Math.random(), then we can write a constructor to seed or
	// otherwise initialize that PRNG, and in that case we will need to start
	// using instances of Random.

	// Return a uniformly distributed random number between a and b
	static uniform(a, b) {
		return a + (b - a) * Math.random();
	}

	// Returns a uniformly distibuted random int between a and b inclusive
	// TODO Dead code as of 2024-09-21, delete if still unused a while from now
	static uniformInt(a, b) {
		return Math.floor(a + (b - a + 1) * Math.random());
	}

	// Return a random angle, in radians
	static angle() {
		return 2 * Math.PI * Math.random();
	}

	// Return a random element of array
	// If the chosen element is an object, this method returns a reference to
	// it.
	static element(array) {
		return array[Math.floor(array.length * Math.random())];
	}

	// Return a weighted random integer
	// weights is an array of non-negative real numbers. This function returns
	// an integer in the range [0, weights.length), with the probability of
	// returning i being proportional to weights[i].
	static weighted(weights) {
		let target = weights.reduce((a, c) => (a + c)) * Math.random();
		let current = 0;
		for (let i = 0; i < weights.length - 1; ++i) {
			current += weights[i];
			if (target < current) return i;
		}
		return weights.length - 1;
	}
}
