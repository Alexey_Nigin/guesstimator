import {Random} from "./i.js"; // ./random.js
import {Vector} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
import {functionFactory} from "./i.js"; // ./code-factories.js
import {Asteroid} from "./i.js"; // ./asteroid.js
import {Compass} from "./i.js";
import {Beacony} from "./i.js"; // ./size-reversal/beacony.js
import {Sizy} from "./i.js"; // ./size-reversal/sizy.js

export class Density {
	// Spawn pools, for keeping balance between different things
	static POOLS = {
		// Largely static things, e.g. Beacony
		STATIC: "STATIC",
		// More dynamic things, e.g. Asteroid
		DYNAMIC: "DYNAMIC"
	};

	static ASTEROID_FIELD_CENTER = new Vector();
	// These centers are copies of the posGenerator parameters of Meteoruses
	// TODO: DRY this up to make such copying unnecessary.
	static METEORUS_HOLE_CENTERS = [
		functionFactory(
			`motion/circle/0/0/100/${30 * 60}`
							+ `/${- Math.PI / 2}`
		),
		functionFactory(
			`motion/circle/0/0/100/${30 * 60}/0`
		),
		functionFactory(
			`motion/circle/0/0/100/${30 * 60}/${Math.PI / 2}`
		)
	];
	// Meteoritus doesn't exist yet but someday
	static METEORITUS_HOLE_CENTER = functionFactory(
		`motion/circle/0/0/150/${30 * 60}/${Math.PI}`
	);

	// Create a new density object
	// Do we even need an instance of Density or can we change the methods to be
	// static?
	constructor(space) {
		this.space = space;

		// The asteroid field around the origin station
		this._asteroidField = this._createDonut(
			Density.ASTEROID_FIELD_CENTER,
			[
				{r:   0, d:  2},
				{r: 150, d: 25},
				{r: 200, d:  0}
			]
		);

		// Holes in the asteroid field around the origin station
		this._holes = Density.METEORUS_HOLE_CENTERS.map(
			center => this._createDonut(
				center,
				[
					{r:  0, d: 0},
					{r: 10, d: 0},
					{r: 20, d: 1}
				]
			)
		).concat([
			this._createDonut(
				Density.METEORITUS_HOLE_CENTER,
				[
					{r:  0, d: 0},
					{r: 20, d: 0},
					{r: 40, d: 1}
				]
			)
		]);

		this._asteroidPeaks = [
			{size: 1, r:  25},
			{size: 2, r:  75},
			{size: 3, r: 125},
			{size: 4, r: 175}
		];
		this._asteroidPeakHalfWidth = 75;

		let sizeReversalAreaStaticDonut = this._createDonut(
			Density.ASTEROID_FIELD_CENTER,
			[
				{r: 200, d: 0},
				{r: 250, d: 3},
				{r: 350, d: 4},
				{r: 400, d: 0}
			]
		);
		let sizeReversalAreaDynamicDonut = this._createDonut(
			Density.ASTEROID_FIELD_CENTER,
			[
				{r: 200, d:   0},
				{r: 220, d: 2.5},
				{r: 350, d:  10},
				{r: 400, d:   0}
			]
		);
		let sizeReversalArea = (pos, time) => {
			// Rotate ccw with 2-hour period
			let midAngle = - Math.PI / 2 - (time / (2 * 60 * 60)) * (2 * Math.PI);

			// This is a hack for half-circles
			let projToMidVector = Vector.dot(
				pos,
				new Vector(1, 0).rotateBy(midAngle)
			);

			if (projToMidVector < 5) return {static: 0, dynamic: 0};

			let staticDonutDensity = sizeReversalAreaStaticDonut(pos, time);
			let dynamicDonutDensity = sizeReversalAreaDynamicDonut(pos, time);
			return {
				static: Math.min(staticDonutDensity, (projToMidVector - 5) * (3 / 50)),
				dynamic: Math.min(dynamicDonutDensity, (projToMidVector - 5) * (2.5 / 20))
			};
		};
		this._sizeReversalAreaStatic = (pos, time) => {
			return sizeReversalArea(pos, time).static;
		};
		this._sizeReversalAreaDynamic = (pos, time) => {
			return sizeReversalArea(pos, time).dynamic;
		};

		this._sizeReversalPeaks = [
			{size: 2, r: 225},
			{size: 1, r: 300},
			{size: 0, r: 375}
		];
		this._sizeReversalPeakHalfWidth = 125;

		let damselsDonut = this._createDonut(
			Density.ASTEROID_FIELD_CENTER,
			[
				{r: 200, d:  0},
				{r: 250, d:  8},
				{r: 350, d: 12},
				{r: 400, d:  0}
			]
		);
		this._damselsDesolation = (pos, time) => {
			// Rotate ccw with 2-hour period
			let midAngle = Math.PI / 2 - (time / (2 * 60 * 60)) * (2 * Math.PI);

			// This is a hack for half-circles
			let projToMidVector = Vector.dot(
				pos,
				new Vector(1, 0).rotateBy(midAngle)
			);

			if (projToMidVector < 5) return 0;

			let donutDensity = damselsDonut(pos, time);
			return Math.min(donutDensity, (projToMidVector - 5) / 10);
		};

		let damselHoleDonut = this._createDonut(
			new Vector(),
			[
				{r:  0, d: 0},
				{r: 10, d: 0},
				{r: 20, d: 1}
			]
		);
		this._damselHole = (pos, time) => {
			let damselPos = this._getDamselPos();
			if (damselPos === null) return 1;
			return damselHoleDonut(Vector.diff(pos, damselPos), time);
		}
	}

	// Get densities at given pos and time
	// pos is the pos at which the density should be evaluated (usually this
	// will be the pos of the player). time is the total in-game time, in
	// seconds. This method returns a map from Density.POOLS to non-negative real numbers.
	getDensities(pos, time) {
		let staticDensity = this._sizeReversalAreaStatic(pos, time)
						+ this._damselsDesolation(pos, time)
						* this._damselHole(pos, time);

		let dynamicDensity = this._asteroidField(pos, time)
						+ this._sizeReversalAreaDynamic(pos, time);
		for (let hole of this._holes) {
			dynamicDensity *= hole(pos, time);
		}

		return new Map([
			[Density.POOLS.STATIC, staticDensity],
			[Density.POOLS.DYNAMIC, dynamicDensity]
		]);
	}

	// Select thing to spawn
	//
	// Args:
	// pos  - the (likely guesstimated) pos of the thing to be spawned
	// time - time since the start of the game, in seconds
	// pool - one of Density.POOLS
	selectThing(pos, time, pool) {
		let r = pos.mag();
		if (pool === Density.POOLS.STATIC) {
			let damselMidAngle = Math.PI / 2 - (time / (2 * 60 * 60)) * (2 * Math.PI);
			if (Vector.dot(pos, new Vector(1, 0).rotateBy(damselMidAngle)) > 0) {
				// Damsel's Desolation
				return [Compass, {pos: pos}];
			}
			// Size reversal area
			return [Beacony, {pos: pos}];
		}

		if (r > 200) {
			// Size reversal area
			// TODO: This code can spawn sizies right outside the asteroid field even in
			// the Damsel's Desolation.
			let weights = this._sizeReversalPeaks.map(peak =>
				Math.max(this._sizeReversalPeakHalfWidth - Math.abs(r - peak.r), 0)
			);
			let size = this._sizeReversalPeaks[Random.weighted(weights)].size;
			return [Sizy,
				{
					pos: pos,
					size: size
				}
			];
		} else {
			// Asteroid field
			let weights = this._asteroidPeaks.map(peak =>
				Math.max(this._asteroidPeakHalfWidth - Math.abs(r - peak.r), 0)
			);
			let size = this._asteroidPeaks[Random.weighted(weights)].size;
			return [Asteroid,
				{
					pos: pos,
					size: size
				}
			];
		}
	}

	// Find which area pos falls in at time
	//
	// Args:
	//   pos  - Vector
	//   time - time in seconds
	getCurrentArea(pos, time) {
		let r = pos.mag();

		if (r < 200) return "ASTEROID_FIELD";

		let damselMidAngle = Math.PI / 2 - (time / (2 * 60 * 60)) * (2 * Math.PI);
		if (Vector.dot(pos, new Vector(1, 0).rotateBy(damselMidAngle)) > 0) {
			return "DAMSELS_DESOLATION";
		}
		return "SIZE_REVERSAL"; // Compressive constructs?
	}

	// Get info about density areas for the GPS
	// For now, returns an array, where each element is an array of the form
	// ["CIRCLE", color (or "EMPTY"), originPos, radiusFlares]
	getAreaInfo(time) {
		let damselMidAngle = Math.PI / 2 - (time / (2 * 60 * 60)) * (2 * Math.PI);
		return [
			// Damsel's Desolation
			[
				"ARC",
				0xdc643a,
				Density.ASTEROID_FIELD_CENTER,
				400,
				damselMidAngle - Math.PI / 2,
				damselMidAngle + Math.PI / 2
			],
			[
				"CIRCLE",
				"EMPTY",
				this._getDamselPos() ?? new Vector(1000, 0), // TODO: Remove this hack
				10
			],
			// Size reversal
			[
				"ARC",
				0x6392b3,
				Density.ASTEROID_FIELD_CENTER,
				400,
				damselMidAngle + Math.PI / 2,
				damselMidAngle - Math.PI / 2
			],
			// Clear space between Damsel's Desolation and size reversal
			[
				"LINE",
				"EMPTY",
				10,
				new Vector(500, 0).rotateBy(damselMidAngle + Math.PI / 2),
				new Vector(500, 0).rotateBy(damselMidAngle - Math.PI / 2)
			],
			// Clear space for the asteroid field
			[
				"CIRCLE",
				"EMPTY",
				Density.ASTEROID_FIELD_CENTER,
				205
			],
			// Asteroid field
			[
				"CIRCLE",
				0x666666,
				Density.ASTEROID_FIELD_CENTER,
				195
			],
			[
				"CIRCLE",
				"EMPTY",
				Density.METEORUS_HOLE_CENTERS[0](time),
				10
			],
			[
				"CIRCLE",
				"EMPTY",
				Density.METEORUS_HOLE_CENTERS[1](time),
				10
			],
			[
				"CIRCLE",
				"EMPTY",
				Density.METEORUS_HOLE_CENTERS[2](time),
				10
			],
			[
				"CIRCLE",
				"EMPTY",
				Density.METEORITUS_HOLE_CENTER(time),
				20
			],
		];
	}

	// Create a donut-shaped density function
	// center is the center of the donut - either a vector or a function that
	// takes in the total in-game time and returns a vector. rdArray is an array
	// of objects, where each object contains properties r (radius, i.e.
	// Euclidean distance from center) and d (density at that radius). The
	// densities for radii between the r values in rdArray are interpolated
	// linearly. The density for radii below the smallest r value in rdArray is
	// the same as the d value for that smallest r value; same for the radii
	// above the largest r value. Entries in rdArray must be in the order of
	// increasing r values. This method creates a function that takes in pos and
	// time and returns the density at that pos/time.
	_createDonut(center, rdArray) {
		let valid = true;

		// Check types
		if (!((center instanceof Vector) || (center instanceof Function))) {
			valid = false;
		}
		if (valid && (center instanceof Function)
						&& !(center(0) instanceof Vector)) {
			valid = false;
		}
		if (valid && !Array.isArray(rdArray)) valid = false;
		if (valid && !rdArray.every(rdPoint =>
						(Number.isFinite(rdPoint.r)
						&& Number.isFinite(rdPoint.d)))) {
			valid = false;
		}

		// Check r
		if (valid && !rdArray.length) valid = false;
		if (valid && rdArray[0].r < 0) valid = false;
		if (valid) {
			let rPrev = rdArray[0].r;
			for (let i = 1; i < rdArray.length; ++i) {
				if (rdArray[i].r <= rPrev) valid = false;
				rPrev = rdArray[i].r;
			}
		}

		// Check d
		// Negative densities can make sense in some contexts, so it might be a
		// good idea to remove this check.
		if (valid && !rdArray.every(rdPoint => (rdPoint.d >= 0))) valid = false;

		// Complain
		if (!valid) {
			logger.warning(
				"density.js",
				`invalid donut (center: ${JSON.stringify(center)}, rdArray:`
								+ ` ${JSON.stringify(rdArray)})`
			);
			return (pos, time) => 0;
		}

		return (pos, time) => {
			let currentCenter = (center instanceof Function) ? center(time)
							: center;
			let r = Vector.diff(pos, currentCenter).mag();

			// Use <= everywhere since the density in the donut is continuous.

			if (r <= rdArray[0].r) return rdArray[0].d;
			if (rdArray[rdArray.length - 1].r <= r) {
				return rdArray[rdArray.length - 1].d;
			}

			for (let i = 0; i < rdArray.length - 1; ++i) {
				if (rdArray[i].r <= r && r < rdArray[i + 1].r) {
					let run = rdArray[i + 1].r - rdArray[i].r;
					let rise = rdArray[i + 1].d - rdArray[i].d;
					let slope = rise / run;
					return rdArray[i].d + slope * (r - rdArray[i].r);
				}
			}
		}
	}

	// Returns pos or null
	_getDamselPos() {
		let damselReport = this.space.requestTacticalReport(
			entry => entry.name === "DAMSEL"
		);
		if (damselReport.length === 0) return null;
		return new Vector(damselReport[0].pos);
	}
}
