import {round} from "../utils.js";
import {logger} from "../logger.js";
import {getTexture} from "../assets.js";
import {Vector, isVectorable} from "../i.js";
import {ConfigStructure, OptionalProperty} from "../i.js";
import {AI, AIUtils} from "../i.js";
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js";
import {DropList, ITEMS} from "../i.js";

export class BoxAI extends AI {
	constructor(thing, config) {
		super(thing);
	}

	move(dt) {
		let freeFloat = false;
		let holder = null;
		if (this.thing.holder !== undefined) {
			holder = this.thing.space.requestThingById(this.thing.holder);
			if (holder === null) {
				delete this.thing.holder;
				freeFloat = true;
			}
		} else {
			freeFloat = true;
		}

		if (freeFloat) {
			// I don't want to create an entire StraightLineAI for these 2 lines
			AIUtils.rotateBy(this.thing, this.thing.spn, dt);
			AIUtils.translate(this.thing, new Vector(), 0, dt);
		} else {
			if (holder.getBoxLocation === undefined) {
				logger.error("box.js", "getBoxLocation() not found");
				return;
			}
			let desiredLocation = holder.getBoxLocation();
			AIUtils.setPos(this.thing, desiredLocation.pos, dt);
			AIUtils.setRot(this.thing, desiredLocation.rot, dt);
		}
	}
}

export class Box extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the thing holding the box, defaults to free-float if omitted
		new OptionalProperty("holder", v => Number.isFinite(v)),
		// Movement data relevant for free-float
		// Save these even if not free-floating, if for some reason holder disappears
		// immediately after loading.
		new OptionalProperty("pos", v => isVectorable(v)),
		new OptionalProperty("vel", v => isVectorable(v)),
		new OptionalProperty("rot", v => Number.isFinite(v)),
		new OptionalProperty("spn", v => Number.isFinite(v))
	]);

	static DROP_LIST = new DropList([
		[ITEMS.NOTE_WELCOME, [1], [1]],
		[ITEMS.RED_GEM,      [3], [1]],
		[ITEMS.YELLOW_GEM,   [3], [1]],
		[ITEMS.GREEN_GEM,    [3], [1]]
	]);

	// Create a new box
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Box.CONFIG_STRUCTURE.test(config);

		config.name = "Box";
		config.texture = "images/guardians/box.svg";
		config.zIndex = Z_INDICES.NPC;
		config.len = 0.36;
		config.pos = new Vector(config.pos);
		config.vel = new Vector(config.vel);
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = 25;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.NONE;
		config.canDespawn = false;
		super(config);

		if (config.holder !== undefined) this.holder = config.holder;
		this.spn = config.spn;

		this.ai = new BoxAI(this);
	}

	// Take damage from an attack
	// Args:
	//   damage    - number
	//   dropItems - is an optional boolean (defaults to true if omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// Save this box into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();

		if (this.holder !== undefined) config.holder = this.holder;
		config.pos = this.pos.save();
		config.vel = this.vel.save();
		config.rot = round(this.rot);
		config.spn = round(this.spn);

		return config;
	}

	// Prepare this box for removal
	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		// Drop items if dropItems is true or undefined
		if (dropItems !== false) this.space.requestDrop(
			this.pos,
			this.radius,
			this.vel,
			Box.DROP_LIST
		);

		super.prepareToDie(dropItems);
	}
}
