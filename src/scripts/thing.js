// Define a parent class for all things in space

import {Container, Sprite} from "pixi.js";
import {Circle, Polygon} from "detect-collisions";
import {getTexture} from "./assets.js";
import {round} from "./utils.js";
import {Vector} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {Density} from "./i.js"; // ./density.js
import {Space} from "./i.js"; // ./space.js

// Standard pixels per flare for thing sprites
// Not used by all sprites yet.
export const STANDARD_PPF = 220;

export const Z_INDICES = {
	// Bottom
	STATION:      0,
	BOSS_CHILD:   1,
	BOSS:         2,
	NPC_CHILD:    3,
	NPC:          4,
	PLAYER_CHILD: 5,
	DROP:         6,
	PLAYER:       7
	// Top
}

export const FACTIONS = {
	NONE: "NONE",
	PLAYER: "PLAYER",
	METEORIC: "METEORIC",
	COSSETTE: "COSSETTE",
	SIZE_REVERSAL: "SIZE_REVERSAL",
	DAMSEL: "DAMSEL"
}

export const THING_TYPES = {
	SPACESHIP:   "SPACESHIP",
	STATION:     "STATION",
	ASTEROID:    "ASTEROID",
	DROP:        "DROP",
	HEALING:     "HEALING",
	ENERGY:      "ENERGY",
	// Temporary thing types, to be generalized
	ETHEREAL:    "ETHEREAL"
}

// How much a thing can poke out of its radius
// This is used for redrawing things on the screen. If things start appearing
// and disappearing in a glitchy way near the edges of the screen, increase this
// value.
const DISPLAY_BUFFER = 2;

export class Thing {
	// Possible states for a thing
	// Limbo applies only to things that can respawn.
	static STATES = {
		ALIVE: "ALIVE",
		LIMBO: "LIMBO",
		DEAD:  "DEAD"
	};

	static CONFIG_STRUCTURE = new ConfigStructure([
		// Properties from config enrichment
		// Don't check for these properties in subclasses.

		new RequiredProperty("id", v => (Number.isFinite(v) && v >= 0)),
		new RequiredProperty("space", v => (v instanceof Space)),

		// Other properties

		// Identity of this thing, used in the code
		new OptionalProperty("identity", v => (typeof v === "string")),
		// Name of this thing
		// Can be shown to the player in a variety of places.
		new OptionalProperty("name", v => (typeof v === "string")),
		// State of this thing
		// Defaults to alive if omitted.
		new OptionalProperty("state",
						v => (Object.values(Thing.STATES).includes(v)
						&& (v !== Thing.STATES.DEAD))),
		// Filename string for the texture
		// Creates a container if omitted. Because a container is like an empty
		// sprite right?
		new OptionalProperty("texture", v => ((typeof v === "string")
						&& (true))), // TODO check that texture exists
		// z-index of this thing in space
		new RequiredProperty("zIndex",
						v => (Object.values(Z_INDICES).includes(v))),
		// Length of this thing, in flares
		new RequiredProperty("len", v => (Number.isFinite(v) && v > 0)),
		// hitbox of this thing, a detect-collisions body
		new OptionalProperty("hitbox", v => (v instanceof Polygon)),
		// pos of this thing, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// vel of this thing, a vector with values in flares / second
		new RequiredProperty("vel", v => (v instanceof Vector)),
		// rot of this thing, in radians
		new RequiredProperty("rot", v => (Number.isFinite(v))),
		// Max health of this thing
		new RequiredProperty("maxHealth", v => (Number.isFinite(v) && v >= 0)),
		// Current health of this thing, set to maxHealth if omitted
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		// Consecutive time this thing has been outside the bubble, in seconds
		// Defaults to 0 if omitted. Gets overridden with null if
		// canDespawn is false.
		new OptionalProperty("timeOutsideBubble",
						v => (Number.isFinite(v) && v >= 0)),
		// Thing type
		new RequiredProperty("type",
						v => (Object.values(THING_TYPES).includes(v))),
		// Faction
		// Defaults to FACTIONS.NONE if omitted.
		new OptionalProperty("faction", v => (Object.values(FACTIONS).includes(v))),
		// Whether this thing can despawn
		new RequiredProperty("canDespawn", v => (v === false || v === true)),
		// Whether the thing can pick up items
		// Must be true if specified, defaults to false if omitted. Applies only
		// to items, the player is the only thing that can pick up components
		// (TODO: Write the code to actually enforce this sentence).
		new OptionalProperty("canPickUpItems", v => (v === true)),
		// The weight of the thing for guesstimation calculations
		// Defaults to 0 if omitted.
		new OptionalProperty("guesstimationWeight",
						v => (Number.isFinite(v) && v >= 0)),
		// Window during which this thing can't despawn, in seconds
		// Defaults to null if omitted, also gets overridden with null if
		// canDespawn is false.
		new OptionalProperty("despawnSafetyWindow",
						v => (Number.isFinite(v) && v >= 0)),
		// Spawn pool, one of Density.POOLS
		// Defaults to null if omitted, also gets overridden with null if
		// guesstimationWeight is 0.
		new OptionalProperty("pool",
						v => (Object.values(Density.POOLS).includes(v))),
		// Filename string for the GPS icon
		// If omitted, this thing isn't shown in the GPS.
		new OptionalProperty("gpsIcon", v => ((typeof v === "string")
						&& (true))), // TODO check that texture exists
		// Whether the gps icon should rotate with the thing
		// Must be true if specified, defaults to false if omitted. Gets
		// overridden with null if gpsIcon is omitted.
		new OptionalProperty("rotateGpsIcon", v => (v === true)),
		// Whether to show a boss bar for this thing
		// Must be true if specified, defaults to false if omitted.
		new OptionalProperty("showBossBar", v => (v === true))
	]);

	// Create a thing with given parameters
	// config is an object with the properties specified in CONFIG_STRUCTURE
	// above. Don't create instances of Thing directly, this class should only
	// be used to create child classes.
	constructor(config) {
		Thing.CONFIG_STRUCTURE.test(config);

		this.identity = config.identity ?? "";
		this.name = config.name ?? "";
		// Invariant: no thing has state dead at the end of a tick
		this.state = config.state ?? Thing.STATES.ALIVE;

		// this.display is a PIXI object that displays the thing on the screen
		if (config.texture === undefined) {
			this.display = new Container();
		} else {
			this.display = new Sprite(getTexture(config.texture));
			this.display.anchor.set(0.5, 0.5);
		}
		this.display.zIndex = config.zIndex;

		this.pos = new Vector(config.pos);
		this.vel = new Vector(config.vel);
		this.rot = config.rot;
		this.spn = 0;
		this.len = config.len;
		this.radius = this.len / 2;

		this.hitbox = config.hitbox ?? new Circle({x: 0, y: 0}, this.radius);
		this.hitbox.thing = this;

		this.id = config.id;
		this.space = config.space;
		this.maxHealth = config.maxHealth;
		this.health = config.health ?? config.maxHealth;
		if (this.health > this.maxHealth) {
			logger.warning("thing.js", `health > maxHealth when creating`
							+ ` thing: ${JSON.stringify(config)}`);
		}
		this.timeOutsideBubble = config.timeOutsideBubble ?? 0;
		this.TYPE = config.type;
		this.faction = config.faction ?? FACTIONS.NONE;
		this.CAN_DESPAWN = config.canDespawn;
		this.CAN_PICK_UP_ITEMS = (config.canPickUpItems === true);
		this.GUESSTIMATION_WEIGHT = config.guesstimationWeight ?? 0;
		this.DESPAWN_SAFETY_WINDOW = config.despawnSafetyWindow ?? null;
		this.pool = config.pool ?? null;

		this.gpsIcon = config.gpsIcon ?? null;
		this.rotateGpsIcon = (config.rotateGpsIcon === true);

		this.showBossBar = (config.showBossBar === true);

		// Overrides
		if (!this.CAN_DESPAWN) {
			this.timeOutsideBubble = null;
			this.DESPAWN_SAFETY_WINDOW = null;
		}
		if (this.GUESSTIMATION_WEIGHT === 0) this.pool = null;
		if (this.gpsIcon === null) this.rotateGpsIcon = null;

		// Set AI in child class constructor after calling super()
		this.ai = null;
	}

	// Move this thing
	// Called by space.
	//
	// Args:
	//   dt   - time since last update, in seconds
	//   time - total in-game time, in seconds
	move(dt, time) {
		if (this.ai !== null) this.ai.move(dt, time);
		// Check state here in case the AI changed the state
		if (this.state === Thing.STATES.ALIVE) {
			if (this.animation ?? null !== null) this.animation.update(dt, time);
		}
	}

	// Save this thing into a config object
	// Note that this base method only saves the most essential dynamic info: the info
	// that none (or almost none) of the subclasses would set statically.
	//
	// If you wish to include a subclass of Thing in the savefile, you must override this
	// method in the subclass like so:
	//
	//     save() {
	//         let config = super.save();
	//         let aiState = this.ai.save();
	//         config.someProperty = someValue;
	//         config.someAiProperty = aiState.someAiProperty;
	//         return config;
	//     }
	//
	// After a correct override, the method should return a config object that can be used
	// to construct an identical copy of the thing:
	//
	//     config = thing.save();
	//     space._requestSpawn(ThingClass, config);  // Identical copy!
	//
	// The above two lines are just for demonstration; you should add the subclass to
	// `SAVE_CLASSNAMES` in `space.js` to indicate that the subclass is save-able, and the
	// existing save/load code will pick it up. You might also need to modify the `Space`
	// constructor.
	save() {
		let config = {id: this.id};

		if (this.state !== Thing.STATES.ALIVE) config.state = this.state;

		if (this.health !== this.maxHealth) {
			config.health = this.health;
		}

		if (this.timeOutsideBubble !== null) {
			config.timeOutsideBubble = this.timeOutsideBubble;
		}

		return config;
	}

	// Redraw the thing on the screen
	// This function must be called every tick whenever the thing has even the
	// slightest chance of being on the screen. Probably just always call it
	// every tick. screenSize is a vector containing the screen size, in pixels.
	// pixelsPerFlare is a real number. positionOverride is an optional vector
	// that can be used to override the normal position calculation; it should
	// only be used by Player.
	redraw(screenSize, pixelsPerFlare, positionOverride) {
		// Handle limbo state
		if (this.state === Thing.STATES.LIMBO) {
			this.display.visible = false;
			if (this.healthBar) this.healthBar.display.visible = false;
			return;
		}

		// Switch from pos to position
		// pos is absolute, position is relative to the screen.
		let position;
		if (positionOverride !== undefined) {
			position = positionOverride;
		} else {
			position = this.space.requestPosToPosition(this.pos,
							pixelsPerFlare);
		}

		let buffered_radius = DISPLAY_BUFFER * pixelsPerFlare * this.radius;
		if (position.x + buffered_radius < 0
						|| position.x - buffered_radius > screenSize.x
						|| position.y + buffered_radius < 0
						|| position.y - buffered_radius > screenSize.y) {
			this.display.visible = false;
			if (this.healthBar) this.healthBar.display.visible = false;
			return;
		}

		this.display.visible = true;
		if (this.healthBar) this.healthBar.display.visible = true;
		// Rescale the thing so that its height is equal to len
		if (this.display instanceof Sprite) {
			this.display.scale.set(this.len * pixelsPerFlare
							/ this.display.texture.height);
		} else {
			// display is a container
			// Inside the container, everything is measured in flares.
			this.display.scale.set(pixelsPerFlare);
		}
		this.display.position.copyFrom(position);

		// I draw images pointing up, but at rot 0 they should point right
		// (towards positive x), so add a 90 degree angle here.
		this.display.rotation = this.rot + Math.PI / 2;
	}

	// Prepare this thing for removal
	// Note that this method doesn't actually remove the thing from space.
	// dropItems is a boolean. enterLimbo is a boolean that defaults to false.
	prepareToDie(dropItems, enterLimbo) {
		// Figure out what state to go to
		let targetState = (enterLimbo ?? false) ? Thing.STATES.LIMBO
						: Thing.STATES.DEAD;

		// Exit if already prepared
		if (this.state === targetState) return;

		this.state = targetState;
		// TODO: Manage health bars more consistently
		if (this.healthBar && targetState === Thing.STATES.DEAD) {
			this.healthBar.selfDestruct();
		}
		if (targetState === Thing.STATES.DEAD) {
			this.space.interactor.removeHitbox(this.hitbox);
			if (this.display instanceof Sprite)
				this.display.destroy(false);
			else {
				this.display.destroy({
					children: true,
					texture: false,
					textureSource: false,
					context: true
				});
			}
		}
	}

	// Respawn from limbo
	respawn() {
		if (this.state !== Thing.STATES.LIMBO) {
			logger.warning("thing.js", "can't respawn, thing has state"
							+ ` ${this.state}`);
			return;
		}

		this.state = Thing.STATES.ALIVE;
	}
}
