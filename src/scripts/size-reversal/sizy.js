import {Vector} from "../i.js"; // ../vector.js
import {Random} from "../i.js"; // ../random.js
import {angleDiff} from "../i.js"; // ../dump.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js"; // ../validators.js
import {Density} from "../i.js"; // ../density.js
import {AI, AIUtils} from "../i.js"; // ../ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js"; // ../thing.js
import {DropList, ITEMS} from "../i.js"; // ../drop.js
import {Blaster, BLASTER_VARIETIES} from "../i.js";

class SizyAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// size of this sizy, an integer from 0 to 2
		new RequiredProperty("size", v => [0, 1, 2].includes(v))
	]);

	static SIZES = [
		{ // Smally
			personalSpace: 0.3,
			maxSpeed: 3,
			friction: 4,
			rSpeed: 3,
			rFriction: 4,
			shotsPerSalvo: 3
		},
		{ // Middy
			personalSpace: 0.6,
			maxSpeed: 2.5,
			friction: 3.5,
			rSpeed: 2,
			rFriction: 4,
			shotsPerSalvo: 2
		},
		{ // Biggy
			personalSpace: 1.2,
			maxSpeed: 2,
			friction: 3,
			rSpeed: 1.5,
			rFriction: 4,
			shotsPerSalvo: 1
		}
	];

	// Distance at which a sizy can detect things to be repelled from
	static MAX_REPEL_DISTANCE = 2;
	static DESIRED_DIST_TO_PLAYER = 1.75;
	// If the distance to player differs from the desired distance by this amount, engage
	// max speed to regain proper distance
	static SEVERE_DIST_ERROR = 1;
	// Time between the starts of successive salvos, in seconds
	static SALVO_PERIOD = 2;
	// Cooldown between shots in a salvo, in seconds
	static COOLDOWN = 0.1;
	// Speed at which activation propagates from a beacony to a sizy
	static HEARING_SPEED = 3;

	constructor(thing, config) {
		SizyAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._size = config.size;
		this._activated = false;
		this._lastSalvoIndex = null;
		this._shotsLeft = 0;
		this._cooldown = 0;
	}

	move(dt, time) {
		let sizeInfo = SizyAI.SIZES[this._size];

		if (!this._activated) {
			this._listenForBeaconies(time);
		}

		let idleAcc = this._getRepelAcc();

		// Which direction the sizy wants to face, in radians
		let desiredDir = null;
		// positioningAcc has magnitude <= 1, where 1 is max engine power
		let positioningAcc = null;
		if (this._activated) {
			let playerTacticalReport = this.thing.space.requestTacticalReport(
				entry => entry.isPlayer,
				"SIZY_PLAYER"
			).filter(
				entry => (
					Vector.diff(entry.pos, this.thing.pos).mag() <= 5
				)
			);
			if (playerTacticalReport.length === 0) {
				this._activated = false;
			} else {
				let playerPos = playerTacticalReport[0].pos;
				desiredDir = Vector.diff(playerPos, this.thing.pos).dir();
				let vectorToPlayer = Vector.diff(playerPos, this.thing.pos);
				let distToPlayer = vectorToPlayer.mag();
				positioningAcc = Vector.norm(
					vectorToPlayer,
					(distToPlayer - SizyAI.DESIRED_DIST_TO_PLAYER)
									* 1
									/ SizyAI.SEVERE_DIST_ERROR
				);
				positioningAcc.limit(1);
			}
		} else { // !this._activated
			idleAcc.add(this._getAttractAcc());
			if (idleAcc.mag() > 0.01) {
				desiredDir = idleAcc.dir();
			}
		}

		if (desiredDir === null) {
			AIUtils.rotateBy(
				this.thing,
				this.thing.spn,
				dt,
				sizeInfo.rFriction
			);
		} else {
			AIUtils.rotateTo(
				this.thing,
				desiredDir,
				sizeInfo.rSpeed,
				dt
			);
		}
		let desiredAcc = idleAcc;
		if (positioningAcc !== null) desiredAcc.add(positioningAcc);
		let forwardDirUnit = new Vector(1, 0).rotateBy(this.thing.rot);
		let rightDirUnit = new Vector(1, 0).rotateBy(this.thing.rot + Math.PI / 2);
		let forwardAcc = Vector.proj(desiredAcc, forwardDirUnit);
		let rightAcc = Vector.proj(desiredAcc, rightDirUnit);
		let isGoingBackwards = Vector.dot(forwardAcc, forwardDirUnit) < 0;
		forwardAcc.limit(isGoingBackwards ? 0.3 : 1);
		rightAcc.limit(0.3);
		let acc = Vector.sum(forwardAcc, rightAcc);
		// From here on out, magnitudes are "real" and not 0..1
		acc.limit(1).scale(sizeInfo.maxSpeed * sizeInfo.friction);
		AIUtils.translate(this.thing, acc, sizeInfo.friction, dt);

		this._cooldown -= dt;
		let currSalvoIndex = Math.floor(time / SizyAI.SALVO_PERIOD);
		if (currSalvoIndex != this._lastSalvoIndex) {
			this._shotsLeft = sizeInfo.shotsPerSalvo;
			this._lastSalvoIndex = currSalvoIndex;
			this._cooldown = 0;
		}
		if (this._activated && this._shotsLeft > 0 && this._cooldown <= 0) {
			if (Math.abs(angleDiff(this.thing.rot, desiredDir)) <= Math.PI / 4) {
				this.thing.space.requestSpawn(Blaster, {
					variety: BLASTER_VARIETIES.SIZY,
					pos: this.thing.pos,
					rot: this.thing.rot,
					parentVel: this.thing.vel
				});
			}
			--this._shotsLeft;
			this._cooldown = SizyAI.COOLDOWN;
		}

		// Check for player proximity

		// let playerTacticalReport = this.thing.space.requestTacticalReport(
		// 	entry => entry.isPlayer,
		// 	"BEACONY_PLAYER"
		// ).filter(
		// 	entry => (
		// 		Vector.diff(entry.pos, this.thing.pos).mag() <= 1.4
		// 	)
		// );
		// if (playerTacticalReport.length > 0) this._activate();
	}

	recordDamage() {
		// TODO: Only activate if damaged by the player
		this._activate();
	}

	_activate() {
		this._activated = true;
		this._shotsLeft = 0;
	}

	_listenForBeaconies(time) {
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (entry.name === "Beacony"),
			"SIZY_BEACONY"
		).filter(
			entry => (
				Vector.diff(entry.pos, this.thing.pos).mag() <= 3
			)
		);

		for (let entry of tacticalReport) {
			let activatedAt = entry.thing?.ai?._activatedAt ?? null;
			if (activatedAt !== null) {
				let distToEntry = Vector.diff(entry.pos, this.thing.pos).mag();
				if ((time - activatedAt) > distToEntry / SizyAI.HEARING_SPEED) {
					this._activate();
				}
			}
		}
	}

	// Get acceleration needed to attract to the nearest beacony
	// Returns a vector with magnitude <= 1, where 1 is the max engine power.
	_getAttractAcc() {
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (entry.name === "Beacony"),
			"SIZY_BEACONY_2"
		).filter(
			entry => (
				// TODO: This is apparently very slow
				Vector.diff(entry.pos, this.thing.pos).mag() <= 5
			)
		);
		if (tacticalReport.length === 0) return new Vector();

		let closestEntry = null;
		let closestDistance = Infinity;
		for (let entry of tacticalReport) {
			let dist = Vector.diff(entry.pos, this.thing.pos).mag();
			if (dist < closestDistance) {
				closestDistance = dist;
				closestEntry = entry;
			}
		}

		return AIUtils.getAttractAcc(
			this.thing,
			closestEntry,
			0.3,
			2.5
		);
	}

	// Get acceleration needed to repel from from nearby things
	// Returns a vector with magnitude <= 1, where 1 is the max engine power.
	_getRepelAcc() {
		let sizeInfo = SizyAI.SIZES[this._size];

		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (
				["Beacony", "Smally", "Middy", "Biggy"].includes(entry.name)
				|| entry.TYPE === THING_TYPES.ASTEROID
			),
			"SIZY"
		).filter(
			entry => (
				// TODO: This is apparently very slow
				Vector.diff(entry.pos, this.thing.pos).mag() <= SizyAI.MAX_REPEL_DISTANCE
			)
		);

		let repelAcc = new Vector();
		for (let entry of tacticalReport) {
			let vectorToEntry = Vector.diff(entry.pos, this.thing.pos);
			let distToEntry = vectorToEntry.mag();
			// Found itself!
			if (distToEntry === 0) continue;

			let entryPersonalSpace = 0;
			if (entry.TYPE === THING_TYPES.ASTEROID) {
				entryPersonalSpace = 2;
			} else if (entry.name === "Beacony") {
				if (!this._activated) entryPersonalSpace = 1;
			} else if (entry.name === "Biggy") {
				entryPersonalSpace = SizyAI.SIZES[2].personalSpace;
			} else if (entry.name === "Middy") {
				entryPersonalSpace = SizyAI.SIZES[1].personalSpace;
			} else { // "Smally"
				entryPersonalSpace = SizyAI.SIZES[0].personalSpace;
			}
			let desiredDistance = Math.max(
				sizeInfo.personalSpace, entryPersonalSpace
			);

			repelAcc.add(AIUtils.getRepelAcc(
				this.thing,
				entry,
				1,
				desiredDistance
			));
		}
		// Apply upper bound to acceleration
		repelAcc.limit(0.3);

		return repelAcc;
	}
}



export class Sizy extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// size of this sizy, an integer from 0 to 2
		new RequiredProperty("size", v => [0, 1, 2].includes(v)),
		// pos of this sizy, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// vel of this sizy, a vector with values in flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// rot of this sizy, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this sizy
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
	]);

	static SIZES = [
		{
			name: "Smally",
			len: 0.20,
			health: 30,
			guesstimationWeight: 4,
			dropList: new DropList([[ITEMS.RED_GEM, [6], [1]]])
		},
		{
			name: "Middy",
			len: 0.40,
			health: 20,
			guesstimationWeight: 2,
			dropList: new DropList([[ITEMS.RED_GEM, [4], [1]]])
		},
		{
			name: "Biggy",
			len: 0.80,
			health: 15,
			guesstimationWeight: 1,
			dropList: new DropList([[ITEMS.RED_GEM, [2], [1]]])
		}
	];

	// Create a new sizy
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Sizy.CONFIG_STRUCTURE.test(config);

		let sizeInfo = Sizy.SIZES[config.size];

		// Fill in missing config fields
		config.name = sizeInfo.name;
		config.texture = "images/size-reversal/sizy.svg";
		config.zIndex = Z_INDICES.NPC;
		config.len = sizeInfo.len;
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = sizeInfo.health;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.SIZE_REVERSAL;
		config.canDespawn = true;
		config.guesstimationWeight = sizeInfo.guesstimationWeight;
		config.despawnSafetyWindow = 5;
		config.pool = Density.POOLS.DYNAMIC;

		// Call parent constructor
		super(config);

		this._size = config.size;

		this.ai = new SizyAI(this, {size: config.size});
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
			this.ai.recordDamage();
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		// Drop items if dropItems is true or undefined
		if (dropItems !== false) this.space.requestDrop(
			this.pos,
			this.radius,
			this.vel,
			Sizy.SIZES[this._size].dropList
		);

		super.prepareToDie(dropItems);
	}
}
