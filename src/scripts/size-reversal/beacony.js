import {Vector} from "../i.js"; // ../vector.js
import {Random} from "../i.js"; // ../random.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js"; // ../validators.js
import {Density} from "../i.js"; // ../density.js
import {Particles} from "../i.js"; // ../particles.js
import {AI, AIUtils} from "../i.js"; // ../ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js"; // ../thing.js
import {DropList, ITEMS} from "../i.js"; // ../drop.js
import {ACHIEVEMENTS} from "../i.js";

class BeaconyAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([]);

	// Desired distance from other beaconies, f
	static DESIRED_DISTANCE = 3;
	// Max speed of a beacony, f/s
	static MAX_SPEED = 0.2;
	// Friction, in 1/s
	static FRICTION = 2;
	// Time before deactivation, in seconds
	static DEACTIVATION_TIME = 2;

	constructor(thing, config) {
		BeaconyAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		// Time when this beacony was activated, or null if not activated
		this._activatedAt = null;
	}

	move(dt, time) {
		if (this._activatedAt !== null &&
						(time - this._activatedAt) > BeaconyAI.DEACTIVATION_TIME) {
			this._activatedAt = null;
		}

		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (entry.name === "Beacony" || entry.TYPE === THING_TYPES.ASTEROID),
			"BEACONY"
		).filter(
			entry => (
				Vector.diff(entry.pos, this.thing.pos).mag() <= BeaconyAI.DESIRED_DISTANCE
			)
		);

		let acc = new Vector();
		for (let entry of tacticalReport) {
			acc.add(AIUtils.getRepelAcc(
				this.thing,
				entry,
				BeaconyAI.MAX_SPEED * BeaconyAI.FRICTION,
				BeaconyAI.DESIRED_DISTANCE
			));
		}
		// Apply upper bound to acceleration
		acc.norm(Math.max(acc.mag(), BeaconyAI.MAX_SPEED * BeaconyAI.FRICTION));

		AIUtils.rotateBy(
			this.thing,
			-0.05,
			dt
		);
		AIUtils.translate(this.thing, acc, BeaconyAI.FRICTION, dt);

		// Check for player proximity

		let playerTacticalReport = this.thing.space.requestTacticalReport(
			entry => entry.isPlayer,
			"BEACONY_PLAYER"
		).filter(
			entry => (
				Vector.diff(entry.pos, this.thing.pos).mag() <= 1.4
			)
		);
		if (playerTacticalReport.length > 0) this._activate(time);
	}

	recordDamage(time) {
		// TODO: Only activate if damaged by the player
		this._activate(time);
	}

	_activate(time) {
		if (this._activatedAt === null) {
			this._activatedAt = time;
			this.thing.space.particles.add(
				Particles.TYPES.BEACONY_CALL,
				this.thing.pos,
				this.thing.vel,
				1,
				this.thing.id
			);
		}
		this.thing.space.game.unicenter.ship.achievements.recordAchievement(
			ACHIEVEMENTS.BEACONY
		);
	}
}



export class Beacony extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// pos of this beacony, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// vel of this beacony, a vector with values in flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// rot of this beacony, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this beacony
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
	]);

	static DROP_LIST = new DropList([
		[ITEMS.RED_GEM, [1], [1]],
		[ITEMS.SIREN,   [1], [1]]
	]);

	// Create a new beacony
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Beacony.CONFIG_STRUCTURE.test(config);

		// Fill in missing config fields
		config.name = "Beacony";
		config.texture = "images/size-reversal/beacony.svg";
		config.zIndex = Z_INDICES.NPC;
		config.len = 0.75;
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 45;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.SIZE_REVERSAL;
		config.canDespawn = true;
		config.guesstimationWeight = 1;
		config.despawnSafetyWindow = 5;
		config.pool = Density.POOLS.STATIC;

		// Call parent constructor
		super(config);

		this.ai = new BeaconyAI(this, {});
	}

	// Take damage from an attack
	// Args:
	//   damage    - number
	//   dropItems - is an optional boolean (defaults to true if omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
			this.ai.recordDamage(this.space.time);
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		// Drop items if dropItems is true or undefined
		if (dropItems !== false) this.space.requestDrop(
			this.pos,
			this.radius,
			this.vel,
			Beacony.DROP_LIST
		);

		super.prepareToDie(dropItems);
	}
}
