// Define a class for the Meteorus boss

import {Vector} from "./i.js"; // ./vector.js
import {Random} from "./i.js"; // ./random.js
import {logger} from "./i.js"; // ./logger.js
import {deepCopy} from "./i.js"; // ./dump.js
import {functionFactory} from "./i.js"; // ./code-factories.js
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {AI, AIUtils, PathFollowAI} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {COMPONENTS, DropList, ITEMS} from "./i.js"; // ./drop.js
import {createAsteroidShape} from "./i.js"; // ./asteroid.js
import {ACHIEVEMENTS} from "./i.js";

// TODO: Possibly make a weapon superclass?
class MeteorusCannonball extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		new EitherOr([
			// pos of the parent Meteorus, a vector with values in flares
			new RequiredProperty("parentPos", v => (v instanceof Vector)),
			// vel of the parent Meteorus, a vector with values in
			// flares / second
			new RequiredProperty("parentVel", v => (v instanceof Vector)),
			// pos of the target, a vector with values in flares
			new RequiredProperty("targetPos", v => (v instanceof Vector))
		], [
			// pos of this Meteorus Cannonball, a vector with values in flares
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this Meteorus Cannonball, a vector with values in
			// flares / second
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// rot of this Meteorus Cannonball, in radians
			new RequiredProperty("rot", v => (Number.isFinite(v))),
			// Health of this Meteorus Cannonball
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	static SPEED = 1.5;  // flares / second
	// Rotation speed, in clockwise radians / second
	static SPIN = -3;

	// Create a Meteorus Cannonball
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		MeteorusCannonball.CONFIG_STRUCTURE.test(config);

		config.zIndex = Z_INDICES.BOSS_CHILD;
		config.len = 0.32;
		if (config.pos === undefined) config.pos = config.parentPos;
		// TODO: Factor parentVel into this vel calculation
		if (config.vel === undefined) {
			config.vel = Vector.diff(config.targetPos, config.parentPos)
				.norm(MeteorusCannonball.SPEED);
		}
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 9;
		config.type = THING_TYPES.ASTEROID;
		config.faction = FACTIONS.METEORIC;
		config.canDespawn = true;
		config.despawnSafetyWindow = 0;
		super(config);

		createAsteroidShape(this.display, this.len, 3);
	}

	// Move the Meteorus Cannonball
	// dt is the time since last update, in seconds.
	move(dt) {
		AIUtils.translate(this, new Vector(), 0, dt);
		AIUtils.rotateBy(this, MeteorusCannonball.SPIN, dt);
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.prepareToDie(false);
		}
	}

	// Save this Meteorus Cannonball into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;
		return {
			config:     config,
			isComplete: true
		};
	}
}



class MeteorusFortification extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the parent Meteorus
		new RequiredProperty("parentId", v => (Number.isFinite(v) && v >= 0)),
		// String representation of a function that takes in the in-game time
		// and returns the displacement of the target pos from the pos of the
		// parent Meteorus
		new RequiredProperty("targetGenerator", v => (typeof v === "string")),
		// pos of this Meteorus Fortification, a vector with values in flares
		new OptionalProperty("pos", v => (v instanceof Vector)),
		// vel of this Meteorus Fortification, a vector with values in
		// flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// rot of this Meteorus Fortification, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this Meteorus Fortification
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0))
	]);

	// Maximum acceleration
	static MAX_ACC = 2;  // flares / second^2
	// Friction
	static FRICTION = 3;
	// Rotation speed, in clockwise radians / second
	static SPIN = 1;

	// Create a Meteorus Fortification
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		MeteorusFortification.CONFIG_STRUCTURE.test(config);

		let parent = config.space.requestThingById(config.parentId);

		config.zIndex = Z_INDICES.BOSS_CHILD;
		config.len = 0.64;
		if (config.pos === undefined) config.pos = parent.pos;
		if (config.vel === undefined) config.vel = parent.vel;
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 48;
		config.type = THING_TYPES.ASTEROID;
		config.faction = FACTIONS.METEORIC;
		config.canDespawn = false;
		super(config);

		createAsteroidShape(this.display, this.len, 6);

		this._parentId = config.parentId;
		this._targetGenerator = functionFactory(config.targetGenerator);
	}

	// Move the Meteorus Fortification
	// dt is the time since last update, in seconds. time is the total in-game
	// time since the start of the game, in seconds.
	move(dt, time) {
		let parentPos = this.space.requestThingById(this._parentId).pos;
		let targetPos = Vector.sum(parentPos, this._targetGenerator(time));
		// TODO: Reduce acceleration when close to target
		let acc = Vector.diff(targetPos, this.pos)
			.norm(MeteorusFortification.MAX_ACC);

		AIUtils.translate(this, acc, MeteorusFortification.FRICTION, dt);
		AIUtils.rotateBy(this, MeteorusFortification.SPIN, dt);
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.prepareToDie(false);
		}
	}

	// Save this Meteorus Fortification into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;

		config.parentId = this._parentId;
		config.targetGenerator = this._targetGenerator.str;

		return {
			config:     config,
			isComplete: true
		};
	}
}



class MeteorusAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// String representation of a function that takes in the in-game time
		// and returns pos at that time
		new RequiredProperty("posGenerator", v => (typeof v === "string"))
	]);

	constructor(thing, config) {
		MeteorusAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._movementAI = new PathFollowAI(this.thing, {
			posGenerator: config.posGenerator,
			spn: 0.1
		});
		this._timeSincePlayerSeen = 0;
	}

	move(dt, time) {
		// Move

		this._movementAI.move(dt, time);

		// Attack

		this.thing._cooldown = Math.max(0, this.thing._cooldown - dt);

		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => entry.isPlayer,
			"METEORUS"
		).filter(entry => (Vector.diff(entry.pos, this.thing.pos).mag() <= 15));

		// The player is far away, reset health and fortifications
		if (tacticalReport.length === 0) {
			this._timeSincePlayerSeen += dt;
			if (this._timeSincePlayerSeen > 20) {
				this.removeAllFortifications();
				this.thing.health = this.thing.maxHealth;
			}
			return;
		}
		this._timeSincePlayerSeen = 0;

		// If we are on cooldown, we can't attack
		if (this.thing._cooldown > 0) return;

		// If more than one player is found, complain
		// I might implement proper support for attacking multiple players if I
		// make a Dark/Corrupted Player boss, but for now complaining is fine.
		if (tacticalReport.length > 1) {
			logger.warning("meteorus.js", "multiple players found; attacking"
							+ " multiple players is not supported");
		}

		let playerInfo = tacticalReport[0];

		// The player is close enough to not reset, but far enough to not attack or defend
		if (Vector.diff(playerInfo.pos, this.thing.pos) > 5) return;

		if (!this.thing._fortificationInfo.find(f => !f.deployed)) {
			this.thing.space.requestSpawn(MeteorusCannonball, {
				parentPos: this.thing.pos,
				parentVel: this.thing.vel,
				targetPos: playerInfo.pos
			});
			this.thing._cooldown = Meteorus.CANNONBALL_COOLDOWN;
		} else {
			let index = this.thing._chooseFortificationIndex();
			let fortification = this.thing.space.requestSpawn(MeteorusFortification, {
				parentId:        this.thing.id,
				targetGenerator: this.thing._getFortificationTargetGenerator(index)
			});
			this.thing._fortificationInfo[index].deployed = true;
			this.thing._fortificationInfo[index].id = fortification.id;
			this.thing._cooldown = Meteorus.FORTIFICATION_COOLDOWN;
		}
	}

	removeAllFortifications() {
		for (let i = 0; i < this.thing._fortificationInfo.length; ++i) {
			let id = this.thing._fortificationInfo[i].id;
			// Don't even need to check if the fortification is deployed
			let fortification = this.thing.space.requestThingById(id);
			if (fortification !== null) {
				fortification.prepareToDie();
			}
			this.thing._fortificationInfo[i].deployed = false;
			this.thing._fortificationInfo[i].id = null;
		}
	}
}



export class Meteorus extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// String representation of a function that takes in the in-game time
		// and returns pos at that time
		new RequiredProperty("posGenerator", v => (typeof v === "string")),
		new EitherOr([
			// Total in-game time in seconds
			new RequiredProperty("time", v => (Number.isFinite(v) && v >= 0))
		], [
			// pos of this Meteorus, a vector with values in flares
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this Meteorus, a vector with values in flares / second
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// rot of this Meteorus, in radians
			new RequiredProperty("rot", v => (Number.isFinite(v))),
			// Health of this Meteorus
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0)),
			// Fortification info
			new RequiredProperty("fortificationInfo", v => (Array.isArray(v))),
			// Cooldown before the next action, in seconds
			new RequiredProperty("cooldown", v => (Number.isFinite(v) && v > 0))
		])
	]);

	static DROP_LIST = new DropList([
		[ITEMS.COOL_ROCK,      [16], [1]],
		[ITEMS.ALUMINUM_CHUNK, [4],  [1]],
		[ITEMS.TITANIUM_CHUNK, [4],  [1]],
		[ITEMS.PLATINUM_CHUNK, [4],  [1]],
		[ITEMS.RED_GEM,        [2],  [1]],
		[ITEMS.YELLOW_GEM,     [2],  [1]],
		[ITEMS.GREEN_GEM,      [2],  [1]],
		[ITEMS.GYROSCOPE,      [4],  [1]]
	], 0, [COMPONENTS.STAR]);

	// Cooldown after firing a Meteorus Cannonball, in seconds
	static CANNONBALL_COOLDOWN = 0.25;
	// Cooldown after deploying a Meteorus Fortification, in seconds
	static FORTIFICATION_COOLDOWN = 0.25;
	// Maximum number of fortifications
	static NUM_FORTIFICATIONS = 16;

	// Create a Meteorus with given parameters
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Meteorus.CONFIG_STRUCTURE.test(config);

		let posGenerator = functionFactory(config.posGenerator);

		config.name = "Meteorus";
		config.texture = "images/meteorus.svg";
		config.zIndex = Z_INDICES.BOSS;
		config.len = 3;
		if (config.pos === undefined) config.pos = posGenerator(config.time);
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = 480;
		config.type = THING_TYPES.ASTEROID;
		config.faction = FACTIONS.METEORIC;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/meteorus.svg";
		config.showBossBar = true;
		super(config);

		this.ai = new MeteorusAI(this, {
			posGenerator: config.posGenerator
		});

		this._posGenerator = posGenerator;

		// Information about fortifications
		if (config.fortificationInfo === undefined) {
			this._fortificationInfo = [];
			for (let i = 0; i < Meteorus.NUM_FORTIFICATIONS; ++i) {
				this._fortificationInfo.push({
					deployed: false,
					id:       null
				});
			}
		} else {
			this._fortificationInfo = deepCopy(config.fortificationInfo);
		}

		// The cooldown until the next action, in seconds
		if (config.cooldown === undefined) {
			this._cooldown = 0;
		} else {
			this._cooldown = config.cooldown;
		}
	}

	// Save this Meteorus into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;

		config.posGenerator = this._posGenerator.str;
		config.fortificationInfo = deepCopy(this._fortificationInfo);
		config.cooldown = this._cooldown;

		return {
			config:     config,
			isComplete: true
		};
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.space.game.unicenter.ship.achievements.recordAchievement(
				ACHIEVEMENTS.ROCKY_START
			);
			this.prepareToDie(dropItems);
		}
	}

	// Prepare this Meteorus for removal
	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		this.ai.removeAllFortifications();

		// Drop items if dropItems is true or undefined
		if (dropItems !== false) {
			this.space.requestDrop(
				this.pos,
				this.radius,
				this.vel,
				Meteorus.DROP_LIST
			);
		}

		super.prepareToDie(dropItems);
	}

	// Return the index of a random non-deployed fortification
	_chooseFortificationIndex() {
		let indices = this._fortificationInfo
			// Combine each fortification with its index
			.map((f, i) => [i, f])
			// Select only non-deployed fortifications
			.filter(element => !element[1].deployed)
			// Keep only the indices
			.map(element => element[0]);

		if (indices.length === 0) {
			logger.error("meteorus.js", "no fortification to deploy");
			return 0;
		}

		return Random.element(indices);
	}

	// Return string representation of target generator for a fortification
	// index is the index of the fortification.
	_getFortificationTargetGenerator(index) {
		// TODO: Possibly randomize initial angle?
		let angle = index * (2 * Math.PI / Meteorus.NUM_FORTIFICATIONS);
		return `motion/circle/0/0/2/100/${angle}`;
	}
}
