import {Container, Graphics, Text} from "pixi.js";
import {Vector} from "./i.js"; // ./vector.js
import {G, VERSION} from "./i.js"; // ./dump.js
import {keyboard} from "./i.js"; // ./keyboard.js
import {UIUtils, Clicker} from "./i.js"; // ./ui/utils.js
import {Button} from "./i.js";
import {Game} from "./i.js"; // ./game.js

export class Menu {
	constructor() {
		this.display = new Container();
		this.game = null;
		this._mainMenu = new Container();
		this.display.addChild(this._mainMenu);

		// This is a hack to get buttons working for now
		this._fakeUnicenter = {
			rulerLength: 0,
			requestStroke: (color) => ({
				width: 1,
				color: color,
				alignment: 0
			}),
			requestFontSize: () => 0.05 * this._fakeUnicenter.rulerLength
		}

		this._background = new Graphics();
		this._continueButton = new Button(this._fakeUnicenter);
		this._newGameButton = new Button(this._fakeUnicenter);
		this._version = new Text({
			text: `Project Guesstimator\n${VERSION}`,
			style: UIUtils.getFont(0xffffff, {align: "center"})
		});
		this._mainMenu.addChild(
			this._background, this._continueButton, this._newGameButton, this._version
		);

		this._gameStarted = false;
	}

	update(dt, screenSize) {
		if (this._gameStarted) {
			this.game.update(dt, screenSize);
			return;
		}

		// Start a new game right away
		// this._gameStarted = true;
		// this.display.removeChild(this._mainMenu);
		// this.game = new Game();
		// this.display.addChild(this.game);

		if (this._continueButton.wasClicked() || this._newGameButton.wasClicked()) {
			this._gameStarted = true;
			this.display.removeChild(this._mainMenu);
			if (this._continueButton.wasClicked()) {
				let savefile = JSON.parse(localStorage.getItem("singularSave"));
				if (`v${savefile?.v}` === VERSION) {
					this.game = new Game(savefile);
				} else {
					alert(
						"I see your savefile, but it is from a different version of the "
						+ "game, so I can't load it. I will copy that savefile to "
						+ "`localStorage.oldSave` so you don't lose it, and I will start "
						+ "a new game for you. If you really want to keep the progress "
						+ "from your savefile, reach out to me on GitLab and I will do "
						+ "what I can."
					);
					localStorage.setItem("oldSave", localStorage.getItem("singularSave"));
					this.game = new Game();
				}
			} else {
				this.game = new Game();
			}
			this.display.addChild(this.game);
		}

		let rulerLength = UIUtils.getRulerLength(screenSize);

		this._background
			.clear()
			.rect(0, 0, screenSize.x, screenSize.y)
			.fill(0x000000);

		let continueAvailable = (localStorage.getItem("singularSave") !== null);

		let buttonSize = new Vector(
			0.4 * rulerLength,
			0.15 * rulerLength
		).round();
		let continueButtonPosition, newGameButtonPosition;
		if (continueAvailable) {
			continueButtonPosition = new Vector(
				screenSize.x / 2 - buttonSize.x / 2,
				screenSize.y / 2 - 0.03 * rulerLength - buttonSize.y
			).round();
			newGameButtonPosition = new Vector(
				screenSize.x / 2 - buttonSize.x / 2,
				screenSize.y / 2 + 0.03 * rulerLength
			).round();
		} else {
			newGameButtonPosition = new Vector(
				screenSize.x / 2 - buttonSize.x / 2,
				screenSize.y / 2 - buttonSize.y / 2
			).round();
		}

		this._fakeUnicenter.rulerLength = rulerLength;
		if (continueAvailable) {
			this._continueButton.visible = true;
			this._continueButton.update(
				0x111111, 0xffffff,
				continueButtonPosition,
				buttonSize,
				"Continue"
			);
		} else {
			this._continueButton.visible = false;
		}
		this._newGameButton.update(
			0x111111, 0xffffff,
			newGameButtonPosition,
			buttonSize,
			"New game"
		);

		this._version.style.fontSize = 0.02 * rulerLength;
		let versionPosition = new Vector(
			screenSize.x / 2 - this._version.width / 2,
			screenSize.y - 0.05 * rulerLength - this._version.height / 2
		).round();
		this._version.position.copyFrom(versionPosition);

		this._continueButton.clearStuff();
		this._newGameButton.clearStuff();
	}
}
