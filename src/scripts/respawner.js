// The respawn screen
// TODO: Scale this

import {Container, Graphics, Text} from "pixi.js";
import {Vector} from "./i.js"; // ./vector.js
import {keyboard, Keyboard} from "./i.js"; // ./keyboard.js
import {UIUtils, Clicker} from "./i.js"; // ./ui/utils.js

export class Respawner extends Container {
	// Color with which the background is shaded
	static BACKGROUND_COLOR = 0x250909;
	// Alpha with which the background is shaded
	static BACKGROUND_ALPHA = 0.8;

	// Create a new respawner
	// Only create one respawner per game. player is a reference to the player.
	constructor(player) {
		super();

		// Save the reference to the player
		this._player = player;

		// The screen size as of last resize
		// TODO: Contemplate using a null vector here.
		this._lastScreenSize = new Vector();

		this._background = new Graphics();
		this.addChild(this._background);

		this._youShatteredText = new Text({
			text: "You shattered!",
			style: UIUtils.getFont(0xff2424, {fontSize: 48})
		});
		this._youShatteredText.anchor.set(0.5, 1);
		this.addChild(this._youShatteredText);

		this._respawnText = new Text({
			text: "Press (R) to respawn",
			style: UIUtils.getFont(0xff2424, {fontSize: 24})
		});
		this._respawnText.anchor.set(0.5, 0);
		this.addChild(this._respawnText);

		// Respawn when player clicks anywhere on the screen
		// TODO: Maybe activate this clicker after a fraction of a second to
		// prevent accidental respawning?
		this._backgroundClicker = new Clicker(this._background);

		this.visible = false;
	}

	// Update the respawner
	// This function should be called every tick when the respawner has any
	// chance of being visible - probably just always call it every tick.
	// screenSize is a vector in pixels.
	update(screenSize) {
		if (!this.visible) return;

		if (keyboard.getActions("RESPAWNER").has("RESPAWN")
						|| this._backgroundClicker.wasClicked()) {
			this._player.respawn();
			this.hide();
		} else if (!this._lastScreenSize.equal(screenSize)) {
			this._lastScreenSize.copyFrom(screenSize);
			this._refresh();
		}

		this._backgroundClicker.clearStuff();
	}

	// Show the respawner
	// Call this function before calling update() in a tick.
	show() {
		this.visible = true;

		if (!keyboard.hasControl("RESPAWNER")) {
			keyboard.pushControl(new Keyboard.Control(
				"RESPAWNER",
				new Map([
					[".RESPAWN", "RESPAWN"]
				]),
				new Set(["PLAYER", "GAME", "UNICENTER"])
			));
		}

		// As of the time of writing, this call is not necessary, but I included
		// it here to prevent future bugs.
		this._refresh();
	}

	// Hide the respawner
	// Call this function before calling update() in a tick.
	hide() {
		this.visible = false;

		keyboard.popControl("RESPAWNER");
	}

	// Refresh the respawner, redrawing everything from scratch
	// Unlike redraw() methods elsewhere in the codebase, this method should
	// only be called when something actually changed, and not every tick.
	_refresh() {
		this._background
			.clear()
			.rect(
				0, 0,
				this._lastScreenSize.x, this._lastScreenSize.y
			)
			.fill({color: Respawner.BACKGROUND_COLOR, alpha: Respawner.BACKGROUND_ALPHA});

		let middle = Vector.scale(this._lastScreenSize, 0.5).round();

		this._youShatteredText.position.set(middle.x, middle.y - 20);
		this._respawnText.position.set(middle.x, middle.y + 20);
	}
}
