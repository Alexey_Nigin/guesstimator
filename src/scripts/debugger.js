import {Container, Graphics, Text} from "pixi.js";
import {round} from "./utils.js";
import {Vector} from "./i.js";
import {ITEMS, RESOURCES} from "./i.js";
import {UIUtils} from "./i.js";
import {Button} from "./i.js";

const BUTTON_NAMES = ["it", "re", "up"];
const TELEPORT_DISTANCE = 200 / 3;

export class Debugger {
	constructor(game) {
		this.game = game;

		this.display = new Container();

		this.averageFPS = [];
		this.averageMSPT = [];
		this.lastTickStart = null;
		this.lastDensities = new Map();

		this._background = new Graphics();
		this.text = new Text({text: "", style: UIUtils.getFont(0xffffff)});

		this._cheatItems = new Button(game.unicenter);
		this._cheatResources = new Button(game.unicenter);
		this._cheatUpgrades = new Button(game.unicenter);
		this._rowButtons = [
			this._cheatItems, this._cheatResources, this._cheatUpgrades
		];

		this._left = new Button(game.unicenter);
		this._right = new Button(game.unicenter);
		this._up = new Button(game.unicenter);
		this._down = new Button(game.unicenter);
		this._dirButtons = [
			this._left, this._right, this._up, this._down
		];

		this._buttons = [...this._rowButtons, ...this._dirButtons];

		this.display.addChild(this._background, this.text, ...this._buttons);
	}

	update(screenSize) {
		if (!this.display.visible) {
			this._clearStuff();
			return;
		}

		this._cheat();

		let rulerLength = UIUtils.getRulerLength(screenSize);
		let marginWidth = Math.round(0.01 * rulerLength);

		this.text.position.copyFrom(new Vector(
			2 * marginWidth,
			2 * marginWidth
		).round());
		this.text.style.fontSize = 0.02 * rulerLength;

		let averageFPSDisplay = this.averageFPS.length === 0
			? 1
			: Math.round(
				this.averageFPS.reduce((a, b) => a + b) / this.averageFPS.length
			);
		let averageMSPTDisplay = this.averageMSPT.length === 0
			? 1000
			: this.averageMSPT.reduce((a, b) => a + b) / this.averageMSPT.length;
		let numThings = this.game.space.things.length;
		let numCollisionCooldowns = this.game.space.interactor._collisionCooldowns.size;

		let densityDisplay = Array.from(this.lastDensities.entries())
			.map(entry => `${entry[0].toLowerCase()}: ${round(entry[1], 2)}`)
			.join("\n");

		this.text.text = `fps:  ${averageFPSDisplay}`
			+ `\nmspt:  ${round(averageMSPTDisplay, 3)}`
			+ `\nfps limit:  ${Math.floor(1000 / averageMSPTDisplay)}`
			+ `\nthings:  ${numThings}`
			+ `\ncooldowns:  ${numCollisionCooldowns}`
			+ "\n"
			+ "\ndensity"
			+ `\n${densityDisplay}`
			+ "\n"
			+ "\ncheats:";

		let buttonSize = new Vector(0.05 * rulerLength).round();
		let buttonOrigin = new Vector(
			2 * marginWidth, this.text.height + 3 * marginWidth
		).round();
		function getGridOrigin(x, y) {
			return new Vector(
				x * (buttonSize.x + marginWidth),
				y * (buttonSize.y + marginWidth)
			).add(buttonOrigin);
		}

		this._rowButtons.forEach((button, index) => {
			button.update(
				0x000000, 0xffffff,
				getGridOrigin(index, 0), buttonSize,
				BUTTON_NAMES[index]
			);
		})

		this._left.update(0x000000, 0xffffff, getGridOrigin(0, 2), buttonSize, "<");
		this._right.update(0x000000, 0xffffff, getGridOrigin(2, 2), buttonSize, ">");
		this._up.update(0x000000, 0xffffff, getGridOrigin(1, 1), buttonSize, "^");
		this._down.update(0x000000, 0xffffff, getGridOrigin(1, 3), buttonSize, "v");

		let backgroundOrigin = new Vector(marginWidth, marginWidth);
		let backgroundSize = new Vector(
			Math.max(
				this.text.width,
				this._rowButtons.length * (buttonSize.x + marginWidth) - marginWidth,
				3 * (buttonSize.x + marginWidth) - marginWidth
			) + 2 * marginWidth,
			this.text.height + 4 * buttonSize.y + 6 * marginWidth
		).round();
		this._background
			.clear()
			.rect(
				backgroundOrigin.x, backgroundOrigin.y,
				backgroundSize.x, backgroundSize.y
			)
			.fill({color: 0x000000, alpha: 0.6});

		this._clearStuff();
	}

	// Toggle the debugger between visible and invisible
	toggle() {
		this.display.visible = !this.display.visible;
	}

	// Report the start of a tick
	// Call as early in the tick as possible.
	reportTickStart() {
		this.lastTickStart = performance.now();
	}

	// Report the end of a tick
	// Call as late in the tick as possible.
	reportTickEnd() {
		if (this.lastTickStart !== null) {
			this.averageMSPT.unshift(performance.now() - this.lastTickStart);
			if (this.averageMSPT.length > 256) this.averageMSPT.pop();
			this.lastTickStart = null;
		}
	}

	// Report time between frames
	// Args:
	//   dt - time from last frame to this frame, in seconds
	reportDt(dt) {
		let fps = 1 / dt;
		this.averageFPS.unshift(fps);
		if (this.averageFPS.length > 256) this.averageFPS.pop();
	}

	// Report current densities
	// Args:
	//   densities: map from Density.POOLS to floats
	reportDensities(densities) {
		this.lastDensities = new Map(densities);
	}

	// Execute cheat button actions
	_cheat() {
		if (this._cheatItems.wasClicked()) {
			Object.values(ITEMS).forEach(i => this.game.unicenter.inventory.add(i));
			this.game.cheatsUsed = true;
		}

		if (this._cheatResources.wasClicked()) {
			this.game.unicenter.refinery.deduct(new Map([
				[RESOURCES.MINERALS,    -1000000],
				[RESOURCES.ALLOYS,      -1000000],
				[RESOURCES.POLYMERS,    -1000000],
				[RESOURCES.LIGHTWEAVES, -1000000]
			]));
			this.game.cheatsUsed = true;
		}

		if (this._cheatUpgrades.wasClicked()) {
			this.game.unicenter.upgrader.maxOut();
			this.game.unicenter.forge.maxOut();
			this.game.cheatsUsed = true;
		}

		if (this._left.wasClicked()) {
			this.game.space.player.pos.x -= TELEPORT_DISTANCE;
			this.game.cheatsUsed = true;
		}
		if (this._right.wasClicked()) {
			this.game.space.player.pos.x += TELEPORT_DISTANCE;
			this.game.cheatsUsed = true;
		}
		if (this._up.wasClicked()) {
			this.game.space.player.pos.y -= TELEPORT_DISTANCE;
			this.game.cheatsUsed = true;
		}
		if (this._down.wasClicked()) {
			this.game.space.player.pos.y += TELEPORT_DISTANCE;
			this.game.cheatsUsed = true;
		}
	}

	// Clear stuff for all buttons
	_clearStuff() {
		this._buttons.forEach(button => button.clearStuff());
	}
}
