// Define a class for a friendly station

import {Graphics, Sprite} from "pixi.js";
import {round} from "./utils.js";
import {getTexture} from "./assets.js";
import {Vector, isVectorable} from "./i.js"; // ./vector.js
import {Random} from "./i.js";
import {functionFactory} from "./i.js"; // ./code-factories.js
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {AI, AIUtils} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {UPGRADER_POWER} from "./i.js";

const STANDARD_CONFIGS = new Map([
	["ORIGIN", {
		name: "Origin Station",
		// Radius-0 "circle" around the origin
		posGenerator: "motion/circle/0/0/0/1/0",
		len: 2.4,
		bodyTexture: "images/stations/origin.svg",
		morseLedLocation: new Vector(60, 204),
		actionLedLocation: new Vector(60, 224)
	}],
	["FORGE", {
		name: "Forge Station",
		posGenerator: `motion/circle/0/0/200/${60 * 60}/0`,
		len: 2.4,
		bodyTexture: "images/stations/origin.svg",
		morseLedLocation: new Vector(60, 204),
		actionLedLocation: new Vector(60, 224)
	}],
	["GRAND", {
		name: "Grand Station",
		posGenerator: `motion/circle/0/0/200/${60 * 60}/${- 4 * Math.PI / 5}`,
		// Match the scale of the other stations
		len: 2.4 * 520 / 372,
		bodyTexture: "images/stations/grandma.svg",
		morseLedLocation: new Vector(448, 52),
		actionLedLocation: new Vector(468, 52)
		// TODO: Expand shield for this station
	}]
]);



export class StationAI extends AI {
	constructor(thing, config) {
		super(thing);
	}

	move(dt, time) {
		// Move the station
		AIUtils.setPos(this.thing, this.thing.posGenerator(time), dt);
		AIUtils.rotateBy(this.thing, Station.SPIN, dt);

		// Extend or retract the dock

		// Calculate the fraction by which the dock can extend or retract this
		// tick
		let tick_extension_fraction = dt / Station.EXTENSION_TIME;
		// Figure out whether the dock should aim for being extended or
		// retracted
		let goalIsExtended = (this.thing.space.player.dockingTo === this.thing.id);
		// Adjust the extension fraction with no regard for bounds
		this.thing._extensionFraction += tick_extension_fraction
						* (goalIsExtended ? 1 : -1);
		// Clamp the extension fraction between 0 and 1
		this.thing._extensionFraction = Math.min(
			Math.max(this.thing._extensionFraction, 0), 1
		);
	}
}



class StationAnimation {
	static CONFIG_STRUCTURE = new ConfigStructure([
		new RequiredProperty("bodyTexture", v => v => ((typeof v === "string")
						&& (true))), // TODO check that texture exists
		new RequiredProperty("morseLedLocation", v => isVectorable(v)),
		new RequiredProperty("actionLedLocation", v => isVectorable(v))
	]);

	constructor(thing, config) {
		StationAnimation.CONFIG_STRUCTURE.test(config);

		this.thing = thing;

		this._body = new Sprite(getTexture(config.bodyTexture));
		this._body.anchor.set(0.5, 0.5);
		this._body.position.set(0, 0);
		this._body.scale.set((2 / 3) * this.thing.len / this._body.texture.height);

		this._dock = new Sprite(getTexture("images/stations/dock.svg"));
		this._dock.anchor.set(0, 0.5);
		this._dock.position.copyFrom(
			this._texturePixelsToFlares(Station.DOCK_RETRACTED)
		);
		this._dock.scale.copyFrom(this._body.scale);

		this._morseLed = new Sprite(getTexture("images/stations/morse.svg"));
		this._morseLed.anchor.set(0.5, 0.5);
		this._morseLed.position.copyFrom(
			this._texturePixelsToFlares(new Vector(config.morseLedLocation))
		);
		this._morseLed.scale.copyFrom(this._body.scale);

		this._actionLed = new Sprite(getTexture("images/stations/action.svg"));
		this._actionLed.anchor.set(0.5, 0.5);
		this._actionLed.position.copyFrom(
			this._texturePixelsToFlares(new Vector(config.actionLedLocation))
		);
		this._actionLed.scale.copyFrom(this._body.scale);

		this._shield = new Graphics();
		this._shield
			.circle(0, 0, thing.radius)
			.fill(0x00ff44);
		this._shield.alpha = 0;

		thing.display.addChild(
			this._dock,
			this._body,
			this._morseLed,
			this._actionLed,
			this._shield
		);
	}

	update(dt, time) {
		// Fade the shield
		this._shield.alpha = Math.max(
			this._shield.alpha - Station.SHIELD_ALPHA_DECAY * dt,
			0
		);

		// Adjust the display to match the extension fraction
		this._dock.position.copyFrom(Vector.interpolate(
			this._texturePixelsToFlares(Station.DOCK_RETRACTED),
			this._texturePixelsToFlares(Station.DOCK_EXTENDED),
			this.thing._extensionFraction
		));

		// Turn LEDs on or off
		let morsePhase = Math.floor(time / Station.MORSE_RHYTHM)
						% this.thing._morseName.length;
		this._morseLed.visible = (this.thing._morseName[morsePhase] === "1");

		let actionPhase = Math.floor(time / Station.ACTION_RHYTHM)
						% 2;
		let somethingToDo = (
			(this.thing._s === "ORIGIN")
				&& this.thing.space.game.unicenter.upgrader
					.anythingToDo(UPGRADER_POWER.BASIC)
			|| (this.thing._s === "GRAND")
				&& this.thing.space.game.unicenter.upgrader
					.anythingToDo(UPGRADER_POWER.INTERMEDIATE)
			|| (this.thing._s === "FORGE")
				&& this.thing.space.game.unicenter.forge.anythingToDo()
		);
		this._actionLed.visible = somethingToDo && (actionPhase === 0);
	}

	// Convert from texture pixels on this._body to flares used by this.display
	// tp is a vector in texture pixels
	_texturePixelsToFlares(tp) {
		return new Vector(- this._body.width / 2, - this._body.height / 2)
			.add(Vector.scale(tp, this._body.scale.x));
	}
}



export class Station extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		new EitherOr([
			// Standard station shortcut
			// One of STANDARD_CONFIG.keys(). Fills in `name` and `posGenerator`.
			new RequiredProperty("s",
				v => Array.from(STANDARD_CONFIGS.keys()).includes(v)
			)
		], [
			// Name of this station
			// Shown to the player via morse code, and in the future in other ways too.
			new RequiredProperty("name", v => (typeof v === "string")),
			// String representation of a function that takes in the in-game time
			// and returns pos at that time
			new RequiredProperty("posGenerator", v => (typeof v === "string")),
		]),
		// rot of this station, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this station
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		// The fraction by which the dock is currently visually extended
		new OptionalProperty("extensionFraction", v =>
						(Number.isFinite(v) && v >= 0 && v <= 1))
	]);

	// Note that this map uses dots and dashes, but most other code represents
	// morse code as a string of 0s and 1s encoding lengths of all signals and
	// gaps.
	// Also note that space is handled separately and isn't included in this map.
	static MORSE_CODE = new Map([
		["A", ".-"  ],
		["B", "-..."],
		["C", "-.-."],
		["D", "-.." ],
		["E", "."   ],
		["F", "..-."],
		["G", "--." ],
		["H", "...."],
		["I", ".."  ],
		["J", ".---"],
		["K", "-.-" ],
		["L", ".-.."],
		["M", "--"  ],
		["N", "-."  ],
		["O", "---" ],
		["P", ".--."],
		["Q", "--.-"],
		["R", ".-." ],
		["S", "..." ],
		["T", "-"   ],
		["U", "..-" ],
		["V", "...-"],
		["W", ".--" ],
		["X", "-..-"],
		["Y", "-.--"],
		["Z", "--.."]
	]);

	static SPIN = 0.1;
	static SHIELD_ALPHA_DECAY = 0.6;

	// The duration of the shortest flash of the morse LED, in seconds
	static MORSE_RHYTHM = 0.5;
	// The duration of the flash of the action LED, in seconds
	static ACTION_RHYTHM = 0.5;
	// Time it takes the dock to fully extend / retract, in seconds
	static EXTENSION_TIME = 1;

	// Positions of station elements in texture pixels
	static DOCK_RETRACTED = new Vector(4, 258);
	static DOCK_EXTENDED = new Vector(-45, 258);

	// Create a new station
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Station.CONFIG_STRUCTURE.test(config);

		// Default values for non-standard configs
		// TODO: Either properly support non-standard configs, or remove them entirely.
		config.bodyTexture = "images/stations/origin.svg";
		config.len = 2.4;
		config.morseLedLocation = new Vector();
		config.actionLedLocation = new Vector();

		if (config.s !== undefined) {
			let standardConfig = STANDARD_CONFIGS.get(config.s);
			config.name = standardConfig.name;
			config.posGenerator = standardConfig.posGenerator;
			config.len = standardConfig.len;
			config.bodyTexture = standardConfig.bodyTexture;
			config.morseLedLocation = new Vector(standardConfig.morseLedLocation);
			config.actionLedLocation = new Vector(standardConfig.actionLedLocation);
		}

		config.identity = "STATION";
		config.zIndex = Z_INDICES.STATION;
		config.pos = new Vector();
		config.vel = new Vector();
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 1;
		config.type = THING_TYPES.STATION;
		config.faction = FACTIONS.PLAYER;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/station.svg";
		config.showBossBar = true;
		super(config);

		if (config.s !== undefined) this._s = config.s;

		this.posGenerator = functionFactory(config.posGenerator);

		// Read by the player
		this.timeToDock = Station.EXTENSION_TIME;
		this.mainTab = "REFINERY";

		// The fraction by which the dock is currently visually extended
		this._extensionFraction = config.extensionFraction ?? 0;

		this._morseName = Station._morsify(this.name);

		this.ai = new StationAI(this);
		this.animation = new StationAnimation(this, {
			bodyTexture: config.bodyTexture,
			morseLedLocation: config.morseLedLocation,
			actionLedLocation: config.actionLedLocation
		});
	}

	// Raise the station's shield
	// This function is called by the interactor whenever something is foolish
	// enough to attack a station.
	raiseShield() {
		this.animation._shield.alpha = 0.2;
	}

	// Return a {pos, vel, rot, spn} of the docking location for the player
	// This location changes over time.
	// Args:
	//   timeFromNow - number in seconds
	getDockLocation(timeFromNow) {
		if (timeFromNow === undefined) timeFromNow = 0;

		let getStationPosAt = (timeFromNow) => {
			let futureStationPos = this.posGenerator(this.space.time + timeFromNow);
			let futureStationRot = this.rot + timeFromNow * this.spn;
			return Vector.rotateBy(
				this.animation._texturePixelsToFlares(Station.DOCK_EXTENDED),
				futureStationRot + Math.PI / 2
			).add(futureStationPos);
		};

		// Really small time delta to calculate vel
		const EPSILON = 0.0001;

		return {
			pos: getStationPosAt(timeFromNow),
			vel: Vector.diff(
				getStationPosAt(timeFromNow + EPSILON),
				getStationPosAt(timeFromNow)
			).scale(1 / EPSILON),
			rot: this.rot + timeFromNow * this.spn,
			spn: this.spn
		};
	}

	// Return a {pos, rot} of the box location
	getBoxLocation() {
		let time = this.space.time;
		return {
			pos: new Vector(2, 0).rotateBy((time / 60) * (2 * Math.PI)),
			rot: (time / 20) * (2 * Math.PI)
		};
	}

	// Save this station into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();

		config.rot = round(this.rot);
		if (this._s !== undefined) {
			config.s = this._s;
		} else {
			config.name = this.name;
			config.posGenerator = this.posGenerator.str;
		}
		if (this._extensionFraction !== 0) {
			config.extensionFraction = round(this._extensionFraction);
		}

		return config;
	}

	receiveBoop(otherThing) {
		if (otherThing.identity === "PLAYER") {
			this.space.canDock = " the station";
		}
	}

	// Convert str to morse code represented as a sequence of 0s and 1s
	// For example "AB" -> "1011100011101010100000000000". 11 zeros at the end
	// are for easy looping. This function ignores unknown characters.
	static _morsify(str) {
		const SPACE = " ";

		let knownChars = new Set(Station.MORSE_CODE.keys());
		knownChars.add(SPACE);

		let result = "";

		for (let char of str.toUpperCase()) {
			// Ignore unknown characters
			if (!knownChars.has(char)) continue;

			if (char === SPACE) {
				// Add a gap for space
				result += "0".repeat(7);
			} else {
				// Add a gap between non-space characters
				if (result.length > 0 && result[result.length - 1] === "1") {
					result += "0".repeat(3);
				}
				// Convert dots and dashes to 0s and 1s
				result += Station.MORSE_CODE.get(char).split("")
					.map(symbol => (symbol === "-") ? "1".repeat(3) : "1")
					.join("0");
			}
		}

		// Add a gap at the end for easy looping.
		result += "0".repeat(11);

		return result;
	}
}
