// Define a vector class
// This class should be used for all internal calculations. For display, convert
// a vector to a PIXI.Point like this:
//     sprite.position.copyFrom(vector);
// Throughout this file, "vector-like object" refers to an object with numeric x
// and y properties. Examples of vector-like objects are Vector and PIXI.Point.

import {round} from "./utils.js";

export class Vector {
	// Create a new vector
	// If a and b are numbers, and v is a vector-like object, then
	//     new Vector(a, b) gives (a, b)
	//     new Vector(a)    gives (a, a)
	//     new Vector()     gives (0, 0)
	//     new Vector(v)    gives a deep copy of v
	//     new Vector(arr)  gives a vector from array like [1.234, -5.678]
	constructor(x, y) {
		if (Array.isArray(x)) {
			this.x = parseFloat(x[0]);
			this.y = parseFloat(x[1]);
			return;
		}
		if (x !== undefined && x.x !== undefined && x.y !== undefined) {
			this.x = x.x;
			this.y = x.y;
			return;
		}
		if (x === undefined) x = 0;
		if (y === undefined) y = x;
		this.x = x;
		this.y = y;
	}

	// Copy another vector into this vector
	// v is a vector-like object. Useful when you want to keep references to
	// this vector intact.
	copyFrom(v) {
		this.x = v.x;
		this.y = v.y;
	}

	// Add another vector, modifying this vector in place
	// delta is a vector-like object.
	add(delta) {
		this.x += delta.x;
		this.y += delta.y;
		return this;
	}

	// Subtract another vector, modifying this vector in place
	// delta is a vector-like object.
	sub(delta) {
		this.x -= delta.x;
		this.y -= delta.y;
		return this;
	}

	// Multiply by a scalar, modifying this vector in place
	// k must be a number.
	scale(k) {
		this.x *= k;
		this.y *= k;
		return this;
	}

	// Normalize this vector, modifying it in place
	// If length is specified, vector is normalized to that length. Otherwise,
	// the vector is normalized to length 1. If the vector has magnitude 0, it
	// is left unchanged.
	norm(length) {
		if (length === undefined) length = 1;
		let m = this.mag();
		if (m > 0) this.scale(length / m);
		return this;
	}

	// Round components of this vector to integers
	// This is useful for UI elements.
	round() {
		this.x = Math.round(this.x);
		this.y = Math.round(this.y);
		return this;
	}

	// Return magnitude of this vector
	mag() {
		return Math.hypot(this.x, this.y);
	}

	// Return squared magnitude of this vector
	mag2() {
		return this.x * this.x + this.y * this.y;
	}

	// Return the direction of this vector in radians
	dir() {
		return Math.atan2(this.y, this.x);
	}

	// Rotate this vector by a specific angle
	// theta is an angle in radians.
	rotateBy(theta) {
		let x = this.x;
		let y = this.y;
		this.x = x * Math.cos(theta) - y * Math.sin(theta);
		this.y = x * Math.sin(theta) + y * Math.cos(theta);
		return this;
	}

	// If this vector is longer than length, shorten it to length
	// length is a non-negative number.
	limit(length) {
		if (this.mag2() <= (length * length)) return this;
		return this.norm(length);
	}

	// Return a new random vector close to this vector
	// The returned vector lies within a circle of radius r around this vector.
	chooseNearby(r) {
		return Vector.chooseLimited(r).add(this);
	}

	// Return true iff this vector is equal to v
	// v is a vector-like object.
	equal(v) {
		return (this.x === v.x) && (this.y === v.y);
	}

	// Save this vector to array like [1.234, -5.678]
	// places is the number of places after decimal point to round to, defaults to 6.
	save() {
		return [round(this.x), round(this.y)];
	}

	// Return a new random vector with magnitude not exceeding limit
	// To generate the vectors more uniformly, I used this algorithm:
	// https://stackoverflow.com/a/50746409
	static chooseLimited(limit) {
		let dir = 2 * Math.PI * Math.random();
		let mag = limit * Math.sqrt(Math.random());
		return new Vector(mag * Math.cos(dir), mag * Math.sin(dir));
	}

	// Return a new vector equal to the sum of two input vectors
	// Input arguments are vector-like objects.
	static sum(v0, v1) {
		return new Vector(v0.x + v1.x, v0.y + v1.y);
	}

	// Return a new vector equal to the first input vector minus the second
	// Input arguments are vector-like objects.
	static diff(v0, v1) {
		return new Vector(v0.x - v1.x, v0.y - v1.y);
	}

	// Return a new vector equal to the input vector times the input scalar
	// v is a vector-like object, k must be a number.
	static scale(v, k) {
		return new Vector(k * v.x, k * v.y);
	}

	// Return a new vector equal to the normalization of a given vector
	// v is a vector. If length is specified, vector is normalized to that
	// length. Otherwise, the vector is normalized to length 1. If the vector
	// has magnitude 0, it is left unchanged.
	static norm(v, length) {
		if (length === undefined) length = 1;
		let m = v.mag();
		if (m > 0) {
			return Vector.scale(v, length / m);
		} else {
			return new Vector(v);
		}
	}

	// Return a new vector equal to the projection of v onto axis
	// v is a vector-like object, axis must be a vector.
	static proj(v, axis) {
		return new Vector(axis.x, axis.y)
			.scale((v.x * axis.x + v.y * axis.y) / axis.mag2());
	}

	// Return the squared distance between two vectors
	// This is equivalent to Vector.diff(v0, v1).mag2(), but faster.
	static dist2(v0, v1) {
		let dx = v0.x - v1.x;
		let dy = v0.y - v1.y;
		return dx * dx + dy * dy;
	}

	// Return the dot product of two vectors
	// Input arguments are vector-like objects.
	static dot(v0, v1) {
		return v0.x * v1.x + v0.y * v1.y;
	}

	// Return a vector equal to given vector rotated by a specific angle
	// v is a vector, theta is an angle in radians.
	static rotateBy(v, theta) {
		return new Vector(
			v.x * Math.cos(theta) - v.y * Math.sin(theta),
			v.x * Math.sin(theta) + v.y * Math.cos(theta)
		);
	}

	// Perform linear interpolation between two vectors
	// v0 and v1 are vectors, k is a number. If k is 0, return a copy of v0. If
	// k is 1, return a copy of v1. If k is between 0 and 1, return a vector
	// between v0 and v1.
	static interpolate(v0, v1, k) {
		return new Vector(
			(1 - k) * v0.x + k * v1.x,
			(1 - k) * v0.y + k * v1.y
		);
	}
}

// Return true iff v can be turned into a vector
export function isVectorable(v) {
	if (Array.isArray(v)) {
		if (v.length !== 2) return false;
		let x = parseFloat(v[0]);
		let y = parseFloat(v[1]);
		return Number.isFinite(x) && Number.isFinite(y);
	} else {
		return Number.isFinite(v?.x) && Number.isFinite(v?.y);
	}
}
