import {Sprite} from "pixi.js";
import {Polygon} from "detect-collisions";
// import {createSmoothInterpolation, round} from "./utils.js";
import {getTexture} from "./assets.js";
import {Vector, isVectorable} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
// import {angleDiff, lerp, MOUSE, sq} from "./i.js"; // ./dump.js
import {keyboard} from "./i.js"; // ./keyboard.js
import {ConfigStructure, RequiredProperty} from "./i.js"; // ./validators.js
import {AI, AIUtils} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, STANDARD_PPF, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {Blaster, BLASTER_VARIETIES} from "./i.js";



class DartAI extends AI {
	constructor(thing, config) {
		super(thing);

		this._orbit = {
			rot: 0,
			spn: 0
		};
	}

	move(dt, time) {
		if (this.thing.space.game.unicenter.ship.numPlayers === 1) {
			this.thing.prepareToDie();
			return;
		}

		let player = this.thing.space.player;

		let actions = keyboard.getActions("DART");

		let movement = (actions.has("MOVE_LEFT") ? -1 : 0)
			+ (actions.has("MOVE_RIGHT") ? 1 : 0);
		AIUtils.rotateBy(this._orbit, 3 * movement, dt, 3);

		let turning = (actions.has("TURN_LEFT") ? -1 : 0)
			+ (actions.has("TURN_RIGHT") ? 1 : 0);
		AIUtils.rotateBy(this.thing, 4 * turning, dt, 5);

		AIUtils.setPos(
			this.thing,
			Vector.sum(player.pos, new Vector(1, 0).rotateBy(this._orbit.rot)),
			dt
		);

		if (actions.has("FIRE")) this.thing.isFiring = !this.thing.isFiring;

		this.thing.cooldown = Math.max(this.thing.cooldown - dt, 0);
		if (this.thing.isFiring && this.thing.cooldown === 0) {
			this.thing.space.requestSpawn(Blaster, {
				variety: BLASTER_VARIETIES.DART,
				pos: this.thing.pos,
				rot: this.thing.rot,
				parentVel: this.thing.vel,
				scale: 3 / 5
			});
			this.thing.cooldown = 0.1;
		}
	}
}



export class Dart extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([]);

	// Create a dart
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Dart.CONFIG_STRUCTURE.test(config);

		config.identity = "DART";
		config.name = "Dart";
		config.texture = "images/dart.svg";
		config.zIndex = Z_INDICES.PLAYER;
		config.len = 0.3;
		config.hitbox = new Polygon({x: 0, y: 0}, [
			{x: 0.15, y: 0},
			{x: -0.15, y: -0.1},
			{x: -0.15, y: 0.1}
		]);
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.PLAYER;
		config.canDespawn = false;
		if (config.pos === undefined) config.pos = new Vector();
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = Math.ceil(
			config.space.game.unicenter.upgrader.getValue("HEALTH") / 3
		);
		super(config);

		this.cooldown = 0;
		this.isFiring = false;

		this.ai = new DartAI(this);
	}

	// Repair the dart a little bit
	// dt is the time since last update, in seconds. The interactor executes
	// this function every tick if the dart is inside a station. stationId is the id of
	// the station providing the repairs.
	repair(dt, stationId) {
		this.health = Math.min(
			this.health + this.maxHealth * dt,
			this.maxHealth
		);
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.health = 0;
			this.prepareToDie(false);
		}
	}

	// Save this player into a config object
	// See the comment on Thing.save() for more details.
	// save() {}
}
