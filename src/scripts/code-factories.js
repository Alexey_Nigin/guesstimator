import {Vector} from "./i.js"; // ./vector.js

// Create a function from its string representation str
// Currently supported functions:
//     motion/circle/<originX>/<originY>/<radius>/<period>/<initTheta>
export function functionFactory(str) {
	let f;

	let [name, ...otherArgs] = str.split("/");
	switch (name) {
		case "motion":
			f = _motionFactory(otherArgs);
			break;
		default:
			throw new CodeFactoryError(`unknown function "${name}"`);
	}

	// Attach the original string to the function to simplify saving
	f.str = str;
	return f;
}



export class CodeFactoryError extends Error {
	constructor(...args) {
		super(...args);
		this.name = "CodeFactoryError";
	}
}



function _motionFactory(args) {
	if (args.length === 0) {
		throw new CodeFactoryError(`function "motion" requires a subfunction`);
	}

	let [name, ...otherArgs] = args;
	switch (name) {
		case "circle":
			return _motionCircleFactory(otherArgs);
		default:
			throw new CodeFactoryError(`unknown function "motion/${name}"`);
	}
}

function _motionCircleFactory(args) {
	if (args.length !== 5) {
		throw new CodeFactoryError(`function "motion/circle" requires exactly 5`
						+ ` arguments, ${args.length} argument(s) provided`);
	}

	let originX   = parseFloat(args[0]);
	let originY   = parseFloat(args[1]);
	let radius    = parseFloat(args[2]);
	let period    = parseFloat(args[3]);
	let initTheta = parseFloat(args[4]);

	let floatArgs = [originX, originY, radius, period, initTheta];
	if (!floatArgs.every(v => Number.isFinite(v))) {
		throw new CodeFactoryError(`arguments to function "motion/circle" must`
						+ ` all be finite floats`);
	}
	if (period === 0) {
		throw new CodeFactoryError(`in function "motion/circle", period must`
						+ ` not be 0`);

	}

	let origin = new Vector(originX, originY);
	let omega = 2 * Math.PI / period;
	return time => Vector.sum(
		origin,
		new Vector(
			radius * Math.cos(omega * time + initTheta),
			radius * Math.sin(omega * time + initTheta)
		)
	);
}
