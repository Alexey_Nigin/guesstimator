import {Graphics, Sprite} from "pixi.js";
import {round} from "../utils.js";
import {getTexture} from "../assets.js";
import {Vector, isVectorable} from "../i.js";
import {Random} from "../i.js";
import {functionFactory} from "../i.js";
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "../i.js";
import {AI, AIUtils} from "../i.js";
import {FACTIONS, STANDARD_PPF, Thing, THING_TYPES, Z_INDICES} from "../i.js";
import {UPGRADER_POWER} from "../i.js";



const SPIN = 0.05;
const EXTENSION_TIME = 3;
const DOCK_EXTENDED = new Vector(220, 390);
const DOCK_A_ANGLE = - 110 * (Math.PI / 180);
const DOCK_B_ANGLE = - 70 * (Math.PI / 180);
const DOCK_A_RETRACTED = new Vector(60, 0).rotateBy(DOCK_A_ANGLE).add(DOCK_EXTENDED);
const DOCK_B_RETRACTED = new Vector(60, 0).rotateBy(DOCK_B_ANGLE).add(DOCK_EXTENDED);

export class SpiderdomeAI extends AI {
	constructor(thing, config) {
		super(thing);
	}

	move(dt, time) {
		// Move the station
		AIUtils.setPos(this.thing, this.thing.posGenerator(time), dt);
		AIUtils.rotateBy(this.thing, SPIN, dt);

		// Extend or retract the dock

		// Calculate the fraction by which the dock can extend or retract this
		// tick
		let tick_extension_fraction = dt / EXTENSION_TIME;
		// Figure out whether the dock should aim for being extended or
		// retracted
		let goalIsExtended = (this.thing.space.player.dockingTo === this.thing.id);
		// Adjust the extension fraction with no regard for bounds
		this.thing._extensionFraction += tick_extension_fraction
						* (goalIsExtended ? 1 : -1);
		// Clamp the extension fraction between 0 and 1
		this.thing._extensionFraction = Math.min(
			Math.max(this.thing._extensionFraction, 0), 1
		);
	}
}



class SpiderdomeAnimation {
	constructor(thing) {
		this.thing = thing;

		this._body = new Sprite(getTexture("images/stations/spiderdome.svg"));
		this._body.anchor.set(0.5, 0.5);
		this._body.position.set(0, 0);
		this._body.scale.set(1 / STANDARD_PPF);

		this._dockA = new Graphics();
		this._dockA
			.moveTo(0, 0)
			.lineTo(0.3, 0)
			.stroke({
				width: 0.02,
				color: 0x367799,
				cap: "round"
			});
		this._dockA.rotation = DOCK_A_ANGLE;
		this._dockB = new Graphics();
		this._dockB
			.moveTo(0, 0)
			.lineTo(0.3, 0)
			.stroke({
				width: 0.02,
				color: 0x367799,
				cap: "round"
			});
		this._dockB.rotation = DOCK_B_ANGLE;

		thing.display.addChild(
			this._dockA,
			this._dockB,
			this._body
		);
	}

	update(dt, time) {
		// I want the dock to only extend at the very end
		// Convert linear extensionFraction into non-linear extensionDistance.
		let extensionDistance = Math.max(4 * this.thing._extensionFraction - 3, 0);

		this._dockA.position.copyFrom(Vector.interpolate(
			this._texturePixelsToFlares(DOCK_A_RETRACTED),
			this._texturePixelsToFlares(DOCK_EXTENDED),
			extensionDistance
		));
		this._dockB.position.copyFrom(Vector.interpolate(
			this._texturePixelsToFlares(DOCK_B_RETRACTED),
			this._texturePixelsToFlares(DOCK_EXTENDED),
			extensionDistance
		));
	}

	// Convert from texture pixels on this._body to flares used by this.display
	// tp is a vector in texture pixels
	_texturePixelsToFlares(tp) {
		return new Vector(- this._body.width / 2, - this._body.height / 2)
			.add(Vector.scale(tp, this._body.scale.x));
	}
}



export class Spiderdome extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// rot of this spiderdome, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this spiderdome
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		// The fraction by which the dock is currently visually extended
		new OptionalProperty("extensionFraction", v =>
						(Number.isFinite(v) && v >= 0 && v <= 1))
	]);

	static SPIN = 0.1;

	// Create a new Spiderdome
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Spiderdome.CONFIG_STRUCTURE.test(config);

		config.identity = "SPIDERDOME";
		config.name = "Spiderdome";
		config.zIndex = Z_INDICES.STATION;
		config.len = 2;
		config.pos = new Vector(1000, 0);
		config.vel = new Vector();
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 591;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.NONE;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/station.svg";
		config.showBossBar = true;
		super(config);

		this.posGenerator = functionFactory("motion/circle/0/0/4/600/0");

		// Read by the player
		this.timeToDock = EXTENSION_TIME;
		this.mainTab = "WORKSHOP";

		// The fraction by which the dock is currently visually extended
		this._extensionFraction = config.extensionFraction ?? 0;

		this.ai = new SpiderdomeAI(this);
		this.animation = new SpiderdomeAnimation(this);
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// Return a {pos, vel, rot, spn} of the docking location for the player
	// This location changes over time.
	// Args:
	//   timeFromNow - number in seconds
	getDockLocation(timeFromNow) {
		if (timeFromNow === undefined) timeFromNow = 0;

		let getStationPosAt = (timeFromNow) => {
			let futureStationPos = this.posGenerator(this.space.time + timeFromNow);
			let futureStationRot = this.rot + timeFromNow * this.spn;
			return Vector.rotateBy(
				this.animation._texturePixelsToFlares(DOCK_EXTENDED),
				futureStationRot + Math.PI / 2
			).add(futureStationPos);
		};

		// Really small time delta to calculate vel
		const EPSILON = 0.0001;

		return {
			pos: getStationPosAt(timeFromNow),
			vel: Vector.diff(
				getStationPosAt(timeFromNow + EPSILON),
				getStationPosAt(timeFromNow)
			).scale(1 / EPSILON),
			rot: this.rot + timeFromNow * this.spn,
			spn: this.spn
		};
	}

	// Save this spiderdome into a config object
	// See the comment on Thing.save() for more details.
	// save() {
	// 	let config = super.save();

	// 	config.rot = round(this.rot);
	// 	if (this._s !== undefined) {
	// 		config.s = this._s;
	// 	} else {
	// 		config.name = this.name;
	// 		config.posGenerator = this.posGenerator.str;
	// 	}
	// 	if (this._extensionFraction !== 0) {
	// 		config.extensionFraction = round(this._extensionFraction);
	// 	}

	// 	return config;
	// }

	receiveBoop(otherThing) {
		if (otherThing.identity === "PLAYER") {
			this.space.canDock = "... whatever this is?";
		}
	}
}
