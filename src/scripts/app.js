import {loadAssets} from "./assets.js";
import {Vector} from "./i.js"; // ./vector.js
import {G, setMousePosition} from "./i.js";
import {keyboard} from "./i.js"; // ./keyboard.js
import {Menu} from "./i.js";

let menu;

async function init() {
	// TODO: Try to load in parallel like this:
	//
	// await Promise.all([
	// 	G.init({
	// 		resizeTo: window,
	// 		antialias: true,
	// 		background: 0x222222
	// 	}),
	// 	loadAssets()
	// ]);
	//
	// That currently breaks XML bitmap font loading for some reason.

	await G.init({
		resizeTo: window,
		antialias: true,
		background: 0x222222
	});
	document.body.appendChild(G.canvas);

	await loadAssets();

	menu = new Menu();
	G.stage.addChild(menu.display);

	G.stage.eventMode = "static";
	G.stage.addEventListener("globalpointermove",
		(e) => setMousePosition(new Vector(e.pageX, e.pageY))
	);

	// Workaround for the PixiJS ticker CPU usage bug
	G.ticker.maxFPS = 144;
	G.ticker.add(tick);
}

function tick() {
	let screenSize = new Vector(window.innerWidth, window.innerHeight);
	G.renderer.resize(screenSize.x, screenSize.y);
	menu.update(G.ticker.deltaMS / 1000, screenSize);
	keyboard.clearStuff();
}

init();
