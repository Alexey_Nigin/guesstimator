// Currently supported alignments: "UP_LEFT", "UP_MIDDLE", "LEFT_MIDDLE", "RIGHT_MIDDLE"

import {Container, Graphics, Text} from "pixi.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {UIUtils} from "../i.js"; // ./utils.js

export class Tooltip {
	constructor(tooltipper, anchor, text, alignment) {
		this.tooltipper = tooltipper;

		this.text = text;
		this.alignment = alignment;

		this._isHovering = false;
		this._timeHovering = 0;

		// Add event listeners
		this._anchor = anchor;
		anchor.eventMode = "static";
		anchor.addEventListener("pointerover", this._recordHoverStart.bind(this));
		anchor.addEventListener("pointerout", this._recordHoverEnd.bind(this));
	}

	// Arguments are position and size of the anchor
	update(dt, position, size) {
		// XXX: This code won't show the tooltip if the anchor becomes visible and the
		// mouse is already over it - but at least it will remove the tooltip if the
		// object becomes invisible
		if (!this._anchor.visible) this._isHovering = false;
		if (this._isHovering) {
			this._timeHovering += dt;
		} else {
			this._timeHovering = 0;
		}

		if (this._timeHovering >= 0.5) {
			let originPosition;
			switch (this.alignment) {
				case "DOWN_MIDDLE":
					originPosition = new Vector(
						position.x + size.x / 2,
						position.y + size.y
					)
					break;
				case "UP_LEFT":
					originPosition = new Vector(
						position.x,
						position.y
					)
					break;
				case "UP_MIDDLE":
					originPosition = new Vector(
						position.x + size.x / 2,
						position.y
					)
					break;
				case "LEFT_MIDDLE":
					originPosition = new Vector(
						position.x,
						position.y + size.y / 2
					)
					break;
				case "RIGHT_MIDDLE":
					originPosition = new Vector(
						position.x + size.x,
						position.y + size.y / 2
					)
					break;
				default:
					logger.error("tooltipper.js", "unknown alignment in Tooltip");
					return;
			}
			this.tooltipper.requestTooltip(
				this.text,
				0.2,
				originPosition,
				this.alignment
			);
		}
	}

	_recordHoverStart() {
		this._isHovering = true;
	}

	_recordHoverEnd() {
		this._isHovering = false;
	}
}



export class Tooltipper {
	constructor(game) {
		this.game = game;

		this.display = new Container();
		this._background = new Graphics();
		this._text = new Text(
			"",
			UIUtils.getFont(0xffffff, {align: "left", wordWrap: true})
		);
		this.display.addChild(this._background, this._text);

		this._requestedTooltip = {
			isRequested: false,
			text: "",
			relativeWidth: 0,
			origin: new Vector(),
			alignment: null
		};
	}

	update(dt, screenSize) {
		if (!this._requestedTooltip.isRequested) {
			this.display.visible = false;
			return;
		}

		this.display.visible = true;

		let rulerLength = UIUtils.getRulerLength(screenSize, false);
		let width = Math.round(this._requestedTooltip.relativeWidth * rulerLength);
		let offset = Math.round(0.015 * rulerLength);
		let padding = Math.round(0.01 * rulerLength);

		this._text.text = this._requestedTooltip.text;
		this._text.style.fontSize = 0.015 * rulerLength;
		this._text.style.wordWrapWidth = width - 2 * padding;

		let size = new Vector(
			this._text.width + 2 * padding,
			this._text.height + 2 * padding
		).round();
		let position;
		switch (this._requestedTooltip.alignment) {
			case "DOWN_MIDDLE":
				position = new Vector(
					this._requestedTooltip.origin.x - size.x / 2,
					this._requestedTooltip.origin.y + offset
				).round();
				break;
			case "UP_LEFT":
				position = new Vector(
					this._requestedTooltip.origin.x,
					this._requestedTooltip.origin.y - size.y - offset
				).round();
				break;
			case "UP_MIDDLE":
				position = new Vector(
					this._requestedTooltip.origin.x - size.x / 2,
					this._requestedTooltip.origin.y - size.y - offset
				).round();
				break;
			case "LEFT_MIDDLE":
				position = new Vector(
					this._requestedTooltip.origin.x - size.x - offset,
					this._requestedTooltip.origin.y - size.y / 2
				).round();
				break;
			case "RIGHT_MIDDLE":
				position = new Vector(
					this._requestedTooltip.origin.x + offset,
					this._requestedTooltip.origin.y - size.y / 2
				).round();
				break;
			default:
				logger.error("tooltipper.js", "unknown alignment");
				return;
		}

		this._text.position.set(
			position.x + padding,
			position.y + padding
		);
		this._background
			.clear()
			.rect(
				position.x, position.y,
				size.x, size.y
			)
			.fill(0x000000)
			.stroke({
				width: 1,
				color: 0xffffff,
				join: "round",
				alignment: 0
			});

		this._requestedTooltip.isRequested = false;
	}

	requestTooltip(text, relativeWidth, origin, alignment) {
		if (this._requestedTooltip.isRequested) {
			logger.warning("tooltipper.js", "multiple tooltips requested");
			return;
		}
		this._requestedTooltip.isRequested = true;
		this._requestedTooltip.text = text;
		this._requestedTooltip.relativeWidth = relativeWidth;
		this._requestedTooltip.origin.copyFrom(origin);
		this._requestedTooltip.alignment = alignment;
	}
}
