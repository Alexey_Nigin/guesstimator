import {Container, Graphics, Sprite} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../logger.js";
import {THING_TYPES} from "../i.js";
import {Clicker} from "../i.js";

export class WorldSight {
	// Args:
	//   <obvious>
	//   onIconClick - event listener for icon click, args: (id, isPlayer)
	//                 Omit if !trackIconClicks.
	constructor(game, colorBase, flaresVertical, baseIconScale, playerLocation,
		trackIconClicks, onIconClick
	) {
		this.game = game;
		this._colorBase = colorBase ?? 0x000000;
		this._flaresVertical = flaresVertical ?? 100;
		this._baseIconScale = baseIconScale ?? 0.001;
		this._playerLocation = playerLocation ?? new Vector(0.5, 0.5);
		this._trackIconClicks = trackIconClicks ?? false;
		this._onIconClick = onIconClick ?? (() => {});

		this.display = new Container();

		this._areas = new Graphics();
		this._lines = new Graphics();
		this._icons = new Container();
		this.display.addChild(this._areas, this._lines, this._icons);
	}

	update(visible, size) {
		if (visible) {
			if (this._trackIconClicks) {
				for (let icon of this._icons.children) {
					if (icon.clicker.wasClicked()) {
						this._onIconClick(icon.id, icon.isPlayer);
					}
				}
			}

			// Scale: pixels in the worldsight window per flare in space
			let gpsPixelsPerFlare = size.y / this._flaresVertical;
			// pos corresponding to the top-left of the worldsight window
			let posTopLeft = Vector.diff(
				this.game.space.player.pos,
				new Vector(
					size.x * this._playerLocation.x / gpsPixelsPerFlare,
					size.y * this._playerLocation.y / gpsPixelsPerFlare
				)
			);
			// A function to convert to GPS pixels
			function posToGpsPixels(pos) {
				return Vector.diff(pos, posTopLeft).scale(gpsPixelsPerFlare);
			}
			this._refreshAreas(size, posToGpsPixels, gpsPixelsPerFlare);
			this._refreshLines(size, posToGpsPixels);
			this._refreshIcons(size, posToGpsPixels);
		}

		if (this._trackIconClicks) {
			for (let icon of this._icons.children) {
				icon.clicker.clearStuff();
			}
		}
	}

	// Refresh areas
	// Args:
	//   size              - a vector in pixels representing the size of the display
	//   posToGpsPixels    - a function mapping a vector to a vector
	//   gpsPixelsPerFlare - number
	_refreshAreas(size, posToGpsPixels, gpsPixelsPerFlare) {
		let areaInfo = this.game.space.density.getAreaInfo(
			this.game.space.time
		);

		this._areas
			.clear()
			.rect(0, 0, size.x, size.y)
			.fill(this._colorBase);
		for (let area of areaInfo) {
			switch (area[0]) {
				case "CIRCLE": {
					let color = area[1];
					let centerPos = area[2];
					let radiusFlares = area[3];

					let centerPixels = posToGpsPixels(centerPos);
					let radiusPixels = radiusFlares * gpsPixelsPerFlare;

					this._areas
						.circle(centerPixels.x, centerPixels.y, radiusPixels)
						.fill((color === "EMPTY") ? this._colorBase : color);
					break;
				}
				case "ARC": {
					let color = area[1];
					let centerPos = area[2];
					let radiusFlares = area[3];
					let startAngle = area[4];
					let endAngle = area[5];

					let centerPixels = posToGpsPixels(centerPos);
					let radiusPixels = radiusFlares * gpsPixelsPerFlare;
					this._areas
						// Not sure why this moveTo() is needed, but arcs don't render
						// correctly otherwise
						.moveTo(centerPixels.x, centerPixels.y)
						.arc(
							centerPixels.x, centerPixels.y,
							radiusPixels,
							startAngle, endAngle
						)
						.fill((color === "EMPTY") ? this._colorBase : color);
					break;
				}
				case "LINE": {
					let color = area[1];
					let widthFlares = area[2];
					let startPointFlares = area[3];
					let endPointFlares = area[4];

					let widthPixels = widthFlares * gpsPixelsPerFlare;
					let startPointPixels = posToGpsPixels(startPointFlares);
					let endPointPixels = posToGpsPixels(endPointFlares);
					this._areas
						.moveTo(startPointPixels.x, startPointPixels.y)
						.lineTo(endPointPixels.x, endPointPixels.y)
						.stroke({
							color: (color === "EMPTY") ? this._colorBase : color,
							width: widthPixels
						});
					break;
				}
				default:
					logger.error("worldsight.js", `unknown area shape: ${area[0]}`);
					break;
			}
		}
	}

	// Refresh lines corresponding to arrows
	// Args:
	//   size           - a vector in pixels representing the size of the display
	//   posToGpsPixels - a function mapping a vector to a vector
	_refreshLines(size, posToGpsPixels) {
		let arrows = this.game.unicenter.gps.arrows;

		let playerGpsPixels = posToGpsPixels(this.game.space.player.pos);

		this._lines.clear();

		arrows.forEach(arrow => {
			let target = this.game.space.requestThingById(arrow);
			if (target === null) return;
			let targetGpsPixels = posToGpsPixels(target.pos);
			this._lines
				.moveTo(playerGpsPixels.x, playerGpsPixels.y)
				.lineTo(targetGpsPixels.x, targetGpsPixels.y)
				.stroke({
					width: this.game.unicenter.requestStroke(0x000000, "NARROW").width,
					color: (target.TYPE === THING_TYPES.STATION) ? 0x00ff00 : 0xffffff,
					cap: "round"
				});
		});
	}

	// Refresh the thing icons
	// Args:
	//   size           - a vector in pixels representing the size of the display
	//   posToGpsPixels - a function mapping a vector to a vector
	_refreshIcons(size, posToGpsPixels) {
		let gpsIconsData = this.game.space.things
			.filter(
				thing => (thing.gpsIcon !== null)
			)
			.map(thing => ({
				isPlayer: thing === this.game.space.player,
				id: thing.id,
				pos: thing.pos,
				rot: thing.rot,
				gpsIcon: thing.gpsIcon,
				rotateGpsIcon: thing.rotateGpsIcon,
				zIndex: thing.display.zIndex
			}))
			.sort((thingA, thingB) => thingA.zIndex - thingB.zIndex);

		// Remove extra icons
		while (this._icons.children.length > gpsIconsData.length) {
			this._icons.removeChildAt(0);
		}
		// Add icons if necessary
		while (this._icons.children.length < gpsIconsData.length) {
			let iconToAdd = new Sprite();
			iconToAdd.anchor.set(0.5, 0.5);
			// TODO: Remove this clicker when icon is removed
			if (this._trackIconClicks) iconToAdd.clicker = new Clicker(iconToAdd);
			this._icons.addChild(iconToAdd);
		}

		for (let i = 0; i < gpsIconsData.length; ++i) {
			let icon = this._icons.children[i];
			let iconPos = posToGpsPixels(gpsIconsData[i].pos);
			// TODO: Hide icons that are way outside the window
			icon.texture = getTexture(gpsIconsData[i].gpsIcon);
			icon.position.copyFrom(iconPos);
			icon.rotation = gpsIconsData[i].rotateGpsIcon
				? gpsIconsData[i].rot + Math.PI / 2
				: 0;

			let iconScale = this._baseIconScale * size.x;
			if (this._trackIconClicks) {
				if (icon.clicker.isHovering()) iconScale *= 1.1;
			}
			icon.scale.set(iconScale);

			icon.id = gpsIconsData[i].id;
			icon.isPlayer = gpsIconsData[i].isPlayer;
		}
	}
}
