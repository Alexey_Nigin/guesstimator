import {BitmapText, Container, Sprite, TextStyle} from "pixi.js";
import {logger} from "../i.js";

// Utilities for all UI elements - that is, for everything on the screen that is not part
// of space
export const UIUtils = {
	// Screen aspect ratio at which UI elements are the biggest
	PERFECT_FIT_ASPECT_RATIO: 4 / 3,

	FONT_FAMILY: "Verdana, Geneva, sans-serif",

	// Get ruler length for a given screen size.
	// This function combines width and height of the screen into a single linear value
	// UI code can use for scaling. The size of all UI elements should be proportional to
	// the ruler length.
	// Args:
	//   screenSize - a vector in pixels
	//   doRounding - boolean. If true, round ruler length to the nearest integer.
	//                Defaults to true, set to false if you plan to round values later
	//                down the line.
	// Returns:
	//   Ruler length, in pixels
	getRulerLength(screenSize, doRounding) {
		// TODO: Write a unit test for this
		let rulerLength = Math.min(
			screenSize.x,
			UIUtils.PERFECT_FIT_ASPECT_RATIO * screenSize.y
		);
		if (doRounding !== false) rulerLength = Math.round(rulerLength);
		return rulerLength;
	},

	// Convert an HSV color to a Pixi-compatible RGB color
	// From: https://stackoverflow.com/a/17243070
	//
	// Args:
	//   h - hue, 0..360
	//   s - saturation, 0..100
	//   v - value, 0..100
	hsv(h, s, v) {
		// Convert all inputs to range 0..1
		h = h / 360;
		s = s / 100;
		v = v / 100;

		// Magic math from StackOverflow
		let i = Math.floor(h * 6);
		let f = h * 6 - i;
		let p = v * (1 - s);
		let q = v * (1 - f * s);
		let t = v * (1 - (1 - f) * s);

		// More magic math
		let r, g, b;
		switch (i % 6) {
			case 0: r = v, g = t, b = p; break;
			case 1: r = q, g = v, b = p; break;
			case 2: r = p, g = v, b = t; break;
			case 3: r = p, g = q, b = v; break;
			case 4: r = t, g = p, b = v; break;
			case 5: r = v, g = p, b = q; break;
		}

		// Convert r,g,b from floats in range 0..1 inclusive to integers in range 0..255
		// inclusive
		r = Math.min(Math.floor(r * 256), 255);
		g = Math.min(Math.floor(g * 256), 255);
		b = Math.min(Math.floor(b * 256), 255);

		// Combine into a single hex number (0xRRGGBB)
		return r * 0x10000 + g * 0x100 + b;
	},

	getFont(color, additionalProperties) {
		return new TextStyle({
			fontFamily: UIUtils.FONT_FAMILY,
			fill: color ?? 0xffffff,
			...(additionalProperties ?? {})
		});
	},

	getBitmapText(text, color, additionalProperties) {
		let bitmapText = new BitmapText({
			text: text,
			style: {
				fontName: "DejaVuSans",
				...(additionalProperties ?? {})
			},
			roundPixels: true
		});
		bitmapText.tint = color;
		return bitmapText;
	},

	// Remove UI elements beyond maxCount
	// Useful for dealing with variable-count UI elements.
	// Args:
	//   elements - an array where each element has a display property
	//   maxCount - integer
	cullElements(elements, maxCount) {
		while (elements.length > maxCount) {
			let victim = elements.pop();
			victim.display.parent.removeChild(victim.display);
			if (victim.display instanceof Sprite) {
				// Don't destroy the texture or textureSource
				victim.display.destroy(false);
			} else {
				victim.display.destroy(true);
			}
		}
	}
};



// A class for adding mouse events to a PIXI object
// Usage:
//     let button = new Pixi.<Something>;
//     button.clicker = new Clicker(button);
//     if (button.clicker.wasClicked()) <...>
//     // At the end of the tick
//     button.clicker.clearStuff();
export class Clicker {
	// Create a new clicker
	// anchor is a PIXI object to track events on
	constructor(anchor) {
		this._isHovering = false;
		this._wasClicked = false;

		// Add event listeners
		this._anchor = anchor;
		anchor.eventMode = "static";
		anchor.addEventListener("pointerup", this._recordClick.bind(this));
		anchor.addEventListener("pointerover", this._recordHoverStart.bind(this));
		anchor.addEventListener("pointerout", this._recordHoverEnd.bind(this));
	}

	// Return true iff the clicker was clicked in the last tick
	wasClicked() {
		return this._wasClicked;
	}

	// Return true iff the clicker is currently hovered over
	// Returns false if the anchor is hidden.
	isHovering() {
		// Previously:
		//
		// If the visibility check wasn't here, then the method would return
		// the last seen value while the anchor was hidden, and readjust when
		// the anchor was displayed again. That wouldn't be too bad, but I felt
		// like explicitly returning false would help with consistency.
		//
		// return this._isHovering && this._anchor.worldVisible;
		//
		// I had to remove this check because worldVisible isn't a thing in PixiJS v8.
		// TODO: Figure out a replacement solution.
		// To try: https://github.com/pixijs/pixijs/discussions/10105
		return this._isHovering;
	}

	// Clear the clicked status of the clicker
	// Call this at the end of every tick.
	clearStuff() {
		this._wasClicked = false;
	}

	// Event listeners

	_recordClick() {
		this._wasClicked = true;
	}

	_recordHoverStart() {
		this._isHovering = true;
	}

	_recordHoverEnd() {
		this._isHovering = false;
	}
}



// Container that can efficiently change the number of its similar elements
// TODO: Is this solving the correct problem?
export class DynamicContainer {
	// elementFactory should produce an object with a PIXI element as `display` property
	constructor(elementFactory) {
		this.display = new Container();

		this._elementFactory = elementFactory;
		this._elements = [];
		// Invariant: this._size <= this._elements.length
		this._size = 0;
	}

	// Remove unused elements
	cleanup() {
		for (let i = this._elements.length; i >= this._size; --i) {
			let victim = this._elements[i];
			this.display.removeChild(victim.display);
			if (victim.display instanceof Sprite) {
				// Don't destroy the texture or textureSource
				victim.display.destroy(false);
			} else {
				victim.display.destroy(true);
			}
		}
		this._elements.splice(this._size);
	}

	getSize() {
		return this._size;
	}

	setSize(size) {
		if (size < this._size) {
			for (let i = size; i < this._size; ++i) {
				this._elements[i].display.visible = false;
			}
		} else {
			for (let i = this._size; i < size; ++i) {
				if (this._elements.length <= i) {
					let baby = this._elementFactory();
					this._elements.push(baby);
					this.display.addChild(baby.display);
				}
				this._elements[i].display.visible = true;
			}
		}

		this._size = size;
	}

	getElement(i) {
		if (i < 0 || i >= this._size) {
			logger.error("ui/utils.js", `invalid getElement index ${i}`);
			return;
		}

		return this._elements[i];
	}
}
