import {Vector} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
import {angleDiff, sq} from "./i.js"; // ./dump.js
import {functionFactory} from "./i.js"; // ./code-factories.js
import {ConfigStructure, RequiredProperty} from "./i.js"; // ./validators.js

export class AIUtils {
	// Translate thing
	// acc is a vector representing acceleration. friction is a non-negative
	// number. dt is the time since last update, in seconds. The equilibrium
	// speed of the thing after a long time is acc.mag() / friction.
	static translate(thing, acc, friction, dt) {
		if (friction === 0) {
			// Use the standard Physics 140 formulas for constant acceleration
			thing.pos
				.add(Vector.scale(thing.vel, dt))
				.add(Vector.scale(acc, dt * dt / 2));
			thing.vel.add(Vector.scale(acc, dt));
		} else {
			// Use smart math
			// The formula for updating velocity comes from solving the
			// differential equation
			//     vel' = acc - friction * vel,
			// where the initial condition is the previous velocity. The formula
			// for updating position comes from integrating the formula for
			// velocity from 0 to dt. In case I ever want to repeat these
			// calculations, I can make my life simpler by noticing that x and y
			// components are independent, so I can treat all variables as
			// scalars throughout the derivation and only convert them to
			// vectors at the very end.
			let slowdown = Math.exp(- friction * dt);
			thing.pos
				.add(Vector.scale(acc, (dt / friction)))
				.add(Vector.scale(acc, 1 / friction).sub(thing.vel)
					.scale((slowdown - 1) / friction)
				);
			thing.vel
				.scale(slowdown)
				.add(Vector.scale(acc, (1 - slowdown) / friction));
		}
	}

	// Rotate thing at a specific angular speed
	// omega is the angular speed in radians/second. dt is the time since last
	// update, in seconds. In the future, I might update this function to handle
	// angular acceleration as well. friction is a non-negative number, or undefined.
	// TODO: Rearrange params to match translate().
	static rotateBy(thing, omega, dt, friction) {
		if (friction === undefined || friction === 0) {
			thing.spn = omega;
			thing.rot += thing.spn * dt;
		} else {
			// Same math as in translate()
			let acc = omega * friction;
			let slowdown = Math.exp(- friction * dt);
			thing.rot += acc * dt / friction
							+ (acc / friction - thing.spn) * ((slowdown - 1) / friction);
			thing.spn *= slowdown;
			thing.spn += acc * (1 - slowdown) / friction;
		}
	}

	// Rotate thing towards a specific direction
	// dir is the desired direction in radians. speed is the speed of rotation.
	// dt is time since last update, in seconds. Unlike the other translation /
	// rotation routines, this function isn't perfect and produces slightly
	// different speeds based on the framerate, so I might want to improve it
	// later using differential equations.
	static rotateTo(thing, dir, speed, dt) {
		// Formula from https://bit.ly/2rLCGhk
		thing.spn = Math.sin(angleDiff(dir, thing.rot) / 2) * speed;
		thing.rot += thing.spn * dt;
	}

	// Set a thing's pos to a specific value, and update vel accordingly
	// Args:
	//   thing   - reference to the thing to be modified
	//   nextPos - a vector-like object with the new pos
	//   dt      - time since last tick, in seconds
	static setPos(thing, nextPos, dt) {
		let oldPos = new Vector(thing.pos);
		thing.pos.copyFrom(nextPos);
		thing.vel.copyFrom(Vector.diff(nextPos, oldPos).scale(1 / dt));
	}

	// Set a thing's rot to a specific value, and update spn accordingly
	// Args:
	//   thing   - reference to the thing to be modified
	//   nextRot - a number in radians
	//   dt      - time since last tick, in seconds
	static setRot(thing, nextRot, dt) {
		let oldRot = thing.rot;
		thing.rot = nextRot;
		thing.spn = (nextRot - oldRot) / dt;
	}

	// Get an acceleration for a thing to repel from entry
	// thing is a thing (duh), entry is an object with pos (usually from a tactical
	// report). maxAcc is the acceleration if the entry is right next to the thing,
	// desiredDistance is the distance at which acceleration drops to 0.
	static getRepelAcc(thing, entry, maxAcc, desiredDistance) {
		let vectorToEntry = Vector.diff(entry.pos, thing.pos);
		let distToEntry = vectorToEntry.mag();
		if (distToEntry > desiredDistance) return new Vector();
		let repellingStrength = maxAcc
						* sq((desiredDistance - distToEntry) / desiredDistance);
		return vectorToEntry.norm(- repellingStrength);
	}

	// Get an acceleration for a thing to attract to entry
	// thing is a thing (duh), entry is an object with pos (usually from a tactical
	// report). maxAcc is the acceleration if the entry is very far from the thing,
	// desiredDistance is the distance at which acceleration drops to 0.
	static getAttractAcc(thing, entry, maxAcc, desiredDistance) {
		let vectorToEntry = Vector.diff(entry.pos, thing.pos);
		let distToEntry = vectorToEntry.mag();
		if (distToEntry < desiredDistance) return new Vector();
		let attractionStrength = maxAcc * Math.min(
			(distToEntry - desiredDistance) / desiredDistance,
			1
		);
		return vectorToEntry.norm(attractionStrength);
	}
}



export class AI {
	constructor(thing) {
		this.thing = thing;
	}

	move(dt, time) {
		logger.error("ai.js", "must override move()");
	}
}



export class StraightLineAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([]);

	constructor(thing, config) {
		StraightLineAI.CONFIG_STRUCTURE.test(config);

		super(thing);
	}

	move(dt, time) {
		AIUtils.rotateBy(this.thing, this.thing.spn, dt);
		AIUtils.translate(this.thing, new Vector(), 0, dt);
	}
}



export class PathFollowAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// String representation of a function that takes in the in-game time
		// and returns pos at that time
		new RequiredProperty("posGenerator", v => (typeof v === "string")),
		// Spin of the thing, in radians / second
		new RequiredProperty("spn", v => Number.isFinite(v))
	]);

	constructor(thing, config) {
		PathFollowAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._posGenerator = functionFactory(config.posGenerator);

		this.thing.spn = config.spn;
	}

	move(dt, time) {
		AIUtils.rotateBy(this.thing, this.thing.spn, dt);
		AIUtils.setPos(this.thing, this._posGenerator(time), dt);
	}
}
