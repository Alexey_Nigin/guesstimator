import {Polygon} from "detect-collisions";
import {Vector} from "../i.js";
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js";
import {AI, AIUtils} from "../i.js";
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js";



const BOOMDASH_LIFESPAN = 0.3;



function boomdashRadius(age) {
	return (1 - (age / BOOMDASH_LIFESPAN - 1) ** 2) * (2.5 / 2);
}

function getPoint(radius, alpha) {
	let v = new Vector(2 * radius, 0).rotateBy(alpha).sub(new Vector(radius, 0));
	return {x: v.x, y: v.y};
}



export class BoomdashAI extends AI {
	constructor(thing, config) {
		super(thing);
	}

	move(dt, time) {
		this.thing.age += dt;
		if (this.thing.age > BOOMDASH_LIFESPAN) {
			this.thing.prepareToDie();
			return;
		}

		let isDirectional = (this.thing.direction !== undefined);

		let radius = boomdashRadius(this.thing.age);
		if (isDirectional) {
			this.thing.len = 2 * radius;
			this.thing.hitbox.setPoints([
				// This is clockwise. For some reason it doesn't work if I list the points
				// anticlosckwise, I didn't dig into it
				{x: - radius, y: 0},
				getPoint(radius, - Math.PI / 6),
				getPoint(radius, - Math.PI / 12),
				getPoint(radius, 0),
				getPoint(radius, Math.PI / 12),
				getPoint(radius, Math.PI / 6)
			]);
			this.thing.hitbox.markAsDirty();
			//console.log(this.thing.hitbox);
		} else {
			this.thing.len = 2 * radius;
			this.thing.hitbox.r = radius;
			this.thing.hitbox.markAsDirty();
		}

		let parent = this.thing.space.requestThingById(this.thing.parent);
		if (parent === null) {
			this.thing.prepareToDie();
			return;
		}
		this.thing.center.copyFrom(parent.pos);

		if (isDirectional) {
			let rot = parent.rot + this.thing.direction * Math.PI / 4;
			AIUtils.setPos(this.thing,
				new Vector(radius, 0).rotateBy(rot).add(parent.pos), dt
			);
			AIUtils.setRot(this.thing, rot, dt);
		} else {
			AIUtils.setPos(this.thing, parent.pos, dt);
		}
	}
}



export class Boomdash extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Id of the thing that fired the boomdash
		new RequiredProperty("parent", v => (Number.isFinite(v))),
		// Direction of the boomdash, in multiples of 45 degrees
		// Defaults to non-directional boomdash if omitted.
		new OptionalProperty("direction",
			v => (Number.isFinite(v) && v >= 0 && v <= 7)
		)
	]);

	// Create a boomdash
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Boomdash.CONFIG_STRUCTURE.test(config);

		let isDirectional = (config.direction !== undefined);

		config.texture = isDirectional
			? "images/boomdash-direction.svg"
			: "images/boomdash.svg";
		config.zIndex = Z_INDICES.PLAYER_CHILD; // TODO: Probably move lower?
		config.len = 1.5;
		config.pos = new Vector();
		config.vel = new Vector();
		config.rot = 0;
		config.type = THING_TYPES.ENERGY;
		config.faction = FACTIONS.PLAYER;
		config.canDespawn = false;
		config.maxHealth = 10;
		if (isDirectional) {
				config.hitbox = new Polygon({x: 0, y: 0}, [
				{x: 0, y: 0},
				{x: 1, y: 0}
			]);
		}
		super(config);

		this.parent = config.parent;
		if (isDirectional) this.direction = config.direction;
		this.age = 0;
		// For repelling asteroids
		this.center = new Vector();

		this.ai = new BoomdashAI(this, {});
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {}

	// Save this boomdash into a config object
	// TODO
	// save() {}

}
