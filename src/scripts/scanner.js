// TODO: Update for absolute pos

// Usage:
//     class ScaryEnemy extends CoolParent {
//         constructor(scanner) {
//             ...
//             this.healthBar = scanner.addBar(this.pos, this.radius + 10,
//                             this.health, this.maxHealth);
//             ...
//         }
//         ...
//         defendOrRegen() {
//             ...
//             this.healthBar.val = updatedHealth;
//             ...
//         }
//         ...
//         fade() {
//             ...
//             this.healthBar.selfDestruct();
//             ...
//         }
//         ...
//     }
//     ...
//     class Space extends PIXI.Container {
//         constructor() {
//             ...
//             this.scanner = new Scanner();
//             this.addChild(this.scanner);
//             ...
//         }
//         ...
//         update() {
//             ...
//             this.scanner.redraw();
//             ...
//         }
//         ...
//         spawnScaryEnemy() {
//             ...
//             let scaryEnemy = new ScaryEnemy(this.scanner);
//             ...
//         }
//         ...
//     }

import {Container, Graphics, Sprite, Text} from "pixi.js";
import {getTexture} from "./assets.js";
import {Vector} from "./i.js"; // ./vector.js
import {modulo} from "./i.js"; // ./dump.js
import {DynamicContainer, UIUtils} from "./i.js"; // ./ui/utils.js
import {THING_TYPES} from "./i.js"; // ./thing.js

// TODO: Integrate this with scanner
export class Grid extends Graphics {
	static LINE_STYLE = {
		width: 1,
		color: 0xdddddd
	};

	// Create a grid
	// Create only one grid per space.
	constructor() {
		super();
	}

	// Redraw the grid to match offset
	// screenSize is a vector containing the screen size, in pixels.
	// pixelsPerFlare is a real number. offset is the position of the origin.
	redraw(screenSize, pixelsPerFlare, offset) {
		this.clear();

		let xStart = modulo(offset.x, pixelsPerFlare);
		for (let x = xStart; x < screenSize.x; x += pixelsPerFlare) {
			this
				.moveTo(x, 0)
				.lineTo(x, screenSize.y);
		}
		let yStart = modulo(offset.y, pixelsPerFlare);
		for (let y = yStart; y < screenSize.y; y += pixelsPerFlare) {
			this
				.moveTo(0, y)
				.lineTo(screenSize.x, y);
		}

		this.stroke(Grid.LINE_STYLE);
	}
}



const BAR_WIDTH = 30;
const BAR_HEIGHT = 3;
const BAR_COLOR_BACK = 0xff0000;
const BAR_COLOR_FRONT = 0x00ff00;

class Bar {
	// Create a new bar
	//
	// Args:
	//   scanner   - reference to the scanner
	//   parentPos - pass pos of a thing as parentPos, and the bar will store a reference
	//               to that pos and track the thing's movement.
	//   offset    - y-offset from parentPos
	//   val       - current value of the bar
	//   maxVal    - max value of the bar
	constructor(scanner, parentPos, offset, val, maxVal) {
		this.scanner = scanner;

		this.display = new Graphics();

		this._parentPos = parentPos;
		this._offset = offset;
		this.val = val;
		this._maxVal = maxVal;

		this.display.visible = false;
	}

	// Redraw the bar
	// This function should be called every time the position or value of the
	// bar changes. Probably just run it every tick to be safe.
	redraw() {
		let x = this._parentPos.x - BAR_WIDTH / 2;
		let y = this._parentPos.y + this._offset;
		this.display
			.clear()
			.rect(x, y, BAR_WIDTH, BAR_HEIGHT)
			.fill(BAR_COLOR_BACK)
			.rect(x, y, (this.val / this._maxVal) * BAR_WIDTH, BAR_HEIGHT)
			.fill(BAR_COLOR_FRONT);
	}

	selfDestruct() {
		this.display.parent.removeChild(this.display);
		this.display.destroy(true);
		// TODO: This is hacky
		this.scanner._bars.delete(this);
	}

	// There is no function to change val, just change it directly.
}

export class Scanner extends Container {
	// Create a scanner
	// space is a reference to space that this scanner is a part of.
	constructor(space) {
		super();

		// Save the reference to space
		this._space = space;

		this._bars = new Set();
		this._arrows = new DynamicContainer(() => {
			let arrow = new Sprite(getTexture("images/arrow.svg"));
			arrow.anchor.set(0.5, 1);

			return {
				display: arrow
			};
		});
		this.addChild(this._arrows.display);

		this.readyToDock = new Text({text: "", style: UIUtils.getFont(0xffffff)});
		this.addChild(this.readyToDock);
	}

	// Create a new bar and add it to the scanner
	// Pass pos of a thing as parentPos, and the bar will store a reference to
	// that position and track the thing's movement. Other params: offset is
	// y-offset from parentPos, val is the current value of the bar, maxVal is
	// the max value of the bar, duh. This function returns the bar added.
	addBar(parentPos, offset, val, maxVal) {
		let bar = new Bar(this, parentPos, offset, val, maxVal);
		this._bars.add(bar);
		this.addChild(bar.display);
		return bar;
	}

	// Redraw all bars
	// This function should be called every time the position or value of any
	// bar changes. Probably just run it every tick to be safe. screenSize is a
	// vector in pixels. pixelsPerFlare is a real number.
	redraw(screenSize, pixelsPerFlare) {
		let rulerLength = UIUtils.getRulerLength(screenSize);

		// Redraw bars
		this._bars.forEach(bar => bar.redraw());

		// Redraw arrows

		// Get list of target ids from the gps
		let gpsArrows = Array.from(this._space.game.unicenter.gps.arrows);

		// Correct the number of arrows
		this._arrows.setSize(gpsArrows.length);

		let player = this._space.player;

		// Put arrows in the correct spots
		for (let i = 0; i < gpsArrows.length; ++i) {
			let arrow = this._arrows.getElement(i).display;
			arrow.visible = true;

			arrow.scale.set(0.003 * pixelsPerFlare);

			let target = this._space.requestThingById(gpsArrows[i]);
			if (target === null) {
				arrow.visible = false;
				continue;
			}

			let angle = Vector.diff(target.pos, player.pos).dir();

			// Arrow sprite points up by default, correct for that
			arrow.rotation = angle + Math.PI / 2;

			// Distance from the center of the player to the back of the arrow
			let distanceToArrow = 0.55 * pixelsPerFlare;

			arrow.position.copyFrom(Vector.sum(
				player.display.position,
				new Vector(
					distanceToArrow * Math.cos(angle),
					distanceToArrow * Math.sin(angle)
				)
			));

			// Distance from the player to the target
			let distanceToTarget = Vector.diff(
				target.pos,
				player.pos
			).mag();

			if (target.TYPE === THING_TYPES.STATION) {
				arrow.visible = (distanceToTarget > 3);
				arrow.tint = 0x00ff00;
			} else {
				arrow.tint = (distanceToTarget < 10) ? 0xff0000 : 0xdddddd;
			}
		}

		// Redraw the ready-to-dock hint
		this.readyToDock.visible = this._space.canDock;
		if (this._space.canDock) {
			this.readyToDock.text = `Press (X) to dock to${this._space.canDock}`;
			this.readyToDock.style.fontSize = Math.round(0.02 * rulerLength);
			let readyToDockOrigin = new Vector(
				screenSize.x / 2 - this.readyToDock.width / 2,
				screenSize.y / 4
			).round();
			this.readyToDock.position.copyFrom(readyToDockOrigin);
		}
	}
}
