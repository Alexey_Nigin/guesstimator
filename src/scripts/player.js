// Define a class for the player spaceship

import {Sprite} from "pixi.js";
import {Polygon} from "detect-collisions";
import {createSmoothInterpolation, round} from "./utils.js";
import {getTexture} from "./assets.js";
import {Vector, isVectorable} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
import {angleDiff, lerp, MOUSE, sq} from "./i.js"; // ./dump.js
import {keyboard, Keyboard} from "./i.js"; // ./keyboard.js
import {ConfigStructure, OptionalProperty} from "./i.js"; // ./validators.js
import {AI, AIUtils} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, STANDARD_PPF, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {Nanobots} from "./i.js"; // ./nanobots.js
import {Blaster, BLASTER_VARIETIES} from "./i.js";
import {Boomdash} from "./i.js";
import {Dart} from "./i.js";
import {ACHIEVEMENTS} from "./i.js";



// Cooldown between boomdashes, in seconds
const BOOMDASH_COOLDOWN = 0.45;
// Acceleration from boomdash, f/s^2
const BOOMDASH_ACC_STRENGTH = 50;
// Duration of boomdash acceleration, s
const BOOMDASH_ACC_DURATION = 0.15;
// Boomdash energy cost
const BOOMDASH_COST = 3;



// TODO: I just moved move() (no pun intended) from the Player class... A lot more could
// be moved to the AI from the Player class, and refactored in smart ways.
export class PlayerAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Healing cooldown, in seconds (defaults to 0 if omitted)
		new OptionalProperty("healingCooldown",
			v => (Number.isFinite(v) && v >= 0 && v <= PlayerAI.HEALING_COOLDOWN)
		)
	]);

	// Cooldown between successive heals, in seconds
	static HEALING_COOLDOWN = 5;
	// Number of healing swarms to spawn per heal
	static HEALING_SWARMS = 10;

	constructor(thing, config) {
		PlayerAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this.healingCooldown = config.healingCooldown ?? 0;

		// Set by cossette
		this.tractorBeamAcc = new Vector();
	}

	// Move the player based on input from mouse and keyboard
	// Args:
	//   dt   - time since last update, in seconds
	//   time - total in-game time, in seconds
	move(dt, time) {
		// Don't move if the player is in limbo
		if (this.thing.state === Thing.STATES.LIMBO) return;

		if (this.thing.pos.mag() > 400) {
			this.thing.space.game.unicenter.ship.achievements.recordAchievement(
				ACHIEVEMENTS.ESCAPE
			);
		}

		let nextMaxHealth = getMaxHealth(this.thing.space);
		if (this.thing.maxHealth != nextMaxHealth) {
			this.thing.health = (this.thing.health / this.thing.maxHealth) * nextMaxHealth;
			this.thing.maxHealth = nextMaxHealth;
			// TODO: Find a cleaner way to update the health bar
			this.thing.healthBar._maxVal = this.thing.maxHealth;
			this.thing.healthBar.val = this.thing.health;
		}

		let actions = keyboard.getActions("PLAYER");

		// Begin docking
		if (actions.has("DOCK")) this.thing.attemptToDock();

		// Find the station that the player is docking to
		let station = null;
		if (this.thing.dockingTo !== null) {
			station = this.thing.space.requestThingById(this.thing.dockingTo);
			// If the station got destroyed, undock
			// TODO: If the station is in limbo, also undock.
			if (station === null) this.thing.undock();
		}

		if (this.thing.dockingTo === null) {
			this._moveFreely(dt, actions);
		} else {
			this._moveToDock(dt, time, station);
		}

		// Energy & weaponry

		let firing = this.thing.firingMouse || actions.has("FIRE");
		if (firing && this.thing.cooldown === 0
						&& this._consumeEnergy(Player.FIRING_COST)) {
			this.thing.space.requestSpawn(Blaster, {
				variety: BLASTER_VARIETIES.PLAYER,
				pos: this.thing.pos,
				rot: this.thing.rot,
				parentVel: this.thing.vel
			});
			this.thing.space.game.unicenter.ship.reportStat("BLASTER_BOLTS_FIRED", 1);
			this.thing.cooldown = Player.COOLDOWN;
		}
		let boomdashForged = this.thing.space.game.unicenter.forge.boomdashForged;
		if (boomdashForged
						&& actions.has("BOOMDASH") && this.thing.boomdashCooldown === 0
						&& this._consumeEnergy(BOOMDASH_COST)) {
			// Determine boomdash direction
			let direction = null;
			let forward = actions.has("THRUST") && !actions.has("BRAKE");
			let backward = actions.has("BRAKE") && !actions.has("THRUST");
			let left = actions.has("SLIDE_LEFT") && !actions.has("SLIDE_RIGHT");
			let right = actions.has("SLIDE_RIGHT") && !actions.has("SLIDE_LEFT");
			if (forward && right) {
				direction = 5;
			} else if (backward && right) {
				direction = 7;
			} else if (backward && left) {
				direction = 1;
			} else if (forward && left) {
				direction = 3;
			} else if (forward) {
				direction = 4;
			} else if (right) {
				direction = 6;
			} else if (backward) {
				direction = 0;
			} else if (left) {
				direction = 2;
			}

			let boomdashConfig = {
				parent: this.thing.id
			};
			if (direction !== null) boomdashConfig.direction = direction;
			this.thing.space.requestSpawn(Boomdash, boomdashConfig);
			this.thing.boomdashCooldown = BOOMDASH_COOLDOWN;
			if (direction !== null) this.thing._boomdashInfo = {
				direction: (direction + 4) % 8,
				timeRemaining: BOOMDASH_ACC_DURATION
			};
		}

		this.thing._timeSinceEnergyUsed += dt;
		if (this.thing._timeSinceEnergyUsed >= 0.75) {
			this.thing.energy += (Player.POWER * dt);
		}
		this.thing.energy = Math.min(this.thing.energy, Player.MAX_ENERGY);
		this.thing.cooldown -= dt;
		this.thing.cooldown = Math.max(this.thing.cooldown, 0);
		this.thing.boomdashCooldown -= dt;
		this.thing.boomdashCooldown = Math.max(this.thing.boomdashCooldown, 0);

		this._handleHealing(actions, dt, time);

		let dart = null;
		if (this.thing.dartId !== null) {
			dart = this.thing.space.requestThingById(this.thing.dartId);
		}

		if (dart === null) {
			this.thing.timeSinceDartExisted += dt;
			let dateMode = this.thing.space.game.unicenter.ship.numPlayers === 2;
			if (dateMode && this.thing.timeSinceDartExisted > 10) {
				this.thing.dartId = this.thing.space.requestSpawn(Dart, {}).id;
				this.thing.timeSinceDartExisted = 0;
			}
		} else {
			this.thing.timeSinceDartExisted = 0;
		}

		// Stat gathering

		if (this.thing.space.density.getCurrentArea(this.thing.pos, time)
						=== "DAMSELS_DESOLATION") {
			this.thing.space.game.unicenter.ship
				.reportStat("TIME_IN_DAMSELS_DESOLATION", dt);
		}
	}

	save() {
		let state = {};
		if (this.healingCooldown > 0) state.healingCooldown = round(this.healingCooldown);
		return state;
	}

	// Return true iff the player is currently vulnerable
	isVulnerable() {
		return this.healingCooldown > 0;
	}

	_moveFreely(dt, actions) {
		// Turn towards the mouse
		let position = new Vector(
			window.innerWidth / 2,
			window.innerHeight / 2
		);
		AIUtils.rotateTo(this.thing, Vector.diff(MOUSE, position).dir(), Player.ROT_SPEED,
						dt);

		// Calculate boomdash acceleration, if any
		let externalAcc = new Vector();
		if (this.thing._boomdashInfo !== null) {
			let direction = this.thing._boomdashInfo.direction;
			let rot = this.thing.rot + direction * Math.PI / 4;
			let timeRemaining = this.thing._boomdashInfo.timeRemaining;
			let actualAccStrength = (timeRemaining > dt)
				? BOOMDASH_ACC_STRENGTH
				: BOOMDASH_ACC_STRENGTH * (timeRemaining / dt);
			externalAcc.add(new Vector(actualAccStrength, 0).rotateBy(rot));

			this.thing._boomdashInfo.timeRemaining -= dt;
			if (this.thing._boomdashInfo.timeRemaining < 0) {
				this.thing._boomdashInfo = null;
			}
		}
		// Add tractor beam acc
		externalAcc.add(this.tractorBeamAcc);

		// Move according to the keyboard
		let speed = this.thing.space.game.unicenter.upgrader.getValue("SPEED");
		let thrustersForged = this.thing.space.game.unicenter.forge.thrustersForged;
		let boostFactor = this.thing.space.background.getBoostFactor();
		// Even when the player isn't holding THRUST, boost provides a bit of forward
		// acceleration
		let thrustAcc = speed * Player.FRICTION
						* (1 + Math.pow(boostFactor / 10, 2/3));
		let passiveThrustAcc = 0.3 * speed * Player.FRICTION
						* Math.pow(boostFactor / 10, 2/3);
		let brakeAcc = thrustersForged ? (0.5 * thrustAcc) : 0;
		let slideAcc = thrustersForged ? (0.5 * thrustAcc) : 0;
		let accForward = (actions.has("THRUST") ? thrustAcc : passiveThrustAcc)
						- (actions.has("BRAKE") ? brakeAcc : 0);
		let accLeft = (actions.has("SLIDE_LEFT") ? slideAcc : 0)
						- (actions.has("SLIDE_RIGHT") ? slideAcc : 0);
		let accX = accForward * Math.cos(this.thing.rot)
						+ accLeft * Math.sin(this.thing.rot);
		let accY = accForward * Math.sin(this.thing.rot)
						- accLeft * Math.cos(this.thing.rot);
		let acc = new Vector(accX, accY);
		acc.add(externalAcc);

		let oldPos = new Vector(this.thing.pos);
		AIUtils.translate(this.thing, acc, Player.FRICTION, dt);
		this.thing.space.game.unicenter.ship.reportStat(
			"DISTANCE_TRAVELED", Vector.diff(this.thing.pos, oldPos).mag()
		);

		this.thing._thrustActive = actions.has("THRUST");
	}

	_moveToDock(dt, time, station) {
		this.thing._thrustActive = false;

		if (time >= this.thing.timeDockingCompletes) {
			if (this.thing.timeDockingCompletes > 0) {
				// Just docked
				delete this._dockingTrajectory;
				this.thing.timeDockingCompletes = 0;
				this.thing.space.game.unicenter.selectTab(station.mainTab);
				this.thing.space.game.unicenter.show();
			}
			let dockLocation = station.getDockLocation();
			AIUtils.setPos(this.thing, dockLocation.pos, dt);
			AIUtils.setRot(this.thing, dockLocation.rot, dt);
		} else {
			if (this._dockingTrajectory === undefined) {
				this._createDockingTrajectory(time, station);
			}
			AIUtils.setPos(
				this.thing,
				new Vector(
					this._dockingTrajectory.x(time),
					this._dockingTrajectory.y(time)
				),
				dt
			);
			AIUtils.setRot(this.thing, this._dockingTrajectory.rot(time), dt);
		}
	}

	_handleHealing(actions, dt, time) {
		this.healingCooldown -= dt;
		if (this.healingCooldown < 0) {
			this.healingCooldown = 0;
		}

		if (this.healingCooldown > 0) {
			let desiredSwarms = Math.min(
				Math.floor(
					(PlayerAI.HEALING_COOLDOWN - this.healingCooldown) / 0.03
				) + 1,
				PlayerAI.HEALING_SWARMS
			);

			// How many swarms we would have desired last tick
			let spawnedSwarms = Math.min(
				Math.floor(
					(PlayerAI.HEALING_COOLDOWN - (this.healingCooldown + dt)) / 0.03
				) + 1,
				PlayerAI.HEALING_SWARMS
			);

			while (spawnedSwarms < desiredSwarms) {
				this.thing.space.requestSpawn(Nanobots, {
					parentId: this.thing.id,
					relativeDir: - 2 * Math.PI * spawnedSwarms
									/ PlayerAI.HEALING_SWARMS
				});
				++spawnedSwarms;
			}
		}

		let wantsToHealKeyboard = actions.has("HEAL");
		let wantsToHealMouse = this.thing.space.game.dashboard.healingButton.wasClicked();
		let wantsToHeal = wantsToHealKeyboard || wantsToHealMouse;
		if (wantsToHeal && this.healingCooldown <= 0
						&& this._consumeEnergy(Player.HEALING_COST)) {
			// TODO: The tiny addition is a hack to avoid an off-by-one error in the
			// spawnedSwarms calculation
			this.healingCooldown = PlayerAI.HEALING_COOLDOWN + 0.0001;
		}
	}

	// Return true iff there was enough energy
	_consumeEnergy(amount) {
		if (this.thing.energy < amount) return false;

		this.thing.energy -= amount;
		this.thing._timeSinceEnergyUsed = 0;
		return true;
	}

	_createDockingTrajectory(time, station) {
		let dockLocation = station
			.getDockLocation(this.thing.timeDockingCompletes - time);
		let dockPos = dockLocation.pos;
		let dockVel = dockLocation.vel;
		let dockRot = dockLocation.rot;
		let dockSpn = dockLocation.spn;

		// Prevent player from spinning wildly
		dockRot = this.thing.rot + angleDiff(dockRot, this.thing.rot);

		this._dockingTrajectory = {
			x: createSmoothInterpolation(
				time, this.thing.pos.x, this.thing.vel.x,
				this.thing.timeDockingCompletes, dockPos.x, dockVel.x
			),
			y: createSmoothInterpolation(
				time, this.thing.pos.y, this.thing.vel.y,
				this.thing.timeDockingCompletes, dockPos.y, dockVel.y
			),
			rot: createSmoothInterpolation(
				time, this.thing.rot, this.thing.spn,
				this.thing.timeDockingCompletes, dockRot, dockSpn
			)
		};
	}
}



class PlayerAnimation {
	constructor(thing) {
		this.thing = thing;

		this._hull = new Sprite(getTexture("images/player/hull.svg"));
		this._hull.anchor.set(0.5, 0.5);
		this._hull.position.set(0, 0);
		this._hull.scale.set(1 / STANDARD_PPF);
		this.thing.display.addChild(this._hull);

		this._exhaust = new Sprite(getTexture("images/player/exhaust.svg"));
		this._exhaust.anchor.set(0.5, 0);
		this._exhaust.position.set(0, 0.17);
		this._exhaust.scale.set(1 / STANDARD_PPF);
		this.thing.display.addChild(this._exhaust);

		this._sphere = new Sprite(getTexture("images/player/sphere.svg"));
		this._sphere.anchor.set(0.5, 0.5);
		this._sphere.position.set(0, 0.17);
		this.thing.display.addChild(this._sphere);
	}

	update(dt, time) {
		this._exhaust.visible = this.thing._thrustActive;

		// Energy sphere
		let absoluteMaxScale = 1 / STANDARD_PPF;
		let minScale = (0.04 / 0.18) * absoluteMaxScale;
		let maxScale = (0.16 / 0.18) * absoluteMaxScale;
		this._sphere.scale.set(lerp(minScale, maxScale,
						this.thing.energy / Player.MAX_ENERGY));
	}
}



export class Player extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// pos of the player, a vector with values in flares
		new OptionalProperty("pos", v => isVectorable(v)),
		// vel of the player, a vector with values in flares / second
		new OptionalProperty("vel", v => isVectorable(v)),
		// rot of the player, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of the player
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		// id of the station the player is docking to, null if none
		new OptionalProperty("dockingTo", v =>
						(v === null || Number.isFinite(v))),
		// Time when the player will fully dock, 0 if the player is fully docked,
		// omit if not docking
		new OptionalProperty("timeDockingCompletes", v => (Number.isFinite(v) && v >= 0)),
		// id of the last visited station
		new OptionalProperty("lastVisitedStationId", v => (Number.isFinite(v))),
		// Energy (defaults to max if omitted)
		new OptionalProperty("energy",
			v => (Number.isFinite(v) && v >= 0 && v <= Player.MAX_ENERGY)
		),
		// Time since energy was last used, in seconds (defaults to 0 if omitted)
		new OptionalProperty("timeSinceEnergyUsed", v => (Number.isFinite(v) && v >= 0)),
		// Blaster cooldown, in seconds (defaults to 0 if omitted)
		new OptionalProperty("cooldown",
			v => (Number.isFinite(v) && v >= 0 && v <= Player.COOLDOWN)
		),
		// Boomdash cooldown, in seconds (defaults to 0 if omitted)
		new OptionalProperty("boomdashCooldown",
			v => (Number.isFinite(v) && v >= 0 && v <= BOOMDASH_COOLDOWN)
		)
	]);

	// Rotation speed, radians / second
	static ROT_SPEED = 12;
	// Friction, 1 / second
	// Max speed is <direction>_ACC / FRICTION.
	static FRICTION = 4;

	// Max energy
	static MAX_ENERGY = 5;
	// Power, AKA the rate at which the energy regenerates, in energy / second
	static POWER = 10;
	// Energy cost to fire the blaster
	static FIRING_COST = 1;
	// Energy cost to heal
	static HEALING_COST = 5;

	// Cooldown of the weapon, in seconds
	static COOLDOWN = 0.2;
	// Time needed to dock to a station, in seconds
	static TIME_TO_DOCK = 1;

	// Create a player
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Player.CONFIG_STRUCTURE.test(config);

		// Ensure that properties are real vectors, where needed
		if (config.pos !== undefined) config.pos = new Vector(config.pos);
		if (config.vel !== undefined) config.vel = new Vector(config.vel);

		config.identity = "PLAYER";
		config.name = "Player";

		config.zIndex         = Z_INDICES.PLAYER;
		config.len            = 0.5;
		config.hitbox         = new Polygon({x: 0, y: 0}, [
			{x: 0.25, y: 0},
			{x: -0.25, y: -0.15},
			{x: -0.25, y: 0.15}
		]);
		config.type           = THING_TYPES.SPACESHIP;
		config.faction        = FACTIONS.PLAYER;
		config.canDespawn     = false;
		config.canPickUpItems = true;
		config.gpsIcon        = "images/icons/gps/player.svg";
		config.rotateGpsIcon  = true;
		if (config.pos === undefined) config.pos = new Vector();
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = getMaxHealth(config.space);

		super(config);

		this._boomdashInfo = null;

		this.ai = new PlayerAI(this, {
			healingCooldown: config.healingCooldown
		});

		this._thrustActive = false;
		this.animation = new PlayerAnimation(this);

		// Set weapon-related properties
		this.firingMouse = false;
		this.energy = config.energy ?? Player.MAX_ENERGY;
		this._timeSinceEnergyUsed = config.timeSinceEnergyUsed ?? 0;
		this.cooldown = config.cooldown ?? 0;
		this.boomdashCooldown = config.boomdashCooldown ?? 0;

		// id of the station the player is docking to, null if none
		this.dockingTo = config.dockingTo ?? null;
		// Time when the player will fully dock, 0 if the player is fully docked,
		// omit if not docking
		if (config.timeDockingCompletes !== undefined) {
			this.timeDockingCompletes = config.timeDockingCompletes;
		}
		if (this.dockingTo !== null && this.timeDockingCompletes === undefined) {
			logger.warning("player.js", "dockingTo set but timeDockingCompletes unset");
			this.timeDockingCompletes = 0;
		}

		this._lastVisitedStationId = config.lastVisitedStationId ?? null;

		this.dartId = null;
		this.timeSinceDartExisted = Infinity;

		// Add health bar
		this.healthBar = this.space.scanner.addBar(
			this.display.position,
			this.radius + 10,
			this.health,
			this.maxHealth
		);

		// TODO: Pop this control when it's no longer needed
		keyboard.pushControl(new Keyboard.Control(
			"PLAYER",
			new Map([
				["_UP",       "THRUST"     ],
				["_DOWN",     "BRAKE"      ],
				["_LEFT",     "SLIDE_LEFT" ],
				["_RIGHT",    "SLIDE_RIGHT"],
				["_FIRE",     "FIRE"       ],
				[".DOCK",     "DOCK"       ],
				["_HEAL",     "HEAL"       ],
				["_BOOMDASH", "BOOMDASH"   ]
			]),
			new Set()
		));
	}

	// Redraw the player on the screen
	// This function overrides Thing.redraw().
	redraw(screenSize, pixelsPerFlare) {
		super.redraw(
			screenSize,
			pixelsPerFlare,
			new Vector(screenSize.x / 2, screenSize.y / 2)
		);
	}

	// Attempt to dock to a nearby station
	// You don't need to specify what station to dock to - this function finds
	// a nearby station automatically, if there's any. This function does
	// nothing if the player is already docked, or if there's no nearby station.
	attemptToDock() {
		if (this.dockingTo !== null || !this.space.canDock) return;

		let tacticalReport = this.space.requestTacticalReport(
			entry => ["STATION", "SPIDERDOME"].includes(entry.identity),
			"DOCK"
		);

		let minDistance = Infinity;
		let station = null;
		tacticalReport.forEach(entry => {
			let dist = Vector.diff(this.pos, entry.pos).mag();
			if (dist < minDistance) {
				minDistance = dist;
				station = entry.thing;
			}
		});

		if (station === null) {
			logger.error("player.js", "can't find station to dock to");
			return;
		}
		this.dockingTo = station.id;

		this.timeDockingCompletes = this.space.time + station.timeToDock;

		station.dockExtended = true;
	}

	// Undock from a station
	// This function does nothing if the player isn't docked. Called by the
	// unicenter.
	undock() {
		if (this.dockingTo === null) return;

		this.dockingTo = null;
		delete this.timeDockingCompletes;

		// Both Player.undock() and Unicenter.hide() call each other, since either one can
		// get called first
		this.space.game.unicenter.hide();
	}

	// Return the index of the station the player is fully docked to, or null
	dockedTo() {
		if (this.dockingTo === null) return null;
		return (this.timeDockingCompletes === 0) ? this.dockingTo : null;
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		if (this.isVulnerable()) damage *= 3;

		if (this.health > damage) {
			this._changeHealth(this.health - damage);
		} else {
			// Die!
			this._changeHealth(0);
			this.prepareToDie(false);
		}
	}

	// Repair the player a little bit
	// dt is the time since last update, in seconds. The interactor executes
	// this function every tick if the player is inside a station. stationId is the id of
	// the station providing the repairs.
	repair(dt, stationId) {
		this._changeHealth(Math.min(this.health + this.maxHealth * dt,
						this.maxHealth));
		this._lastVisitedStationId = stationId;
	}

	// Receive a drop
	receiveDrop(drop) {
		this.space.game.unicenter.inventory.add(drop);
	}

	// Return true iff the player is currently vulnerable
	isVulnerable() {
		return this.ai.isVulnerable();
	}

	// Save this player into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();
		let aiState = this.ai.save();

		config.pos = this.pos.save();
		config.vel = this.vel.save();
		config.rot = round(this.rot);
		if (this.dockingTo !== null) {
			config.dockingTo = this.dockingTo;
			config.timeDockingCompletes = round(this.timeDockingCompletes);
		}
		config.lastVisitedStationId = this._lastVisitedStationId;
		if (this.energy !== Player.MAX_ENERGY) config.energy = round(this.energy);
		config.timeSinceEnergyUsed = round(this._timeSinceEnergyUsed);
		if (this.cooldown > 0) config.cooldown = round(this.cooldown);
		if (this.boomdashCooldown > 0) {
			config.boomdashCooldown = round(this.boomdashCooldown);
		}
		if (aiState.healingCooldown !== undefined) {
			config.healingCooldown = aiState.healingCooldown;
		}

		return config;
	}

	// Set up this player to respawn
	prepareToDie(dropItems) {
		// Already prepared
		if (this.state === Thing.STATES.LIMBO) return;

		this.space.game.unicenter.ship.reportStat("SHATTERS", 1);

		super.prepareToDie(dropItems, true);

		// TODO: Shouldn't modify private properties here
		this.space.scanner._arrows.visible = false;

		// Lose all items on shattering
		this.space.game.unicenter.inventory.clearAllItems();
	}

	// Respawn from limbo
	// Called by respawner.
	respawn() {
		super.respawn();
		let lastVisitedStation = this.space.requestThingById(this._lastVisitedStationId);
		if (lastVisitedStation === null) {
			this.pos = new Vector();
		} else {
			this.pos.copyFrom(lastVisitedStation.pos);
		}
		this.vel = new Vector();
		this.rot = 0;
		this.health = this.maxHealth;
		this.energy = Player.MAX_ENERGY;
		this.cooldown = 0;
		// TODO: Shouldn't modify private properties here
		this.space.scanner._arrows.visible = true;
	}

	// Change health to nextHealth, and update all visual indicators of health
	_changeHealth(nextHealth) {
		this.health = nextHealth;
		this.healthBar.val = this.health;
	}
}



function getMaxHealth(space) {
	let maxHealth = space.game.unicenter.upgrader.getValue("HEALTH");
	let dateMode = space.game.unicenter.ship.numPlayers === 2;
	if (dateMode) maxHealth = Math.round(maxHealth * 0.8);
	return maxHealth;
}
