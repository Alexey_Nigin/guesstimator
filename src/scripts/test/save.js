// Test saving and loading parts of the game

import {Ship, ACHIEVEMENTS} from "../i.js";
import {Refinery} from "../i.js";
import {Upgrader} from "../i.js";
import {TestError} from "./framework.js";

// Run factory with saveFileBefore, save, compare result with expectedSaveFileAfter
function checkLoadAndSave(factory, saveFileBefore, expectedSaveFileAfter) {
	let objectUnderTest = factory(saveFileBefore);
	let actualSaveFileAfter = objectUnderTest.save();

	let expectedString = JSON.stringify(expectedSaveFileAfter);
	let actualString = JSON.stringify(actualSaveFileAfter);
	if (actualString !== expectedString) {
		throw new TestError(
			`checkLoadAndSave failed:`
			+ ` '${actualString}' (actual) !== '${expectedString}' (expected)`
		);
	}
}



export function testSaveShip() {
	// Fresh file
	checkLoadAndSave(
		savefile => new Ship({}, savefile),
		undefined,
		{
			a: Object.fromEntries([
				[ACHIEVEMENTS.UPGRADE, false],
				[ACHIEVEMENTS.PRANK_BRO, false],
				[ACHIEVEMENTS.ROCKY_START, false],
				[ACHIEVEMENTS.THRUSTERS, false],
				[ACHIEVEMENTS.COSSETTE, false],
				[ACHIEVEMENTS.BEACONY, false],
				[ACHIEVEMENTS.COMPASS, false],
				[ACHIEVEMENTS.ESCAPE, false],
				[ACHIEVEMENTS.DAMSEL, false],
				[ACHIEVEMENTS.BOOMDASH, false]
			]),
			s: Object.fromEntries([
				["DAMSEL_PHASE_1_COMPLETE", 0],
				["DISTANCE_TRAVELED", 0],
				["BLASTER_BOLTS_FIRED", 0],
				["SHATTERS", 0],
				["TIME_ACTIVE", 0],
				["TIME_IN_DAMSELS_DESOLATION", 0],
				["TIME_NEAR_DAMSEL", 0],
				["TIMES_REFINERY_USED", 0],
				["UPGRADES_OBTAINED", 0]
			])
		}
	);

	// Existing save
	checkLoadAndSave(
		savefile => new Ship({}, savefile),
		{
			a: Object.fromEntries([
				[ACHIEVEMENTS.UPGRADE, true],
				[ACHIEVEMENTS.PRANK_BRO, false],
				[ACHIEVEMENTS.ROCKY_START, true],
				[ACHIEVEMENTS.THRUSTERS, true],
				[ACHIEVEMENTS.COSSETTE, false],
				[ACHIEVEMENTS.BEACONY, true],
				[ACHIEVEMENTS.COMPASS, true],
				[ACHIEVEMENTS.ESCAPE, true],
				[ACHIEVEMENTS.DAMSEL, true],
				[ACHIEVEMENTS.BOOMDASH, false]
			]),
			s: Object.fromEntries([
				["DAMSEL_PHASE_1_COMPLETE", 1],
				["DISTANCE_TRAVELED", 2],
				["BLASTER_BOLTS_FIRED", 3],
				["SHATTERS", 4],
				["TIME_ACTIVE", 5.555],
				["TIME_IN_DAMSELS_DESOLATION", 0],
				["TIME_NEAR_DAMSEL", 0],
				["TIMES_REFINERY_USED", 0],
				["UPGRADES_OBTAINED", 0]
			])
		},
		{
			a: Object.fromEntries([
				[ACHIEVEMENTS.UPGRADE, true],
				[ACHIEVEMENTS.PRANK_BRO, false],
				[ACHIEVEMENTS.ROCKY_START, true],
				[ACHIEVEMENTS.THRUSTERS, true],
				[ACHIEVEMENTS.COSSETTE, false],
				[ACHIEVEMENTS.BEACONY, true],
				[ACHIEVEMENTS.COMPASS, true],
				[ACHIEVEMENTS.ESCAPE, true],
				[ACHIEVEMENTS.DAMSEL, true],
				[ACHIEVEMENTS.BOOMDASH, false]
			]),
			s: Object.fromEntries([
				["DAMSEL_PHASE_1_COMPLETE", 1],
				["DISTANCE_TRAVELED", 2],
				["BLASTER_BOLTS_FIRED", 3],
				["SHATTERS", 4],
				["TIME_ACTIVE", 5.555],
				["TIME_IN_DAMSELS_DESOLATION", 0],
				["TIME_NEAR_DAMSEL", 0],
				["TIMES_REFINERY_USED", 0],
				["UPGRADES_OBTAINED", 0]
			])
		}
	);
}



export function testSaveRefinery() {
	// Fresh file
	checkLoadAndSave(
		savefile => new Refinery({}, savefile),
		undefined,
		[0, 0, 0, 0]
	);

	// Existing save
	checkLoadAndSave(
		savefile => new Refinery({}, savefile),
		[1, 2, 3, 4],
		[1, 2, 3, 4]
	);
}



export function testSaveUpgrader() {
	// Fresh file
	checkLoadAndSave(
		savefile => new Upgrader({}, savefile),
		undefined,
		{l: [0, 0, 0], tf: false}
	);

	// Existing save
	checkLoadAndSave(
		savefile => new Upgrader({}, savefile),
		{l: [1, 2, 3], tf: true},
		{l: [1, 2, 3], tf: true}
	);
}
