// Framework used by all tests

export class TestError extends Error {
	constructor(...args) {
		super(...args);
		this.name = "TestError";
	}
}

export function assertEqual(a, b) {
	if (!(a === b)) throw new TestError(`assertion failed: ${a} === ${b}`);
}

export function assertLessThan(a, b) {
	if (!(a < b)) throw new TestError(`assertion failed: ${a} < ${b}`);
}

export function assertGreaterThan(a, b) {
	if (!(a > b)) throw new TestError(`assertion failed: ${a} > ${b}`);
}
