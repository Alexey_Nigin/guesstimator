import {createSmoothInterpolation} from "../utils.js";
import {assertGreaterThan, assertEqual, assertLessThan, TestError} from "./framework.js";

export function testCreateSmoothInterpolation() {
	let smoothInterpolator = createSmoothInterpolation(
		100, // startTime
		10,  // startValue
		-2,  // startSlope
		104, // endTime
		11,  // endValue
		-3,  // endSlope
	);

	// Edges
	assertEqual(smoothInterpolator(99), 12);
	assertEqual(smoothInterpolator(100), 10);
	assertEqual(smoothInterpolator(104), 11);
	assertEqual(smoothInterpolator(105), 8);

	// Interpolation
	let pointA = smoothInterpolator(101);
	let pointB = smoothInterpolator(102);
	let pointC = smoothInterpolator(103);
	assertLessThan(pointA, 10);
	assertGreaterThan(pointC, 11);
	assertLessThan(pointA, pointB);
	assertLessThan(pointB, pointC);
}
