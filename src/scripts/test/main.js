import {Application} from "pixi.js";
import {testSaveShip, testSaveRefinery, testSaveUpgrader} from "./save.js";
import {testCreateSmoothInterpolation} from "./utils.js";
import {testHsv} from "./ui/utils.js";

document.body.innerText = "WAIT";

// Make sure `addEventListener()` is initialized
export let G = new Application();
await G.init({});

let tests = [
	testSaveShip,
	testSaveRefinery,
	testSaveUpgrader,
	testCreateSmoothInterpolation,
	testHsv
];

let pass = true;

for (let test of tests) {
	try {
		test();
	} catch(e) {
		console.error(e.message);
		document.body.innerText = "FAIL";
		pass = false;
	}
}

if (pass) {
	document.body.innerText = "PASS";
}
