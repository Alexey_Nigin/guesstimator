import {UIUtils} from "../../i.js";
import {assertGreaterThan, assertEqual, assertLessThan, TestError} from "../framework.js";

export function testHsv() {
	assertEqual(UIUtils.hsv(110, 70, 100), 0x6aff4c);
}
