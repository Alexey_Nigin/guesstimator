import {Vector} from "./i.js"; // ./vector.js
import {ConfigStructure, EitherOr, RequiredProperty} from "./i.js"; // ./validators.js
import {Particles} from "./i.js"; // ./particles.js
import {StraightLineAI} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js



export const BLASTER_VARIETIES = {
	PLAYER: "PLAYER",
	DART: "DART",
	SIZY: "SIZY"
};

const VARIETY_INFO = new Map([
	[BLASTER_VARIETIES.PLAYER, {
		texture: "images/blaster.svg",
		zIndex: Z_INDICES.PLAYER_CHILD,
		len: 0.22,
		faction: FACTIONS.PLAYER,
		maxHealth: config => config.space.game.unicenter.upgrader.getValue("DAMAGE"),
		speed: 4,  // flares / second
		particleType: Particles.TYPES.BLASTER
	}],
	[BLASTER_VARIETIES.DART, {
		texture: "images/blaster.svg",
		zIndex: Z_INDICES.PLAYER_CHILD,
		len: 0.22 * 3 / 5,
		faction: FACTIONS.PLAYER,
		maxHealth: config => Math.floor(
			config.space.game.unicenter.upgrader.getValue("DAMAGE") / 3
		),
		speed: 4,  // flares / second
		particleType: Particles.TYPES.BLASTER
	}],
	[BLASTER_VARIETIES.SIZY, {
		texture: "images/size-reversal/blaster.svg",
		zIndex: Z_INDICES.NPC_CHILD,
		len: 0.22,
		faction: FACTIONS.SIZE_REVERSAL,
		maxHealth: 1,
		speed: 2.5,  // flares / second
		particleType: Particles.TYPES.SIZY_BLASTER
	}]
]);



export class Blaster extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Variety of this blaster bolt, usually determined by what fired it
		new RequiredProperty("variety",
			v => Object.values(BLASTER_VARIETIES).includes(v)
		),
		// pos of this blaster bolt, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// rot of this blaster bolt, in radians
		new RequiredProperty("rot", v => Number.isFinite(v)),
		new EitherOr([
			// vel of the thing firing the blaster bolt, a vector in flares / second
			new RequiredProperty("parentVel", v => (v instanceof Vector))
		], [
			// vel of this blaster bolt, a vector with values in flares / second
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// Health of this blaster bolt
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	// Create a blaster bolt
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Blaster.CONFIG_STRUCTURE.test(config);

		let varietyInfo = VARIETY_INFO.get(config.variety);

		config.texture = varietyInfo.texture;
		config.zIndex  = varietyInfo.zIndex;
		config.len     = varietyInfo.len;
		config.type    = THING_TYPES.ENERGY;
		config.faction = varietyInfo.faction;
		config.canDespawn = true;
		config.despawnSafetyWindow = 0;
		if (config.vel === undefined) {
			// If vel is undefined, then parentVel must be defined
			config.vel = new Vector(varietyInfo.speed, 0)
				.rotateBy(config.rot)
				.add(config.parentVel);
		}
		if (Number.isFinite(varietyInfo.maxHealth)) {
			config.maxHealth = varietyInfo.maxHealth;
		} else {
			config.maxHealth = varietyInfo.maxHealth(config);
		}
		super(config);

		this.variety = config.variety;

		this.ai = new StraightLineAI(this, {});
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		// The blaster bolt is not piercing
		this.prepareToDie(false);
	}

	// Save this blaster bolt into a config object
	// TODO
	// save() {
	// 	let config = super.save().config;
	// 	return {
	// 		config:     config,
	// 		isComplete: true
	// 	};
	// }

	// Prepare this blaster bolt for removal
	// See the comment on Thing.prepareToDie() for more details.
	// dropParticles is a boolean that defaults to true.
	// TODO: Make the dropParticles argument consistent across all things.
	prepareToDie(dropItems, dropParticles) {
		if (dropParticles !== false) {
			this.space.particles.add(
				VARIETY_INFO.get(this.variety).particleType,
				// Fire particles from the front tip of the blaster bolt
				Vector.sum(
					this.pos,
					new Vector(this.len / 2, 0).rotateBy(this.rot)
				),
				// Don't preserve the blaster bolt's momentum because I want to create
				// an impression that the blaster bolt lost its momentum upon hitting
				// the target
				null,
				5
			);
		}

		super.prepareToDie(dropItems);
	}
}
