import {Container, Graphics, Sprite, Text} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {keyboard, Keyboard} from "../i.js"; // ../keyboard.js
import {UIUtils, Clicker} from "../i.js"; // ../ui/utils.js
import {Tooltip} from "../i.js"; // ../ui/tooltipper.js
import {Tab} from "../i.js"; // ./tab.js
import {Ship} from "../i.js"; // ./ship.js
import {Inventory} from "../i.js"; // ./inventory.js
import {Chat} from "../i.js"; // ./chat.js
import {Upgrader} from "../i.js"; // ./upgrader.js
import {Refinery} from "../i.js"; // ./refinery.js
import {GPS} from "../i.js"; // ./gps.js
import {Forge} from "../i.js";
import {Cryohall} from "../i.js";
import {Workshop} from "../i.js";

export class Unicenter {
	// Color with which the space around the box is shaded
	static BACKGROUND_COLOR = 0x222222;
	// Alpha with which the space around the box is shaded
	static BACKGROUND_ALPHA = 0.75;

	// Max fraction of the screen width or height that the box can occupy
	static BOX_FRACTION = 0.75;
	// (Box width) / (box height)
	static BOX_ASPECT_RATIO = 4 / 3;
	// (Tab width) / (box width)
	static TAB_FRACTION = 1 / 15;
	// (Tab rounded corner radius) / (tab width)
	static TAB_ROUNDING = 1 / 4;
	// (Icon width) / (tab width)
	static ICON_FRACTION = 2 / 3;

	// Create a new unicenter
	// Only create one unicenter per game.
	//
	// Args:
	//   game     - reference to the game
	//   savefile - optional savefile (object)
	constructor(game, savefile) {
		this.game = game;

		// Current sizes for common elements
		this._sizes = {
			baseFontSize: null,
			marginWidth: null
		};

		// Create tabs
		this.ship      = new Ship(this, savefile?.s);
		this.inventory = new Inventory(this, savefile?.i);
		this.upgrader  = new Upgrader(this, savefile?.u);
		this.refinery  = new Refinery(this, savefile?.r);
		this.gps       = new GPS(this);
		this.chat      = new Chat(this, savefile?.c);
		this.forge     = new Forge(this, savefile?.f);
		this.cryohall  = new Cryohall(this, savefile?.lc);
		this.workshop  = new Workshop(this, savefile?.w);
		this._tabs = new Map([
			["DOCK", {
				// A fake tab, used for creating a tab selector without an
				// associated tab
				isFake: true,
				isAccessible: () => !this._isOpen && this.game.space.canDock,
				location: Tab.LOCATIONS.LEFT,
				icon: "images/icons/dock.svg",
				colorBase: this.refinery.colorBase,
				colorHighlight: this.refinery.colorHighlight,
				tooltipText: "Dock"
			}],
			["UNDOCK", {
				// A fake tab, used for creating a tab selector without an
				// associated tab
				isFake: true,
				isAccessible: () => this._isOpen
								&& (this.game.space.player.dockedTo() !== null),
				location: Tab.LOCATIONS.LEFT_BOTTOM,
				icon: "images/icons/dock.svg",
				colorBase: this.refinery.colorBase,
				colorHighlight: this.refinery.colorHighlight,
				tooltipText: "Undock"
			}],
			["SHIP",      this.ship     ],
			["INVENTORY", this.inventory],
			["GPS",       this.gps      ],
			["CHAT",      this.chat     ],
			["REFINERY",  this.refinery ],
			["UPGRADER",  this.upgrader ],
			["FORGE",     this.forge    ],
			["CRYOHALL",  this.cryohall ],
			["WORKSHOP",  this.workshop ]
		]);

		// The screen size as of last resize
		// TODO: Contemplate using a null vector here.
		this._lastScreenSize = new Vector();


		this.display = new Container();
		this.display.background = new Graphics();
		this.display.addChild(this.display.background);

		this.display.tabSelectors = new Container();
		this.display.addChild(this.display.tabSelectors);

		this._tabSelectorClickers = new Map();
		this._tabSelectorTooltips = new Map();
		for (let [tabName, tab] of this._tabs.entries()) {
			let tabSelector = new Graphics();
			tabSelector.tabName = tabName;
			this._tabSelectorClickers.set(tabName, new Clicker(tabSelector));
			let shortcut
				= ` (${(tabName === "DOCK" || tabName === "UNDOCK") ? "X" : "U"})`;
			this._tabSelectorTooltips.set(tabName, new Tooltip(
				this.game.tooltipper,
				tabSelector,
				tab.tooltipText + shortcut,
				"LEFT_MIDDLE"
			));
			this.display.tabSelectors.addChild(tabSelector);
			let invIcon = new Sprite(getTexture(tab.icon));
			tabSelector.addChild(invIcon);
		}

		this.display.box = new Graphics();
		this.display.addChild(this.display.box);

		this.display.tabDisplayContainer = new Container();
		this.display.addChild(this.display.tabDisplayContainer);

		// Close the unicenter when the background is clicked
		this._backgroundClicker = new Clicker(this.display.background);
		// Give the box an empty pointerup listener, so clicking it doesn't
		// trigger the listener above
		this.display.box.interactive = true;
		this.display.box.on("pointerup", () => {});

		// This should be a non-fake key in this._tabs
		this._selectedTab = savefile?.t ?? "CHAT";

		// Saved for tooltips and what not
		this._tabOrigin = new Vector();

		// Accessed by the dashboard
		this.tabSelectorWidth = 0;

		// The unicenter is hidden until shown by the game
		this._isOpen = false;
	}

	// Update the unicenter
	// This function should be called every tick.
	// screenSize is a vector in pixels.
	update(dt, screenSize) {
		this._handleInputs();

		let accessibleTabs = this._getAccessibleTabs();
		if (!accessibleTabs.has(this._selectedTab)) {
			this._selectedTab = "INVENTORY";
		}

		this._lastScreenSize.copyFrom(screenSize);
		let tabSize = this._refresh(dt, accessibleTabs);

		for (let [tabName, tab] of this._tabs.entries()) {
			if (tab.isFake) continue;
			let tabVisible = (tabName === this._selectedTab)
							? this._isOpen : false;
			tab.update(tabVisible, tabSize, dt);
		}
	}

	// Save the state of the unicenter into an object
	save() {
		return {
			s:  this.ship.save(),
			i:  this.inventory.save(),
			c:  this.chat.save(),
			r:  this.refinery.save(),
			u:  this.upgrader.save(),
			f:  this.forge.save(),
			lc: this.cryohall.save(),
			t:  this._selectedTab
		};
	}

	// Show the unicenter
	// Call this function before calling update() in a tick.
	show() {
		this._isOpen = true;

		if (!keyboard.hasControl("UNICENTER")) {
			// TODO: What if the control is present but isn't at the top?
			keyboard.pushControl(new Keyboard.Control(
				"UNICENTER",
				new Map([
					[".UNICENTER", "HIDE"],
					[".DOCK",      "HIDE"]
				]),
				new Set(["PLAYER", "GAME"])
			));
		}
	}

	// Hide the unicenter
	// Call this function before calling update() in a tick.
	hide() {
		if (!this._isOpen) return;

		this._isOpen = false;

		keyboard.popControl("UNICENTER");

		// Both Player.undock() and Unicenter.hide() call each other, since either one can
		// get called first
		let player = this.game.space.player;
		if (player.dockingTo !== null) player.undock();
	}

	selectTab(tabName) {
		this._selectedTab = tabName;
	}

	// Request a stroke for a unicenter element
	// Returns an object ready to be passed to PixiJS.
	// Args:
	//   color         - number in RGB
	//   relativeWidth - one of "NARROW", "WIDE", or "ULTRAWIDE". "ULTRAWIDE" was added as
	//                   a temporary hack, time will show if it will stick around.
	//   center        - optional boolean. If true, center the stroke.
	requestStroke(color, relativeWidth, center) {
		// TODO: Scale stroke width with screen size
		let absoluteWidth = {NARROW: 1, WIDE: 2, ULTRAWIDE: 8}[relativeWidth];
		return {
			width: absoluteWidth,
			color: color,
			alignment: center ? 0.5 : 1,
			cap: "round"
		};
	}

	// Request font size
	// relativeSize is "SMALL" or "LARGE".
	requestFontSize(relativeSize) {
		if (this._sizes.baseFontSize === null) {
			logger.error("unicenter.js", "not ready to request font");
			return 12;
		}
		let sizeMultiplier = {SMALL: 1, LARGE: 1.5}[relativeSize];
		return this._sizes.baseFontSize * sizeMultiplier;
	}

	// Request margin width
	// Multiplier is a positive real number that determines the relative width
	// of the margin. Returns an integer.
	requestMarginWidth(multiplier) {
		if (this._sizes.marginWidth === null) {
			logger.error("unicenter.js", "not ready to request margin");
			return 1;
		}
		return Math.round(multiplier * this._sizes.marginWidth);
	}

	// Request tab origin, in pixels
	requestTabOrigin() {
		return new Vector(this._tabOrigin);
	}

	// Figure out what to show based on the inputs from the last tick
	_handleInputs() {
		let justHidden = false;
		if (this._isOpen) {
			let hideByKeyboard = this._isOpen
							&& keyboard.getActions("UNICENTER").has("HIDE");
			let hideByMouse = this._backgroundClicker.wasClicked();
			if (hideByKeyboard || hideByMouse) {
				this.hide();
				justHidden = true;
			}
		}

		if (!justHidden) {
			for (let [tabName, clicker]
							of this._tabSelectorClickers.entries()) {
				if (clicker.wasClicked()) {
					if (this._tabs.get(tabName).isFake) {
						// Handle a fake tab
						if (tabName === "DOCK") {
							this.game.space.player.attemptToDock();
						} else {
							this.hide();
						}
						break;
					}
					this._selectedTab = tabName;
					this.show();
					break;
				}
			}
		}

		this._backgroundClicker.clearStuff();
		for (let clicker of this._tabSelectorClickers.values()) {
			clicker.clearStuff();
		}
	}

	// Return a set of all tab names accessible to the player right now
	_getAccessibleTabs() {
		return new Set(
			Array.from(this._tabs.entries())
				.filter(entry => entry[1].isAccessible())
				.map(entry => entry[0])
		);
	}

	// Rendering from here forth

	// Refresh the unicenter, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick. Returns a vector in pixels representing
	// the size of the tab. accessibleTabs is a set of all tab names accessible
	// to the player right now.
	_refresh(dt, accessibleTabs) {
		this._refreshBackground();
		let [boxSize, boxOrigin] = this._refreshBox();
		this._refreshTabSelectors(dt, boxSize, boxOrigin, accessibleTabs);
		return this._refreshTab(boxSize, boxOrigin);
	}

	// Refresh the background
	// This is a sub-function of _refresh().
	_refreshBackground() {
		let background = this.display.background;
		if (!this._isOpen) {
			background.visible = false;
			return;
		}

		background.visible = true;
		background
			.clear()
			.rect(0, 0, this._lastScreenSize.x, this._lastScreenSize.y)
			.fill({color: Unicenter.BACKGROUND_COLOR, alpha: Unicenter.BACKGROUND_ALPHA});
	}

	// Refresh the box
	// This is a sub-function of _refresh(). Returns [boxSize, boxOrigin].
	_refreshBox() {
		let rulerLength = UIUtils.getRulerLength(this._lastScreenSize, false);
		let boxSize = new Vector(rulerLength, rulerLength / Unicenter.BOX_ASPECT_RATIO)
			.scale(Unicenter.BOX_FRACTION)
			.round();
		let boxOrigin = Vector.diff(this._lastScreenSize, boxSize)
			.scale(0.5)
			.round();

		// Update sizes
		this._sizes.baseFontSize = boxSize.x * 20 / 800;
		this._sizes.marginWidth = boxSize.x * 10 / 800;

		let box = this.display.box;
		if (!this._isOpen) {
			box.visible = false;
			return [boxSize, boxOrigin];
		}

		box.visible = true;
		box
			.clear()
			.rect(boxOrigin.x, boxOrigin.y, boxSize.x, boxSize.y)
			.fill(this._tabs.get(this._selectedTab).colorBase)
			.stroke(this.requestStroke(
				this._tabs.get(this._selectedTab).colorHighlight, "WIDE"
			));

		return [boxSize, boxOrigin];
	}

	// Refresh the tab selectors
	// This is a sub-function of _refresh(). boxSize and boxOrigin are returned
	// by _refreshBox(). accessibleTabs is a set of all tab names accessible to
	// the player right now.
	_refreshTabSelectors(dt, boxSize, boxOrigin, accessibleTabs) {
		let tabSize = new Vector(boxSize.x * Unicenter.TAB_FRACTION).round();
		let iconSize = Vector.scale(tabSize, Unicenter.ICON_FRACTION).round();
		let tabRounding = Math.round(tabSize.x * Unicenter.TAB_ROUNDING);

		this.tabSelectorWidth = tabSize.x;

		let tabParamsClosed = new Map([
			[
				Tab.LOCATIONS.LEFT,
				index => [
					// Add tabRounding to the left to hide rounded corners
					// tabOrigin
					new Vector(
						- tabRounding,
						boxOrigin.y + index * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.add(new Vector(tabRounding, 0))
						.round(),
					// tooltipPosition
					new Vector(
						0,
						boxOrigin.y + index * tabSize.y
					).round(),
					// tooltipAlignment
					"RIGHT_MIDDLE"
				]
			],
			[
				Tab.LOCATIONS.RIGHT,
				index => [
					// Add tabRounding to the right to hide rounded corners
					// tabOrigin
					new Vector(
						this._lastScreenSize.x - tabSize.x,
						boxOrigin.y + index * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.round(),
					// tooltipPosition
					new Vector(
						this._lastScreenSize.x - tabSize.x,
						boxOrigin.y + index * tabSize.y
					).round(),
					// tooltipAlignment
					"LEFT_MIDDLE"
				]
			],
			[
				Tab.LOCATIONS.LEFT_BOTTOM,
				index => [
					// Add tabRounding to the left to hide rounded corners
					// tabOrigin
					new Vector(
						- tabRounding,
						boxOrigin.y + boxSize.y - (index + 1) * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.add(new Vector(tabRounding, 0))
						.round(),
					// tooltipPosition
					new Vector(
						0,
						boxOrigin.y + boxSize.y - (index + 1) * tabSize.y
					).round(),
					// tooltipAlignment
					"RIGHT_MIDDLE"
				]
			]
		]);

		let tabParamsOpen = new Map([
			[
				Tab.LOCATIONS.LEFT,
				index => [
					// Add tabRounding to the right to hide rounded corners
					// tabOrigin
					new Vector(
						boxOrigin.x - tabSize.x,
						boxOrigin.y + index * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.round(),
					// tooltipPosition
					new Vector(
						boxOrigin.x - tabSize.x,
						boxOrigin.y + index * tabSize.y
					).round(),
					// tooltipAlignment
					"RIGHT_MIDDLE"
				]
			],
			[
				Tab.LOCATIONS.RIGHT,
				index => [
					// Add tabRounding to the left to hide rounded corners
					// tabOrigin
					new Vector(
						boxOrigin.x + boxSize.x - tabRounding,
						boxOrigin.y + index * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.add(new Vector(tabRounding, 0))
						.round(),
					// tooltipPosition
					new Vector(
						boxOrigin.x + boxSize.x,
						boxOrigin.y + index * tabSize.y
					).round(),
					// tooltipAlignment
					"LEFT_MIDDLE"
				]
			],
			[
				Tab.LOCATIONS.LEFT_BOTTOM,
				index => [
					// Add tabRounding to the right to hide rounded corners
					// tabOrigin
					new Vector(
						boxOrigin.x - tabSize.x,
						boxOrigin.y + boxSize.y - (index + 1) * tabSize.y
					).round(),
					// rectSize
					new Vector(
						tabSize.x + tabRounding,
						tabSize.y
					).round(),
					// iconOrigin
					Vector.diff(tabSize, iconSize)
						.scale(0.5)
						.round(),
					// tooltipPosition
					new Vector(
						boxOrigin.x - tabSize.x,
						boxOrigin.y + boxSize.y - (index + 1) * tabSize.y
					).round(),
					// tooltipAlignment
					"RIGHT_MIDDLE"
				]
			]
		]);

		// How many tab selectors we have rendered from each location
		let locationCounts = new Map(Object.values(Tab.LOCATIONS).map(
			location => [location, 0]
		));

		for (let i = 0; i < this.display.tabSelectors.children.length; ++i) {
			let tabSelector = this.display.tabSelectors.children[i];
			let tabName = tabSelector.tabName;
			let tab = this._tabs.get(tabName);

			if (!accessibleTabs.has(tabName)) {
				tabSelector.visible = false;
				continue;
			}
			tabSelector.visible = true;

			let selected = this._isOpen && (tabName === this._selectedTab);

			let tabParams = this._isOpen ? tabParamsOpen : tabParamsClosed;
			let [tabOrigin, rectSize, iconOrigin, tooltipPosition, tooltipAlignment]
				= tabParams
				.get(tab.location)(locationCounts.get(tab.location));

			// Record that we rendered a tab at this location
			locationCounts.set(tab.location,
							locationCounts.get(tab.location) + 1);

			tabSelector.position.copyFrom(tabOrigin);

			tabSelector
				.clear()
				.roundRect(0, 0, rectSize.x, rectSize.y, tabRounding)
				.fill(selected ? tab.colorHighlight : tab.colorBase)
				.stroke(this.requestStroke(tab.colorHighlight, "WIDE"));

			let invIcon = tabSelector.children[0];
			invIcon.position.copyFrom(iconOrigin);
			invIcon.scale.set(iconSize.x / invIcon.texture.width);
			invIcon.tint = selected ? tab.colorBase
							: tab.colorHighlight;

			let tooltip = this._tabSelectorTooltips.get(tabName);
			tooltip.alignment = tooltipAlignment;
			tooltip.update(dt, tooltipPosition, tabSize);
		}
	}

	// Refresh the current tab
	// This is a sub-function of _refresh(). boxSize and boxOrigin are returned
	// by _refreshBox(). This function returns a vector in pixels representing
	// the size of the tab.
	_refreshTab(boxSize, boxOrigin) {
		let tabDisplayContainer = this.display.tabDisplayContainer;
		if (!this._isOpen) {
			tabDisplayContainer.visible = false;
			return new Vector();
		}

		tabDisplayContainer.visible = true;
		let tabOrigin = Vector.sum(
			boxOrigin,
			new Vector(this.requestMarginWidth(2))
		).round();
		this._tabOrigin.copyFrom(tabOrigin);
		tabDisplayContainer.position.copyFrom(tabOrigin);

		if (!this._tabs.has(this._selectedTab)) {
			logger.error("unicenter.js", "trying to open an unknown tab"
							+ ` "${this._selectedTab}"`);
			this._selectedTab = Array.from(this._tabs.keys())[0];
		}
		let tabDisplay = this._tabs.get(this._selectedTab).display;

		if ((tabDisplayContainer.children.length === 0)
						|| tabDisplayContainer.children[0]
						!== tabDisplay) {
			if (tabDisplayContainer.children.length > 1) {
				logger.warning("unicenter.js", "tab display container has"
								+ " multiple children, removing");
			}
			while (tabDisplayContainer.children.length > 0) {
				tabDisplayContainer.removeChildAt(0);
			}
			tabDisplayContainer.addChild(tabDisplay);
		}

		return Vector.diff(boxSize, new Vector(2 * this.requestMarginWidth(2)))
			.round();
	}
};
