import {Container, Graphics, Text} from "pixi.js";
import {Vector} from "../i.js";
import {UIUtils} from "../i.js";
import {Tab} from "../i.js";



class Puzzle {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this.board = [
			[0, 1, 2],
			[3, 4, 5],
			[6, 7, null]
		];

		this.tmpGraphics = new Graphics();
		this.display.addChild(this.tmpGraphics);
	}

	update(size) {
		this.tmpGraphics.clear();

		for (let row = 0; row < 3; ++row) {
			for (let col = 0; col < 3; ++col) {
				if (this.board[row][col] === null) continue;

				this.tmpGraphics
					.rect(col * size.x / 3, row * size.y / 3, size.x / 3, size.y / 3)
					.fill(UIUtils.hsv((3 * row + col) * 360 / 9, 100, 100));
			}
		}
	}
}



export class Cryohall extends Tab {
	// Create a new cryohall
	// Each unicenter should create exactly one cryohall.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(unicenter,
			new Set(["Origin Station"]),
			Tab.LOCATIONS.LEFT,
			"images/icons/upgrader.svg",
			"WHITE",
			"Lore"
		);

		this.puzzle = new Puzzle(this);
		this._whatIsThis = new Text(
			"I wonder what this is...\nStay tuned to find out!",
			UIUtils.getFont(this.colorHighlight)
		);
		this._whatIsThis.anchor.set(0, 1);
		this.display.addChild(this.puzzle.display, this._whatIsThis);
	}

	// Update the cryohall
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		if (visible) {
			// Handle inputs
		}

		super.update(visible, size);

		// Clear stuff
	}

	save() {
		return {};
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;
		this.puzzle.update(new Vector(size.y / 6, size.y / 6).round().scale(3));
		this._whatIsThis.style.fontSize = this.unicenter.requestFontSize("LARGE");
		this._whatIsThis.position.set(0, size.y);
	}
}
