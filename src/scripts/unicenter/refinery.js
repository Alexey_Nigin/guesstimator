import {Text} from "pixi.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {ITEM_INFO, RESOURCES} from "../i.js"; // ../drop.js
import {UIUtils} from "../i.js"; // ../ui/utils.js
import {Button, ResourceDisplay} from "../i.js"; // ./button.js
import {Tab} from "../i.js"; // ./tab.js

export class Refinery extends Tab {
	// Create a new refinery
	// Each unicenter should create exactly one refinery.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (array)
	constructor(unicenter, savefile) {
		super(
			unicenter,
			new Set(["Origin Station", "Forge Station", "Grand Station"]),
			Tab.LOCATIONS.LEFT,
			"images/icons/refinery.svg",
			120,
			"Refinery"
		);

		this._resources = new Map(Object.values(RESOURCES).map(
			resourceName => [resourceName, 0]
		));

		if (Array.isArray(savefile) && savefile.length >= 4) {
			this._resources.set(RESOURCES.MINERALS,    savefile[0]);
			this._resources.set(RESOURCES.ALLOYS,      savefile[1]);
			this._resources.set(RESOURCES.POLYMERS,    savefile[2]);
			this._resources.set(RESOURCES.LIGHTWEAVES, savefile[3]);
		}

		this.display.stationName = new Text({
			text: "Station",
			style: UIUtils.getFont(this.colorHighlight)
		});
		this.display.stationName.anchor.set(0, 0);
		this.display.stationName.position.set(0, 0);
		this.display.addChild(this.display.stationName);

		let refineAllButton = new Button(unicenter);
		this.display.refineAllButton = refineAllButton;
		this.display.addChild(refineAllButton);

		let resourceDisplay = new ResourceDisplay(this.unicenter);
		this.display.resourceDisplay = resourceDisplay;
		this.display.addChild(resourceDisplay);
	}

	// Update the refinery
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		if (this.display.refineAllButton.wasClicked()) this._refineAll();

		super.update(visible, size);

		this.display.refineAllButton.clearStuff();
	}

	// Save the state of the refinery into an object
	save() {
		return [
			this._resources.get(RESOURCES.MINERALS),
			this._resources.get(RESOURCES.ALLOYS),
			this._resources.get(RESOURCES.POLYMERS),
			this._resources.get(RESOURCES.LIGHTWEAVES)
		];
	}

	// Return true iff the player has enough resources to cover cost
	// Cost is a map from resource names to resource amounts.
	canAfford(cost) {
		for (let [resource, amountRequested] of cost.entries()) {
			if (!Object.values(RESOURCES).includes(resource)) {
				logger.error("refinery.js", `unknown resource ${resource}`);
				return false;
			}
			let amountAvailable = this._resources.get(resource);
			if (amountRequested > amountAvailable) return false;
		}

		return true;
	}

	// Deduct cost from the player's resources
	// Cost is a map from resource names to resource amounts. This method errors
	// out if !this.canAfford(cost).
	deduct(cost) {
		if (!this.canAfford(cost)) {
			logger.error("refinery.js", "trying to deduct too much");
			return;
		}

		for (let [resource, amountRequested] of cost.entries()) {
			this._resources.set(resource,
							this._resources.get(resource) - amountRequested);
		}
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;

		this.display.stationName.style.fontSize = this.unicenter.requestFontSize("LARGE");

		let refineAllButtonOrigin = new Vector(0, size.y / 2).round();
		let refineAllButtonSize = new Vector(
			size.x / 2 - this.unicenter.requestMarginWidth(1),
			size.y - refineAllButtonOrigin.y
		).round();
		let refineAllButton = this.display.refineAllButton;
		refineAllButton.update(
			this.colorBase,
			this.colorHighlight,
			refineAllButtonOrigin,
			refineAllButtonSize,
			"Refine all"
		);

		let resourcesOrigin = new Vector(
			size.x / 2 + this.unicenter.requestMarginWidth(1),
			size.y / 2
		).round();
		let resourcesSize = Vector.diff(size, resourcesOrigin).round();
		let numBlocks = this._resources.size;
		let iconHeight = Math.round(
			(resourcesSize.y - this.unicenter.requestMarginWidth(1)
							* (numBlocks - 1)) / numBlocks
		);
		this.display.resourceDisplay.update(this.colorHighlight, this._resources,
						iconHeight);
		this.display.resourceDisplay.position.copyFrom(resourcesOrigin);
	}

	_refineAll() {
		let items = this.unicenter.inventory.getAllItems();

		if (items.size > 0) this.unicenter.ship.reportStat("TIMES_REFINERY_USED", 1);

		for (let [itemName, itemCount] of items.entries()) {
			let refinesTo = ITEM_INFO.get(itemName).refinesTo;
			for (let [resourceName, resourceCount] of refinesTo.entries()) {
				this._resources.set(
					resourceName,
					this._resources.get(resourceName)
									+ resourceCount * itemCount
				);
			}
		}

		this.unicenter.inventory.clearAllItems();
	}
}
