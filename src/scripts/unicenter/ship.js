import {Container, Text} from "pixi.js";
import {round} from "../utils.js";
import {Vector} from "../i.js";
import {UIUtils} from "../i.js";
import {Button} from "../i.js"; // ./button.js
import {Tab} from "../i.js"; // ./tab.js

export const ACHIEVEMENTS = {
	UPGRADE: "UPGRADE",
	PRANK_BRO: "PRANK_BRO",
	ROCKY_START: "ROCKY_START",
	THRUSTERS: "THRUSTERS",
	COSSETTE: "COSSETTE",
	BEACONY: "BEACONY",
	COMPASS: "COMPASS",
	ESCAPE: "ESCAPE",
	DAMSEL: "DAMSEL",
	BOOMDASH: "BOOMDASH"
};

const ACHIEVEMENTS_INFO = new Map([
	[ACHIEVEMENTS.UPGRADE, {
		name: "???",
		description: "Get an upgrade"
	}],
	[ACHIEVEMENTS.PRANK_BRO, {
		name: "Just a prank, bro!",
		description: "Defeat the Gyroscope Prankster"
	}],
	[ACHIEVEMENTS.ROCKY_START, {
		name: "A rocky start",
		description: "Defeat a Meteorus"
	}],
	[ACHIEVEMENTS.THRUSTERS, {
		name: "???",
		description: "Forge the maneuvering thrusters"
	}],
	[ACHIEVEMENTS.COSSETTE, {
		name: "???",
		description: "Get a lift from Cossette"
	}],
	[ACHIEVEMENTS.BEACONY, {
		name: "???",
		description: "Alert a beacony"
	}],
	[ACHIEVEMENTS.COMPASS, {
		name: "???",
		description: "Destroy a compass"
	}],
	[ACHIEVEMENTS.ESCAPE, {
		name: "???",
		description: "Escape the game area"
	}],
	[ACHIEVEMENTS.DAMSEL, {
		name: "???",
		description: "Save the DAMSEL"
	}],
	[ACHIEVEMENTS.BOOMDASH, {
		name: "???",
		description: "Forge the boomdash"
	}]
]);


const STATS = [
	"DAMSEL_PHASE_1_COMPLETE",
	// DISTANCE_TRAVELED doesn't count movement while docking/docked to a station
	"DISTANCE_TRAVELED",
	"BLASTER_BOLTS_FIRED",
	"SHATTERS",
	"TIME_ACTIVE",
	"TIME_IN_DAMSELS_DESOLATION", // TODO: Count only active time?
	"TIME_IN_DATE_MODE", // Only counts active time
	"TIME_NEAR_DAMSEL",
	"TIMES_REFINERY_USED",
	"UPGRADES_OBTAINED"
];



class MainSubTab {
	constructor(tab) {
		this.tab = tab;
		this.display = new Container();

		this._switchNumPlayers = new Button(tab.unicenter);

		this._achievementsButton = new Button(tab.unicenter);
		this.display.addChild(this._switchNumPlayers, this._achievementsButton);
	}

	// Return name of the tab to switch to, or null
	wantToSwitch() {
		return this._achievementsButton.wasClicked() ? "ACHIEVEMENTS" : null;
	}

	update(size, visible) {
		if (!visible) {
			this.display.visible = false;
			this._clearStuff();
			return;
		}
		this.display.visible = true;

		if (this._switchNumPlayers.wasClicked()) {
			this.tab.numPlayers = 3 - this.tab.numPlayers;
		}

		let switchButtonSize = new Vector(size.x / 2, size.y / 4).round();
		this._switchNumPlayers.update(
			this.tab.colorBase,
			this.tab.colorHighlight,
			new Vector(),
			switchButtonSize,
			(this.tab.numPlayers === 1) ? "Single mode" : "Date mode\n(I) (J) (K) (L) (M)"
		);

		let achievementsButtonSize = new Vector(size.x / 3, size.y / 4).round();
		let achievementsButtonPosition = Vector.diff(size, achievementsButtonSize);
		this._achievementsButton.update(
			this.tab.colorBase,
			this.tab.colorHighlight,
			achievementsButtonPosition,
			achievementsButtonSize,
			"Achievements"
		);

		this._clearStuff();
	}

	_clearStuff() {
		this._switchNumPlayers.clearStuff();
		this._achievementsButton.clearStuff();
	}
}



class AchievementsSubTab {
	constructor(tab, savefile) {
		this.tab = tab;
		this.display = new Container();

		this._completion = new Map(
			Object.values(ACHIEVEMENTS).map(
				(achievement) => [achievement, (savefile ?? {})[achievement] ?? false]
			)
		);

		this._backButton = new Button(tab.unicenter);
		this._text = new Text("", UIUtils.getFont(tab.colorHighlight));
		this.display.addChild(this._backButton, this._text);
	}

	// Return name of the tab to switch to, or null
	wantToSwitch() {
		return this._backButton.wasClicked() ? "MAIN" : null;
	}

	update(size, visible) {
		this._backButton.clearStuff();

		if (!visible) {
			this.display.visible = false;
			return;
		}
		this.display.visible = true;

		let backButtonSize = new Vector(size.x / 3, size.y / 4).round();
		let backButtonPosition = new Vector();
		this._backButton.update(
			this.tab.colorBase,
			this.tab.colorHighlight,
			backButtonPosition,
			backButtonSize,
			"Back"
		);

		let textPosition = new Vector(
			0,
			backButtonPosition.y + backButtonSize.y
							+ this.tab.unicenter.requestMarginWidth(2)
		);
		this._text.position.copyFrom(textPosition);
		this._text.style.fontSize = this.tab.unicenter.requestFontSize("SMALL");
		this._text.text = Object.values(ACHIEVEMENTS)
			.map(achievement => {
				let isObtained = this._completion.get(achievement)
								? "\u2612" : "\u2610";
				let description = ACHIEVEMENTS_INFO.get(achievement).description;
				return `${isObtained} ${description}`;
			})
			.join("\n");
	}

	save() {
		return Object.fromEntries(this._completion);
	}

	recordAchievement(achievement) {
		this._completion.set(achievement, true);
	}
}



export class Ship extends Tab {
	// Create a new ship monitor
	// Each unicenter should create exactly one ship monitor.
	//
	// Args:
	// unicenter - reference to the unicenter
	// savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(
			unicenter,
			null,
			Tab.LOCATIONS.RIGHT,
			"images/icons/ship.svg",
			270,
			"Ship"
		);

		this.numPlayers = 1;

		this.achievements = new AchievementsSubTab(this, savefile?.a);
		this._subTabs = new Map([
			["MAIN", new MainSubTab(this)],
			["ACHIEVEMENTS", this.achievements]
		]);
		this._subTabs.forEach(subTab => this.display.addChild(subTab.display));
		this._selectedSubTab = "MAIN";

		// Stats temporarily live here, will eventually be moved to a separate sub-tab
		this._stats = new Map(
			STATS.map(
				(statName) => [statName, (savefile?.s ?? {})[statName] ?? 0]
			)
		);
	}

	// Add delta to stat
	reportStat(stat, delta) {
		this._stats.set(stat, this._stats.get(stat) + delta);
	}

	getStat(stat) {
		return this._stats.get(stat);
	}

	learnedBasics() {
		return this._stats.get("DISTANCE_TRAVELED") > 3
			&& this._stats.get("BLASTER_BOLTS_FIRED") >= 3;
	}

	// Update the ship
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		super.update(visible, size);
	}

	save() {
		return {
			a: this.achievements.save(),
			s: Object.fromEntries(
				Array.from(this._stats.entries())
					.map(entry => [entry[0], round(entry[1])])
			)
		};
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;

		let desiredSubTab = this._subTabs.get(this._selectedSubTab).wantToSwitch();
		if (desiredSubTab !== null) this._selectedSubTab = desiredSubTab;

		this._subTabs.forEach(
			(subTab, name) => subTab.update(size, name === this._selectedSubTab)
		);
	}
}
