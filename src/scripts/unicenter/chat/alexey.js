import {MESSAGE_BANK} from "./bank.js";
import Brain, {Topic} from "./brain.js";

import {Random} from "../../i.js";



class AlexeyTopicGreeting extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);
	}

	update(dt) {
		super.update(dt);

		let timeActive = this.tab.unicenter.ship.getStat("TIME_ACTIVE");
		if (!this.covered && !this.active && timeActive > 30) {
			if (this.requestActivation()) {
				let startMessageId = ["GREETING", "GREETING_MISSPELLED"][
					Random.weighted([0.9, 0.1])
				];
				this.startTyping(startMessageId);
			}
		}
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "GREETING_MISSPELLED": {
				this.startTyping("CORRECT_GREETING_MISSPELLING");
				return;
			}
			case "GREETING":
			case "CORRECT_GREETING_MISSPELLING": {
				this.startTyping("INTRO");
				return;
			}
			case "INTRO": {
				this.startTyping("GIVE_TIME");
				return;
			}
			case "GIVE_TIME": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class AlexeyTopicRespawn extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;

		// Set to true if the player shatters before the greeting finishes
		this._earlyShatter = savefile?.earlyShatter ?? false;
	}

	update(dt) {
		super.update(dt);

		let numShatters = this.tab.unicenter.ship.getStat("SHATTERS");
		if (!this.covered && !this.active && numShatters > 0) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("GREETING")) {
				this._earlyShatter = true;
				return;
			}
			if (this.requestActivation()) {
				let messageId = "OUCH";
				if (this._earlyShatter) messageId = "EARLY_OUCH";
				this.startTyping(messageId);
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		config.earlyShatter = this._earlyShatter;
		return config;
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "OUCH":
			case "EARLY_OUCH": {
				this.startTyping("SHATTER");
				return;
			}
			case "SHATTER": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class AlexeyTopicBasicsHint extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		let timeActive = this.tab.unicenter.ship.getStat("TIME_ACTIVE");
		let distanceTraveled = this.tab.unicenter.ship.getStat("DISTANCE_TRAVELED");
		let blasterBoltsFired = this.tab.unicenter.ship.getStat("BLASTER_BOLTS_FIRED");
		if (this.tab.unicenter.ship.learnedBasics()) this.covered = true;
		if (
			!this.covered && !this.active
			&& timeActive > 2 * 60
		) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("GREETING")) return;

			if (this.requestActivation()) {
				if (distanceTraveled < 3) {
					if (blasterBoltsFired < 3) {
						this.startTyping("HINT_THRUST_BLASTER");
					} else {
						this.startTyping("HINT_THRUST");
					}
				} else this.startTyping("HINT_BLASTER");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}
}



class AlexeyTopicUpgradeHint extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		let timeActive = this.tab.unicenter.ship.getStat("TIME_ACTIVE");
		let upgradesObtained = this.tab.unicenter.ship.getStat("UPGRADES_OBTAINED");

		if (upgradesObtained > 0) this.covered = true;
		if (
			!this.covered && !this.active
			&& timeActive > 6 * 60
			// Wait for the player to learn the basics hint
			&& this.tab.unicenter.ship.learnedBasics()
		) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("BASICS_HINT")) return;

			if (this.requestActivation()) {
				this.startTyping("NEED_UPGRADES");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "NEED_UPGRADES": {
				let timesRefineryUsed = this.tab.unicenter.ship
					.getStat("TIMES_REFINERY_USED");
				switch (timesRefineryUsed) {
					case 0: {
						this.startTyping("HINT_REFINING");
						return;
					}
					case 1: {
						this.startTyping("HINT_REFINING_REMINDER");
						return;
					}
					default: {
						this.startTyping("HINT_UPGRADE");
						return;
					}
				}
			}
			case "HINT_REFINING":
			case "HINT_REFINING_REMINDER": {
				this.startTyping("HINT_UPGRADE");
				return;
			}
			case "HINT_UPGRADE": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class AlexeyTopicDateMode extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		let timeInDateMode = this.tab.unicenter.ship.getStat("TIME_IN_DATE_MODE");
		if (!this.covered && !this.active && timeInDateMode > 10) {
			this._wantToCover = true;
		}

		if (this._wantToCover) {
			if (!this.brain.covered("GREETING")) return;

			if (this.requestActivation()) {
				this.startTyping("DATE_MODE_TRY");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "DATE_MODE_TRY": {
				this.startTyping("DATE_MODE_HISTORY");
				return;
			}
			case "DATE_MODE_HISTORY": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



export default class AlexeyBrain extends Brain {
	// I wish I could type this fast in real life
	static WPM = 70;

	constructor(tab, savefile) {
		super(tab, 0, savefile);

		function topicSave(i) {
			if (!Array.isArray(savefile?.topics)) return undefined;
			return savefile.topics[i];
		}

		this.topics = new Map([
			["GREETING",     new AlexeyTopicGreeting(tab, this, topicSave(0))   ],
			["RESPAWN",      new AlexeyTopicRespawn(tab, this, topicSave(1))    ],
			["BASICS_HINT",  new AlexeyTopicBasicsHint(tab, this, topicSave(2)) ],
			["UPGRADE_HINT", new AlexeyTopicUpgradeHint(tab, this, topicSave(3))],
			["DATE_MODE",    new AlexeyTopicDateMode(tab, this, topicSave(4))   ]
		]);
	}

	calculateTimeToType(messageId) {
		let message = MESSAGE_BANK[0].get(messageId, "");
		let numWords = message.split(" ").length;
		return numWords * (60 / AlexeyBrain.WPM);
	}
}
