import {Container, Graphics, Rectangle, Sprite, Text} from "pixi.js";

import {PEOPLE_INFO} from "./bank.js";

import {logger} from "../../i.js";
import {getTexture} from "../../assets.js";
import {Vector} from "../../i.js";
import {Clicker, DynamicContainer, UIUtils} from "../../i.js";
import {Tab} from "../../i.js";



class Person {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._pfp = new Sprite();
		this._name = UIUtils.getBitmapText("", tab.colorHighlight);
		this._lastMsg = UIUtils.getBitmapText("", tab.colorHighlight);
		this._border = new Graphics();

		this.display.addChild(this._pfp, this._name, this._lastMsg, this._border);

		this._clicker = new Clicker(this.display);
	}

	// Args:
	//   info - object {
	//     name    - string
	//     pfp     - filename string
	//     lastMsg - string
	//   }
	//   position  - vector in pixels
	//   size      - vector in pixels
	update(info, position, size) {
		let narrowMargin = this.tab.unicenter.requestMarginWidth(1);

		this.display.hitArea = new Rectangle(0, 0, size.x, size.y);

		this.display.position.copyFrom(position);

		this._pfp.texture = getTexture(info.pfp);
		this._pfp.position.set(narrowMargin, narrowMargin);
		this._pfp.width = this._pfp.height = size.y - 2 * narrowMargin;

		this._name.text = info.name;
		this._name.style.fontSize = this.tab.unicenter.requestFontSize("SMALL");
		this._name.position.set(this._pfp.width + 2 * narrowMargin, narrowMargin);

		this._lastMsg.style.fontSize = this.tab.unicenter.requestFontSize("SMALL");
		this._setLastMsgText(info.lastMsg, size.x - this._pfp.width - 3 * narrowMargin);
		this._lastMsg.position.set(
			this._pfp.width + 2 * narrowMargin,
			size.y - this._lastMsg.height - narrowMargin
		);

		this._border
			.clear()
			.rect(
				0, 0,
				size.x, size.y
			)
			.stroke(
				this.tab.unicenter.requestStroke(this.tab.colorHighlight, "NARROW")
			);

		let switchConversationToMe = this._clicker.wasClicked();
		this._clicker.clearStuff();
		return switchConversationToMe;
	}

	// Call after setting lastMsg font size/style
	_setLastMsgText(fullText, maxWidth) {
		// Enough room for full text?
		this._lastMsg.text = fullText;
		if (this._lastMsg.width <= maxWidth) return;

		// Binary search time
		console.assert(fullText.length > 4);
		let minLength = 1;
		let maxLength = fullText.length - 3;
		while (maxLength - minLength > 1) {
			let midLength = Math.floor((minLength + maxLength) / 2);
			this._lastMsg.text = `${fullText.substring(0, midLength)}...`;
			if (this._lastMsg.width <= maxWidth) {
				minLength = midLength;
			} else {
				maxLength = midLength;
			}
		}

		this._lastMsg.text = `${fullText.substring(0, minLength)}...`;
	}
}



export class Selector {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._people = new DynamicContainer(() => new Person(tab));
		this.display.addChild(this._people.display);

		this._border = new Graphics();
		this.display.addChild(this._border);
	}

	// Args:
	//   info - array of objects {
	//     id      - id in PEOPLE_INFO
	//     lastMsg - string
	//   }
	//   position  - vector in pixels
	//   size      - vector in pixels
	update(info, position, size) {
		this.display.position.copyFrom(position);

		let personOrigin = new Vector();
		let personSize = new Vector(size.x, size.y / 8).round();

		this._people.setSize(info.length);
		let switchConversationTo = null;
		info.forEach((personInfo, i) => {
			if (this._people.getElement(i).update(
				{
					name: PEOPLE_INFO[personInfo.id].name,
					pfp: PEOPLE_INFO[personInfo.id].pfp,
					lastMsg: personInfo.lastMsg
				},
				personOrigin, personSize
			)) switchConversationTo = personInfo.id;
			personOrigin.y += personSize.y - 1; // -1 for margin width
		});

		this._border
			.clear()
			.rect(
				0, 0,
				size.x, size.y
			)
			.stroke(
				this.tab.unicenter.requestStroke(this.tab.colorHighlight, "NARROW")
			);

		return switchConversationTo;
	}
}



class Message {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();
		this._icon = new Sprite(getTexture("images/icons/profile.svg"));
		this._icon.tint = tab.colorHighlight;
		this._background = new Graphics();
		this._textDisplay = new Text({
			text: "",
			style: UIUtils.getFont(tab.colorHighlight, {wordWrap: true})
		});
		this.display.addChild(this._icon, this._background, this._textDisplay);
	}

	// Returns the height of the rendered message
	update(message, tabWidth, position) {
		let margin = this.tab.unicenter.requestMarginWidth(1);

		this.display.position.copyFrom(position);

		this._icon.scale.set(0.1 * tabWidth / this._icon.texture.width);

		this._textDisplay.text = message;
		this._textDisplay.style.fontSize = this.tab.unicenter.requestFontSize("SMALL");
		this._textDisplay.style.wordWrapWidth = 2 / 3 * tabWidth;

		let boxSize = new Vector(
			this._textDisplay.width + 2 * margin,
			this._textDisplay.height + 2 * margin
		).round();
		let boxPosition = new Vector(this._icon.width + margin, 0).round();
		if (boxSize.y < this._icon.height) boxPosition.y = Math.round(
			(this._icon.height - boxSize.y) / 2
		);

		this._textDisplay.position.copyFrom(Vector.sum(
			boxPosition, new Vector(margin, margin)
		));
		this._background
			.clear()
			.rect(
				boxPosition.x, boxPosition.y,
				boxSize.x, boxSize.y
			)
			.stroke(
				this.tab.unicenter.requestStroke(this.tab.colorHighlight, "NARROW")
			);

		return Math.max(boxSize.y, Math.round(this._icon.height));
	}
}



export class Conversation {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._messages = new DynamicContainer(() => new Message(tab));
		this._messagesMask = new Graphics();
		this._messages.display.mask = this._messagesMask;
		this._border = new Graphics();

		this.display.addChild(this._messages.display, this._messagesMask, this._border);
	}

	// Args:
	//   info      - array of message strings
	//   position  - vector in pixels
	//   size      - vector in pixels
	update(info, origin, size) {
		let narrowMargin = this.tab.unicenter.requestMarginWidth(1);

		this.display.position.copyFrom(origin);

		this._messagesMask
			.clear()
			.rect(
				0, 0,
				size.x, size.y
			)
			.fill(0xffffff);

		let messageOrigin = new Vector(narrowMargin, narrowMargin);

		this._messages.setSize(info.length);
		info.forEach((message, i) => {
			let messageHeight = this._messages.getElement(i).update(
				message,
				size.x, messageOrigin
			);
			messageOrigin.y += messageHeight
				+ this.tab.unicenter.requestMarginWidth(2);
		});
		this._messages.display.position.y = size.y - messageOrigin.y;

		this._border
			.clear()
			.rect(
				0, 0,
				size.x, size.y
			)
			.stroke(
				this.tab.unicenter.requestStroke(this.tab.colorHighlight, "NARROW")
			);
	}
}
