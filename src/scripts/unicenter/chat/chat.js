import {Container} from "pixi.js";

import {MESSAGE_BANK, PEOPLE_INFO} from "./bank.js";
import AlexeyBrain from "./alexey.js";
import DamselBrain from "./damsel.js";
import {Conversation, Selector} from "./elements.js";

import {round} from "../../utils.js";
import {logger} from "../../i.js";
import {Vector} from "../../i.js";
import {UIUtils} from "../../i.js";
import {Tab} from "../../i.js";



// Same order as in PEOPLE_INFO
const BRAINS = [
	AlexeyBrain,
	DamselBrain
];



export class Chat extends Tab {
	// Create a new chat
	// Each unicenter should create exactly one chat.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(
			unicenter,
			null,
			Tab.LOCATIONS.RIGHT,
			"images/icons/chat.svg",
			60,
			"Chat"
		);

		// Indices match PEOPLE_INFO
		if (Array.isArray(savefile?.conversations)
						&& savefile.conversations.length === PEOPLE_INFO.length) {
			this._conversationsInfo = savefile.conversations.map(
				(convo, i) => ({
					brain: new BRAINS[i](this, convo.brain),
					messages: convo.messages,
					lastMsgTime: convo.lastMsgTime
				})
			);
		} else {
			this._conversationsInfo = BRAINS.map(BrainClass => ({
				brain: new BrainClass(this),
				messages: [],
				lastMsgTime: -1
			}));
		}
		// For this.getLastMessageInfo() - TODO make less hacky
		this._conversationsInfo.forEach((convo, index) => {convo.index = index});
		// Index in PEOPLE_INFO
		// TODO: Make nullable
		this._selectedConversation = savefile?.selectedConversation ?? 0;

		// Visuals

		this._selector = new Selector(this);
		this._conversation = new Conversation(this);

		this._emptyNote = new Container();
		this._emptyNoteTop = UIUtils.getBitmapText(
			"Nobody has texted you yet.", this.colorHighlight
		);
		this._emptyNoteBottom = UIUtils.getBitmapText(
			"Will you die alone?", this.colorHighlight
		);
		this._emptyNote.addChild(this._emptyNoteTop, this._emptyNoteBottom);

		this.display.addChild(
			this._emptyNote,
			this._selector.display,
			this._conversation.display
		);
	}

	// Update the chat
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size, dt) {
		super.update(visible, size);

		this._conversationsInfo.forEach(
			conversationInfo => conversationInfo.brain.update(dt)
		);
	}

	save() {
		return {
			conversations: this._conversationsInfo.map(convo => ({
				brain: convo.brain.save(),
				messages: Array.from(convo.messages),
				lastMsgTime: round(convo.lastMsgTime)
			})),
			selectedConversation: this._selectedConversation
		};
	}

	postMessage(personId, message) {
		let conversationInfo = this._conversationsInfo[personId];
		conversationInfo.messages.push(message);
		// TODO: Accessing space here is a bit hacky
		conversationInfo.lastMsgTime = this.unicenter.game.space.time;
	}

	// Returns [lastMsgTime, senderName, messageText]
	getLastMessageInfo() {
		let latestConversation = this._conversationsInfo.reduce(
			(convoA, convoB) => convoA.lastMsgTime > convoB.lastMsgTime ? convoA : convoB
		);
		if (latestConversation.lastMsgTime < 0) return [-1, "", ""];

		if (latestConversation.messages.length === 0) {
			this.logger.warning("chat.js", "last message not found");
			return [-1, "", ""];
		}

		return [
			latestConversation.lastMsgTime,
			PEOPLE_INFO[latestConversation.index].name,
			MESSAGE_BANK[latestConversation.index].get(
				latestConversation.messages[latestConversation.messages.length - 1]
			)
		];
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;
		let wideMargin = this.unicenter.requestMarginWidth(2);

		let noMessages = this._conversationsInfo.every(
			conversationInfo => (conversationInfo.messages.length === 0)
		);
		if (noMessages) {
			this._emptyNote.visible = true;
			this._selector.visible = false;
			this._conversation.visible = false;

			this._emptyNoteTop.style.fontSize = this.unicenter.requestFontSize("LARGE");
			this._emptyNoteBottom.style.fontSize
				= this.unicenter.requestFontSize("SMALL");

			let topOrigin = new Vector(
				(size.x - this._emptyNoteTop.width) / 2,
				size.y / 2 - wideMargin - this._emptyNoteTop.height
			).round();
			let bottomOrigin = new Vector(
				(size.x - this._emptyNoteBottom.width) / 2,
				size.y / 2 + wideMargin
			).round();
			this._emptyNoteTop.position.copyFrom(topOrigin);
			this._emptyNoteBottom.position.copyFrom(bottomOrigin);

			return;
		}

		this._emptyNote.visible = false;
		this._selector.visible = true;
		this._conversation.visible = true;

		let selectorOrigin = new Vector();
		let selectorSize = new Vector(0.3 * (size.x - wideMargin), size.y).round();
		let conversationOrigin = new Vector(selectorSize.x + wideMargin, 0);
		let conversationSize = Vector.diff(size, conversationOrigin);

		let switchConversationTo = this._selector.update(
			this._conversationsInfo
				.map((entry, id) => ({
					id: id,
					lastMsg: entry.messages.length === 0
						? null
						: MESSAGE_BANK[id].get(entry.messages[entry.messages.length - 1]),
					lastMsgTime: entry.lastMsgTime
				}))
				.toSorted((entryA, entryB) => entryB.lastMsgTime - entryA.lastMsgTime)
				.filter(entry => entry.lastMsg !== null)
				.map(({id, lastMsg}) => ({id, lastMsg})),
			selectorOrigin, selectorSize
		);

		if (switchConversationTo !== null) {
			this._selectedConversation = switchConversationTo;
		}

		this._conversation.update(
			this._conversationsInfo[this._selectedConversation].messages.map(
				message => MESSAGE_BANK[this._selectedConversation].get(message)
			),
			conversationOrigin, conversationSize
		);
	}
}
