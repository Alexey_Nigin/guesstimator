import {round} from "../../utils.js";
import {logger} from "../../i.js";

export class Topic {
	constructor(tab, brain, savefile) {
		this.tab = tab;
		this.brain = brain;

		// Whether the person is currently focused on this topic
		// To prevent a person from talking about 2 things at the same time, at most
		// 1 topic can be active at any time.
		this.active = savefile?.active ?? false;

		// Whether the topic has already been covered in the chat
		// Can be checked by other topics. By default, set to true when the topic is
		// deactivated.
		this.covered = savefile?.covered ?? false;

		// {message, remainingTime} or null
		if (savefile?.typingMessage !== undefined
						&& savefile?.typingRemainingTime !== undefined) {
			this._typing = {
				message: savefile.typingMessage,
				remainingTime: savefile.typingRemainingTime
			};
		} else {
			this._typing = null;
		}
	}

	update(dt) {
		if (this._typing !== null) this._progressTyping(dt);
	}

	save() {
		// TODO: Saving this.active here is redundant - it's already saved in
		// Brain.activeTopic
		let config = {
			active: this.active,
			covered: this.covered
		};

		if (this._typing !== null) {
			config.typingMessage = this._typing.message;
			config.typingRemainingTime = round(this._typing.remainingTime);
		}

		return config;
	}

	// Return true if activation successful, false if some other topic is already active
	// and this topic should try again later
	requestActivation() {
		return (this.active = this.brain.requestActivation(this));
	}

	requestDeactivation() {
		this.brain.requestDeactivation(this);
		this.active = false;
		this.covered = true;
	}

	startTyping(messageId) {
		if (this._typing !== null) logger.warning("chat.js",
			`overwriting unfinished message ${this._typing.message}`
		);
		this._typing = {
			message: messageId,
			remainingTime: this.brain.calculateTimeToType(messageId)
		};
	}

	// Feel free to override this event listener
	onTypingComplete(messageId) {
		this.requestDeactivation();
	}

	_progressTyping(dt) {
		this._typing.remainingTime -= dt;
		if (this._typing.remainingTime <= 0) {
			let message = this._typing.message;
			this.tab.postMessage(this.brain.index, message);
			this._typing = null;
			this.onTypingComplete(message);
		}
	}
}

export default class Brain {
	// List topics with highest priority first
	constructor(tab, index, savefile) {
		this.tab = tab;

		this.topics = new Map();

		this.index = index;

		// Null or key of the topic
		this.activeTopic = savefile?.activeTopic ?? null;
	}

	update(dt) {
		for (let topic of this.topics.values()) topic.update(dt);
	}

	save() {
		return {
			topics: Array.from(this.topics.values()).map(topic => topic.save()),
			activeTopic: this.activeTopic
		};
	}

	// Return true if activation successful, false if some other topic is already active
	// and this topic should try again later
	// Args:
	//   topic - reference to the topic
	requestActivation(topic) {
		if (this.activeTopic !== null) {
			if (topic === this.topics.get(this.activeTopic)) {
				logger.warning("chat.js",
					`topic ${this.activeTopic} requested activation twice in a row`
				);
				return true;
			}
			return false;
		}

		for (let [key, value] of this.topics.entries()) {
			if (value === topic) {
				this.activeTopic = key;
				return true;
			}
		}
		logger.error("chat.js", "topic not found");
		return false;
	}

	requestDeactivation(topic) {
		if (this.activeTopic === null) {
			logger.error("chat.js",
				"requested deactivation of a topic when no topic is active"
			);
			return;
		}
		if (topic !== this.topics.get(this.activeTopic)) {
			logger.error("chat.js",
				"deactivating a different topic than what was activated"
			);
			return;
		}

		this.activeTopic = null;
	}

	// Return true iff topic has been covered
	// Args:
	//   topicKey - key in this.topics
	covered(topicKey) {
		if (!this.topics.has(topicKey)) {
			logger.error("chat.js",
				`covered() called on non-existent topic ${topicKey}`
			);
			return false;
		}

		return this.topics.get(topicKey).covered;
	}

	// Override in subclasses
	calculateTimeToType(messageId) {
		return 1;
	}
}
