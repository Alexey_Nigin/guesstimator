import Brain, {Topic} from "./brain.js";



class DamselTopicGreeting extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);
	}

	update(dt) {
		super.update(dt);

		if (this.covered || this.active) return;

		let timeInArea = this.tab.unicenter.ship.getStat("TIME_IN_DAMSELS_DESOLATION");
		if (timeInArea > 20) {
			if (this.requestActivation()) {
				this.startTyping("GREETING");
			}
		}
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "GREETING": {
				this.startTyping("HERO");
				return;
			}
			case "HERO": {
				this.startTyping("SAVE_ME");
				return;
			}
			case "SAVE_ME": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class DamselTopicCallToAction extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		if (this.covered || this.active) return;

		let timeInArea = this.tab.unicenter.ship.getStat("TIME_IN_DAMSELS_DESOLATION");
		let timeNearDamsel = this.tab.unicenter.ship.getStat("TIME_NEAR_DAMSEL");

		if (timeNearDamsel > 0) {
			this.covered = true;
			this._wantToCover = false;
			return;
		}

		if (timeInArea > 120) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("GREETING")) return;

			if (this.requestActivation()) {
				this.startTyping("BACKSTORY");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "BACKSTORY": {
				this.startTyping("CALL_TO_ACTION");
				return;
			}
			case "CALL_TO_ACTION": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class DamselTopicMeeting extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		if (this.covered || this.active) return;

		let timeNearDamsel = this.tab.unicenter.ship.getStat("TIME_NEAR_DAMSEL");

		if (timeNearDamsel > 0) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("GREETING")) return;

			if (this.requestActivation()) {
				this.startTyping("MEETING");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}

	onTypingComplete(messageId) {
		switch (messageId) {
			case "MEETING": {
				this.startTyping("ENCOURAGEMENT");
				return;
			}
			case "ENCOURAGEMENT": {
				this.requestDeactivation();
				return;
			}
		}
	}
}



class DamselTopicSaved extends Topic {
	constructor(tab, brain, savefile) {
		super(tab, brain, savefile);

		this._wantToCover = savefile?.wantToCover ?? false;
	}

	update(dt) {
		super.update(dt);

		if (this.covered || this.active) return;

		let phase1Complete = this.tab.unicenter.ship.getStat("DAMSEL_PHASE_1_COMPLETE");

		if (phase1Complete > 0) this._wantToCover = true;

		if (this._wantToCover) {
			if (!this.brain.covered("MEETING")) return;

			if (this.requestActivation()) {
				this.startTyping("SAVED");
				this._wantToCover = false;
			}
		}
	}

	save() {
		let config = super.save();
		config.wantToCover = this._wantToCover;
		return config;
	}
}



export default class DamselBrain extends Brain {
	constructor(tab, savefile) {
		super(tab, 1, savefile);

		function topicSave(i) {
			if (!Array.isArray(savefile?.topics)) return undefined;
			return savefile.topics[i];
		}

		this.topics = new Map([
			["GREETING",       new DamselTopicGreeting(tab, this, topicSave(0))    ],
			["CALL_TO_ACTION", new DamselTopicCallToAction(tab, this, topicSave(1))],
			["MEETING",        new DamselTopicMeeting(tab, this, topicSave(2))     ],
			["SAVED",          new DamselTopicSaved(tab, this, topicSave(3))       ]
		]);
	}

	calculateTimeToType(messageId) {
		return 3;
	}
}
