// Order matters! People are referenced by index
export const PEOPLE_INFO = [
	{
		name: "Alexey",
		pfp: "images/icons/profile.svg"
	},
	{
		name: "DAMSEL",
		pfp: "images/desolation/damsel.svg" // TODO: Draw real pfp
	}
];



// Indexed on people
export const MESSAGE_BANK = [
	// 0 - Alexey
	new Map([
		// Topic: Greeting
		["GREETING", "wazzup!"],
		["GREETING_MISSPELLED", "wazzuo!"],
		["CORRECT_GREETING_MISSPELLING", "*wazzup! typing is hard"],
		["INTRO", "my name is alexey, and i will be guiding you on your journey!"],
		["GIVE_TIME",
			"i'll give you some time to try things out on your own, and i will "
				+ "check in if it looks like you're stuck. have fun!"
		],
		// Topic: Respawn
		["OUCH", "ouch! that didn't look good"],
		["EARLY_OUCH", "wait, did you already shatter? oh."],
		["SHATTER",
			"don't worry, shattering isn't death, and it's perfectly normal. "
				+ "you just temporarily lose your physical form, "
				+ "but you can respawn and keep going. "
				+ "happens to the best of us :)"
		],
		// Topic: Basics Hint
		["HINT_THRUST",
			"hey, just a small hint - you can fly around by holding (W). "
				+ "have fun exploring!"
		],
		["HINT_BLASTER",
			"hey, just a small hint - you can shoot by holding the left mouse "
				+ "button, it's a lot less painful than just running into the asteroids "
				+ "lol"
		],
		["HINT_THRUST_BLASTER",
			"hey, just a small hint - you can fly around by holding (W) "
				+ "and shoot by holding the left mouse button. "
				+ "go destroy some stuff :)"
		],
		// TODO: Intermediate hint about docking to a station
		// TODO: Split hints for refining and upgrading
		// Topic: Upgrade Hint
		["NEED_UPGRADES", "you know what, you should upgrade your ship"],
		["HINT_REFINING",
			"when you destroy stuff you get items, but you can't directly use these "
				+ "items to upgrade your ship - you firstly need to refine the items "
				+ "into resources. dock to a station (get near a station and press (X)), "
				+ "find the refinery tab, and refine!"
		],
		["HINT_REFINING_REMINDER",
			"when you destroy stuff you get items, but you can't directly use these "
				+ "items to upgrade your ship - you firstly need to refine the items "
				+ "into resources. i see you have already done this once, but just as a "
				+ "reminder: dock to a station, find the refinery tab, & refine!"
		],
		["HINT_UPGRADE",
			"to get an upgrade, dock to a station with an upgrader (the origin station, "
				+ "for example), find the upgrade tab, and get whatever upgrade is most "
				+ "to your liking!"
		],
		// Topic: Date Mode
		["DATE_MODE_TRY",
			"i see you decided to try out date mode!"
		],
		["DATE_MODE_HISTORY",
			"i read that in times of old, station-builder captains would always invite "
				+ "their honeys to join their space adventures. it was considered peak "
				+ "romance - exploring the unknown and vanquishing nightmares of deep "
				+ "space together. i am glad to see you revive this tradition today"
		]
	]),
	// 1 - DAMSEL
	new Map([
		// Topic: Greeting
		["GREETING", "OwO"],
		["HERO", "Are y-you.. a hewo? :3 >~< *blushes*"],
		["SAVE_ME", "~~ Did you come to sawe.. me???~~~ UwU *sobs* ~~"],
		// Topic: Call to Action
		["BACKSTORY",
			"Maybe you don't even.. know.. who I am? aww~~~ "
				+ "I am a suuuuper kawaii girl who just wanted to spread luv "
				+ "and snuggles UwU ~~~ "
				+ "But!!!! I was impwisoned in a fowce field castle in the middle of "
				+ "this howwible howwible maze OwO~~ Now I am soo lonely :'("
		],
		["CALL_TO_ACTION",
			"Only a bwave handsome hewo liek y-you can sawe me!! UwU *blushes* "
				+ "Pweaaaase player-kun come sawe me so we can roam space together and "
				+ "be.. fwieeeends!! *passes out*"
		],
		// Topic: Meeting
		["MEETING",
			"OwO Senpai noticed meeee~~~~ You weally came to sawe poor me!! aaaaa :0 ~~~"
		],
		["ENCOURAGEMENT",
			"Dis will be a scawy fight!! "
				+ "But you are so so so stwong UwU you will totally win!!! "
				+ "I believ in you ~~~ "
				+ "And I will cheer for you like dis: \\_*o*_/"
		],
		// Topic: Saved
		["SAVED",
			"You saved meee!!! I am so gwatefull :3 "
				+ "Now I am supposed to do a plot twist where I betway you OwO "
				+ "But this idiot dev hasn't coded it yet!!! "
				+ "So for now, take my component and git out of here~~~ "
				+ "*nuzzles menacingly*"
		]
	])
];

// For writing reference
//
// const MESSAGES = [
// 	"wazzup! \ud83d\ude43\ud83d\ude80 lemme give you a quick rundown on this thing",
// 	"you can turn around with your mouse, move with (W), and fire by holding down the"
// 		+ " left mouse button. there are a bunch more controls too, but you can"
// 		+ " figure those out by hovering over stuff and reading tooltips",
// 	"when you destroy stuff it gives you items! you can dock to a station to refine"
// 		+ " those items into resources, and then use the resources to buy upgrades."
// 		+ " a lot of steps, i know",
// 	"to find cool places, check out the gps tab! it's the z-shaped icon on the right"
// 		+ " \u27a1\ufe0f",
// 	"sorry for the infodump! it's like a lazy god who is making this universe isn't"
// 		+ " done with it yet, and i gotta investigate that. in the meantime you go"
// 		+ " have fun!"
// ];
