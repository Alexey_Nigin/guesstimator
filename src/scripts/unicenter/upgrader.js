import {Container, Graphics} from "pixi.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {RESOURCES, RESOURCE_INFO} from "../i.js"; // ../drop.js
import {UIUtils} from "../i.js"; // ../ui/utils.js
import {Tooltip} from "../i.js";
import {Button, ResourceDisplay} from "../i.js"; // ./button.js
import {Tab} from "../i.js"; // ./tab.js
import {ACHIEVEMENTS} from "../i.js";

export const UPGRADER_POWER = {
	NONE: 0,
	BASIC: 1,
	INTERMEDIATE: 2,
	ADVANCED: 3
};



const UPGRADE_INFO = [
	{
		name: "Blaster",
		levels: [
			{
				value: 5,
				difficulty: UPGRADER_POWER.NONE,
				cost: new Map(),
				eggDetails: "Damage: 5\nEnergy cost: 1",
				story: ""
			},
			{
				value: 6,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 25]
				]),
				eggDetails: "Damage: 6\nEnergy cost: 1",
				story: "The mirrors inside the blaster have lost their shine over "
					+ "millenia of disuse. You can grind some minerals into a paste "
					+ "and use this paste to polish the mirrors, which will make "
					+ "your blaster bolts more powerful."
			},
			{
				value: 7,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 50]
				]),
				eggDetails: "Damage: 7\nEnergy cost: 1",
				story: "The blaster requires high temperatures to operate. You can "
					+ "install special stones into the blaster to keep it heated "
					+ "between shots, which will make your blaster bolts more "
					+ "powerful.\n\n"
					+ "This addition technically makes your blaster a sauna. A "
					+ "danger sauna."
			},
			{
				value: 8,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.ALLOYS, 50]
				]),
				eggDetails: "Damage: 8\nEnergy cost: 1",
				story: ""
			},
			{
				value: 9,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.POLYMERS, 20]
				]),
				eggDetails: "Damage: 9\nEnergy cost: 1",
				story: ""
			},
			{
				value: 10,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 200]
				]),
				eggDetails: "Damage: 10\nEnergy cost: 1",
				story: ""
			},
			{
				value: 11,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 125]
				]),
				eggDetails: "Damage: 11\nEnergy cost: 1",
				story: ""
			},
			{
				value: 12,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 250]
				]),
				eggDetails: "Damage: 12\nEnergy cost: 1",
				story: ""
			},
			{
				value: 13,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.ALLOYS, 250]
				]),
				eggDetails: "Damage: 13\nEnergy cost: 1",
				story: ""
			},
			{
				value: 14,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.POLYMERS, 100]
				]),
				eggDetails: "Damage: 14\nEnergy cost: 1",
				story: ""
			},
			{
				value: 15,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 1000]
				]),
				eggDetails: "Damage: 15\nEnergy cost: 1",
				story: ""
			},
			{
				value: 16,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Damage: 16\nEnergy cost: 1",
				story: ""
			},
			{
				value: 17,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Damage: 17\nEnergy cost: 1",
				story: ""
			},
			{
				value: 18,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Damage: 18\nEnergy cost: 1",
				story: ""
			},
			{
				value: 19,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Damage: 19\nEnergy cost: 1",
				story: ""
			},
			{
				value: 20,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Damage: 20\nEnergy cost: 1",
				story: ""
			}
		]
	},
	{
		name: "Health",
		levels: [
			{
				value: 25,
				difficulty: UPGRADER_POWER.NONE,
				cost: new Map(),
				eggDetails: "Health: 25",
				story: ""
			},
			{
				value: 30,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 1]
				]),
				eggDetails: "Health: 30",
				story: "Apparently, one of the ceramic rails in the inner layer of "
					+ "the hull is straight up missing - it just isn't there! "
					+ "Whoever originally installed those rails had one job and "
					+ "still screwed it up.\n\n"
					+ "Fortunately, you can make a new rail with just a single unit "
					+ "of minerals, and installing this rail will improve the hull "
					+ "durability by a good amount."
				// TODO: Add a chat message with "derailed" pun
			},
			{
				value: 35,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 50]
				]),
				eggDetails: "Health: 35",
				story: ""
			},
			{
				value: 40,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.ALLOYS, 50]
				]),
				eggDetails: "Health: 40",
				story: ""
			},
			{
				value: 45,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.POLYMERS, 20]
				]),
				eggDetails: "Health: 45",
				story: ""
			},
			{
				value: 50,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 200]
				]),
				eggDetails: "Health: 50",
				story: ""
			},
			{
				value: 60,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 125]
				]),
				eggDetails: "Health: 60",
				story: ""
			},
			{
				value: 70,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 250]
				]),
				eggDetails: "Health: 70",
				story: ""
			},
			{
				value: 80,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.ALLOYS, 250]
				]),
				eggDetails: "Health: 80",
				story: ""
			},
			{
				value: 90,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.POLYMERS, 100]
				]),
				eggDetails: "Health: 90",
				story: ""
			},
			{
				value: 100,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.MINERALS, 1000]
				]),
				eggDetails: "Health: 100",
				story: ""
			},
			{
				value: 110,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Health: 110",
				story: ""
			},
			{
				value: 120,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Health: 120",
				story: ""
			},
			{
				value: 130,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Health: 130",
				story: ""
			},
			{
				value: 140,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Health: 140",
				story: ""
			},
			{
				value: 150,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Health: 150",
				story: ""
			}
		]
	},
	{
		name: "Speed",
		levels: [
			{
				value: 2,
				difficulty: UPGRADER_POWER.NONE,
				cost: new Map(),
				eggDetails: "Speed: 2 f/s",
				story: ""
			},
			{
				value: 2.2,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 25]
				]),
				eggDetails: "Speed: 2.2 f/s",
				story: ""
			},
			{
				value: 2.4,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 50]
				]),
				eggDetails: "Speed: 2.4 f/s",
				story: ""
			},
			{
				value: 2.6,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.ALLOYS, 50]
				]),
				eggDetails: "Speed: 2.6 f/s",
				story: ""
			},
			{
				value: 2.8,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.POLYMERS, 20]
				]),
				eggDetails: "Speed: 2.8 f/s",
				story: ""
			},
			{
				value: 3,
				difficulty: UPGRADER_POWER.BASIC,
				cost: new Map([
					[RESOURCES.MINERALS, 200]
				]),
				eggDetails: "Speed: 3 f/s",
				story: ""
			},
			{
				value: 3.25,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.POLYMERS, 200]
				]),
				eggDetails: "Speed: 3.25 f/s",
				story: ""
			},
			{
				value: 3.5,
				difficulty: UPGRADER_POWER.INTERMEDIATE,
				cost: new Map([
					[RESOURCES.POLYMERS, 400]
				]),
				eggDetails: "Speed: 3.5 f/s",
				story: ""
			},
			{
				value: 3.75,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Speed: 3.75 f/s",
				story: ""
			},
			{
				value: 4,
				difficulty: UPGRADER_POWER.ADVANCED,
				cost: new Map([
					[RESOURCES.MINERALS, 1] // Placeholder
				]),
				eggDetails: "Speed: 4 f/s",
				story: ""
			}
		]
	},
];



const POWER_NAMES = new Map([
	[UPGRADER_POWER.BASIC,        "Basic"       ],
	[UPGRADER_POWER.INTERMEDIATE, "Intermediate"],
	[UPGRADER_POWER.ADVANCED,     "Advanced"    ]
]);
const NOT_POWERFUL_ENOUGH_TEXT = "You have reached the limit of this upgrader's "
	+ "capabilities. To upgrade this technology any further, you will have to find a "
	+ "more powerful upgrader.";
const NOT_IMPLEMENTED_TEXT = "(not yet available - to be added in a future version)";
const MAXED_OUT_TEXT = "You have maxed out this technology to its perfect, ultimate "
	+ "form. There are no more upgrades to get.";



const EGG_COLORS = new Map([
	["PASSED",  UIUtils.hsv(0, 50,  90)],
	["CURRENT", UIUtils.hsv(0, 50,  90)],
	["NEXT",    UIUtils.hsv(0, 20, 100)],
	["UNKNOWN", UIUtils.hsv(0,  0,  30)]
]);

class Egg {
	constructor(tab) {
		this.tab = tab;

		this.display = new Graphics();

		this._tooltip = new Tooltip(
			tab.unicenter.game.tooltipper,
			this.display,
			"",
			"DOWN_MIDDLE"
		);
	}

	// States: one of ["PASSED", "CURRENT", "NEXT", "UNKNOWN"]
	update(dt, level, levelDetails, size, state, screenOrigin) {
		this.display
			.clear()
			.ellipse(size.x / 2, size.y / 2, size.x / 2, size.y / 2)
			.fill(EGG_COLORS.get(state));
		if (state === "NEXT") this.display
			.ellipse(
				size.x / 2, size.y / 2,
				size.x / 2 - Math.round(size.x / 6),
				size.y / 2 - Math.round(size.x / 6)
			)
			.fill(UIUtils.hsv(0, 50, 20));

		let currentText = (state === "CURRENT") ? " (current)" : ""
		if (state === "UNKNOWN") levelDetails = "???";
		this._tooltip.text = `Level ${level}${currentText}\n\n${levelDetails}`;
		this._tooltip.update(dt, screenOrigin, size);
	}
}



class Carton {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._eggs = [];
	}

	update(dt, headerHeight, upgradeBoxOrigin, upgradeBoxSize, upgrade, level) {
		let superNarrowMargin = this.tab.unicenter.requestMarginWidth(2 / 3);
		let wideMargin = this.tab.unicenter.requestMarginWidth(2);

		let numEggs = upgrade.levels.length;
		// Egg indices after which we should insert a wider margin
		let breaksAfter = [];
		for (let i = 0; i < numEggs - 1; ++i) {
			if (upgrade.levels[i].difficulty != upgrade.levels[i + 1].difficulty) {
				breaksAfter.push(i);
			}
		}
		let eggSize = new Vector(
			0.25 * headerHeight,
			0.5 * headerHeight
		).round();

		let eggsContainerOrigin = new Vector(
			numEggs * eggSize.x + (numEggs - 1) * superNarrowMargin
				+ breaksAfter.length * (wideMargin - superNarrowMargin),
			eggSize.y
		);
		let eggsContainerPosition = new Vector(
			upgradeBoxSize.x - wideMargin - eggsContainerOrigin.x,
			(headerHeight - eggsContainerOrigin.y) / 2
		).round();
		this.display.position.copyFrom(eggsContainerPosition);

		let eggX = 0;
		UIUtils.cullElements(this._eggs, numEggs);
		for (let i = 0; i < numEggs; ++i) {
			if (this._eggs.length <= i) {
				let egg = new Egg(this.tab);
				this._eggs.push(egg);
				this.display.addChild(egg.display);
			}

			let egg = this._eggs[i];

			let state = "UNKNOWN";
			if ((i === level + 1) && (upgrade.levels[i].difficulty <= this.tab.power)) {
				state = "NEXT";
			}
			if (i === level) state = "CURRENT";
			if (i < level) state = "PASSED";

			let eggScreenOrigin = new Vector(eggX, 0)
				.add(eggsContainerPosition)
				.add(upgradeBoxOrigin)
				.add(this.tab.unicenter.requestTabOrigin());

			egg.update(
				dt,
				i,
				UPGRADE_INFO[this.tab._selectedTech].levels[i].eggDetails,
				eggSize,
				state,
				eggScreenOrigin
			);
			egg.display.x = eggX;

			eggX += eggSize.x;
			eggX += (breaksAfter.includes(i) ? wideMargin : superNarrowMargin);
		}
	}
}



export class Upgrader extends Tab {
	// Info about nature and cost of upgrades

	// Create a new upgrader
	// Each unicenter should create exactly one upgrader.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(unicenter,
			new Set(["Origin Station", "Grand Station"]),
			Tab.LOCATIONS.LEFT,
			"images/icons/upgrader.svg",
			0,
			"Upgrader"
		);

		this._levels = UPGRADE_INFO.map(() => 0);
		let savedLevels = savefile?.l ?? [];
		if (savedLevels.length >= this._levels.length) {
			for (let i = 0; i < this._levels.length; ++i) {
				if (0 <= savedLevels[i] && savedLevels[i]
								< UPGRADE_INFO[i].levels.length) {
					this._levels[i] = savedLevels[i];
				}
			}
		}
		this.thrustersForged = savefile?.tf ?? false;

		this.power = UPGRADER_POWER.BASIC;

		this._selectedTech = null;

		// Visuals

		this.display.title = UIUtils.getBitmapText("", this.colorHighlight);

		this.display.resourceDisplay = new ResourceDisplay(this.unicenter);

		this.display.techSelectors = new Container();
		UPGRADE_INFO.forEach(upgrade => {
			let techButton = new Button(unicenter);
			this.display.techSelectors.addChild(techButton);
		});

		this.display.selectTechTip = UIUtils.getBitmapText(
			"Select a technology to upgrade",
			this.colorHighlight
		);

		this.display.upgradeBox = new Graphics();

		this.display.upgradeTitle = UIUtils.getBitmapText("", this.colorHighlight);
		this.display.carton = new Carton(this);
		this.display.upgradeStory = UIUtils.getBitmapText("", this.colorHighlight,
			{wordWrap: true}
		);
		this.display.upgradeButton = new Button(unicenter);

		this.display.upgradeBox.addChild(
			this.display.upgradeTitle,
			this.display.carton.display,
			this.display.upgradeStory,
			this.display.upgradeButton
		);

		this.display.addChild(
			this.display.title,
			this.display.resourceDisplay,
			this.display.techSelectors,
			this.display.selectTechTip,
			this.display.upgradeBox
		);
	}

	// Update the upgrader
	// Call this every tick.
	//
	// Args:
	//   visible - boolean, true iff the display is visible this tick
	//   size    - vector in pixels representing the desired size of the display
	//   dt      - time since last tick, in seconds
	update(visible, size, dt) {
		if (visible) {
			this._updatePower();

			if (this.display.upgradeButton.wasClicked()) {
				this._tryUpgrade(this._selectedTech);
			}
			this.display.techSelectors.children.forEach((techButton, i) => {
				if (techButton.wasClicked()) {
					this._selectedTech = (this._selectedTech === i) ? null : i;
				}
			});
		}

		super.update(visible, size, dt);

		this.display.techSelectors.children.forEach(
			techButton => techButton.clearStuff()
		);
		this.display.upgradeButton.clearStuff();
	}

	save() {
		return {
			l: [...this._levels],
			tf: this.thrustersForged
		};
	}

	// Get the value for an upgradable number
	// All upgrades are pulled by whatever needs them, not pushed by upgrader.
	// name is a string.
	getValue(name) {
		let techIndices = new Map([
			["DAMAGE", 0],
			["HEALTH", 1],
			["SPEED", 2]
		]);

		if (!techIndices.has(name)) {
			logger.error("upgrader.js", `unknown name ${name}`);
			return 0;
		}

		let techIndex = techIndices.get(name);
		let upgrade = UPGRADE_INFO[techIndex];
		let level = this._levels[techIndex];
		return upgrade.levels[level].value;
	}

	// Check whether the player can currently upgrade / forge anything
	anythingToDo(power) {
		for (let i = 0; i < UPGRADE_INFO.length; ++i) {
			let upgrade = UPGRADE_INFO[i];
			let level = this._levels[i];

			// Max level, upgrade impossible
			if (level === upgrade.levels.length - 1) continue;

			// Next upgrade is too diffucult, upgrade impossible
			if (upgrade.levels[level + 1].difficulty > power) continue;

			// Enough resources to upgrade, that's something to do!
			let cost = upgrade.levels[level + 1].cost;
			if (this.unicenter.refinery.canAfford(cost)) {
				return true;
			}
		}

		// Nothing to do :(
		return false;
	}

	// Max out all upgrades, used for cheats
	maxOut() {
		this._levels = UPGRADE_INFO.map(tech => tech.levels.length - 1);
	}

	// Update the upgrader power based on what station the player is docked to
	_updatePower() {
		let dockedTo = this.unicenter.game.space.player.dockedTo();
		if (dockedTo === null) return;

		let station = this.unicenter.game.space.requestThingById(dockedTo);
		if (station === null) return;

		if (station.name === "Grand Station") {
			this.power = UPGRADER_POWER.INTERMEDIATE;
		} else {
			this.power = UPGRADER_POWER.BASIC;
		}
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay(dt) {
		let size = this.display.size;
		let narrowMargin = this.unicenter.requestMarginWidth(1);
		let wideMargin = this.unicenter.requestMarginWidth(2);

		this.display.title.style.fontSize = this.unicenter.requestFontSize("LARGE");
		this.display.title.text = `${POWER_NAMES.get(this.power)} Upgrader`;

		this.display.resourceDisplay.update(
			this.colorHighlight,
			this.unicenter.refinery._resources,
			Math.round(size.y / 30)
		);
		this.display.resourceDisplay.position.copyFrom(new Vector(
			2/3 * size.x,
			0
		).round());

		let techSelectorsOrigin = new Vector(
			0,
			this.display.title.height + wideMargin
		).round();
		this.display.techSelectors.position.copyFrom(techSelectorsOrigin);

		let techButtonSize = new Vector(size.y / 10, size.y / 10).round();
		let techButtonOrigin = new Vector();
		this.display.techSelectors.children.forEach((techButton, i) => {
			techButton.update(
				this.colorBase,
				this.colorHighlight,
				techButtonOrigin,
				techButtonSize,
				UPGRADE_INFO[i].name[0]
			);
			techButtonOrigin.x += techButtonSize.x + narrowMargin;
		});

		let upgradeBoxOrigin = new Vector(
			0,
			techSelectorsOrigin.y + techButtonSize.y + wideMargin
		);
		let upgradeBoxSize = Vector.diff(size, upgradeBoxOrigin);

		let selectTechTip = this.display.selectTechTip;
		let upgradeBox = this.display.upgradeBox;
		if (this._selectedTech === null) {
			selectTechTip.visible = true;
			selectTechTip.style.fontSize = this.unicenter.requestFontSize("SMALL");
			selectTechTip.position.copyFrom(
				Vector.diff(
					upgradeBoxSize,
					new Vector(selectTechTip.width, selectTechTip.height)
				)
					.scale(0.5)
					.add(upgradeBoxOrigin)
					.round()
			);

			upgradeBox.visible = false;
		} else {
			selectTechTip.visible = false;

			let headerHeight = Math.round(upgradeBoxSize.y / 6);

			let upgrade = UPGRADE_INFO[this._selectedTech];
			let level = this._levels[this._selectedTech];

			upgradeBox.visible = true;
			upgradeBox.position.copyFrom(upgradeBoxOrigin);
			upgradeBox
				.clear()
				.rect(
					0, 0,
					upgradeBoxSize.x, upgradeBoxSize.y / 6
				)
				.fill(0x441111)
				.rect(
					0, 0,
					upgradeBoxSize.x, upgradeBoxSize.y
				)
				.stroke(this.unicenter.requestStroke(this.colorHighlight, "NARROW"));

			let upgradeTitle = this.display.upgradeTitle;
			upgradeTitle.style.fontSize = this.unicenter.requestFontSize("LARGE");
			upgradeTitle.text = upgrade.name;
			let upgradeTitleOrigin = new Vector(
				wideMargin,
				(headerHeight - upgradeTitle.height) / 2
			).round();
			upgradeTitle.position.copyFrom(upgradeTitleOrigin);

			this.display.carton.update(
				dt, headerHeight, upgradeBoxOrigin, upgradeBoxSize, upgrade, level
			);

			this._refreshStory(upgradeBoxSize, headerHeight, level);

			let upgradeButtonSize = new Vector(
				upgradeBoxSize.x - 2 * narrowMargin,
				(upgradeBoxSize.y - 2 * narrowMargin) / 3
			).round();
			let upgradeButtonOrigin = new Vector(
				narrowMargin,
				upgradeBoxSize.y - narrowMargin - upgradeButtonSize.y
			);

			let upgradePotential = "";
			let isMaxLevel = (level === upgrade.levels.length - 1);
			let isTooDifficult = !isMaxLevel
				&& upgrade.levels[level + 1].difficulty > this.power;
			if (isMaxLevel) {
				upgradePotential = "Maxed out";
			} else if (isTooDifficult) {
				upgradePotential = "Too difficult"
			} else {
				let nextValue = upgrade.levels[level + 1].value;
				let costMap = upgrade.levels[level + 1].cost;
				let costString = Array.from(costMap.entries())
					.map(costElement => {
						let costValue = costElement[1];
						let costUnit = RESOURCE_INFO.get(costElement[0])
							.displayName;
						return `${costValue} ${costUnit}`;
					})
					.join(', ');
				upgradePotential = `Upgrade for ${costString}`;
			}
			this.display.upgradeButton.update(
				this.colorBase,
				this.colorHighlight,
				upgradeButtonOrigin,
				upgradeButtonSize,
				upgradePotential
			);
		}
	}

	_refreshStory(upgradeBoxSize, headerHeight, level) {
		let wideMargin = this.unicenter.requestMarginWidth(2);

		let storyOrigin = new Vector(wideMargin, headerHeight + wideMargin);
		let storyWidth = upgradeBoxSize.x - 2 * wideMargin;

		let upgradeStory = this.display.upgradeStory;
		upgradeStory.style.fontSize = this.unicenter.requestFontSize("SMALL");
		upgradeStory.style.wordWrapWidth = storyWidth;
		upgradeStory.position.copyFrom(storyOrigin);

		let levels_info = UPGRADE_INFO[this._selectedTech].levels;
		if (level >= levels_info.length - 1) {
			upgradeStory.text = MAXED_OUT_TEXT;
			return;
		}
		if (levels_info[level + 1].difficulty > this.power) {
			upgradeStory.text = NOT_POWERFUL_ENOUGH_TEXT;
			if (this.power > UPGRADER_POWER.BASIC) {
				upgradeStory.text += ` ${NOT_IMPLEMENTED_TEXT}`;
			}
			return;
		}
		upgradeStory.text = levels_info[level + 1].story;
	}

	// Try to upgrade a technology and take away the items accordingly
	// This function does nothing if the upgrade is impossible. techIndex is an
	// index in UPGRADE_INFO.
	_tryUpgrade(techIndex) {
		let upgrade = UPGRADE_INFO[techIndex];
		let level = this._levels[techIndex];

		// Max level, upgrade impossible
		if (level === upgrade.levels.length - 1) return;

		// Insufficient power, upgrade impossible
		if (upgrade.levels[level + 1].difficulty > this.power) return;

		let cost = upgrade.levels[level + 1].cost;

		// Insufficient funds, upgrade impossible
		if (!this.unicenter.refinery.canAfford(cost)) return;

		// All checks passed, do the upgrade
		this.unicenter.ship.reportStat("UPGRADES_OBTAINED", 1);
		++this._levels[techIndex];
		this.unicenter.refinery.deduct(cost);

		this.unicenter.ship.achievements.recordAchievement(ACHIEVEMENTS.UPGRADE);

		// No need to push upgrades anywhere - whatever needs the upgrade is
		// responsible for pulling it using this.getValue().
	}
}
