import {Container, Graphics, Sprite, Text} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {RESOURCES, RESOURCE_INFO} from "../i.js"; // ../drop.js
import {UIUtils, Clicker} from "../i.js"; // ../ui/utils.js

export class Button extends Graphics {
	// Create a new button. unicenter is a reference to the unicenter.
	constructor(unicenter) {
		super();

		this.unicenter = unicenter;

		// TODO: DRY this up or replace entirely
		this._labelFirst = new Text({text: "", style: UIUtils.getFont(
			0, {align: "center"}
		)});
		this._labelFirst.anchor.set(1, 0.5);
		this.addChild(this._labelFirst);

		this._labelSecond = new Text({text: "", style: UIUtils.getFont(
			0, {align: "center"}
		)});
		this._labelSecond.anchor.set(0, 0.5);
		this.addChild(this._labelSecond);

		this._clicker = new Clicker(this);
	}

	// Update the button
	// This function should be called every tick when the button has any chance
	// of being visible - probably just always call it every tick.
	// colorBase and colorHighlight are numbers. origin and size are vectors in
	// pixels. text is a string or an array of two strings - buttons only
	// support plaintext labels for now.
	update(colorBase, colorHighlight, origin, size, text) {
		this
			.clear()
			.rect(origin.x, origin.y, size.x, size.y)
			.fill(colorBase)
			.stroke(this.unicenter.requestStroke(colorHighlight, "NARROW"));

		this._labelFirst.style.fontSize = this.unicenter.requestFontSize("SMALL");
		this._labelSecond.style.fontSize = this.unicenter.requestFontSize("SMALL");

		// TODO: This is very hacky
		if (Array.isArray(text)) {
			// Assume an array of two strings
			this._labelFirst.text = text[0];
			this._labelFirst.style.fill = colorHighlight;
			this._labelFirst.anchor.x = 1;
			this._labelFirst.position.copyFrom(
				Vector.sum(origin, Vector.scale(size, 0.5))
					.sub(new Vector(this.unicenter.requestMarginWidth(2), 0))
					.round()
			);
			this._labelSecond.visible = true;
			this._labelSecond.text = text[1];
			this._labelSecond.style.fill = colorHighlight;
			this._labelSecond.position.copyFrom(
				Vector.sum(origin, Vector.scale(size, 0.5))
					.add(new Vector(this.unicenter.requestMarginWidth(2), 0))
					.round()
			);
		} else {
			this._labelFirst.text = text;
			this._labelFirst.style.fill = colorHighlight;
			this._labelFirst.anchor.x = 0.5;
			this._labelFirst.position.copyFrom(
				Vector.sum(origin, Vector.scale(size, 0.5)).round()
			);
			this._labelSecond.visible = false;
		}
	}

	// Return true iff the button was clicked in the last tick
	wasClicked() {
		return this._clicker.wasClicked();
	}

	// Clear the clicked status of the button
	// Call this at the end of every tick.
	clearStuff() {
		this._clicker.clearStuff();
	}
}



// TODO: Find a better place for this
export class ResourceDisplay extends Container {
	// Create a new resource display
	// unicenter is a reference to the unicenter.
	constructor(unicenter) {
		super();

		this.unicenter = unicenter;

		for (let i = 0; i < Object.values(RESOURCES).length; ++i) {
			let resourceContainer = new Container();

			let icon = new Sprite();
			resourceContainer.icon = icon;
			resourceContainer.addChild(icon);

			let count = new Text({text: "", style: UIUtils.getFont()});
			count.anchor.set(0, 0.5);
			resourceContainer.count = count;
			resourceContainer.addChild(count);

			this.addChild(resourceContainer);
		}
	}

	// Update the resource display
	// This function should be called every tick when the display has any chance
	// of being visible - probably just always call it every tick.
	// colorHighlight is a number. resourceData is a map from resource names to
	// amounts. iconHeight is a number in pixels. Returns total height in
	// pixels.
	update(colorHighlight, resourceData, iconHeight) {
		for (let i = 0; i < this.children.length; ++i) {
			let resourceContainer = this.children[i];

			if (i >= resourceData.size) {
				// Don't have this many resources, hide the extra resource
				// containers
				resourceContainer.visible = false;
				continue;
			}
			resourceContainer.visible = true;

			let [resourceName, resourceCount] = Array.from(
							resourceData.entries())[i];

			resourceContainer.position.copyFrom(new Vector(
				0,
				i * (iconHeight + this.unicenter.requestMarginWidth(1))
			).round());
			let icon = resourceContainer.icon;
			icon.texture = getTexture(RESOURCE_INFO.get(resourceName).texture);
			let iconScale = iconHeight / icon.texture.height;
			icon.scale.set(iconScale);

			let count = resourceContainer.count;
			count.text = resourceCount;
			count.style.fill = colorHighlight;
			count.style.fontSize = this.unicenter.requestFontSize("SMALL");
			count.position.copyFrom(new Vector(
				icon.width + this.unicenter.requestMarginWidth(1),
				iconHeight / 2
			).round());
		}

		return resourceData.size * iconHeight + (resourceData.size - 1)
						* this.unicenter.requestMarginWidth(1);
	}
}
