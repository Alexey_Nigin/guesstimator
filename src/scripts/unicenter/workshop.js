import {Graphics, Container, Sprite, Text} from "pixi.js";
import {round} from "../utils.js";
import {logger} from "../i.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js";
import {Clicker, UIUtils} from "../i.js";
import {Button} from "../i.js"; // ./button.js
import {Tab} from "../i.js"; // ./tab.js



const STAGES = {
	NEED_SENSORS:         0,
	CHECKING_AIR:         1,
	CHECKING_GRAVITY:     2,
	SENSOR_DATA:          3,
	EQUIPPING_SPACE_SUIT: 4,
	EQUIPPING_CLAW_BOOTS: 5,
	ENTERING:             6,
	DOOR:                 7
};



class AirlockScene {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._stage = STAGES.NEED_SENSORS;

		this._texts = [0, 1, 2].map(() =>new Text({
			text: "",
			style: UIUtils.getFont(tab.colorHighlight, {wordWrap: true})
		}));
		this._button = new Button(tab.unicenter);

		this.display.addChild(...this._texts, this._button);
	}

	update(visible, size, dt) {
		this.display.size = new Vector(size);

		if (visible) {
			if (this._button.wasClicked()) {
				if (this._stage === STAGES.DOOR) {
					this.tab.setSceneTo(1);
					this._button.clearStuff();
					return;
				}

				++this._stage;
				this._flashingTextTimer = 1;
			}
		}

		this._showTextFinal(0);

		switch (this._stage) {
			case STAGES.NEED_SENSORS: {
				this._hideText(1);
				this._showButton(1, "Check sensors");
				break;
			}
			case STAGES.CHECKING_AIR: {
				this._showText(1, "Checking air...");
				this._hideButton();
				this._updateFlashingTimer(dt, STAGES.CHECKING_GRAVITY, 1);
				break;
			}
			case STAGES.CHECKING_GRAVITY: {
				this._showText(1, "Checking gravity...");
				this._hideButton();
				this._updateFlashingTimer(dt, STAGES.SENSOR_DATA);
				break;
			}
			case STAGES.SENSOR_DATA: {
				this._showTextFinal(1);
				this._showButton(2, "Put on proper equipment & enter");
				break;
			}
			case STAGES.EQUIPPING_SPACE_SUIT: {
				this._showTextFinal(1);
				this._showText(2, "Putting on space suit...");
				this._hideButton();
				this._updateFlashingTimer(dt, STAGES.EQUIPPING_CLAW_BOOTS, 1);
				break;
			}
			case STAGES.EQUIPPING_CLAW_BOOTS: {
				this._showTextFinal(1);
				this._showText(2, "Putting on claw boots...");
				this._hideButton();
				this._updateFlashingTimer(dt, STAGES.ENTERING, 2);
				break;
			}
			case STAGES.ENTERING: {
				this._showTextFinal(1);
				this._showText(2, "Entering the station...");
				this._hideButton();
				this._updateFlashingTimer(dt, STAGES.DOOR);
				break;
			}
			case STAGES.DOOR: {
				this._showTextFinal(1);
				this._showTextFinal(2);
				this._showButton(3, "Inspect door");
				break;
			}
		}

		this._button.clearStuff();
	}

	// Args:
	//   area - integer, 0 to 3 inclusive
	//   text - string
	_showButton(area, text) {
		let size = this.display.size;
		this._button.visible = true;
		let buttonSize = new Vector(0.7 * size.x, 0.7 * size.y / 4).round();
		let buttonOrigin = new Vector(
			size.x / 2 - buttonSize.x / 2,
			(2 * area + 1) * size.y / 8 - buttonSize.y / 2
		).round();
		this._button.update(
			this.tab.colorBase,
			this.tab.colorHighlight,
			buttonOrigin,
			buttonSize,
			text
		);
	}

	_hideButton() {
		this._button.visible = false;
	}

	// Args:
	//   area - integer, 0 to 2 inclusive (no text allowed at the very bottom)
	//   text - string
	_showText(area, text) {
		let size = this.display.size;
		this._texts[area].visible = true;
		this._texts[area].text = text;
		this._texts[area].style.fontSize = Math.round(size.x / 30);
		this._texts[area].style.wordWrapWidth = size.x;
		let textOrigin = new Vector(
			0, (2 * area + 1) * size.y / 8 - this._texts[area].height / 2
		).round();
		this._texts[area].position.copyFrom(textOrigin);
	}

	// Show the final appearance of a text
	// Args:
	//   area - integer, 0 to 2 inclusive (no text allowed at the very bottom)
	_showTextFinal(area) {
		const FINAL_TEXTS = [
			"You have docked to the strange station. "
				+ "It would be wise to check your ship's sensors before going in.",
			"Enviroment data for - unknown station\n"
				+ "        Air: none, aetheric vacuum. Space suit required.\n"
				+ "        Gravity: none. Claw boots recommended.\n",
			"As you carefully make your way through the station, "
				+ "one door catches your attention."
		];
		this._showText(area, FINAL_TEXTS[area]);
	}

	_hideText(area) {
		this._texts[area].visible = false;
	}

	// Args:
	//   dt - time since last frame, in seconds
	//   nextStage - one of STAGES
	//   resetTimerTo - number in seconds, omit to delete timer
	_updateFlashingTimer(dt, nextStage, resetTimerTo) {
		this._flashingTextTimer -= dt;
		if (this._flashingTextTimer < 0) {
			this._stage = nextStage;
			if (resetTimerTo === undefined) {
				delete this._flashingTextTimer;
			} else {
				this._flashingTextTimer = resetTimerTo;
			}
		}
	}
}



class Leg {
	// Keeps references to arguments.
	constructor(tab, start, angles, endY, chooseRight) {
		this.tab = tab;

		this.display = new Graphics();

		this._start = start;
		this._lengths = [0.15, 0.25];
		this._angles = angles;
		this._endY = endY;
		this._chooseRight = chooseRight
	}

	update(fractionOpen, doorToTab) {
		this.display
			.clear();

		let start = this._start;
		let angle = (1 - fractionOpen) * this._angles[0]
			+ fractionOpen * this._angles[1];
		let middle = new Vector(this._lengths[0], 0)
			.rotateBy(angle)
			.add(start);
		let endX = middle.x + (this._chooseRight ? 1 : -1) * Math.sqrt(
			this._lengths[1] ** 2 - (this._endY - middle.y) ** 2
		);
		let end = new Vector(endX, this._endY);

		let startTab = doorToTab(start);
		let middleTab = doorToTab(middle);
		let endTab = doorToTab(end);
		this.display
			.moveTo(startTab.x, startTab.y)
			.lineTo(middleTab.x, middleTab.y)
			.lineTo(endTab.x, endTab.y)
			.stroke(this.tab.unicenter.requestStroke(
				this.tab.colorHighlight, "ULTRAWIDE", true
			));
	}
}



class DoorScene {
	constructor(tab) {
		this.tab = tab;

		this.display = new Container();

		this._signCloseUpShown = false;

		this._segmentStartRotation = 0;

		this._door = new Graphics();

		this._legs = [
			new Leg(tab, new Vector(0, 0.2), [- Math.PI + 0.3, - Math.PI + 1], 0.2),
			new Leg(tab, new Vector(0, 0.4), [- Math.PI + 0.3, - Math.PI + 1], 0.4),
			new Leg(tab, new Vector(0, 0.6), [- Math.PI - 0.3, - Math.PI - 1], 0.6),
			new Leg(tab, new Vector(0, 0.8), [- Math.PI - 0.3, - Math.PI - 1], 0.8),
			new Leg(tab, new Vector(0, 0.2), [-0.3, -1], 0.2, true),
			new Leg(tab, new Vector(0, 0.4), [-0.3, -1], 0.4, true),
			new Leg(tab, new Vector(0, 0.6), [0.3, 1], 0.6, true),
			new Leg(tab, new Vector(0, 0.8), [0.3, 1], 0.8, true),
		];

		this._krenSign = new Sprite(getTexture("images/kren-sign.svg"));
		this._krenSignClicker = new Clicker(this._krenSign);

		this._signCloseUp = new Container();
		this._closeUpBackground = new Graphics();
		this._signSaysText = new Text({
			text: "The sign says,",
			style: UIUtils.getFont(tab.colorHighlight)
		});
		this._bigSign = new Sprite(getTexture("images/kren-sign.svg"));
		this._signCloseUp.addChild(
			this._closeUpBackground,
			this._signSaysText,
			this._bigSign
		);
		this._closeUpClicker = new Clicker(this._signCloseUp);

		this.display.addChild(
			this._door,
			...this._legs.map(leg => leg.display),
			this._krenSign,
			this._signCloseUp
		);
	}

	update(visible, size, dt) {
		if (this._krenSignClicker.wasClicked()) this._signCloseUpShown = true;
		if (this._closeUpClicker.wasClicked()) this._signCloseUpShown = false;

		let doorWidth = 0.7 * size.y;
		let doorLeft = Math.round(size.x / 2 - doorWidth / 2);
		let margin = this.tab.unicenter.requestMarginWidth(2);

		// Door x ranges from -a to +a, for some a
		// Door y ranges from 0 to 1
		function doorToTab(doorPosition) {
			let tabPosition = new Vector(doorPosition);
			tabPosition.scale(size.y);
			tabPosition.x += size.x / 2;
			return tabPosition;
		}

		this._door.position.set(doorLeft, 0);
		this._door
			.clear()
			.rect(0, 0, doorWidth, size.y)
			.stroke(this.tab.unicenter.requestStroke(this.tab.colorHighlight, "NARROW"));

		this._segmentStartRotation += dt;
		this._legs.forEach(leg =>
			leg.update(this._segmentStartRotation % 1, doorToTab)
		);

		this._krenSign.scale.set(0.12 * size.x / this._krenSign.texture.width);
		let krenSignOrigin = new Vector(
			doorLeft - margin - this._krenSign.width,
			0.5 * size.y - this._krenSign.height / 2
		).round();
		this._krenSign.position.copyFrom(krenSignOrigin);

		if (this._signCloseUpShown) {
			this._showSignCloseUp(size);
		} else {
			this._hideSignCloseUp();
		}

		this._krenSignClicker.clearStuff();
		this._closeUpClicker.clearStuff();
	}

	_showSignCloseUp(size) {
		this._signCloseUp.visible = true;

		this._closeUpBackground
			.clear()
			.rect(0, 0, size.x, size.y)
			.fill({h: 270, s: 50, v: 13, a: 0.8});

		this._signSaysText.style.fontSize = this.tab.unicenter.requestFontSize("LARGE");
		let textOrigin = new Vector(
			size.x / 2 - this._signSaysText.width / 2,
			size.y / 5 - this._signSaysText.height / 2
		).round();
		this._signSaysText.position.copyFrom(textOrigin);

		let signHeight = Math.round(2 * size.y / 6);
		this._bigSign.scale.set(signHeight / this._bigSign.texture.height);
		this._bigSign.position.copyFrom(new Vector(
			size.x / 2 - this._bigSign.width / 2,
			2 * size.y / 5
			).round());
	}

	_hideSignCloseUp() {
		this._signCloseUp.visible = false;
	}
}



const SCENE_CLASSES = [
	AirlockScene,
	DoorScene
];



export class Workshop extends Tab {
	// Create a new Kren's Workshop
	// Each unicenter should create exactly one workshop.
	//
	// Args:
	// unicenter - reference to the unicenter
	// savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(
			unicenter,
			new Set(["Spiderdome"]),
			Tab.LOCATIONS.LEFT,
			"images/icons/ship.svg",
			270,
			"Workshop"
		);

		this.setSceneTo(0);
	}

	// Update the workshop
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size, dt) {
		this._scene.update(visible, size, dt);

		super.update(visible, size, dt);
	}

	save() {
		return {};
	}

	setSceneTo(sceneId) {
		logger.assert(
			sceneId >= 0 && sceneId < SCENE_CLASSES.length,
			"workshop.js", `invalid sceneId ${sceneId}`
		);

		this._sceneId = sceneId;
		if (this._scene !== undefined) {
			this.display.removeChild(this._scene.display);
		}
		this._scene = new SCENE_CLASSES[sceneId](this);
		this.display.addChild(this._scene.display);
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay(dt) {}
}
