import {Container, Graphics, Sprite} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../logger.js";
import {Thing, THING_TYPES} from "../i.js";
import {Clicker} from "../i.js";
import {WorldSight} from "../i.js";
import {Tab} from "../i.js"; // ./tab.js

// Map is already a term for the data structure...
export class GPS extends Tab {
	// The height of the area displayed by the GPS
	static FLARES_VERTICAL = 500;

	// Create a new gps
	// Each unicenter should create exactly one gps. unicenter is a
	// reference to the unicenter.
	constructor(unicenter) {
		super(
			unicenter,
			null,
			Tab.LOCATIONS.RIGHT,
			"images/icons/gps.svg",
			220,
			"GPS"
		);

		// Array of ids of things to draw arrows to
		this.arrows = new Set();
		// A backup of arrows made when right before clearing all arrows
		// Used to undo a clear.
		this._last_arrows = new Set();

		this._worldsight = new WorldSight(
			unicenter.game,
			this.colorBase,
			GPS.FLARES_VERTICAL,
			0.001,
			new Vector(0.5, 0.5),
			true,
			(id, isPlayer) => {
				if (isPlayer) {
					if (this.arrows.size === 0) {
						this.arrows = new Set(this._last_arrows);
					} else {
						this._last_arrows = new Set(this.arrows);
						this.arrows.clear();
					}
					return;
				}

				// Any other change means that undo is no longer possible
				this._last_arrows.clear();

				if (this.arrows.has(id)) {
					this.arrows.delete(id);
				} else {
					this.arrows.add(id);
				}
			}
		);

		this._availableMask = new Graphics();
		this._worldsight.display.mask = this._availableMask;

		this._border = new Graphics();

		this.display.addChild(
			this._worldsight.display,
			this._availableMask,
			this._border
		);
	}

	// Update the gps
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		this.arrows.forEach(id => {
			let thing = this.unicenter.game.space.requestThingById(id);
			if ((thing === null) || (thing.state !== Thing.STATES.ALIVE)) {
				this.arrows.delete(id);
			}
		});

		this._worldsight.update(visible, size);

		super.update(visible, size);
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;

		this._availableMask
			.clear()
			.rect(0, 0, size.x, size.y)
			.fill(0xffffff);

		this._border
			.clear()
			.rect(0, 0, size.x, size.y)
			.stroke(this.unicenter.requestStroke(this.colorHighlight, "NARROW"));
	}
}
