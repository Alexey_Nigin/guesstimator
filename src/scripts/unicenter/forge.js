import {Text} from "pixi.js";
import {Vector} from "../i.js"; // ../vector.js
import {UIUtils} from "../i.js"; // ../ui/utils.js
import {Button} from "../i.js"; // ./button.js
import {Tab} from "../i.js"; // ./tab.js
import {ACHIEVEMENTS} from "../i.js";

export class Forge extends Tab {
	// Create a new technology forge
	// Each unicenter should create exactly one technology forge.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(unicenter,
			new Set(["Forge Station"]),
			Tab.LOCATIONS.LEFT,
			"images/icons/upgrader.svg",
			0,
			"Technology Forge"
		);

		this.thrustersForged = savefile?.tf ?? false;
		this.boomdashForged = savefile?.bf ?? false;

		// Visuals

		this.display.title = new Text(
			"Technology Forge",
			UIUtils.getFont(this.colorHighlight)
		);

		this.display.thrustersButton = new Button(unicenter);
		this.display.boomdashButton = new Button(unicenter);

		this.display.addChild(
			this.display.title,
			this.display.thrustersButton,
			this.display.boomdashButton
		);
	}

	// Update the forge
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		if (visible) {
			if (this.display.thrustersButton.wasClicked()
							&& this.unicenter.inventory.hasStar()) {
				this.thrustersForged = true;
				this.unicenter.ship.achievements.recordAchievement(
					ACHIEVEMENTS.THRUSTERS
				);
			}
			if (this.display.boomdashButton.wasClicked()
							&& this.unicenter.inventory.hasCorrelationMatrix()) {
				this.boomdashForged = true;
				this.unicenter.ship.achievements.recordAchievement(
					ACHIEVEMENTS.BOOMDASH
				);
			}
		}

		super.update(visible, size);

		this.display.thrustersButton.clearStuff();
		this.display.boomdashButton.clearStuff();
	}

	save() {
		return {
			bf: this.boomdashForged,
			tf: this.thrustersForged
		};
	}

	// Check whether the player can currently upgrade / forge anything
	anythingToDo() {
		let canForgeThrusters = !this.thrustersForged
			&& this.unicenter.inventory.hasStar();
		let canForgeBoomdash = !this.boomdashForged
			&& this.unicenter.inventory.hasCorrelationMatrix();
		if (canForgeThrusters || canForgeBoomdash) {
			return true;
		}

		return false;
	}

	// Forge everything, used for cheats
	maxOut() {
		this.thrustersForged = true;
		this.boomdashForged = true;
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;
		// let narrowMargin = this.unicenter.requestMarginWidth(1);
		// let wideMargin = this.unicenter.requestMarginWidth(2);

		this.display.title.style.fontSize = this.unicenter.requestFontSize("LARGE");

		let buttonSize = new Vector(size.x, size.y / 3).round();
		let thrusterButtonOrigin = new Vector(
			0,
			size.y - 2 * buttonSize.y - this.unicenter.requestMarginWidth(1)
		);
		let forgeText = this.thrustersForged ?
			"Maneuvering thrusters forged, use them with (A)(S)(D)"
							+ " or (\u2b05)(\u2b07)(\u27a1)"
			: "Forge maneuvering thrusters (requires a Star)";
		this.display.thrustersButton.update(
			this.colorBase,
			this.colorHighlight,
			thrusterButtonOrigin,
			buttonSize,
			forgeText
		);

		let boomdashButtonOrigin = Vector.diff(size, buttonSize);
		forgeText = this.boomdashForged ?
			"Boomdash forged, use it with (Shift)"
			: "Forge boomdash (requires a Correlation Matrix)";
		this.display.boomdashButton.update(
			this.colorBase,
			this.colorHighlight,
			boomdashButtonOrigin,
			buttonSize,
			forgeText
		);
	}
}
