import {Container} from "pixi.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {UIUtils} from "../i.js";

export class Tab {
	// Possible locations of tabs in the unicenter
	static LOCATIONS = {
		LEFT:        "LEFT",
		RIGHT:       "RIGHT",
		LEFT_BOTTOM: "LEFT_BOTTOM"
	};

	// Create a new tab
	// Don't create instances of Tab directly, this class should only be used to
	// create subclasses.
	//
	// Args:
	//   unicenter - a reference to the unicenter
	//   stations  - a set of station names that this tab is accessible from, if null then
	//               the tab is accessible from anywhere (station or empty space)
	//   location  - one of Tab.LOCATIONS
	//   icon      - a filename string
	//   hue       - number in range 0..360
	//   tooltip   - a string
	//
	// TODO: Should I really use the user-facing station names here?
	constructor(unicenter, stations, location, icon, hue, tooltipText) {
		this.unicenter = unicenter;

		this._stations = (stations === null) ? null : new Set(stations);

		console.assert(Object.values(Tab.LOCATIONS).includes(location));
		this.location = location;
		this.icon = icon;
		this.tooltipText = tooltipText;

		if (hue === "WHITE") {
			this.colorBase = UIUtils.hsv(0, 0, 13);
			this.colorHighlight = UIUtils.hsv(0, 0, 100);
		} else {
			this.colorBase = UIUtils.hsv(hue, 50, 13);
			this.colorHighlight = UIUtils.hsv(hue, 50, 100);
		}

		// The container for displaying the tab in the unicenter
		this.display = new Container();
		this.display.size = new Vector();
	}

	// Update the tab
	// Call this every tick.
	//
	// Args:
	//   visible - boolean, true iff the display isvisible this tick
	//   size    - vector in pixels representing the desired size of the display
	//   dt      - time since last tick, in seconds
	update(visible, size, dt) {
		if (visible) {
			this.display.size.copyFrom(size);
			this._refreshDisplay(dt);
		}
	}

	// Return true iff the tab is currently accessible
	isAccessible() {
		if (this._stations === null) return true;

		let dockedTo = this.unicenter.game.space.player.dockedTo();
		if (dockedTo === null) return false;

		let station = this.unicenter.game.space.requestThingById(dockedTo);
		if (station === null) return false;

		return this._stations.has(station.name);
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		logger.error("logger.js", "_refreshDisplay() not overridden");
	}
}



// Return the index of the max value in the array
// TODO: Move this to a more permanent place.
function _indexOfMax(array) {
	console.assert(array.length > 0);

	let max = array[0];
	let index = 0;
	for (let i = 1; i < array.length; ++i) {
		if (array[i] > max) {
			max = array[i];
			index = i;
		}
	}
	return index;
}



// Split the total into a given number of integer-sized pieces, as evenly as
// possible
// Returns an array of numPieces integers, which add up to total.
// TODO: Move this to a more permanent place.
export function intSplit(total, numPieces) {
	// Base amount that fits into every piece
	let base = Math.floor(total / numPieces);
	// Initialize each piece with that base amount
	let result = Array(numPieces).fill(base);

	// Distance to the closest index at which an extra 1 has been added
	let distances = Array(numPieces).fill(Infinity);

	// Leftover amount to distribute
	let leftover = total - base * numPieces;
	while (leftover > 0) {
		// Pick the furthest spot from previous insertions to insert this 1
		let spot = _indexOfMax(distances);
		// Insert the 1
		++result[spot];
		// Update distances
		for (let i = 0; i < distances.length; ++i) {
			distances[i] = Math.min(distances[i], Math.abs(i - spot));
		}
		// One fewer left to insert
		--leftover;
	}

	return result;
}
