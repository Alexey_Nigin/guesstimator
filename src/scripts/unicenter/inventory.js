import {Container, Graphics, Rectangle, Sprite, Text} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {logger} from "../i.js"; // ../logger.js
import {Component, COMPONENTS, Item, ITEM_INFO} from "../i.js"; // ../drop.js
import {UIUtils, Clicker} from "../i.js"; // ../ui/utils.js
import {ResourceDisplay} from "../i.js"; // ./button.js
import {Tab, intSplit} from "../i.js"; // ./tab.js

export class Inventory extends Tab {
	// Fraction of the horizontal space occupied by the details
	static DETAILS_FRACTION = 1 / 3;

	// Fraction of the vertical space occupied by the components
	static COMPONENTS_FRACTION = 1 / 8;

	// Number of boxes in the grid
	static NUM_BOXES = new Vector(5, 5);

	// Create a new inventory
	// Each unicenter should create exactly one inventory.
	//
	// Args:
	//   unicenter - reference to the unicenter
	//   savefile  - optional savefile (object)
	constructor(unicenter, savefile) {
		super(unicenter,
			null,
			Tab.LOCATIONS.RIGHT,
			"images/icons/inventory.svg",
			30,
			"Inventory"
		);

		// TODO: Error checking!
		this._items = new Map(savefile?.i ?? []);

		this._numStars = (savefile?.star ?? false) ? 1 : 0;
		this._numCorrelationMatrices = (savefile?.correlationMatrix ?? false) ? 1 : 0;

		let boxesContainer = new Container();
		boxesContainer.position.set(0, 0);
		this.display.boxesContainer = boxesContainer;
		this.display.addChild(boxesContainer);

		let boxes = [];
		for (let x = 0; x < Inventory.NUM_BOXES.x; ++x) {
			boxes.push([]);
			for (let y = 0; y < Inventory.NUM_BOXES.y; ++y) {
				let box = new Graphics();

				box.clicker = new Clicker(box);

				let itemSprite = new Sprite();
				box.itemSprite = itemSprite;
				box.addChild(itemSprite);

				let itemCount = new Text("", UIUtils.getFont(this.colorHighlight));
				itemCount.anchor.set(1, 1);
				box.itemCount = itemCount;
				box.addChild(itemCount);

				boxes[x].push(box);
				boxesContainer.addChild(box);
			}
		}
		boxesContainer.boxes = boxes;

		let componentsContainer = new Container();
		this.display.componentsContainer = componentsContainer;
		this.display.addChild(componentsContainer);

		let star = new Sprite();
		star.position.set(0, 0);
		star.clicker = new Clicker(star);
		componentsContainer.star = star;
		componentsContainer.addChild(star);

		let correlationMatrix = new Sprite();
		correlationMatrix.position.set(0, 0);
		correlationMatrix.clicker = new Clicker(correlationMatrix);
		componentsContainer.correlationMatrix = correlationMatrix;
		componentsContainer.addChild(correlationMatrix);

		let details = new Graphics();
		this.display.details = details;
		this.display.addChild(details);

		let name = new Text("", UIUtils.getFont(this.colorHighlight, {
			align: "left",
			wordWrap: true
		}));
		name.anchor.set(0, 0);
		details.itemName = name;
		details.addChild(name);
		let description = new Text("", UIUtils.getFont(this.colorHighlight, {
			align: "left",
			wordWrap: true
		}));
		description.anchor.set(0, 0);
		details.description = description;
		details.addChild(description);

		let resourceDisplay = new ResourceDisplay(this.unicenter);
		details.resourceDisplay = resourceDisplay;
		details.addChild(resourceDisplay);
	}

	// Update the inventory
	// Call this every tick. visible is a boolean that is true if the display is
	// visible this tick. size is a vector in pixels representing the desired
	// size of the display.
	update(visible, size) {
		super.update(visible, size);

		for (let x = 0; x < Inventory.NUM_BOXES.x; ++x) {
			for (let y = 0; y < Inventory.NUM_BOXES.y; ++y) {
				this.display.boxesContainer.boxes[x][y].clicker.clearStuff();
			}
		}
		this.display.componentsContainer.star.clicker.clearStuff();
		this.display.componentsContainer.correlationMatrix.clicker.clearStuff();
	}

	save() {
		return {
			i: Array.from(this._items),
			star: (this._numStars > 0),
			correlationMatrix: (this._numCorrelationMatrices > 0)
		};
	}

	// Add an item to the inventory
	// item is a reference to the item/component being picked up, or item name
	add(item) {
		if (typeof item === "string") {
			// `item` is just the item name, cheats do this
			this._items.set(item, (this._items.get(item) ?? 0) + 1);
		}
		if (item instanceof Component) {
			if (item.componentName === COMPONENTS.CORRELATION_MATRIX) {
				++this._numCorrelationMatrices;
			} else {
				++this._numStars;
			}
		} else if (item instanceof Item) {
			let itemName = item.itemName;
			this._items.set(itemName, (this._items.get(itemName) ?? 0) + 1);
		} else {
			logger.error("inventory.js", "unknown item type");
		}
	}

	getAllItems() {
		return new Map(this._items);
	}

	clearAllItems() {
		this._items.clear();
	}

	// Return true iff the player has picked up a star
	hasStar() {
		return this._numStars > 0;
	}

	// Return true iff the player has picked up a star
	hasCorrelationMatrix() {
		return this._numCorrelationMatrices > 0;
	}

	// Refresh the display, redrawing everything from scratch
	// This method only needs to be called when something actually changed, but
	// it's okay to call it every tick.
	_refreshDisplay() {
		let size = this.display.size;

		let detailsSize = new Vector(
			Inventory.DETAILS_FRACTION
							* (size.x - this.unicenter.requestMarginWidth(2)),
			size.y
		).round();
		let detailsOrigin = new Vector(size.x - detailsSize.x, 0).round();

		let boxesContainerSize = new Vector(
			size.x - detailsSize.x - this.unicenter.requestMarginWidth(2),
			(1 - Inventory.COMPONENTS_FRACTION)
							* (size.y - this.unicenter.requestMarginWidth(2))
		).round();

		let componentsContainerOrigin = new Vector(
			0,
			boxesContainerSize.y + this.unicenter.requestMarginWidth(2)
		).round();
		let componentsContainerSize = new Vector(
			boxesContainerSize.x,
			size.y - componentsContainerOrigin.y
		).round();

		let hoveredItem = this._refreshBoxes(boxesContainerSize);
		let hoveredComponents = this._refreshComponents(componentsContainerOrigin,
						componentsContainerSize);
		this._refreshDetails(detailsOrigin, detailsSize, hoveredItem,
						hoveredComponents);
	}

	// Refresh the boxes with items
	// Returns the name of the item currently hovered over, or null if no item
	// is hovered over.
	_refreshBoxes(boxesContainerSize) {
		let allBoxesSize = Vector.diff(
			boxesContainerSize,
			Vector.diff(Inventory.NUM_BOXES, new Vector(1))
				.scale(this.unicenter.requestMarginWidth(1))
		);

		let boxSizesX = intSplit(allBoxesSize.x, Inventory.NUM_BOXES.x);
		let boxPositionsX = [0];
		for (let x = 1; x < Inventory.NUM_BOXES.x; ++x) {
			boxPositionsX.push(boxPositionsX[x - 1]
							+ this.unicenter.requestMarginWidth(1)
							+ boxSizesX[x - 1]);
		}

		let boxSizesY = intSplit(allBoxesSize.y, Inventory.NUM_BOXES.y);
		let boxPositionsY = [0];
		for (let y = 1; y < Inventory.NUM_BOXES.y; ++y) {
			boxPositionsY.push(boxPositionsY[y - 1]
							+ this.unicenter.requestMarginWidth(1)
							+ boxSizesY[y - 1]);
		}

		let itemEntries = Array.from(this._items.entries());

		let hoveredItem = null;

		let boxes = this.display.boxesContainer.boxes;
		for (let x = 0; x < Inventory.NUM_BOXES.x; ++x) {
			for (let y = 0; y < Inventory.NUM_BOXES.y; ++y) {
				let itemEntry = itemEntries[y * Inventory.NUM_BOXES.x + x]
								?? null;

				let box = boxes[x][y];

				let boxPosition = new Vector(boxPositionsX[x],
								boxPositionsY[y]);
				box.position.copyFrom(boxPosition);

				let boxSize = new Vector(boxSizesX[x], boxSizesY[y]);

				box
					.clear()
					.rect(0, 0, boxSize.x, boxSize.y)
					.stroke(this.unicenter.requestStroke(this.colorHighlight, "NARROW"));

				let itemSprite = box.itemSprite;
				let itemCount = box.itemCount;

				if (itemEntry === null) {
					itemSprite.visible = false;
					itemCount.visible = false;
					continue;
				}

				itemSprite.visible = true;
				itemCount.visible = true;

				itemSprite.texture = getTexture(ITEM_INFO.get(itemEntry[0]).texture);
				itemSprite.position.copyFrom(
					new Vector(this.unicenter.requestMarginWidth(1))
				);
				itemSprite.scale.set(
					(boxSize.x - 2 * this.unicenter.requestMarginWidth(1))
									/ itemSprite.texture.width
				);

				itemCount.text = itemEntry[1];
				itemCount.style.fontSize = this.unicenter.requestFontSize("SMALL");
				itemCount.position.copyFrom(
					Vector.diff(
						boxSize,
						new Vector(this.unicenter.requestMarginWidth(1))
					)
				);

				box.hitArea = new Rectangle(0, 0, boxSize.x, boxSize.y);
				if (box.clicker.isHovering()) {
					if (hoveredItem !== null) {
						logger.warning("inventory.js",
										"more than 1 item is hovered");
					}
					hoveredItem = itemEntry[0];
				}
			}
		}

		return hoveredItem;
	}

	// Refresh the components bar
	// Both arguments are vectors in pixels.
	// Returns [starHovered, correlatioMatrixHovered].
	_refreshComponents(componentsContainerOrigin, componentsContainerSize) {
		let componentsContainer = this.display.componentsContainer;
		componentsContainer.position.copyFrom(componentsContainerOrigin);

		let star = componentsContainer.star;
		let correctStarTexture = getTexture(
			this._numStars > 0 ? "images/star.svg" : "images/star-outline.svg"
		);
		if (star.texture !== correctStarTexture) star.texture = correctStarTexture;
		star.scale.set(componentsContainerSize.y / star.texture.height);

		let correlationMatrix = componentsContainer.correlationMatrix;
		let correctCorrelationMatrixTexture = getTexture(
			this._numCorrelationMatrices > 0
				? "images/correlation-matrix.svg"
				: "images/correlation-matrix-outline.svg"
		);
		if (correlationMatrix.texture !== correctCorrelationMatrixTexture) {
			correlationMatrix.texture = correctCorrelationMatrixTexture;
		}
		correlationMatrix.scale.set(
			componentsContainerSize.y / correlationMatrix.texture.height
		);
		correlationMatrix.position.x = star.width + this.unicenter.requestMarginWidth(1);

		return [star.clicker.isHovering(), correlationMatrix.clicker.isHovering()];
	}

	// Refresh the details box
	// detailsOrigin and detailsSize are vectors in pixels. hoveredItem is the
	// name of the item currently hovered over, or null if no item is hovered
	// over. hoveredComponents is an array of 2 booleans.
	_refreshDetails(detailsOrigin, detailsSize, hoveredItem, hoveredComponents) {
		let details = this.display.details;

		details.position.copyFrom(detailsOrigin);
		details
			.clear()
			.rect(0, 0, detailsSize.x, detailsSize.y)
			.stroke(this.unicenter.requestStroke(this.colorHighlight, "NARROW"));

		let name = details.itemName;
		let description = details.description;
		let resourceDisplay = details.resourceDisplay;

		if ((hoveredItem !== null) && hoveredComponents.some(c => c)) {
			logger.warning("inventory.js", "item and component hovered at the"
							+ " same time");
		}

		if ((hoveredItem === null) && !hoveredComponents.some(c => c)) {
			name.visible = false;
			description.visible = false;
			resourceDisplay.visible = false;
			return;
		}

		name.visible = true;
		description.visible = true;
		resourceDisplay.visible = !hoveredComponents.some(c => c);

		name.position.copyFrom(
			new Vector(this.unicenter.requestMarginWidth(1))
		);
		if (hoveredComponents[0]) {
			name.text = this._numStars ? "Star" : "???";
		} else if (hoveredComponents[1]) {
			name.text = this._numCorrelationMatrices ? "Correlation Matrix" : "???";
		} else {
			name.text = ITEM_INFO.get(hoveredItem).displayName;
		}
		name.style.fontSize = this.unicenter.requestFontSize("LARGE");
		name.style.wordWrapWidth = detailsSize.x
						- 2 * this.unicenter.requestMarginWidth(1);

		let descriptionOrigin = new Vector(
			this.unicenter.requestMarginWidth(1),
			name.height + 2 * this.unicenter.requestMarginWidth(1)
		);
		description.position.copyFrom(descriptionOrigin);
		if (hoveredComponents[0]) {
			description.text = this._numStars
				? "A coveted object that proves your skill."
								+ " Congratulations for obtaining it!"
				: "Looks kinda pointy?";
		} else if (hoveredComponents[1]) {
			description.text = this._numCorrelationMatrices
				? "For something created by the DAMSEL, "
					+ "this matrix contains very few UwUs."
				: "Math???";
		} else {
			description.text = ITEM_INFO.get(hoveredItem).description;
		}
		description.style.fontSize = this.unicenter.requestFontSize("SMALL");
		description.style.wordWrapWidth = detailsSize.x
						- 2 * this.unicenter.requestMarginWidth(1);

		if (hoveredItem !== null) {
			let refinesTo = ITEM_INFO.get(hoveredItem).refinesTo;
			let iconHeight = detailsSize.y / 15;
			let totalHeight = resourceDisplay.update(this.colorHighlight, refinesTo,
							iconHeight);
			resourceDisplay.position.copyFrom(new Vector(
				this.unicenter.requestMarginWidth(1),
				detailsSize.y - this.unicenter.requestMarginWidth(1)
								- totalHeight
			).round());
		}
	}
}
