import {Sprite} from "pixi.js";
import {getTexture} from "./assets.js";
import {Vector} from "./i.js"; // ./vector.js
import {Random} from "./i.js";
import {functionFactory} from "./i.js"; // ./code-factories.js
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {Particles} from "./i.js"; // ./particles.js
import {AI, AIUtils, PathFollowAI} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, STANDARD_PPF, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {ACHIEVEMENTS} from "./i.js";

const COSSETTE_STATES = {
	SLEEPING: "SLEEPING",
	WANDERING: "WANDERING",
	SEEKING: "SEEKING",
	DRAGGING: "DRAGGING"
}



class CossetteSleepingAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Reference to parent AI
		new RequiredProperty("parentAI", v => (v instanceof AI)),
		// String representation of a function that takes in the in-game time
		// and returns sleep pos at that time
		new RequiredProperty("sleepPosGenerator", v => (typeof v === "string"))
	]);

	constructor(thing, config) {
		CossetteSleepingAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._sleepPosGenerator = functionFactory(config.sleepPosGenerator);

		this.parentAI = config.parentAI;
		this._pathFollowAI = new PathFollowAI(thing, {
			posGenerator: config.sleepPosGenerator,
			spn: -0.1
		});

		this._particleDelay = 0;

		this.enterState();
	}

	move(dt, time) {
		if (this._damageSinceFallingAsleep >= 30) {
			this.parentAI.requestStateChange(COSSETTE_STATES.WANDERING);
		}

		this.thing._exhaust.visible = false;

		this._pathFollowAI.move(dt, time);

		this._particleDelay -= dt;
		if (this._particleDelay <= 0) {
			this.thing.space.particles.add(
				Particles.TYPES.SLEEP,
				this.thing.pos.chooseNearby(1.5 * this.thing.radius),
				this.thing.vel,
				1
			);
			this._particleDelay += 1;
		}
	}

	enterState() {
		this._damageSinceFallingAsleep = 0;
	}

	recordDamage(damage) {
		this._damageSinceFallingAsleep += damage;
	}
}



// Helper AI for flying to a destination, used for multiple states
class CossetteFlyingAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([]);

	static SPEED = 5;
	static FRICTION = 4;

	constructor(thing, config) {
		CossetteFlyingAI.CONFIG_STRUCTURE.test(config);

		super(thing);
	}

	// destination is a vector
	move(dt, time, destination) {
		// Find nearby stations & bosses to avoid
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (entry.IS_BOSS || entry.TYPE === THING_TYPES.STATION),
			"COSSETTE"
		).filter(
			entry => (Vector.diff(entry.pos, this.thing.pos).mag() < 20)
		);

		// Whether cossette is extremely close to a dangerous thing and may turn off her
		// engines
		let imminentDanger = false;
		// Vector pointing in the direction cossette wants to travel, starts out pointing
		// to the destination
		let desiredDirVector = Vector.diff(destination, this.thing.pos).norm(1);
		// Adjust desiredDirVector away from nearby stations and bosses
		// TODO: Stations should be scarier than bosses, but for now cossette treats them
		// the same.
		for (let entry of tacticalReport) {
			if (Vector.diff(entry.pos, this.thing.pos).mag() < 10) {
				imminentDanger = true;
			}
			desiredDirVector.add(AIUtils.getRepelAcc(
				this.thing,
				entry,
				4,
				20
			));
		}

		// Actually move
		AIUtils.rotateTo(
			this.thing,
			desiredDirVector.dir(),
			4,
			dt
		);
		let acc = new Vector(
			Math.cos(this.thing.rot),
			Math.sin(this.thing.rot)
		).norm(CossetteFlyingAI.SPEED * CossetteFlyingAI.FRICTION);
		// Turn off engine if in imminent danger and rotated more than 90 degrees from the
		// desired direction
		if (imminentDanger && Vector.dot(acc, desiredDirVector) < 0) {
			acc.norm(0);
			this.thing._exhaust.visible = false;
		} else {
			this.thing._exhaust.visible = true;
		}
		AIUtils.translate(this.thing, acc, CossetteFlyingAI.FRICTION, dt);
	}
}



class CossetteWanderingAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Reference to parent AI
		new RequiredProperty("parentAI", v => (v instanceof AI))
	]);

	constructor(thing, config) {
		CossetteWanderingAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this.parentAI = config.parentAI;
		this._flyingAI = new CossetteFlyingAI(thing, {});

		this.enterState();
	}

	move(dt, time) {
		this._timeToWander -= dt;
		if (this._timeToWander <= 0) {
			this.parentAI.requestStateChange(COSSETTE_STATES.SEEKING);
		}
		// Update the destination
		this._timeToDestinationReroll -= dt;
		if (this._timeToDestinationReroll <= 0 || this._destination === null) {
			for (let tries = 0; tries < 100; ++tries) {
				this._destination = this.thing.pos.chooseNearby(100);
				if (CossetteAI.isWithinGameArea(this._destination)) break;
			}
			let distToDestination = Vector.diff(this._destination, this.thing.pos).mag();
			// To ensure cossette doesn't loiter at the destination, give her just enough
			// time to reach the destination at max speed. If she doesn't reach it because
			// of speed-up time or something in the way, no problem - wandering is
			// supposed to be aimless after all.
			this._timeToDestinationReroll = distToDestination / CossetteFlyingAI.SPEED;
		}

		this._flyingAI.move(dt, time, this._destination);
	}

	enterState() {
		// Current destination to pathfind to
		this._destination = null;
		// Time left before rerolling destination
		this._timeToDestinationReroll = 0;
		// Time left to wander
		this._timeToWander = Random.uniform(300, 600);
	}
}



class CossetteSeekingAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Reference to parent AI
		new RequiredProperty("parentAI", v => (v instanceof AI))
	]);

	constructor(thing, config) {
		CossetteSeekingAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this.parentAI = config.parentAI;
		this._flyingAI = new CossetteFlyingAI(thing, {});

		this.enterState();
	}

	move(dt, time) {
		if (Vector.diff(this.thing.pos, this.thing.space.player.pos).mag() <= 3) {
			this.parentAI.requestStateChange(COSSETTE_STATES.DRAGGING);
		}
		this._timeToSeek -= dt;
		if (this._timeToSeek <= 0) {
			this.parentAI.requestStateChange(COSSETTE_STATES.WANDERING);
		}

		// TODO: What if the player is mid-respawn?
		// TODO: Help cossette resolve circling
		this._flyingAI.move(dt, time, this.thing.space.player.pos);
	}

	enterState() {
		let distToPlayer = Vector.diff(
			this.thing.space.player.pos, this.thing.pos
		).mag();
		// Give cossette some extra time to reach the player
		this._timeToSeek = distToPlayer / CossetteFlyingAI.SPEED + 120;
	}
}



class CossetteDraggingAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Reference to parent AI
		new RequiredProperty("parentAI", v => (v instanceof AI))
	]);

	constructor(thing, config) {
		CossetteDraggingAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this.parentAI = config.parentAI;
		this._flyingAI = new CossetteFlyingAI(thing, {});

		this.enterState();
	}

	move(dt, time) {
		let player = this.thing.space.player;

		// Make sure the player doesn't keep accelerating if we stop the tractor beam and
		// exit early
		player.ai.tractorBeamAcc = new Vector();

		if (player.dockingTo !== null) {
			this.parentAI.requestStateChange(COSSETTE_STATES.WANDERING);
			return;
		}
		if (Vector.diff(this.thing.pos, player.pos).mag() > 5) {
			this.parentAI.requestStateChange(COSSETTE_STATES.WANDERING);
			return;
		}
		this._timeToDrag -= dt;
		if (this._timeToDrag <= 0) {
			this.parentAI.requestStateChange(COSSETTE_STATES.WANDERING);
			return;
		}

		// TODO: This code should live in some generic tractor beam abstraction
		let tractorBeamFocus = Vector.sum(
			this.thing.pos,
			new Vector(-0.1, 0).rotateBy(this.thing.rot)
		);
		let desiredPullVector = Vector.diff(tractorBeamFocus, player.pos);
		player.ai.tractorBeamAcc = desiredPullVector.norm(
			10 * Math.abs(desiredPullVector.mag())
		);

		this._flyingAI.move(dt, time, this._destination);

		this.thing.space.game.unicenter.ship.achievements.recordAchievement(
			ACHIEVEMENTS.COSSETTE
		);
	}

	enterState() {
		// Choose a destination within 300 flares from current position, and within the
		// game area
		for (let tries = 0; tries < 100; ++tries) {
			this._destination = this.thing.pos.chooseNearby(300);
			if (CossetteAI.isWithinGameArea(this._destination)) break;
		}
		// Give cossette just enough time to reach the destination at max speed
		this._timeToDrag = Vector.diff(this._destination, this.thing.pos).mag()
						/ CossetteFlyingAI.SPEED;
	}
}



class CossetteAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// String representation of a function that takes in the in-game time
		// and returns sleep pos at that time
		new RequiredProperty("sleepPosGenerator", v => (typeof v === "string"))
	]);

	constructor(thing, config) {
		CossetteAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._state = COSSETTE_STATES.SLEEPING;

		this._stateAIs = new Map([
			[COSSETTE_STATES.SLEEPING, new CossetteSleepingAI(thing, {
				parentAI: this,
				sleepPosGenerator: config.sleepPosGenerator
			})],
			[COSSETTE_STATES.WANDERING, new CossetteWanderingAI(thing, {parentAI: this})],
			[COSSETTE_STATES.SEEKING, new CossetteSeekingAI(thing, {parentAI: this})],
			[COSSETTE_STATES.DRAGGING, new CossetteDraggingAI(thing, {parentAI: this})]
		]);
	}

	move(dt, time) {
		// Heal
		if (this._state !== COSSETTE_STATES.SLEEPING) {
			this.thing.health = Math.min(
				this.thing.health + 500 * dt,
				this.thing.maxHealth
			);
		}
		this._stateAIs.get(this._state).move(dt, time);
	}

	// Called by state AIs
	requestStateChange(nextState) {
		console.assert(Object.values(COSSETTE_STATES).includes(nextState));
		this._state = nextState;
		this._stateAIs.get(this._state).enterState();
	}

	recordDamage(damage) {
		this._stateAIs.get(COSSETTE_STATES.SLEEPING).recordDamage(damage);
	}

	// Return true iff pos is within the game area that cossette can travel to
	static isWithinGameArea(pos) {
		return pos.mag() <= 400;
	}
}



export class Cossette extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// String representation of a function that takes in the in-game time
		// and returns sleep pos at that time
		new RequiredProperty("sleepPosGenerator", v => (typeof v === "string")),
		new EitherOr([
			// Total in-game time in seconds
			new RequiredProperty("time", v => (Number.isFinite(v) && v >= 0))
		], [
			// pos of this cossette
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this station
			new OptionalProperty("vel", v => (v instanceof Vector)),
			// rot of this cossette, in radians
			new OptionalProperty("rot", v => (Number.isFinite(v))),
			// Health of this cossette
			new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	// Create a new cossette
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Cossette.CONFIG_STRUCTURE.test(config);

		let sleepPosGenerator = functionFactory(config.sleepPosGenerator);

		config.name = "Cossette";
		config.zIndex = Z_INDICES.BOSS;
		config.len = 1;
		if (config.pos === undefined) config.pos = sleepPosGenerator(config.time);
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 10000;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.COSSETTE;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/cossette.svg";
		config.rotateGpsIcon = true;
		config.showBossBar = true;
		super(config);

		this.ai = new CossetteAI(this, config);

		this._sleepPosGenerator = sleepPosGenerator;

		this._hull = new Sprite(getTexture("images/cossette/hull.svg"));
		this._hull.anchor.set(0.5, 0.5);
		this._hull.position.set(0, 0);
		this._hull.scale.set(1 / STANDARD_PPF);

		this._exhaust = new Sprite(getTexture("images/cossette/exhaust.svg"));
		this._exhaust.anchor.set(0.5, 0);
		this._exhaust.position.set(0, 0.155);
		this._exhaust.scale.set(1 / STANDARD_PPF);
		this._exhaust.visible = false;

		this.display.addChild(this._exhaust, this._hull);
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		this.ai.recordDamage(damage);
		if (this.health > damage) {
			this.health -= damage;
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// Prepare cossette for removal
	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		// Release the player from the tractor beam
		player.ai.tractorBeamAcc = new Vector();
	}
}
