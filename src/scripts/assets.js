// Asset management
//
// Covers sprites, fonts, etc.
//
// Import this directly, without i.js.

import {Assets} from "pixi.js";
import {logger} from "./i.js";

// Populated with textures once they are loaded
// TODO: This contains a font too, what to do with that?
let textures = null;

// Load all assets
export async function loadAssets() {
	textures = await Assets.load([
			"fonts/dejavu-sans.xml",
			"images/asteroids/body.svg",
			"images/asteroids/body-meatball.svg",
			"images/asteroids/crater.svg",
			"images/asteroids/crater-meatball.svg",
			"images/cossette/exhaust.svg",
			"images/cossette/hull.svg",
			"images/desolation/compass.svg",
			"images/desolation/damsel.svg",
			"images/desolation/plate.svg",
			"images/desolation/tower.svg",
			"images/guardians/box.svg",
			"images/icons/gps/component.svg",
			"images/icons/gps/cossette.svg",
			"images/icons/gps/damsel.svg",
			"images/icons/gps/meteorus.svg",
			"images/icons/gps/player.svg",
			"images/icons/gps/prankster.svg",
			"images/icons/gps/station.svg",
			"images/icons/resources/alloys.svg",
			"images/icons/resources/lightweaves.svg",
			"images/icons/resources/minerals.svg",
			"images/icons/resources/polymers.svg",
			"images/icons/chat.svg",
			"images/icons/dock.svg",
			"images/icons/energy.svg",
			"images/icons/gps.svg",
			"images/icons/healing.svg",
			"images/icons/health.svg",
			"images/icons/inventory.svg",
			"images/icons/profile.svg",
			"images/icons/refinery.svg",
			"images/icons/ship.svg",
			"images/icons/upgrader.svg",
			"images/icons/vulnerable.svg",
			"images/items/affine-adder.svg",
			"images/items/aluminum-chunk.svg",
			"images/items/aluminum-ore.svg",
			"images/items/axonic-emitter.svg",
			"images/items/cool-rock.svg",
			"images/items/cossettite.svg",
			"images/items/crude-oil.svg",
			"images/items/dendritic-receiver.svg",
			"images/items/flower.svg",
			"images/items/green-gem.svg",
			"images/items/gyroscope.svg",
			"images/items/nonlinear-activator.svg",
			"images/items/note.svg",
			"images/items/platinum-chunk.svg",
			"images/items/platinum-ore.svg",
			"images/items/prank-video.svg",
			"images/items/red-gem.svg",
			"images/items/siren.svg",
			"images/items/titanium-chunk.svg",
			"images/items/titanium-ore.svg",
			"images/items/wave-plate.svg",
			"images/items/yellow-gem.svg",
			"images/particles/beacony-call.svg",
			"images/particles/blaster.svg",
			"images/particles/sizy-blaster.svg",
			"images/particles/sleep.svg",
			"images/player/hull.svg",
			"images/player/exhaust.svg",
			"images/player/sphere.svg",
			"images/prankster/fortification.svg",
			"images/prankster/prankster.svg",
			"images/size-reversal/beacony.svg",
			"images/size-reversal/blaster.svg",
			"images/size-reversal/sizy.svg",
			"images/stations/action.svg",
			"images/stations/dock.svg",
			"images/stations/grandma.svg",
			"images/stations/morse.svg",
			"images/stations/origin.svg",
			"images/stations/spiderdome.svg",
			"images/arrow.svg",
			"images/blaster.svg",
			"images/boomdash.svg",
			"images/boomdash-direction.svg",
			"images/correlation-matrix.svg",
			"images/correlation-matrix-outline.svg",
			"images/dart.svg",
			"images/kren-sign.svg",
			"images/meteorus.svg",
			"images/nanobots.svg",
			"images/star.svg",
			"images/star-outline.svg"
		]);
}

// Get a texture
// Textures must be already loaded.
//
// Args:
//   name: name of the texture, which is usually its URL
export function getTexture(name) {
	if (textures === null) {
		logger.error("assets.js", `textures not loaded, attempted to get "${name}"`);
		return null;
	}

	return textures[name];
}