// Usage:
//     logger.error("file.js", "everything broke");

class Logger {
	// Create a new logger
	constructor() {}

	// Log an error
	// location and message are both strings
	error(location, message) {
		console.error(location + ": " + message);
	}

	// Log a warning
	// location and message are both strings
	warning(location, message) {
		console.warn(location + ": " + message);
	}

	// Assert that statement is true, log error otherwise
	assert(statement, location, message) {
		if (!statement) {
			this.error(location, message);
		}
	}
}

// Create an instance of Logger, to be used throughout the game
export let logger = new Logger();
