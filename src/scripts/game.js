import {Container, Text} from "pixi.js";
import {VERSION} from "./i.js";
import {keyboard, Keyboard} from "./i.js"; // ./keyboard.js
import {Thing} from "./i.js";
import {Space} from "./i.js"; // ./space.js
import {UIUtils} from "./i.js"; // ./ui/utils.js
import {Tooltipper} from "./i.js"; // ./ui/tooltipper.js
import {Unicenter} from "./i.js"; // ./unicenter/unicenter.js
import {Dashboard} from "./i.js"; // ./dashboard/dashboard.js
import {Respawner} from "./i.js"; // ./respawner.js
import {Debugger} from "./i.js";

// How often the game is saved if nothing of note happens (in seconds)
const IDLE_SAVE_PERIOD = 5;

export class Game extends Container {
	// Create a game
	//
	// Args:
	//   savefile - optional savefile (object)
	constructor(savefile) {
		super();

		this.cheatsUsed = savefile?.c ?? false;

		this.tooltipper = new Tooltipper(this);
		// Unicenter depends on the tooltipper
		this.unicenter = new Unicenter(this, savefile?.u);
		// Space depends on the unicenter
		this.space = new Space(this, savefile?.s);
		// Dashboard depends on the tooltipper
		this.dashboard = new Dashboard(this);
		this.respawner = new Respawner(this.space.player);

		// The debugger (this.debugger) is initialized when it is first accessed

		this.addChild(this.space);
		this.addChild(this.dashboard.display);
		this.addChild(this.unicenter.display);
		this.addChild(this.respawner);
		this.addChild(this.tooltipper.display);

		// TODO: Pop these controls when they are no longer needed
		keyboard.pushControl(new Keyboard.Control(
			"DEBUGGER",
			new Map([
				[".DEBUGGER", "TOGGLE_DEBUGGER"]
			]),
			new Set()
		));
		keyboard.pushControl(new Keyboard.Control(
			"GAME",
			new Map([
				[".UNICENTER", "SHOW_UNICENTER"]
			]),
			new Set()
		));
		// TODO: Move to Dart after implementing a better control stack system
		keyboard.pushControl(new Keyboard.Control(
			"DART",
			new Map([
				["_DART_MOVE_LEFT",  "MOVE_LEFT" ],
				["_DART_MOVE_RIGHT", "MOVE_RIGHT"],
				["_DART_TURN_LEFT",  "TURN_LEFT" ],
				["_DART_TURN_RIGHT", "TURN_RIGHT"],
				[".DART_FIRE",       "FIRE"      ]
			]),
			new Set()
		));

		this._timeSinceLastActivity = 0;

		this.eventMode = "static";
		this.addEventListener("pointerup",
			(e) => this._timeSinceLastActivity = 0
		);
		this.addEventListener("pointerdown",
			(e) => this._timeSinceLastActivity = 0
		);

		this._timeSinceLastSave = 0;
	}

	// Advance the game by one frame
	//
	// Args:
	//   dt         - time since last update, in seconds
	//   screenSize - vector containing the screen size, in pixels
	update(dt, screenSize) {
		if (this.debugger !== undefined) this.debugger.reportTickStart();

		if (keyboard.activeThisTick) this._timeSinceLastActivity = 0;
		this._timeSinceLastActivity += dt;
		if (this._timeSinceLastActivity < 5) {
			this.unicenter.ship.reportStat("TIME_ACTIVE", dt);
			if (this.unicenter.ship.numPlayers === 2) {
				this.unicenter.ship.reportStat("TIME_IN_DATE_MODE", dt);
			}
		}

		if (keyboard.getActions("GAME").has("SHOW_UNICENTER")) {
			this.unicenter.show();
		}

		if (keyboard.getActions("DEBUGGER").has("TOGGLE_DEBUGGER")) {
			if (this.debugger === undefined) {
				this.debugger = new Debugger(this);
				this.addChild(this.debugger.display);
			} else {
				this.debugger.toggle();
			}
		}

		if (this.debugger !== undefined) this.debugger.reportDt(dt);

		// The unicenter now handles upgrades and stuff, so update it first to
		// avoid a 1-tick delay on upgrades
		this.unicenter.update(dt, screenSize);
		if (this.space.player.state === Thing.STATES.LIMBO) {
			this.respawner.show();
		}
		this.respawner.update(screenSize);

		this.space.update(dt, screenSize);
		this.dashboard.update(dt, screenSize);
		this.tooltipper.update(dt, screenSize);
		if (this.debugger !== undefined) this.debugger.update(screenSize);

		this._timeSinceLastSave += dt;
		if (this._timeSinceLastSave > IDLE_SAVE_PERIOD) this.save();

		if (this.debugger !== undefined) this.debugger.reportTickEnd();
	}

	// Save the state of the game into an object
	save() {
		let saveContents = {
			s: this.space.save(),
			u: this.unicenter.save(),
			v: VERSION.slice(1)
		}
		if (this.cheatsUsed) saveContents.c = true;
		localStorage.setItem("singularSave", JSON.stringify(saveContents));
		this._timeSinceLastSave = 0;
	}
}
