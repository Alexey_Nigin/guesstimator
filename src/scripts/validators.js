import {logger} from "./i.js"; // ./logger.js

export class ConfigStructure {
	// Create a new config structure
	// structure is an array containing elements of types RequiredProperty,
	// OptionalProperty, and EitherOr.
	constructor(structure) {
		if (!Array.isArray(structure)) {
			logger.error("validators.js", "config structure is not an array");
		}
		for (let element of structure) {
			if (!(element instanceof Property)
							&& !(element instanceof EitherOr)) {
				logger.error("validators.js", "an element of config structure"
								+ ` has invalid type "${typeof element}"`);
			}
		}

		this._structure = structure;
	}

	// Test whether config follows the structure
	// This method does nothing if the test succeeds, and throws
	// InvalidConfigError if the test fails.
	test(config) {
		let [allGood, msg] = ConfigStructure._testHelper(config,
						this._structure);
		if (!allGood) {
			throw new InvalidConfigError(msg);
		}
	}

	// Test whether config follows structurePart
	// This is a recursion-friendly helper method used by test(). It returns an
	// array with two elements: firstly, a boolean that is true iff everything
	// is good, and secondly, a string explaining what went wrong, if anything.
	static _testHelper(config, structurePart) {
		for (let element of structurePart) {
			if (element instanceof EitherOr) {
				let [allGoodA, msgA] = ConfigStructure._testHelper(config,
								element.either);
				if (allGoodA) continue;
				let [allGoodB, msgB] = ConfigStructure._testHelper(config,
								element.or);
				if (allGoodB) continue;
				return [false, `either/or check failed (either: ${msgA},`
								+ ` or: ${msgB})`];
			} else {
				let [allGood, msg] = element.test(config);
				if (!allGood) return [false, msg];
			}
		}

		return [true, ""];
	}
}



export class InvalidConfigError extends Error {
	constructor(...args) {
		super(...args);
		this.name = "InvalidConfigError";
	}
}



class Property {
	// Create a new property
	// name is the name of the property (a string); isValid is a function that
	// takes in the value of that property and returns true if the value is
	// valid, and false if it is invalid. Don't create instances of Property
	// directly, this is an abstract class that should only be used to create
	// child classes.
	constructor(name, isValid) {
		if (typeof name !== "string") {
			logger.error("validators.js", `${name} is not a string`);
		}
		if (!(isValid instanceof Function)) {
			logger.error("validators.js", `${isValid} is not a function`);
		}
		this._name = name;
		this._isValid = isValid;
	}
}



export class RequiredProperty extends Property {
	// Test whether the config has this required property
	// This function returns an array with two elements: firstly, a boolean that
	// is true iff everything is good, and secondly, a string explaining what
	// went wrong, if anything.
	test(config) {
		if (config[this._name] === undefined) {
			return [false, `missing property "${this._name}"`];
		}
		if (!this._isValid(config[this._name])) {
			return [false, `property "${this._name}" has invalid value`
							+ ` "${JSON.stringify(config[this._name])}"`];
		}
		return [true, ""];
	}
}



export class OptionalProperty extends Property {
	// Test whether the config has this optional property
	// This function returns an array with two elements: firstly, a boolean that
	// is true iff everything is good, and secondly, a string explaining what
	// went wrong, if anything.
	test(config) {
		if (config[this._name] === undefined) {
			return [true, ""];
		}
		if (!this._isValid(config[this._name])) {
			return [false, `property "${this._name}" has invalid value`
							+ ` "${JSON.stringify(config[this._name])}"`];
		}
		return [true, ""];
	}
}



export class EitherOr {
	// Create a new either / or
	// Both either and or are arrays containing elements of types
	// RequiredProperty, OptionalProperty, and EitherOr.
	constructor(either, or) {
		EitherOr._verifyArray(either);
		EitherOr._verifyArray(or);
		this.either = either;
		this.or = or;
	}

	// This class doesn't do any testing, it's just a container.

	// Verify the either / or arrays have correct structure
	// This is a helper method used by the constructor. This method does nothing
	// if array looks good, and logs an error if the array doesn't look good.
	static _verifyArray(array) {
		if (!Array.isArray(array)) {
			logger.error("validators.js", "either / or is not an array");
		}
		for (let element of array) {
			if (!(element instanceof Property)
							&& !(element instanceof EitherOr)) {
				logger.error("validators.js", "an element of either / or"
								+ ` has invalid type "${typeof element}"`);
			}
		}
	}
}
