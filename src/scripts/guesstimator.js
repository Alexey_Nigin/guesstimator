// Usage:
//     class Space extends PIXI.Container {
//         constructor() {
//             ...
//             this.guesstimator = new Guesstimator(this);
//             ...
//         }
//         ...
//         someCoolMethod() {
//             this.guesstimator.updateBubble(new Vector(
//                 screenWidthInFlares, screenHeightInFlares
//             ));
//             ...
//             someDensity *= this.guesstimator.getOversize();
//             ...
//             somePos = this.guesstimator.selectSpawn();
//             ...
//             if (this.guesstimator.isOutsideBubble(something.pos)) ...
//             ...
//         }
//         ...
//     }

import {Vector} from "./i.js"; // ./vector.js
import {Random} from "./i.js";
import {logger} from "./i.js"; // ./logger.js
import {sq} from "./i.js"; // ./dump.js

// How far the bubble extends beyond the screen, in flares
const BUBBLE_EXTENSION = 5;
// Thickness of the margin where spawning happens, in flares
const GUESSTIMATION_MARGIN = 2;

export class Guesstimator {
	// Create a new guesstimator
	// I recommend creating only one guesstimator per space.
	// Args:
	//   space - reference to space
	constructor(space) {
		if (space === undefined) {
			logger.error("guesstimator.js", "space is undefined");
		}
		this.space = space;

		// Size and shape of the bubble are defined by the _x and _y arrays, like this:
		//
		//    _x0_x1                   _x2_x3
		// _y0  ,-+---------------------+-,      (imagine the poorly drawn curves are
		//     /  |                     |  \     quarter circles)
		// _y1 +--+---------------------+--+
		//     |  |                     |  |
		//     |  |                     |  |     the inner rectangle is the screen,
		//     |  |          >          |  |     centered on the player for now
		//     |  |                     |  |
		//     |  |                     |  |     all these values are absolute, and
		// _y2 +--+---------------------+--+     in flares
		//     \  |                     |  /
		// _y3  `-+---------------------+-`
		this._x = Array(4).fill(null);
		this._y = Array(4).fill(null);

		// Centers of the quarter circles at the corners of the bubble
		this._circleCenters = [];
	}

	// Update the size of the bubble for the tick
	// Call at the start of each tick, before calling other guesstimator methods.
	// Args:
	//   screenFlares - size of the screen, a vector in flares
	updateBubble(screenFlares) {
		let playerPos = this.space.player.pos;

		this._x[0] = playerPos.x - screenFlares.x / 2 - BUBBLE_EXTENSION;
		this._x[1] = playerPos.x - screenFlares.x / 2;
		this._x[2] = playerPos.x + screenFlares.x / 2;
		this._x[3] = playerPos.x + screenFlares.x / 2 + BUBBLE_EXTENSION;

		this._y[0] = playerPos.y - screenFlares.y / 2 - BUBBLE_EXTENSION;
		this._y[1] = playerPos.y - screenFlares.y / 2;
		this._y[2] = playerPos.y + screenFlares.y / 2;
		this._y[3] = playerPos.y + screenFlares.y / 2 + BUBBLE_EXTENSION;

		this._circleCenters = [
			new Vector(this._x[1], this._y[1]),
			new Vector(this._x[1], this._y[2]),
			new Vector(this._x[2], this._y[1]),
			new Vector(this._x[2], this._y[2])
		];
	}

	// Return (area of the bubble / area of the screen)
	getOversize() {
		// Calculate the area of the big rectangle bounding the entire bubble, then
		// make an adjustment to replace its corners with quarter circles
		let bubbleArea
			= (this._x[3] - this._x[0]) * (this._y[3] - this._y[0])
			- (4 - Math.PI) * sq(BUBBLE_EXTENSION);

		let screenArea = (this._x[2] - this._x[1]) * (this._y[2] - this._y[1]);
		return bubbleArea / screenArea;
	}

	// Select a spawn point for a thing
	// Randomly selects a point near the edge of the bubble and returns it as a
	// vector.
	selectSpawn() {
		// This selection algorithm has a slight bias towards choosing points in the
		// corners and deeper inside the bubble, but it's not significant
		let locationWeights = [
			this._y[2] - this._y[1],       // Left
			this._y[2] - this._y[1],       // Right
			this._x[2] - this._x[1],       // Top
			this._x[2] - this._x[1],       // Bottom
			2 * Math.PI * BUBBLE_EXTENSION // Corners
		];
		switch (Random.weighted(locationWeights)) {
			case 0:  // Left
				return new Vector(
					Random.uniform(this._x[0], this._x[0] + GUESSTIMATION_MARGIN),
					Random.uniform(this._y[1], this._y[2])
				);
			case 1:  // Right
				return new Vector(
					Random.uniform(this._x[3] - GUESSTIMATION_MARGIN, this._x[3]),
					Random.uniform(this._y[1], this._y[2])
				);
			case 2:  // Top
				return new Vector(
					Random.uniform(this._x[1], this._x[2]),
					Random.uniform(this._y[0], this._y[0] + GUESSTIMATION_MARGIN)
				);
			case 3:  // Bottom
				return new Vector(
					Random.uniform(this._x[1], this._x[2]),
					Random.uniform(this._y[3] - GUESSTIMATION_MARGIN, this._y[3])
				);
			case 4:  // Corners
				let spawnLocation = new Vector(
					Random.uniform(
						BUBBLE_EXTENSION - GUESSTIMATION_MARGIN,
						BUBBLE_EXTENSION
					),
					0
				).rotateBy(Random.angle());
				spawnLocation.x += (spawnLocation.x > 0) ? this._x[2] : this._x[1];
				spawnLocation.y += (spawnLocation.y > 0) ? this._y[2] : this._y[1];
				return spawnLocation;
			default:
				logger.error("guesstimator.js", "impossible spawn location");
				return new Vector();
		}
	}

	// Select a random point in the bubble
	// This method can return a point anywhere in the bubble, including right next to the
	// player. This is different from selectSpawn(), which only returns points near the
	// edges of the bubble.
	selectPointInsideBubble() {
		// 4 regions:
		//   A - big horizontal rectangle
		//   B - top small rectangle
		//   C - bottom small rectangle
		//   D - corners

		let areaA = (this._x[3] - this._x[0]) * (this._y[2] - this._y[1]);
		// B and C have the same area
		let areaBC = (this._x[2] - this._x[1]) * (this._y[1] - this._y[0]);
		let areaD = Math.PI * sq(BUBBLE_EXTENSION);

		switch (Random.weighted([areaA, areaBC, areaBC, areaD])) {
			case 0:
				// Region A
				return new Vector(
					Random.uniform(this._x[0], this._x[3]),
					Random.uniform(this._y[1], this._y[2])
				);
			case 1:
				// Region B
				return new Vector(
					Random.uniform(this._x[1], this._x[2]),
					Random.uniform(this._y[0], this._y[1])
				);
			case 2:
				// Region C
				return new Vector(
					Random.uniform(this._x[1], this._x[2]),
					Random.uniform(this._y[2], this._y[3])
				);
			case 3:
				// Region D
				let spawnLocation = Vector.chooseLimited(BUBBLE_EXTENSION);
				spawnLocation.x += (spawnLocation.x > 0) ? this._x[2] : this._x[1];
				spawnLocation.y += (spawnLocation.y > 0) ? this._y[2] : this._y[1];
				return spawnLocation;
			default:
				logger.error("guesstimator.js", "impossible point location");
				return new Vector();
		}
	}

	// Return true iff pos is outside the bubble around the player
	// pos is a vector.
	isOutsideBubble(pos) {
		let isInsideHorizontalRectangle = (pos.x >= this._x[0]) && (pos.x <= this._x[3])
			&& (pos.y >= this._y[1]) && (pos.y <= this._y[2]);
		if (isInsideHorizontalRectangle) return false;

		let isInsideVerticalRectangle = (pos.x >= this._x[1]) && (pos.x <= this._x[2])
			&& (pos.y >= this._y[0]) && (pos.y <= this._y[3]);
		if (isInsideVerticalRectangle) return false;

		for (let circleCenter of this._circleCenters) {
			let isInsideCircle = Vector.diff(pos, circleCenter).mag()
				<= BUBBLE_EXTENSION;
			if (isInsideCircle) return false;
		}

		return true;
	}
}
