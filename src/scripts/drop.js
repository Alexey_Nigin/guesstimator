import {Random} from "./i.js"; // ./random.js
import {Vector} from "./i.js"; // ./vector.js
import {logger} from "./i.js"; // ./logger.js
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {AIUtils} from "./i.js"; // ./ai.js
import {Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {PranksterAI} from "./i.js"; // ./prankster.js

// Drops: items and components

export const RESOURCES = {
	MINERALS: "MINERALS",
	ALLOYS:   "ALLOYS",
	POLYMERS: "POLYMERS",
	LIGHTWEAVES: "LIGHTWEAVES"
};

export const RESOURCE_INFO = new Map([
	[
		RESOURCES.MINERALS,
		{
			texture: "images/icons/resources/minerals.svg",
			displayName: "Minerals"
		}
	],
	[
		RESOURCES.ALLOYS,
		{
			texture: "images/icons/resources/alloys.svg",
			displayName: "Alloys"
		}
	],
	[
		RESOURCES.POLYMERS,
		{
			texture: "images/icons/resources/polymers.svg",
			displayName: "Polymers"
		}
	],
	[
		RESOURCES.LIGHTWEAVES,
		{
			texture: "images/icons/resources/lightweaves.svg",
			displayName: "Lightweaves"
		}
	],
]);

// Keep this alphabetical
export const ITEMS = {
	AFFINE_ADDER:        "AFFINE_ADDER",
	ALUMINUM_CHUNK:      "ALUMINUM_CHUNK",
	ALUMINUM_ORE:        "ALUMINUM_ORE",
	AXONIC_EMITTER:      "AXONIC_EMITTER",
	COOL_ROCK:           "COOL_ROCK",
	COSSETTITE:          "COSSETTITE",
	CRUDE_OIL:           "CRUDE_OIL",
	DENDRITIC_RECEIVER:  "DENDRITIC_RECEIVER",
	FLOWER:              "FLOWER",
	GREEN_GEM:           "GREEN_GEM",
	GYROSCOPE:           "GYROSCOPE",
	NONLINEAR_ACTIVATOR: "NONLINEAR_ACTIVATOR",
	NOTE_WELCOME:        "NOTE_WELCOME",
	PLATINUM_CHUNK:      "PLATINUM_CHUNK",
	PLATINUM_ORE:        "PLATINUM_ORE",
	PRANK_VIDEO:         "PRANK_VIDEO",
	RED_GEM:             "RED_GEM",
	SIREN:               "SIREN",
	TITANIUM_CHUNK:      "TITANIUM_CHUNK",
	TITANIUM_ORE:        "TITANIUM_ORE",
	WAVE_PLATE:          "WAVE_PLATE",
	YELLOW_GEM:          "YELLOW_GEM"
};

// Keep this alphabetical
export const ITEM_INFO = new Map([
	[
		ITEMS.AFFINE_ADDER,
		{
			texture: "images/items/affine-adder.svg",
			len: 0.14,
			displayName: "Affine adder",
			description: "You wonder why this item is called \"affine\". "
				+ "Is it even a word? "
				+ "You vaguely remember that there was an ancient city with a similar "
				+ "name, but it's probably unrelated. "
				+ "How do you know about that city, anyway?",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 3]
			])
		}
	],
	[
		ITEMS.ALUMINUM_CHUNK,
		{
			texture: "images/items/aluminum-chunk.svg",
			len: 0.14,
			displayName: "Aluminum chunk",
			description: "You have trouble telling apart all the different"
							+ " metal chunks. This one feels really light.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS,   3]
			])
		}
	],
	[
		ITEMS.ALUMINUM_ORE,
		{
			texture: "images/items/aluminum-ore.svg",
			len: 0.14,
			displayName: "Aluminum ore",
			description: "This rock is really spotty.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 1],
				[RESOURCES.ALLOYS,   1]
			])
		}
	],
	[
		ITEMS.AXONIC_EMITTER,
		{
			texture: "images/items/axonic-emitter.svg",
			len: 0.14,
			displayName: "Axonic emitter",
			description: "This electrode keeps shooting out red lightning. "
				+ "Now that it's disconnected from the rest of the circuirty, "
				+ "the lightning is harmless, but still plenty annoying. "
				+ "Fortunaly, most of the lightning gets absorbed by dendritic receivers "
				+ "lying nearby.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 9]
			])
		}
	],
	[
		ITEMS.COOL_ROCK,
		{
			texture: "images/items/cool-rock.svg",
			len: 0.14,
			displayName: "Cool rock",
			description: "You picked up this rock because it looked kinda cool."
							+ " Could you also pick up the boring rocks and get"
							+ " more minerals for upgrades? Yes. Did you feel"
							+ " like doing that? No.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 1]
			])
		}
	],
	[
		ITEMS.COSSETTITE,
		{
			texture: "images/items/cossettite.svg",
			len: 0.16,
			displayName: "Cossettite",
			description: "This metal shard has incredible regenerative properties. You"
							+ " tried to flatten it into a sheet for a DIY project, but"
							+ " it seemingly came to life and warped back to its original"
							+ " shape. That was a little creepy.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 3]
			])
		}
	],
	[
		ITEMS.CRUDE_OIL,
		{
			texture: "images/items/crude-oil.svg",
			len: 0.16,
			displayName: "Crude oil",
			description: "You wonder what sorts of ancient creatures this oil"
							+ " came from.",
			refinesTo: new Map([
				[RESOURCES.POLYMERS, 1]
			])
		}
	],
	[
		ITEMS.DENDRITIC_RECEIVER,
		{
			texture: "images/items/dendritic-receiver.svg",
			len: 0.14,
			displayName: "Dendritic receiver",
			description: "This branching wire pulls energy from everything in its "
				+ "vicinity. Lights dim, batteries lose charge, electronics malfunction, "
				+ "and even you feel weak in the knees when you stand near it.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 1]
			])
		}
	],
	[
		ITEMS.FLOWER,
		{
			texture: "images/items/flower.svg",
			len: 0.20,
			displayName: "Flower",
			description: "This flower was given to you by the DAMSEL as a thank-you for"
							+ " saving her from those scary towers. It's clearly"
							+ " artificial, but still looks nice.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS,    5],
				[RESOURCES.POLYMERS, 10]
			])
		}
	],
	[
		ITEMS.GREEN_GEM,
		{
			texture: "images/items/green-gem.svg",
			len: 0.14,
			displayName: "Green gem",
			description: "This gem has a metallic glint when you hold it"
							+ " against the light.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 2],
				[RESOURCES.ALLOYS,   1]
			])
		}
	],
	[
		ITEMS.GYROSCOPE,
		{
			texture: "images/items/gyroscope.svg",
			len: 0.16,
			displayName: "Gyroscope",
			description: "This gyroscope kept spinning for a solid hour after"
							+ " you hauled it in.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS,   2]
			])
		}
	],
	[
		ITEMS.NONLINEAR_ACTIVATOR,
		{
			texture: "images/items/nonlinear-activator.svg",
			len: 0.14,
			displayName: "Nonlinear activator",
			description: "You get the feeling that all these circuits, "
				+ "if you think about them enough, "
				+ "could reveal a big plot twist about DAMSEL's Desolation.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 6]
			])
		}
	],
	[
		ITEMS.NOTE_WELCOME,
		{
			texture: "images/items/note.svg",
			len: 0.16,
			displayName: "Note",
			description: "The note reads,\n\n"
				+ "\"good morning! here are a few items to get you started :)\n"
				+ "-A\"\n\n"
				+ "The signature on the note is written in pretty purple ink.",
			refinesTo: new Map([
				[RESOURCES.POLYMERS,   1]
			])
		}
	],
	[
		ITEMS.PLATINUM_CHUNK,
		{
			texture: "images/items/platinum-chunk.svg",
			len: 0.14,
			displayName: "Platinum chunk",
			description: "This metal chunk is definitely way heavier than the"
							+ " rest. So heavy, in fact, that you nearly"
							+ " dropped it on your foot the first time you"
							+ " tried casually picking it up.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS,   12]
			])
		}
	],
	[
		ITEMS.PLATINUM_ORE,
		{
			texture: "images/items/platinum-ore.svg",
			len: 0.14,
			displayName: "Platinum ore",
			description: "You could lick this rock. But you probably shouldn't."
							+ " Must resist intrusive thoughts.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 1],
				[RESOURCES.ALLOYS,   4]
			])
		}
	],
	[
		ITEMS.PRANK_VIDEO,
		{
			texture: "images/items/prank-video.svg",
			len: 0.24,
			displayName: "Prank video",
			description: "The video on this tablet depicts the Gyroscope Prankster"
							+ " putting gyroscopes into a bunch of asteroids. You're not"
							+ " sure how this counts as a prank, but at least it"
							+ " explains where all the wobbly asteroids came from.",
			refinesTo: new Map([
				[RESOURCES.POLYMERS, 10]
			])
		}
	],
	[
		ITEMS.RED_GEM,
		{
			texture: "images/items/red-gem.svg",
			len: 0.14,
			displayName: "Red gem",
			description: "This simple gem gives you a comfortable, almost"
							+ " nostalgic feeling.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 2]
			])
		}
	],
	[
		ITEMS.SIREN,
		{
			texture: "images/items/siren.svg",
			len: 0.14,
			displayName: "Siren",
			description: "If you ever decide to organize a rave, this item will "
				+ "certainly come in handy.",
			refinesTo: new Map([
				[RESOURCES.POLYMERS, 2]
			])
		}
	],
	[
		ITEMS.TITANIUM_CHUNK,
		{
			texture: "images/items/titanium-chunk.svg",
			len: 0.14,
			displayName: "Titanium chunk",
			description: "You could make an unconventional engagement ring out of this."
							+ " But out here you could only get engaged to yourself,"
							+ " for lack of other candidates.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS,   6]
			])
		}
	],
	[
		ITEMS.TITANIUM_ORE,
		{
			texture: "images/items/titanium-ore.svg",
			len: 0.14,
			displayName: "Titanium ore",
			description: "This white powder has stained every part of the cargo"
							+ " bay it touched.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 1],
				[RESOURCES.ALLOYS,   2]
			])
		}
	],
	[
		ITEMS.WAVE_PLATE,
		{
			texture: "images/items/wave-plate.svg",
			len: 0.14,
			displayName: "Wave plate",
			description: "This massive plate is statically charged. "
				+ "Your hair stands on end when you touch it, and that looks very goofy.",
			refinesTo: new Map([
				[RESOURCES.ALLOYS, 1]
			])
		}
	],
	[
		ITEMS.YELLOW_GEM,
		{
			texture: "images/items/yellow-gem.svg",
			len: 0.14,
			displayName: "Yellow gem",
			description: "In the center of this gem, you can vaguely see the"
							+ " preserved remains of a mysterious alien"
							+ " arthropod. You tried cracking the gem to look"
							+ " at the arthropod more closely - not like the"
							+ " refinery would care about the gem being broken"
							+ " - but it wouldn't budge.",
			refinesTo: new Map([
				[RESOURCES.MINERALS, 2],
				[RESOURCES.POLYMERS, 1]
			])
		}
	]
]);

// Keep this alphabetical
export const COMPONENTS = {
	CORRELATION_MATRIX: "CORRELATION_MATRIX",
	STAR: "STAR"
};

// Keep this alphabetical
export const COMPONENT_INFO = new Map([
	[
		COMPONENTS.CORRELATION_MATRIX,
		{
			texture: "images/correlation-matrix.svg"
		}
	],
	[
		COMPONENTS.STAR,
		{
			texture: "images/star.svg"
		}
	]
]);



class Drop extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Filename string for the texture
		// Creates a container if omitted. Because a container is like an empty
		// sprite right?
		new OptionalProperty("texture", v => ((typeof v === "string")
						&& (true))), // TODO check that texture exists
		// Length of this drop, in flares
		new RequiredProperty("len", v => (Number.isFinite(v) && v > 0)),
		// Whether this drop can despawn
		new RequiredProperty("canDespawn", v => (v === false || v === true)),
		// Window during which this drop can't despawn, in seconds
		// Defaults to null if omitted, also gets overridden with null if
		// canDespawn is false.
		new OptionalProperty("despawnSafetyWindow",
						v => (Number.isFinite(v) && v >= 0)),
		new EitherOr([
			// pos of the parent thing
			new RequiredProperty("parentPos", v => (v instanceof Vector)),
			// Radius of the parent thing, in flares
			new RequiredProperty("parentRadius",
							v => (Number.isFinite(v) && v > 0)),
			// vel of the parent thing, a vector with values in flares / second
			new RequiredProperty("parentVel", v => (v instanceof Vector))
		], [
			// pos of this drop, a vector with values in flares
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this drop, a vector with values in flares / second
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// rot of this drop, in radians
			new RequiredProperty("rot", v => (Number.isFinite(v))),
			// Health of this drop
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	// The limit for the initial speed of the drop
	// Afterwards, the drop can accelerate to exceed this speed limit.
	static SPEED_LIMIT = 0.5;  // flares / second
	static FRICTION = 2;
	// Increase this constant and drops will be pulled faster
	static PULL_STRENGTH = 100;
	// Increase this constant and drops will be pulled from further away
	// Technically, the items are pulled from any distance, the strength of the
	// pull just fades exponentially with distance and this constant determines
	// how fast or slowly it fades.
	static PULL_RADIUS = 0.2;  // flares

	// Create a drop
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	// Don't create instances of Drop directly, this class should only be used
	// to create child classes.
	constructor(config) {
		Drop.CONFIG_STRUCTURE.test(config);

		config.zIndex = Z_INDICES.DROP;
		if (config.pos === undefined) {
			config.pos = config.parentPos.chooseNearby(config.parentRadius
							- config.len / 2);
		}
		if (config.vel === undefined) {
			config.vel = config.parentVel.chooseNearby(Drop.SPEED_LIMIT);
		}
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = 1;
		config.type = THING_TYPES.DROP;
		super(config);
	}

	// Move the drop
	// dt is the time since last update, in seconds.
	move(dt) {
		let tacticalReport = this.space.requestTacticalReport(
			entry => entry.CAN_PICK_UP_ITEMS,
			"DROP"
		);

		// Pull this drop towards all things with CAN_PICK_UP_ITEMS === true
		// Usually the only thing with that tag is the player.
		let acc = new Vector();
		for (let entry of tacticalReport) {
			let toTarget = Vector.diff(entry.pos, this.pos);
			acc.add(toTarget.norm(
				Drop.PULL_STRENGTH
				* Math.exp(- toTarget.mag() / Drop.PULL_RADIUS)
			));
		}
		AIUtils.translate(this, acc, Drop.FRICTION, dt);
	}

	// Record that the drop was picked up
	// Called by the interactor.
	recordPickup() {
		this.prepareToDie(false);
	}
}



export class Item extends Drop {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// An item name from ITEMS
		new RequiredProperty("itemName",
						v => Object.values(ITEMS).includes(v)),
		new EitherOr([
			// pos of the parent thing
			new RequiredProperty("parentPos", v => (v instanceof Vector)),
			// Radius of the parent thing, in pixels
			new RequiredProperty("parentRadius",
							v => (Number.isFinite(v) && v > 0)),
			// vel of the parent thing
			new RequiredProperty("parentVel", v => (v instanceof Vector))
		], [
			// pos of this gem
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this gem
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// rot of this gem, in radians
			new RequiredProperty("rot", v => (Number.isFinite(v))),
			// Health of this gem
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	// Create an item
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Item.CONFIG_STRUCTURE.test(config);

		config.texture = ITEM_INFO.get(config.itemName).texture;
		// Temporary scaling - let's see how this looks
		config.len = 1.5 * ITEM_INFO.get(config.itemName).len;
		config.canDespawn = true;
		config.despawnSafetyWindow = 10;
		super(config);

		this.itemName = config.itemName;
	}

	move(dt, time) {
		super.move(dt, time);

		// Gyroscopes continue to wobble when dropped
		if (this.itemName === ITEMS.GYROSCOPE) {
			let wobblePhase = Math.sin(
				(time / PranksterAI.WOBBLE_PERIOD) * (2 * Math.PI)
			);
			AIUtils.setRot(this, - Math.PI / 2 + wobblePhase * (Math.PI / 8), dt);
		}
	}

	// Save this item into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;
		return {
			itemName:   this.itemName,
			config:     config,
			isComplete: true
		};
	}
}



export class Component extends Drop {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Component name from COMPONENTS
		new RequiredProperty("componentName",
						v => Object.values(COMPONENTS).includes(v)),
		new EitherOr([
			// pos of the parent thing
			new RequiredProperty("parentPos", v => (v instanceof Vector)),
			// Radius of the parent thing, in pixels
			new RequiredProperty("parentRadius",
							v => (Number.isFinite(v) && v > 0)),
			// vel of the parent thing
			new RequiredProperty("parentVel", v => (v instanceof Vector))
		], [
			// pos of this component
			new RequiredProperty("pos", v => (v instanceof Vector)),
			// vel of this component
			new RequiredProperty("vel", v => (v instanceof Vector)),
			// rot of this component, in radians
			new RequiredProperty("rot", v => (Number.isFinite(v))),
			// Health of this component
			new RequiredProperty("health", v => (Number.isFinite(v) && v >= 0))
		])
	]);

	// Create a component
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Component.CONFIG_STRUCTURE.test(config);

		config.texture = COMPONENT_INFO.get(config.componentName).texture;
		config.len = 0.24;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/component.svg";
		super(config);

		this.componentName = config.componentName;
	}

	// Save this component into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;
		return {
			config:     config,
			isComplete: true
		};
	}
}



// A list of drops for a thing
export class DropList {
	// Create a new drop list
	//
	// Args:
	//   items    - array of item entries:
	//     [
	//       [
	//         itemName      - item name from ITEMS,
	//         amounts       - amounts of the item that can drop
	//         probabilities - probability for each amount
	//       ],
	//     ]
	//   minItems - minimum number of items to drop, integer, defaults to 0 if omitted
	//   components - array of component names from COMPONENTS, defaults to [] if omitted
	//
	// Note: This constructor keeps and modifies referenced objects.
	constructor(items, minItems, components) {
		this._items = [];
		this._minItems = 0;
		this._components = [];

		// Check that the entire items array is sufficiently well-formed to loop through
		let isValid = Array.isArray(items)
			&& items.every(item => Array.isArray(item) && item.length === 3);
		if (!isValid) {
				logger.error(
					"drop.js",
					`items have wrong structure: ${JSON.stringify(items)}`
				);
				return;
		}

		for (let [itemName, amounts, probabilities] of items) {
			// Check the specific item entry
			let isValid = Object.values(ITEMS).includes(itemName)
				&& Array.isArray(amounts)
				&& amounts.every(a => (Number.isInteger(a) && a > 0))
				&& Array.isArray(probabilities)
				&& probabilities.every(p => (Number.isFinite(p) && p > 0 && p <= 1))
				&& amounts.length > 0 && amounts.length === probabilities.length;
			if (!isValid) {
				logger.error(
					"drop.js",
					`invalid item entry: ${itemName}, ${JSON.stringify(amounts)}, `
						+ `${JSON.stringify(probabilities)}`
				);
				return;
			}

			// Add entry for 0
			let totalProb = probabilities.reduce((a, b) => a + b);
			if (totalProb > 1) {
				logger.error(
					"drop.js",
					`probabilities too large: ${itemName}, `
						+ `${JSON.stringify(probabilities)}`
				);
				return;
			}
			if (totalProb < 1) {
				amounts.push(0);
				probabilities.push(1 - totalProb);
			}

			// Compute expected amount
			let expectation = 0;
			for (let i = 0; i < amounts.length; ++i) {
				expectation += amounts[i] * probabilities[i];
			}

			this._items.push({
				itemName: itemName,
				amounts: amounts,
				probabilities: probabilities,
				expectation: expectation
			});
		}

		isValid = (minItems === undefined) || (
			Number.isInteger(minItems) && minItems >= 0
		);
		if (!isValid) {
				logger.error(
					"drop.js",
					`wrong minItems: ${minItems}`
				);
				return;
		}
		this._minItems = minItems ?? 0;

		isValid = (components === undefined) || (
			Array.isArray(components)
			&& components.every(c => Object.values(COMPONENTS).includes(c))
		);
		if (!isValid) {
				logger.error(
					"drop.js",
					`wrong components: ${JSON.stringify(components)}`
				);
				return;
		}
		this._components = components ?? [];
	}

	// Get a specific list of items to drop
	// Randomized, won't give the same results every time. Returns an array of
	// item names.
	getItems() {
		let itemsToDrop = [];

		for (let {itemName, amounts, probabilities} of this._items) {
			let amount = amounts[Random.weighted(probabilities)];
			for (let i = 0; i < amount; ++i) {
				itemsToDrop.push(itemName);
			}
		}

		// Drop extra items to reach minItems, if needed
		let expectations = this._items.map(itemEntry => itemEntry.expectation);
		while (itemsToDrop.length < this._minItems) {
			let indexToDrop = Random.weighted(expectations);
			itemsToDrop.push(this._items[indexToDrop].itemName);
			expectations[indexToDrop] = Math.max(expectations[indexToDrop] - 1, 0);
		}

		return itemsToDrop;
	}

	// Get a list of components to drop
	// Returns an array of component names.
	getComponents() {
		return Array.from(this._components);
	}
}
