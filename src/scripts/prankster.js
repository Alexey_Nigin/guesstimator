import {round} from "./utils.js";
import {Vector} from "./i.js"; // ./vector.js
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {AI, AIUtils} from "./i.js"; // ./ai.js
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {DropList, ITEMS} from "./i.js"; // ./drop.js
import {ACHIEVEMENTS} from "./i.js";

class PranksterFortificationAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the parent Prankster
		new RequiredProperty("parentId", v => (Number.isFinite(v) && v >= 0)),
		// Final radius of this fortification's orbit around the prankster
		new RequiredProperty("orbitRadius", v => (Number.isFinite(v) && v > 0)),
		new RequiredProperty("baseAngle", v => Number.isFinite(v))
	]);

	constructor(thing, config) {
		PranksterFortificationAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._parentId = config.parentId;
		this._orbitRadius = config.orbitRadius;
		this._baseAngle = config.baseAngle;

		this._radiusFraction = 0;
	}

	move(dt, time) {
		// Can assume the parent is alive

		this._radiusFraction = Math.min(this._radiusFraction + dt / 12, 1);

		let oscillationPhase = Math.sin(
			(time / 6) * (2 * Math.PI)
		);
		let orbitAngle = this._baseAngle + oscillationPhase * (Math.PI / 2);
		AIUtils.setPos(
			this.thing,
			new Vector(this._radiusFraction * this._orbitRadius, 0)
				.rotateBy(orbitAngle)
				.add(this.thing.space.requestThingById(this._parentId).pos),
			dt
		);

		let wobblePhase = Math.sin((time / PranksterAI.WOBBLE_PERIOD) * (2 * Math.PI));
		AIUtils.setRot(this.thing, - Math.PI / 2 + wobblePhase * (Math.PI / 4), dt);
	}
}



class PranksterFortification extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the parent Prankster
		new RequiredProperty("parentId", v => (Number.isFinite(v) && v >= 0)),
		// Final radius of this fortification's orbit around the prankster
		new RequiredProperty("orbitRadius", v => (Number.isFinite(v) && v > 0)),
		new RequiredProperty("baseAngle", v => Number.isFinite(v)),
		// pos of this fortification, a vector with values in flares
		new OptionalProperty("pos", v => (v instanceof Vector)),
		// vel of this fortification, a vector with values in
		// flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// rot of this fortification, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this fortification
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0))
	]);

	// Create a Prankster Fortification
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		PranksterFortification.CONFIG_STRUCTURE.test(config);

		let parent = config.space.requestThingById(config.parentId);

		config.texture = "images/prankster/fortification.svg";
		config.zIndex = Z_INDICES.BOSS_CHILD;
		config.len = 0.40;
		if (config.pos === undefined) config.pos = parent.pos;
		if (config.vel === undefined) config.vel = parent.vel;
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = 15;
		config.type = THING_TYPES.ASTEROID;
		config.faction = FACTIONS.METEORIC;
		config.canDespawn = false;
		super(config);

		this.ai = new PranksterFortificationAI(this, {
			parentId: config.parentId,
			orbitRadius: config.orbitRadius,
			baseAngle: config.baseAngle
		});
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.prepareToDie(false);
		}
	}
}



export class PranksterAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([
		new OptionalProperty("timeSincePlayerSeen", v => (Number.isFinite(v) && v >= 0))
	]);

	// Radius of the prankster's orbit around the Origin Station, in flares
	static ORBIT_RADIUS = 50;
	// Time it takes the center of the prankster's oscillation to make a full circle
	// around the Origin Station, in seconds
	static ORBITAL_PERIOD = 30 * 60;
	// Period of oscillations forwards/backwards on the orbit
	static OSCILLATION_PERIOD = 5 * 60;
	// Time it take the prankster to wobble back and forth (purely decorative), in seconds
	static WOBBLE_PERIOD = 1.5;

	static FORTIFICATION_RAYS = 2;
	static FORTIFICATIONS_PER_RAY = 4;

	constructor(thing, config) {
		PranksterAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._timeSincePlayerSeen = config.timeSincePlayerSeen ?? 0;

		let numFortifications = PranksterAI.FORTIFICATION_RAYS
						* PranksterAI.FORTIFICATIONS_PER_RAY;
		// id is null if fortification is ready to deploy
		this._fortificationIds = Array(numFortifications).fill(null);
	}

	save() {
		return {
			timeSincePlayerSeen: round(this._timeSincePlayerSeen)
		};
	}

	move(dt, time) {
		this._justMove(dt, time);
		this._attackAndDefend(dt, time);
	}

	removeAllFortifications() {
		for (let i = 0; i < this._fortificationIds.length; ++i) {
			if (this._fortificationIds[i] === null) continue;
			// Don't even need to check if the fortification is deployed
			let fortification = this.thing.space.requestThingById(
				this._fortificationIds[i]
			);
			if (fortification !== null) {
				fortification.prepareToDie();
			}
			this._fortificationIds[i] = null;
		}
	}

	_justMove(dt, time) {
		let oscillationPhase = Math.sin(
			(time / PranksterAI.OSCILLATION_PERIOD) * (2 * Math.PI)
		);
		let orbitAngle = Math.PI + (time / PranksterAI.ORBITAL_PERIOD) * (2 * Math.PI)
			+ oscillationPhase * (Math.PI / 4);
		AIUtils.setPos(
			this.thing,
			new Vector(PranksterAI.ORBIT_RADIUS, 0).rotateBy(orbitAngle),
			dt
		);

		let wobblePhase = Math.sin((time / PranksterAI.WOBBLE_PERIOD) * (2 * Math.PI));
		AIUtils.setRot(this.thing, - Math.PI / 2 + wobblePhase * (Math.PI / 4), dt);
	}

	_attackAndDefend(dt, time) {
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => entry.isPlayer,
			"PRANKSTER"
		).filter(entry => (Vector.diff(entry.pos, this.thing.pos).mag() <= 15));

		// The player is far away, reset health and fortifications
		if (tacticalReport.length === 0) {
			this._timeSincePlayerSeen += dt;
			if (this._timeSincePlayerSeen > 20) {
				this.removeAllFortifications();
				this.thing.health = this.thing.maxHealth;
			}
			return;
		}
		this._timeSincePlayerSeen = 0;

		// The player is close enough to not reset, but far enough to not deploy
		// fortifications
		if (Vector.diff(tacticalReport[0].pos, this.thing.pos) > 5) return;

		for (let i = 0; i < this._fortificationIds.length; ++i) {
			if (this._fortificationIds[i] === null) {
				let config = {
					parentId: this.thing.id,
					orbitRadius: 0.875 + 0.45
									* Math.floor(i / PranksterAI.FORTIFICATION_RAYS),
					baseAngle: (i % PranksterAI.FORTIFICATION_RAYS) * 2 * Math.PI
									/ PranksterAI.FORTIFICATION_RAYS
				};
				this._fortificationIds[i] = this.thing.space.requestSpawn(
					PranksterFortification,
					config
				).id;
			}
		}
	}
}



export class Prankster extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		new OptionalProperty("timeSincePlayerSeen", v => (Number.isFinite(v) && v >= 0))
	]);

	static DROP_LIST = new DropList([
		[ITEMS.COOL_ROCK,   [8], [1]],
		[ITEMS.GYROSCOPE,   [8], [1]],
		[ITEMS.PRANK_VIDEO, [2], [1]]
	]);

	// Create a Gyroscope Prankster
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Prankster.CONFIG_STRUCTURE.test(config);

		config.name = "Gyroscope Prankster";
		config.texture = "images/prankster/prankster.svg";
		config.zIndex = Z_INDICES.BOSS;
		config.len = 1.25;
		if (config.pos === undefined) config.pos = new Vector(
			- PranksterAI.ORBIT_RADIUS, 0
		);
		if (config.vel === undefined) config.vel = new Vector();
		if (config.rot === undefined) config.rot = 0;
		config.maxHealth = 200;
		config.type = THING_TYPES.ASTEROID;
		config.faction = FACTIONS.METEORIC;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/prankster.svg";
		config.rotateGpsIcon = true;
		config.showBossBar = true;
		super(config);
		this.ai = new PranksterAI(this, {
			timeSincePlayerSeen: config.timeSincePlayerSeen ?? 0
		});
	}

	// Save this prankster into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();
		let aiState = this.ai.save();

		config.timeSincePlayerSeen = aiState.timeSincePlayerSeen;

		return config;
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			// Die!
			this.space.game.unicenter.ship.achievements.recordAchievement(
				ACHIEVEMENTS.PRANK_BRO
			);
			this.prepareToDie(dropItems);
		}
	}

	prepareToDie(dropItems) {
		this.ai.removeAllFortifications();
		if (dropItems !== false) this.space.requestDrop(
			this.pos,
			this.radius,
			this.vel,
			Prankster.DROP_LIST
		);
		super.prepareToDie(dropItems);
	}
}
