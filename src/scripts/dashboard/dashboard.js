import {Container, Graphics, Sprite, Text} from "pixi.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {Thing} from "../i.js"; // ../thing.js
import {Player, PlayerAI} from "../i.js"; // ../player.js
import {UIUtils, Clicker} from "../i.js"; // ../ui/utils.js
import {WorldSight} from "../i.js";
import {Tooltip} from "../i.js"; // ../ui/tooltipper.js

class BossBar {
	constructor(dashboard) {
		this.dashboard = dashboard;

		this.display = new Container();
		this._labelBackground = new Graphics();
		this._bossName = new Text("", UIUtils.getFont(0xffffff));
		this._barBackground = new Graphics();
		this._bar = new Graphics();
		this._numbers = new Text("", UIUtils.getFont(0xffffff));
		this._barBorder = new Graphics();
		this._barMask = new Graphics();
		this.display.addChild(this._labelBackground, this._bossName, this._barBackground,
						this._bar, this._numbers, this._barBorder, this._barMask);

		this._barBackground.mask = this._barMask;
		this._bar.mask = this._barMask;
	}

	// entry is an object with at least the following properties:
	//   name
	//   health
	//   maxHealth
	update(dt, position, size, entry) {
		let fraction = entry.health / entry.maxHealth;

		let roundingRadius = size.y / 4;
		let barHeight = Math.round(size.y / 2);
		let barTop = size.y - barHeight;
		let lineWidth = Math.max(Math.round(0.02 * size.y), 1);

		this.display.position.copyFrom(position);

		this._labelBackground
			.clear()
			.moveTo(roundingRadius, 0)
			.arcTo(
				size.x, 0,
				size.x, roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.arcTo(
				0, size.y,
				0, roundingRadius,
				roundingRadius
			)
			.arcTo(
				0, 0,
				roundingRadius, 0,
				roundingRadius
			)
			.fill({color: 0x333333, alpha: 0.9});

		this._barMask
			.clear()
			.moveTo(roundingRadius, barTop)
			.arcTo(
				size.x, barTop,
				size.x, barTop + roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.arcTo(
				0, size.y,
				0, size.y - roundingRadius,
				roundingRadius
			)
			.arcTo(
				0, barTop,
				roundingRadius, barTop,
				roundingRadius
			)
			.fill(0xffffff);

		this._barBackground
			.clear()
			.rect(0, barTop, size.x, barHeight)
			.fill(0x111111);

		this._bar
			.clear()
			.rect(
				0, barTop,
				lineWidth + fraction * (size.x - 2 * lineWidth), barHeight
			)
			.fill(0xff4444);

		this._barBorder
			.clear()
			.moveTo(roundingRadius, barTop)
			.arcTo(
				size.x, barTop,
				size.x, barTop + roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.arcTo(
				0, size.y,
				0, size.y - roundingRadius,
				roundingRadius
			)
			.arcTo(
				0, barTop,
				roundingRadius, barTop,
				roundingRadius
			)
			.closePath()
			.stroke({
				width: lineWidth,
				color: 0xffffff,
				join: "round",
				alignment: 1
			});

		this._numbers.text =
			`${Math.floor(entry.health)} / ${Math.floor(entry.maxHealth)}`;
		this._numbers.style.fontSize = 1/3 * size.y;
		this._numbers.position.copyFrom(new Vector(
			(size.x - this._numbers.width) / 2,
			barTop + (barHeight - this._numbers.height) / 2
		).round());

		this._bossName.text = entry.name;
		this._bossName.style.fontSize = 1/3 * size.y;
		this._bossName.position.copyFrom(new Vector(
			(size.x - this._bossName.width) / 2,
			(barTop - this._bossName.height) / 2
		).round());
	}
}



class ChatPopup {
	constructor(dashboard) {
		this.dashboard = dashboard;

		this.display = new Container();
		// Place the popup above boss bars
		this.display.zIndex = 1;

		this._box = new Graphics();
		this._sender = new Text({text: "", style: UIUtils.getFont(
			{h: 60, s: 50, v: 100}
		)});
		this._message = new Text({text: "", style: UIUtils.getFont(
			{h: 60, s: 50, v: 100}, {align: "left", wordWrap: true}
		)});

		this.display.addChild(this._box, this._sender, this._message);

	}

	update(dt, position, width) {
		let lineWidth = Math.max(Math.round(0.01 * width), 1);
		let marginWidth = Math.round(0.05 * width);

		let [lastMsgTime, senderName, messageText] = this.dashboard.game.unicenter.chat
			.getLastMessageInfo();

		if (lastMsgTime < 0 || lastMsgTime < this.dashboard.game.space.time - 7) {
			this.display.visible = false;
			return;
		}

		this.display.visible = true;
		this.display.position.copyFrom(position);

		let senderPosition = new Vector(marginWidth, marginWidth);
		this._sender.position.copyFrom(senderPosition);
		this._sender.text = senderName;
		this._sender.style.fontSize = 0.1 * width;

		let messagePosition = new Vector(
			marginWidth, senderPosition.y + this._sender.height + marginWidth
		).round();
		this._message.position.copyFrom(messagePosition);
		this._message.text = messageText;
		this._message.style.wordWrapWidth = width - 2 * marginWidth;
		this._message.style.fontSize = 0.08 * width;

		let height = Math.round(messagePosition.y + this._message.height + marginWidth);

		this._box
			.clear()
			.rect(0, 0, width, height)
			.fill({h: 60, s: 50, v: 13})
			.stroke({
				width: lineWidth,
				color: {h: 60, s: 50, v: 100},
				join: "round",
				alignment: 1
			});
	}
}



class DashboardBar {
	// colors is an object with the following properties:
	//   barBackground
	//   bar
	//   barBorder
	// tooltip is an object with the following properties:
	//   text
	//   alignment ("LEFT" or "MIDDLE")
	// The instance may store references to these objects, so don't mess with them
	// afterwards!
	constructor(dashboard, getValue, getMaxValue, icon, colors, tooltip) {
		this.dashboard = dashboard;
		this._getValue = getValue;
		this._getMaxValue = getMaxValue;
		this._colors = colors;

		this.display = new Container();
		this._labelBackground = new Graphics();
		this._icon = new Sprite(getTexture(icon));
		this._icon.anchor.set(0.5, 0.5);
		this._barBackground = new Graphics();
		this._bar = new Graphics();
		this._numbers = new Text("", UIUtils.getFont(0xffffff));
		this._numbers.anchor.set(0, 0.5);
		this._barBorder = new Graphics();
		this._barMask = new Graphics();
		this.display.addChild(this._labelBackground, this._icon, this._barBackground,
						this._bar, this._numbers, this._barBorder, this._barMask);

		this._barBackground.mask = this._barMask;
		this._bar.mask = this._barMask;

		this._tooltip = new Tooltip(
			this.dashboard.game.tooltipper,
			this.display,
			tooltip.text,
			(tooltip.alignment === "LEFT") ? "UP_LEFT" : "UP_MIDDLE"
		);
	}

	update(dt, position, size) {
		let value = this._getValue();
		let maxValue = this._getMaxValue();
		let fraction = value / maxValue;

		let labelWidth = 1.2 * size.y;
		let roundingRadius = size.y / 2;
		let lineWidth = Math.max(Math.round(0.05 * size.y), 1);

		this.display.position.copyFrom(position);

		this._labelBackground
			.clear()
			.moveTo(roundingRadius, 0)
			.lineTo(size.x - roundingRadius, 0)
			.arcTo(
				size.x, 0,
				size.x, roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.lineTo(roundingRadius, size.y)
			.arcTo(
				0, size.y,
				0, roundingRadius,
				roundingRadius
			)
			.arcTo(
				0, 0,
				roundingRadius, 0,
				roundingRadius
			)
			.fill({color: 0x333333, alpha: 0.9});

		this._icon.scale.set(0.8 * size.y / this._icon.texture.height);
		this._icon.position.set(labelWidth / 2, size.y / 2);

		this._barMask
			.clear()
			.moveTo(labelWidth, 0)
			.lineTo(size.x - roundingRadius, 0)
			.arcTo(
				size.x, 0,
				size.x, roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.lineTo(labelWidth, size.y)
			.fill(0xffffff);

		this._barBackground
			.clear()
			.moveTo(labelWidth, 0)
			.lineTo(size.x - roundingRadius, 0)
			.arcTo(
				size.x, 0,
				size.x, roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.lineTo(labelWidth, size.y)
			.fill(this._colors.barBackground(fraction));

		this._bar
			.clear()
			.rect(
				labelWidth, 0,
				lineWidth + fraction * (size.x - labelWidth - 2 * lineWidth),
				size.y
			)
			.fill(this._colors.bar);

		this._barBorder
			.clear()
			.moveTo(labelWidth, 0)
			.lineTo(size.x - roundingRadius, 0)
			.arcTo(
				size.x, 0,
				size.x, roundingRadius,
				roundingRadius
			)
			.arcTo(
				size.x, size.y,
				size.x - roundingRadius, size.y,
				roundingRadius
			)
			.lineTo(labelWidth, size.y)
			.closePath()
			.stroke({
				width: lineWidth,
				color: (fraction >= 1) ? 0xffffff : this._colors.barBorder,
				join: "round",
				alignment: 1
			});

		this._numbers.text = `${Math.floor(value)} / ${Math.floor(maxValue)}`;
		this._numbers.position.set(1.2 * labelWidth, size.y / 2);
		this._numbers.style.fontSize = 2/3 * size.y;

		this._tooltip.update(dt, position, size);
	}
}



// Source: https://levelup.gitconnected.com/advanced-drawing-with-pixi-js-cd3fddc1d69e
function drawWedge(target,
				   x,
				   y,
				   radius,
				   arc,
				   startAngle = 0,
				   yRadius = 0) {
	let segs = Math.ceil(Math.abs(arc) / 45);
	let segAngle = arc / segs;
	let theta = -(segAngle / 180) * Math.PI;
	let angle = -(startAngle / 180) * Math.PI;
	let ax = x + Math.cos(startAngle / 180 * Math.PI) * radius;
	let ay = y + Math.sin(-startAngle / 180 * Math.PI) * yRadius;
	let angleMid, bx, by, cx, cy;
	if (yRadius === 0)
		yRadius = radius;
	target.moveTo(x, y);
	target.lineTo(ax, ay);
	for (let i = 0; i < segs; ++i) {
		angle += theta;
		angleMid = angle - (theta / 2);
		bx = x + Math.cos(angle) * radius;
		by = y + Math.sin(angle) * yRadius;
		cx = x + Math.cos(angleMid) * (radius / Math.cos(theta / 2));
		cy = y + Math.sin(angleMid) * (yRadius / Math.cos(theta / 2));
		target.quadraticCurveTo(cx, cy, bx, by);
	}
	target.lineTo(x, y);
}



class DashboardButton {
	constructor(dashboard, icon) {
		this.dashboard = dashboard;

		this.display = new Container();
		this._backdrop = new Graphics();
		this._icon = new Sprite(getTexture(icon));
		this._icon.anchor.set(0.5, 0.5);
		this._cooldown = new Graphics();
		this._cooldownMask = new Graphics();
		this.display.addChild(this._backdrop, this._icon, this._cooldown,
						this._cooldownMask);

		this._cooldown.mask = this._cooldownMask;

		this._clicker = new Clicker(this.display);
		this._tooltip = new Tooltip(
			this.dashboard.game.tooltipper,
			this.display,
			"Nanobots (E)"
							+ "\n\nRepair you, or whatever else they hit, for a total of"
							+ " 10 b."
							+ "\n\nCost:"
							+ "\n5 energy"
							+ "\n5 seconds of Vulnerable"
							+ "\n\nCooldown: 5 seconds",
			"UP_MIDDLE"
		);
	}

	update(dt, position, size) {
		let cooldownFraction = this.dashboard.game.space.player.ai.healingCooldown
						/ PlayerAI.HEALING_COOLDOWN;
		let roundingRadius = size.y / 4;
		let lineWidth = Math.max(Math.round(0.05 * size.y), 1);

		this.display.position.copyFrom(position);

		this._backdrop
			.clear()
			.roundRect(0, 0, size.x, size.y, roundingRadius)
			.fill((cooldownFraction > 0) ? 0x202020
				: (this._clicker.isHovering() ? 0x005000 : 0x004000))
			.stroke({
				width: lineWidth,
				color: (cooldownFraction > 0) ? 0x888888
					: (this._clicker.isHovering() ? 0xffffff : 0x21e521),
				join: "round",
				alignment: 1
			});

		this._icon.scale.set(0.7 * size.y / this._icon.texture.height);
		this._icon.position.set(size.x / 2, size.y / 2);
		this._icon.tint = (cooldownFraction > 0) ? 0xbbbbbb : 0x21e521;

		this._cooldown.clear();
		if (cooldownFraction > 0) {
			drawWedge(
				this._cooldown,
				size.x / 2, size.y / 2,
				size.x,
				-360 * cooldownFraction,
				90,
				size.y
			);
			this._cooldown.fill({color: 0xaaaaaa, alpha: 0.7});
		}

		this._cooldownMask
			.clear()
			.roundRect(0, 0, size.x, size.y, roundingRadius)
			.fill(0xffffff);

		this._tooltip.update(dt, position, size);

		this._clicker.clearStuff();
	}

	wasClicked() {
		return this._clicker.wasClicked();
	}
}



class DashboardIndicator {
	constructor(dashboard, icon) {
		this.dashboard = dashboard;

		this.display = new Container();
		this._icon = new Sprite(getTexture(icon));
		this.display.addChild(this._icon);

		this._tooltip = new Tooltip(
			this.dashboard.game.tooltipper,
			this.display,
			"Vulnerable"
							+ "\nStatus effect"
							+ "\n\nMakes you take 3x damage from all sources.",
			"UP_MIDDLE"
		);
	}

	update(dt, position, size) {
		let visible = this.dashboard.game.space.player.isVulnerable();

		this._tooltip.update(dt, position, size);

		if (!visible) {
			this.display.visible = false;
			return;
		}

		this.display.visible = true;
		this.display.position.copyFrom(position);
		this._icon.scale.set(size.y / this._icon.texture.height);
	}
}



class MiniGPS {
	constructor(dashboard, icon) {
		this.dashboard = dashboard;

		this.display = new Container();
		this._worldsight = new WorldSight(
			dashboard.game,
			0x000000,
			150,
			0.003,
			new Vector(0.57, 0.57),
			false
		);

		this._mask = new Graphics();
		this._worldsight.display.mask = this._mask;

		this._posBackground = new Graphics();

		this._posDisplay = new Text({text: "", style: UIUtils.getFont(0xffffff)});
		this._posDisplay.anchor.set(1, 1);

		this._border = new Graphics();

		this.display.addChild(
			this._worldsight.display,
			this._mask,
			this._posBackground,
			this._posDisplay,
			this._border
		);

		this._tooltip = new Tooltip(
			this.dashboard.game.tooltipper,
			this.display,
			"Mini-GPS"
				+ "\n\nShows the area around you. "
				+ "Click the z-shaped icon above to see a larger version.",
			"UP_MIDDLE"
		);
	}

	// position is the bottom-right corner in this case
	update(dt, position, size, straightLineLength) {
		this._tooltip.update(dt, Vector.diff(position, size), size);

		// TODO: Standardize this
		let lineWidth = Math.max(Math.round(0.01 * size.y), 1);
		let padding = Math.max(Math.round(0.03 * size.y), 1);

		function baseShape(graphic) {
			graphic
				.clear()
				.moveTo(- size.x, 10)
				.arcTo(
					- size.x, - size.y,
					- straightLineLength, - size.y,
					size.x - straightLineLength
				)
				.lineTo(10, - size.y)
		}

		this.display.position.copyFrom(position);

		this._worldsight.display.position.set(- size.x, - size.y);
		this._worldsight.update(true, size);

		baseShape(this._mask);
		this._mask
			.lineTo(10, 10)
			.closePath()
			.fill(0xffffff);

		// Draw the actual text before the background to get accurate text width
		let playerPos = this.dashboard.game.space.player.pos;
		this._posDisplay.text = `${Math.floor(playerPos.x)}, ${Math.floor(playerPos.y)}`;
		this._posDisplay.position.set(- padding, - padding);
		this._posDisplay.style.fontSize = 0.09 * size.y;

		let posBackgroundOrigin = new Vector(
			- this._posDisplay.width - 2 * padding,
			- this._posDisplay.height - 2 * padding
		).round();
		// Make it a little larger so the right & bottom borders aren't visible
		let posBackgroundSize = new Vector(
			- posBackgroundOrigin.x + padding,
			- posBackgroundOrigin.y + padding
		).round();
		this._posBackground
			.clear()
			.rect(
				posBackgroundOrigin.x, posBackgroundOrigin.y,
				posBackgroundSize.x, posBackgroundSize.y
			)
			.fill({r: 0, g: 0, b: 0, a: 0.4})
			.stroke({
				width: 1,
				color: {h: 220, s: 50, v: 100},
				join: "round"
			});

		baseShape(this._border);
		this._border.stroke({
			width: lineWidth,
			color: {h: 220, s: 50, v: 100},
			join: "round"
		});
	}
}



export class Dashboard {
	// Create a new dashboard
	// Only create one dashboard per game. game is a reference to the game.
	constructor(game) {
		this.game = game;

		this.display = new Container();

		this._bossBars = [];

		this._chatPopup = new ChatPopup(this);
		this.display.addChild(this._chatPopup.display);

		this.healthBar = new DashboardBar(
			this,
			() => this.game.space.player.health,
			() => this.game.space.player.maxHealth,
			"images/icons/health.svg",
			{
				barBackground: (fraction) => (fraction > 0.5)
					? 0x004000
					: ((fraction > 0.25) ? 0x404000 : 0x400000),
				bar: 0x00e600,
				barBorder: 0xb2ffb2
			},
			{
				text: "Health"
								+ "\n\nGet this number to 0 to lose all your items and"
								+ " respawn at the last visited station :)",
				alignment: "LEFT"
			}
		);
		this.display.addChild(this.healthBar.display);

		this.energyBar = new DashboardBar(
			this,
			() => this.game.space.player.energy,
			() => Player.MAX_ENERGY,
			"images/icons/energy.svg",
			{
				barBackground: (fraction) => 0x2d004d, // hue 275
				bar: 0xb654ff,
				barBorder: 0xe4bfff
			},
			{
				text: "Energy"
								+ "\n\nUsed for all sorts of actions."
								+ " Regenerates over time, but only while you don't use"
								+ " it.",
				alignment: "MIDDLE"
			}
		);
		this.display.addChild(this.energyBar.display);

		this.healingButton = new DashboardButton(
			this,
			"images/icons/healing.svg"
		);
		this.display.addChild(this.healingButton.display);

		this.vulnerableIndicator = new DashboardIndicator(
			this,
			"images/icons/vulnerable.svg"
		);
		this.display.addChild(this.vulnerableIndicator.display);

		this.miniGps = new MiniGPS(this);
		this.display.addChild(this.miniGps.display);
	}

	update(dt, screenSize) {
		let rulerLength = UIUtils.getRulerLength(screenSize);
		let dashboardOffset = Math.round((screenSize.x - rulerLength) / 2);

		this._updateTop(dt, screenSize, rulerLength);
		this._updateBottom(dt, screenSize, rulerLength, dashboardOffset);
	}

	// Update top part of the dashboard
	_updateTop(dt, screenSize, dashboardWidth) {
		let marginWidth = Math.round(0.02 * dashboardWidth);

		// Draw a cheat notch if cheats have been used
		if (this.game.cheatsUsed) {
			if (this._cheatNotch === undefined) {
				this._cheatNotch = new Graphics();
				this.display.addChild(this._cheatNotch);
			}

			let notchSize = 2 / 3 * marginWidth;
			let vertexA = new Vector(
				screenSize.x / 2 - 2 * notchSize, - notchSize
			).round();
			let vertexB = new Vector(screenSize.x / 2, notchSize).round();
			let vertexC = new Vector(
				screenSize.x / 2 + 2 * notchSize, - notchSize
			).round();
			this._cheatNotch
				.clear()
				.moveTo(vertexA.x, vertexA.y)
				.lineTo(vertexB.x, vertexB.y)
				.lineTo(vertexC.x, vertexC.y)
				.closePath()
				.fill(0xff4444);
		}

		// TODO: Make sure this is sorted by ID
		// Not using tactical report here to avoid 1-frame lag and avoid cluttering the
		// tactical report with showBossBar / health / maxHealth.
		let playerPos = this.game.space.player.pos;
		let bossBarData = this.game.space.things
			.filter(
				thing =>
					(thing.state === Thing.STATES.ALIVE)
					&& thing.showBossBar
					&& (Vector.diff(thing.pos, playerPos).mag() < 10)
				)
			.map(thing => ({
				id: thing.id,
				name: thing.name,
				health: thing.health,
				maxHealth: thing.maxHealth
			}))
			// Note: This will break if ids ever become non-numeric
			.sort((a, b) => a.id - b.id);
		let numBars = bossBarData.length;

		let singleBarWidth = Math.round(Math.min(
			0.5 * dashboardWidth,
			(0.75 * dashboardWidth + marginWidth) / numBars - marginWidth
		));
		let barSize = new Vector(singleBarWidth, 0.05 * dashboardWidth).round();

		let totalWidth = numBars * (singleBarWidth + marginWidth) - marginWidth;
		let basePosition = new Vector(
			(screenSize.x - totalWidth) / 2,
			marginWidth
		).round();

		UIUtils.cullElements(this._bossBars, numBars);
		for (let i = 0; i < numBars; ++i) {
			if (this._bossBars.length <= i) {
				let bossBar = new BossBar(this);
				this._bossBars.push(bossBar);
				this.display.addChild(bossBar.display);
			}

			let bossBar = this._bossBars[i];
			let barPosition = Vector.sum(basePosition, new Vector(
				i * (singleBarWidth + marginWidth), 0
			));
			bossBar.update(dt, barPosition, barSize, bossBarData[i]);
		}

		let popupWidth = Math.round(0.25 * dashboardWidth);
		let unicenterColumnWidth = this.game.unicenter.tabSelectorWidth;
		let popupPosition = new Vector(
			screenSize.x - unicenterColumnWidth - marginWidth - popupWidth,
			marginWidth
		);
		this._chatPopup.update(dt, popupPosition, popupWidth);
	}

	// Update bottom part of the dashboard
	_updateBottom(dt, screenSize, dashboardWidth, dashboardOffset) {
		let healthBarSize = new Vector(
			0.2 * dashboardWidth,
			0.035 * dashboardWidth
		).round();
		let healthBarPosition = new Vector(
			dashboardOffset + 0.02 * dashboardWidth,
			screenSize.y - 0.02 * dashboardWidth - healthBarSize.y
		).round();
		this.healthBar.update(dt, healthBarPosition, healthBarSize);

		let energyBarSize = new Vector(healthBarSize);
		let energyBarPosition = new Vector(
			healthBarPosition.x + healthBarSize.x + 0.02 * dashboardWidth,
			healthBarPosition.y
		).round();
		this.energyBar.update(dt, energyBarPosition, energyBarSize);

		let healingButtonSize = new Vector(
			0.035 * dashboardWidth,
			0.035 * dashboardWidth
		).round();
		let healingButtonPosition = new Vector(
			energyBarPosition.x + energyBarSize.x + 0.02 * dashboardWidth,
			healthBarPosition.y
		).round();
		this.healingButton.update(dt, healingButtonPosition, healingButtonSize);

		let vulnerableIndicatorSize = new Vector(
			0.035 * dashboardWidth,
			0.035 * dashboardWidth
		).round();
		let vulnerableIndicatorPosition = new Vector(
			healingButtonPosition.x + healingButtonSize.x + 0.02 * dashboardWidth,
			healthBarPosition.y
		).round();
		this.vulnerableIndicator.update(dt, vulnerableIndicatorPosition,
						vulnerableIndicatorSize);

		let miniGpsSize = new Vector(
			0.25 * dashboardWidth,
			0.25 * dashboardWidth
		).round();
		this.miniGps.update(dt, screenSize, miniGpsSize,
			screenSize.y - healthBarPosition.y
		);
	}
}
