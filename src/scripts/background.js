import {Container, Graphics} from "pixi.js";
import {Vector} from "./i.js";
import {Random} from "./i.js";
import {MOUSE, sq} from "./i.js";

// Duration of a starboost, in seconds
const BOOST_DURATION = 1.5;

// Time until a broken starboost reconstructs, in seconds
const RECONSTRUCTION_TIME = 3;
// Color of a starboost
const STARBOOST_COLOR = 0xffaad5;
// Period of starboost flicker, in seconds
const FLICKER_PERIOD = 0.75;

export class Background {
	constructor(space) {
		this.space = space;

		this.display = new Container();

		// Each element is an object with the following properties
		// pos          - pos of the starboost, or its last pos pre-break
		// timeBroken   - time the starboost was broken (in seconds since start of the
		//                game), or null if unbroken
		// finalPos     - final pos the starboost will assume after reconstruction, or
		//                null if unbroken
		// particles    - array of {maxOffset: <Vector>, maxRadius: <number in flares>},
		//                or null if unbroken
		// flickerPhase - angle in radians
		// display      - Graphics object
		this._starboosts = [];

		// Array of all times (in seconds since the start of the game) that the player
		// collected a starboost, limited to the last BOOST_DURATION seconds
		this._boostTimes = [];
	}

	update(time, pixelsPerFlare) {
		this._updateStarboosts(time, pixelsPerFlare);
	}

	// Return the number of currently active boosts
	// Returns an integer between 0 and 30 inclusive.
	getBoostFactor() {
		return Math.min(this._boostTimes.length, 30);
	}

	// Update the starboost layer
	_updateStarboosts(time, pixelsPerFlare) {
		this._despawnStarboosts();
		this._spawnStarboosts();
		this._reconstructStarboosts(time);
		this._detectStarboostHover(time, pixelsPerFlare);
		this._removeOldBoostRecords(time);
		this._drawStarboosts(time, pixelsPerFlare);
	}

	// Despawn starboosts outside the bubble
	_despawnStarboosts() {
		let guesstimator = this.space.guesstimator;

		for (let i = this._starboosts.length; i--;) {
			let starboost = this._starboosts[i];
			if (guesstimator.isOutsideBubble(starboost.pos)) {
				this.display.removeChild(starboost.display);
				starboost.display.destroy(true);
				this._starboosts.splice(i, 1);
			}
		}
	}

	// Spawn new starboosts in the bubble
	_spawnStarboosts() {
		let guesstimator = this.space.guesstimator;
		let desiredNumStarboosts = 40 * guesstimator.getOversize();

		// Decide on spawn strategy
		let posSelector;
		if (this._starboosts.length === 0) {
			// Emptiness! Probably either a new game or some teleportation shenanigans
			// Spawn starboosts throughout the bubble.
			posSelector = guesstimator.selectPointInsideBubble.bind(guesstimator);
		} else {
			posSelector = guesstimator.selectSpawn.bind(guesstimator);
		}

		// Actually spawn the starboosts
		while (this._starboosts.length < desiredNumStarboosts) {
			let starboost = {
				pos: posSelector(),
				timeBroken: null,
				finalPos: null,
				particles: null,
				flickerPhase: Random.angle(),
				display: new Graphics()
			};
			this._starboosts.push(starboost);
			this.display.addChild(starboost.display);
		};
	}

	// Reconstruct starboosts broken long ago
	_reconstructStarboosts(time) {
		this._starboosts.forEach(starboost => {
			if (starboost.timeBroken === null) return;
			if (starboost.timeBroken + RECONSTRUCTION_TIME < time) {
				starboost.pos.copyFrom(starboost.finalPos);
				starboost.timeBroken = null;
				starboost.finalPos = null;
				starboost.particles = null;
			}
		});
	}

	// Detect mouse hover over starboosts
	_detectStarboostHover(time, pixelsPerFlare) {
		this._starboosts.forEach(starboost => {
			if (starboost.timeBroken !== null) return;

			// Distance from mouse to star, in flares
			// Convert to pixels in the middle to reuse existing functions.
			let dist = Vector.diff(
				MOUSE,
				this.space.requestPosToPosition(starboost.pos, pixelsPerFlare)
			).mag() / pixelsPerFlare;

			if (dist < 0.125) {
				// Mouse hovering over - break the star
				this._boostTimes.push(time);
				starboost.timeBroken = time;
				starboost.finalPos = new Vector(1, 0)
					.rotateBy(this.space.player.rot)
					.add(starboost.pos);
				starboost.particles = [];
				for (let i = 0; i < 5; ++i) {
					starboost.particles.push({
						maxOffset: Vector.chooseLimited(0.15),
						maxRadius: Random.uniform(0.0075, 0.015)
					});
				}
			}
		});
	}

	// Remove records of collected starboosts from long ago
	_removeOldBoostRecords(time) {
		for (let i = this._boostTimes.length; i--;) {
			if (this._boostTimes[i] < time - BOOST_DURATION) {
				this._boostTimes.splice(i, 1);
			}
		}
	}

	// Draw starboosts on the screen
	_drawStarboosts(time, pixelsPerFlare) {
		this._starboosts.forEach(starboost => {
			starboost.display.position.copyFrom(
				this.space.requestPosToPosition(starboost.pos, pixelsPerFlare)
			);
			starboost.display.clear();

			if (starboost.timeBroken === null) {
				this._drawUnbrokenStarboost(starboost, time, pixelsPerFlare);
			} else {
				this._drawBrokenStarboost(starboost, time, pixelsPerFlare);
			}
		});
	}

	_drawUnbrokenStarboost(starboost, time, pixelsPerFlare) {
		let flickerMultiplier = 1 + 0.05 * Math.sin(
			2 * Math.PI * time / FLICKER_PERIOD + starboost.flickerPhase
		);
		let rayLength = 0.05 * pixelsPerFlare * flickerMultiplier;
		let valleyLength = 0.01 * pixelsPerFlare;
		starboost.display
			.poly([
				{x: - rayLength, y: 0},
				{x: - valleyLength, y: - valleyLength},
				{x: 0, y: - rayLength},
				{x: valleyLength, y: - valleyLength},
				{x: rayLength, y: 0},
				{x: valleyLength, y: valleyLength},
				{x: 0, y: rayLength},
				{x: - valleyLength, y: valleyLength}
			])
			.fill(STARBOOST_COLOR);
	}

	_drawBrokenStarboost(starboost, time, pixelsPerFlare) {
		// Fraction of time that passed from break to reconstruction
		let fractionReconstructed = (time - starboost.timeBroken) / RECONSTRUCTION_TIME;
		// Fraction of distance moved from break to reconstruction
		// A broken starboost moves fast at first and then slows down
		let fractionMoved = Math.pow(fractionReconstructed, 1/4);
		// Movement relative to pos
		let currentMovement = Vector
			.diff(starboost.finalPos, starboost.pos)
			.scale(fractionMoved);
		// How much the particles are spread out
		let fractionSpreadOut = 4 * sq(fractionMoved) * (1 - sq(fractionMoved));
		// Particles become slightly transparent mid-movement
		let particleAlpha = 1 - 0.3 * fractionSpreadOut;

		for (let particle of starboost.particles) {
			// Offset of the particle from the center of the particle cloud
			let particleOffset = Vector.scale(particle.maxOffset, fractionSpreadOut);
			// Location of the particle relative to the pre-break location of the
			// starboost, in pixels
			let particleLocation = Vector
				.sum(particleOffset, currentMovement)
				.scale(pixelsPerFlare);
			// Particle becomes smaller mid-movement
			let particleRadius = (1 - 0.3 * fractionSpreadOut)
							* particle.maxRadius * pixelsPerFlare;
			starboost.display
				.circle(particleLocation.x, particleLocation.y, particleRadius)
				.fill({color: STARBOOST_COLOR, alpha: particleAlpha});
		}
	}
}
