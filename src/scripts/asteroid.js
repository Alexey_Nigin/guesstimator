// Define class for a simple asteroid

import {Sprite} from "pixi.js";
import {getTexture} from "./assets.js";
import {Vector} from "./i.js"; // ./vector.js
import {Random} from "./i.js"; // ./random.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "./i.js"; // ./validators.js
import {Density} from "./i.js"; // ./density.js
import {AIUtils} from "./i.js"; // ./ai.js
import {Thing, THING_TYPES, Z_INDICES} from "./i.js"; // ./thing.js
import {DropList, ITEMS} from "./i.js"; // ./drop.js
import {PranksterAI} from "./i.js"; // ./prankster.js

// Helper function to use here and for Meteorus minions
// cont is the thing container. len is the len of the asteroid, in flares.
// numCraters is the number of craters to draw. isMeatball tells the function whether to
// use space meatball sprites.
export function createAsteroidShape(cont, len, numCraters, isMeatball) {
	let bodyTexture = isMeatball ?
		"images/asteroids/body-meatball.svg"
		: "images/asteroids/body.svg";
	let craterTexture = isMeatball ?
		"images/asteroids/crater-meatball.svg"
		: "images/asteroids/crater.svg";

	let body = new Sprite(getTexture(bodyTexture));
	body.anchor.set(0.5, 0.5);
	body.position.set(0, 0);
	body.scale.set(len / body.texture.height);
	cont.addChild(body);
	for (let i = 0; i < numCraters; ++i) {
		let craterRadius = Math.random() * 0.4 * (len / 2);
		// Max distance from the center of the asteroid to the center of the
		// crater
		let maxDist = 0.85 * (len / 2) - craterRadius;
		// Location of the center of the crater, in flares, relative to the
		// center of the asteroid
		let craterLocation = Vector.chooseLimited(maxDist);

		let crater = new Sprite(getTexture(craterTexture));
		crater.anchor.set(0.5, 0.5);
		crater.position.copyFrom(craterLocation);
		crater.scale.set(2 * craterRadius / crater.texture.height);
		cont.addChild(crater);
	}
}



export class Asteroid extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Size of the asteroid, an integer from 0 to 4 inclusive
		new RequiredProperty("size", v => [0, 1, 2, 3, 4].includes(v)),
		// pos of this asteroid, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// vel of this asteroid, a vector with values in flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// Id of the asteroid that this asteroid is a piece of
		// Defaults to not a piece if omitted.
		new OptionalProperty("parentId", v => Number.isFinite(v)),
		// rot of this asteroid, in radians
		new OptionalProperty("rot", v => (Number.isFinite(v))),
		// Health of this asteroid
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
		// Spin speed, in radians / second
		new OptionalProperty("spin", v => (Number.isFinite(v))),
		// Varieties of the asteroid, an object mapping each variety to true/false
		new OptionalProperty(
			"varieties",
			v => (
				(typeof v === "object")
				&& (v !== null)
				&& Object.entries(v).map(entry => (
					Array.from(Asteroid.VARIETIES.keys()).includes(entry[0])
					&& (entry[1] === false || entry[1] === true)
				)).reduce((a, b) => a && b)
			)
		),
		// Boomdash acceleration, defaults to no boomdash if omitted
		new OptionalProperty("bdAcc", v => (v instanceof Vector)),
		// Boomdash time remaining, defaults to no boomdash if omitted
		new OptionalProperty("bdTime", v => Number.isFinite(v))
	]);

	static SIZES = [
		{   // tiny
			health: 3,
			guesstimationWeight: 0,
			despawnSafetyWindow: 0,
			dropList: new DropList([])
		},
		{   // small
			health: 6,
			guesstimationWeight: 1,
			despawnSafetyWindow: 1,
			dropList: new DropList([
				[ITEMS.COOL_ROCK,    [1, 2], [0.9999, 0.0001]],
				[ITEMS.ALUMINUM_ORE, [1],    [0.015]         ]
			])
		},
		{   // medium
			health: 9,
			guesstimationWeight: 2,
			despawnSafetyWindow: 1,
			dropList: new DropList([
				[ITEMS.COOL_ROCK,      [1], [0.5 ]],
				[ITEMS.ALUMINUM_ORE,   [1], [0.05]],
				[ITEMS.TITANIUM_ORE,   [1], [0.01]],
				[ITEMS.ALUMINUM_CHUNK, [1], [0.01]],
				[ITEMS.RED_GEM,        [1], [0.05]],
				[ITEMS.YELLOW_GEM,     [1], [0.03]],
				[ITEMS.GREEN_GEM,      [1], [0.02]]
			], 1)
		},
		{   // large
			health: 15,
			guesstimationWeight: 4,
			despawnSafetyWindow: 1,
			dropList: new DropList([
				[ITEMS.COOL_ROCK,      [1], [0.5 ]],
				[ITEMS.ALUMINUM_ORE,   [1], [0.1 ]],
				[ITEMS.TITANIUM_ORE,   [1], [0.05]],
				[ITEMS.PLATINUM_ORE,   [1], [0.01]],
				[ITEMS.ALUMINUM_CHUNK, [1], [0.02]],
				[ITEMS.TITANIUM_CHUNK, [1], [0.01]],
				[ITEMS.CRUDE_OIL,      [1], [0.2 ]],
				[ITEMS.RED_GEM,        [1], [0.05]],
				[ITEMS.YELLOW_GEM,     [1], [0.03]],
				[ITEMS.GREEN_GEM,      [1], [0.02]]
			], 1)
		},
		{   // huge
			health: 24,
			guesstimationWeight: 8,
			despawnSafetyWindow: 1,
			dropList: new DropList([
				[ITEMS.COOL_ROCK,      [1],    [0.5]        ],
				[ITEMS.ALUMINUM_ORE,   [1, 2], [0.2, 0.02]  ],
				[ITEMS.TITANIUM_ORE,   [1, 2], [0.3, 0.03]  ],
				[ITEMS.PLATINUM_ORE,   [1, 2], [0.1, 0.01]  ],
				[ITEMS.ALUMINUM_CHUNK, [1],    [0.04]       ],
				[ITEMS.TITANIUM_CHUNK, [1],    [0.06]       ],
				[ITEMS.PLATINUM_CHUNK, [1],    [0.02]       ],
				[ITEMS.CRUDE_OIL,      [1, 2], [0.5, 0.05]  ],
				[ITEMS.RED_GEM,        [1, 2], [0.05, 0.005]],
				[ITEMS.YELLOW_GEM,     [1, 2], [0.03, 0.003]],
				[ITEMS.GREEN_GEM,      [1, 2], [0.02, 0.002]]
			], 2)
		}
	];

	// Map from variety to its probability
	static VARIETIES = new Map([
		["WOBBLY",   0.01],
		["MEATBALL", 0.01]
	]);

	// Additional droplist for a wobbly asteroid
	static WOBBLY_DROPLIST = new DropList([
		[ITEMS.GYROSCOPE, [1], [1]]
	]);

	// Additional droplist for a space meatball
	static MEATBALL_DROPLIST = new DropList([
		[ITEMS.COSSETTITE, [1], [1]]
	]);

	// The speed limit applies only to naturally spawning asteroids, but not to
	// pieces of broken asteroids.
	static SPEED_LIMIT = 1;  // flares / second
	static SPIN_LIMIT = 0.3;

	// len of a maxHealth 1 asteroid
	static BASE_LEN = 0.13;

	// Create a new asteroid
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Asteroid.CONFIG_STRUCTURE.test(config);

		// Fill in missing config fields
		config.zIndex = Z_INDICES.NPC;
		config.len = Asteroid._get_len(config.size);
		if (config.vel === undefined) {
			config.vel = Vector.chooseLimited(Asteroid.SPEED_LIMIT);
		}
		if (config.rot === undefined) config.rot = Random.angle();
		config.maxHealth = Asteroid.SIZES[config.size].health;
		config.type = THING_TYPES.ASTEROID;
		config.canDespawn = true;
		config.guesstimationWeight = Asteroid.SIZES[config.size]
						.guesstimationWeight;
		config.despawnSafetyWindow = Asteroid.SIZES[config.size]
						.despawnSafetyWindow;
		config.pool = Density.POOLS.DYNAMIC;

		// Call parent constructor
		super(config);

		this._size = config.size;

		if (config.bdAcc !== undefined && config.bdTime !== undefined) {
			this._boomdashInfo = {
				acc: config.bdAcc,
				timeRemaining: config.bdTime
			};
		} else {
			this._boomdashInfo = null;
		}

		// Add varieties
		if (config.variety === undefined) {
			this._varieties = new Map();
			for (let variety of Asteroid.VARIETIES.keys()) {
				if (config.parentId !== undefined) {
					this._varieties.set(variety, false);
				} else {
					this._varieties.set(
						variety,
						Math.random() < Asteroid.VARIETIES.get(variety)
					);
				}
			}
		} else {
			this._varieties = new Map(Object.entries(config.varieties));
		}

		createAsteroidShape(
			this.display,
			this.len,
			this.maxHealth / 3,
			this._varieties.get("MEATBALL")
		);

		// Add extra stuff
		if (config.spin === undefined) {
			this.spin = Random.uniform(- Asteroid.SPIN_LIMIT, Asteroid.SPIN_LIMIT);
		} else {
			this.spin = config.spin;
		}
		this.healthBar = this.space.scanner.addBar(
			this.display.position,
			this.radius + 10,
			this.health,
			this.health
		);

		if (config.parentId !== undefined) {
			this.space.interactor.inheritCooldowns(config.parentId, this.id);
		}
	}

	// Move the asteroid
	// dt is the time since last update, in seconds. time is the total in-game
	// time since the start of the game, in seconds.
	move(dt, time) {
		let externalAcc = new Vector();
		if (this._boomdashInfo !== null) {
			externalAcc.copyFrom(this._boomdashInfo.acc);

			this._boomdashInfo.timeRemaining -= dt;
			if (this._boomdashInfo.timeRemaining < 0) {
				this._boomdashInfo = null;
			}
		}

		AIUtils.translate(this, externalAcc, 0, dt);
		if (this._varieties.get("WOBBLY")) {
			let wobblePhase = Math.sin(
				(time / PranksterAI.WOBBLE_PERIOD) * (2 * Math.PI)
			);
			AIUtils.setRot(this, - Math.PI / 2 + wobblePhase * (Math.PI / 4), dt);
		} else {
			AIUtils.rotateBy(this, this.spin, dt);
		}
	}

	// Take damage from an attack
	// damage is a number, dropItems is an optional boolean (defaults to true if
	// omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
			this.healthBar.val = this.health;
		} else {
			this.prepareToDie(dropItems);
		}
	}

	// Save this asteroid into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save().config;

		config.size = this._size;
		config.spin = this.spin;
		config.varieties = Object.fromEntries(this._varieties);

		return {
			config:     config,
			isComplete: true
		};
	}

	// Prepare this asteroid for removal
	// See the comment on Thing.prepareToDie() for more details.
	prepareToDie(dropItems) {
		if (this._size > 0) {
			// Make sure all pieces are fully inside the original asteroid
			// TODO: Improve this for (size - 2).
			let nearbyRadius = (Asteroid._get_len(this._size)
							- Asteroid._get_len(this._size - 1)) / 2;
			// Maintain some semblance of conservation of momentum
			let maxVelChange = 0.5 * Asteroid.SPEED_LIMIT;

			let bdInfo = {};
			if (this._boomdashInfo !== null) {
				bdInfo.bdAcc = new Vector(this._boomdashInfo.acc);
				bdInfo.bdTime = this._boomdashInfo.timeRemaining;
			}

			this.space.requestSpawn(this.constructor, {
				size:     this._size - 1,
				pos:      this.pos.chooseNearby(nearbyRadius),
				vel:      this.vel.chooseNearby(maxVelChange),
				parentId: this.id,
				...bdInfo
			});
			if (this._size > 1) {
				this.space.requestSpawn(this.constructor, {
					size:     this._size - 2,
					pos:      this.pos.chooseNearby(nearbyRadius),
					vel:      this.vel.chooseNearby(maxVelChange),
					parentId: this.id,
					...bdInfo
				});
			}
		}

		// Drop items if dropItems is true or undefined
		if (dropItems !== false) {
			this.space.requestDrop(
				this.pos,
				this.radius,
				this.vel,
				Asteroid.SIZES[this._size].dropList
			);
			// TODO: If dropping multiple droplists from one thing becomes a
			// common pattern, consider adding some methods to DropList to
			// handle it more efficiently.
			if (this._varieties.get("WOBBLY")) {
				this.space.requestDrop(
					this.pos,
					this.radius,
					this.vel,
					Asteroid.WOBBLY_DROPLIST
				);
			}
			if (this._varieties.get("MEATBALL")) {
				this.space.requestDrop(
					this.pos,
					this.radius,
					this.vel,
					Asteroid.MEATBALL_DROPLIST
				);
			}
		};

		super.prepareToDie(dropItems);
	}

	repelFromBoomdash(boomdashCenter) {
		if (this._boomdashInfo !== null) return;
		this._boomdashInfo = {
			acc: Vector.diff(this.pos, boomdashCenter).norm(15),
			timeRemaining: 0.1
		};
	}

	// Calculate len for an asteroid of given size
	// This function calculates len such that the area of the asteroid is
	// proportional to the total health of the asteroid and all its pieces (and
	// the pieces of those pieces and so on).
	static _get_len(size) {
		let total_health = [];
		for (let i = 0; i <= size; ++i) {
			total_health.push(
				Asteroid.SIZES[i].health
				+ ((i > 0) ? total_health[i - 1] : 0)
				+ ((i > 1) ? total_health[i - 2] : 0)
			);
		}
		return Asteroid.BASE_LEN * Math.sqrt(total_health[size]);
	}
}
