// The interactor provides a centralized way for managing all interactions
// between things: attacking, defending, healing, etc.
//
// Usage:
//     // Once per tick
//     interactor.handleAllInteractions(dt, time);

import {System} from "detect-collisions";
import {FACTIONS, Thing, THING_TYPES} from "./i.js";
import {Boomdash} from "./i.js";

const TYPE_CHART = new Map([
	[THING_TYPES.SPACESHIP, new Map([
		[THING_TYPES.SPACESHIP, (sameFaction, shipA, shipB) => boop(shipA, shipB)],
		[THING_TYPES.STATION, (sameFaction, ship, station, dt, shipInStation, stationInShip) => {
			if (sameFaction) {
				healAtStation(ship, station, dt, shipInStation, stationInShip);
			} else {
				annihilate(ship, station);
			}
		}],
		[THING_TYPES.ASTEROID, (sameFaction, ship, asteroid) => collideWithAsteroid(ship, asteroid)],
		[THING_TYPES.DROP, (sameFaction, ship, drop) => tryPickup(drop, ship)],
		[THING_TYPES.HEALING, (sameFaction, ship, healing) => heal(healing, ship)],
		[THING_TYPES.ENERGY, (sameFaction, ship, energy) => {
			if (sameFaction) {
				return false; // no interaction
			} else {
				return collide(ship, energy);
			}
		}]
	])],
	[THING_TYPES.STATION, new Map([
		// THING_TYPES.STATION - no interaction,
		[THING_TYPES.ASTEROID, (sameFaction, station, asteroid) => annihilate(asteroid, station)],
		// THING_TYPES.DROP - no interaction
		[THING_TYPES.HEALING, (sameFaction, station, healing) => heal(healing, station)],
		[THING_TYPES.ENERGY, (sameFaction, station, energy) => annihilate(energy, station)]
	])],
	[THING_TYPES.ASTEROID, new Map([
		// THING_TYPES.ASTEROID - no interaction
		// THING_TYPES.DROP - no interaction
		[THING_TYPES.HEALING, (sameFaction, asteroid, healing) => heal(healing, asteroid)],
		[THING_TYPES.ENERGY, (sameFaction, asteroid, energy) => collide(asteroid, energy)]
	])],
	[THING_TYPES.DROP, new Map([
		// THING_TYPES.DROP - no interaction
		// THING_TYPES.HEALING - no interaction
		// THING_TYPES.ENERGY - no interaction
	])],
	[THING_TYPES.HEALING, new Map([
		// THING_TYPES.HEALING - no interaction
		// THING_TYPES.ENERGY - no interaction
	])],
	[THING_TYPES.ENERGY, new Map([
		// THING_TYPES.ENERGY - no interaction
	])]
]);



export class Interactor {
	// Create a new Interactor
	// In the future, this constructor may set which enemies are friendly, neutral, or
	// hostile towards the player or other enemies, if that's something that I will want
	// to change dynamically.
	// Args:
	//   space - reference to space
	constructor(space) {
		this.space = space;

		this._hitboxes = new Set();
		this._collisionSystem = new System();

		// Map from `${lesserId}-${greaterId}` to cooldown in seconds
		this._collisionCooldowns = new Map();

		this._requestsToInheritCooldowns = [];
	}

	// Add a new hitbox to the interactor
	// Args:
	//   hitbox - a detect-collisions body
	addHitbox(hitbox) {
		this._hitboxes.add(hitbox);
		this._collisionSystem.insert(hitbox);
	}

	// Remove a hitbox from the interactor
	// Does nothing if the hitbox is not in the interactor.
	// Args:
	//   hitbox - a detect-collisions body
	removeHitbox(hitbox) {
		this._hitboxes.delete(hitbox);
		this._collisionSystem.remove(hitbox);
	}

	inheritCooldowns(sourceId, targetId) {
		this._requestsToInheritCooldowns.push([sourceId, targetId]);
	}

	// Detect and handle all interactions between things
	// Args:
	//   dt   - time since last tick, in seconds
	//   time - total in-game time, in seconds
	handleAllInteractions(dt, time) {
		this._updateCooldowns(dt);

		this._hitboxes.forEach(hitbox => {
			let thing = hitbox.thing;
			hitbox
				.setPosition(thing.pos.x, thing.pos.y, false)
				.setAngle(thing.rot)
				.updateBody();
		});

		this._collisionSystem.checkAll(response =>
			this._interact(
				response.a.thing, response.b.thing,
				response.aInB, response.bInA,
				dt, time
			)
		);

		for (let [sourceId, targetId] of this._requestsToInheritCooldowns) {
			this._collisionCooldowns.forEach((cooldown, key, map) => {
				let idA = parseInt(key.substring(0, key.indexOf("-")));
				let idB = parseInt(key.substring(key.indexOf("-") + 1));
				let collidingWith = null;
				if (idA === sourceId) collidingWith = idB;
				if (idB === sourceId) collidingWith = idA;
				if (collidingWith !== null) {
					let lesserId = Math.min(targetId, collidingWith);
					let greaterId = Math.max(targetId, collidingWith);
					map.set(`${lesserId}-${greaterId}`, cooldown);
				}
			});
		}
		this._requestsToInheritCooldowns = [];
	}

	// Update collision cooldowns
	// Call this each tick, before any _interact() calls.
	_updateCooldowns(dt) {
		this._collisionCooldowns.forEach((cooldown, key, map) => {
			if (cooldown <= dt) {
				map.delete(key);
			} else {
				map.set(key, cooldown - dt);
			}
		});
	}

	// Handle the interaction between thingA and thingB
	// Call this every tick for every pair of things that are colliding. Can be skipped
	// for very far-away things, for performance.
	// Args:
	//   thingA, thingB - colliding things; order doesn't matter, but don't call the
	//                    function twice with both orders
	//   aInB           - true iff thingA is contained within thingB
	//   BInA           - true iff thingB is contained within thingA
	//   dt             - time since last tick, in seconds
	//   time           - total in-game time, in seconds
	_interact(thingA, thingB, aInB, bInA, dt, time) {
		// If one of the things is already dead, the things can't interact
		if (thingA.state !== Thing.STATES.ALIVE
						|| thingB.state !== Thing.STATES.ALIVE) return;

		// If one of the things is not ready, the things can't interact
		// Most things don't have the "ready" property at all, so only check if that
		// property is explicitly set to false.
		if (thingA.ready === false || thingB.ready === false) return;

		let lesserId = Math.min(thingA.id, thingB.id);
		let greaterId = Math.max(thingA.id, thingB.id);
		let cooldownKey = `${lesserId}-${greaterId}`;
		if (this._collisionCooldowns.has(cooldownKey)) return;

		let typeA = thingA.TYPE;
		let typeB = thingB.TYPE;
		let sameFaction = (thingA.faction !== FACTIONS.NONE) && (thingA.faction === thingB.faction)

		let cooldownEligible = false;

		// Try (A, B)
		let interactionAB = TYPE_CHART.get(typeA);
		if (interactionAB !== undefined) interactionAB = interactionAB.get(typeB);

		if (interactionAB !== undefined) {
			cooldownEligible = interactionAB(sameFaction, thingA, thingB, dt, aInB, bInA);
		} else {
			// Try (B, A)
			let interactionBA = TYPE_CHART.get(typeB);
			if (interactionBA !== undefined) interactionBA = interactionBA.get(typeA);

			if (interactionAB !== undefined) {
				cooldownEligible = interactionBA(sameFaction, thingB, thingA, dt, bInA, aInB);
			}
		}

		// Record a new cooldown

		if (!cooldownEligible) return;
		// If things are aren't both alive they won't collide again any time soon
		// But we might still want to inherit their cooldowns!
		// if (thingA.state !== Thing.STATES.ALIVE
		// 				|| thingB.state !== Thing.STATES.ALIVE) return;
		// Set a max size of this map, just in case :)
		// I am sure someone can use this max size to break the game.
		if (this._collisionCooldowns.size >= 2048) return;
		this._collisionCooldowns.set(cooldownKey, 1);
	}
}



// Interaction functions
// Called by TYPE_CHART.
//
// Args for each function (functions can omit unneeded args at the end):
//   thingA - thing
//   thingB - thing
//   dt     - time since last frame, in seconds
//   aInB   - boolean
//   bInA   - boolean
//
// Return: true iff the pair of things is eligible for a cooldown.

function boop(thingA, thingB) {
	if (thingA.receiveBoop !== undefined) thingA.receiveBoop(thingB);
	if (thingB.receiveBoop !== undefined) thingB.receiveBoop(thingA);
}

function healAtStation(thing, station, dt, thingInStation, stationInThing) {
	boop(thing, station);
	if (thingInStation) thing.repair(dt, station.id);
	return false; // cooldown eventually but not now
}

function annihilate(thing, station) {
	station.raiseShield();
	thing.prepareToDie(false);
	return false;
}

function collideWithAsteroid(ship, asteroid) {
	let collisionStrength = Math.min(ship.health, Math.ceil(asteroid.health / 3), 10);
	ship.takeDamage(collisionStrength, true);
	asteroid.takeDamage(3 * collisionStrength, true);
	return true;
}

function tryPickup(drop, ship) {
	// TODO: Even if other things get the CAN_PICK_UP_ITEMS tag, the player should still
	// be the only one to pick up components. Or, you know, just remove the
	// CAN_PICKUP_ITEMS tag.
	if (!ship.CAN_PICK_UP_ITEMS) return false;
	ship.receiveDrop(drop);
	drop.recordPickup();
	return false;
}

function heal(healing, thing) {
	if (!healing.isReadyToHeal()) return;
	thing.health = Math.min(thing.health + healing.health, thing.maxHealth);
	if (thing.healthBar) thing.healthBar.val = thing.health;
	healing.takeDamage(healing.health, true);
	return false;
}

function collide(thingA, thingB) {
	let collisionStrength = Math.min(thingA.health, thingB.health);

	// TODO: Handle external acceleration in a better way
	if ((thingA instanceof Boomdash) && (thingB.repelFromBoomdash !== undefined)) {
		thingB.repelFromBoomdash(thingA.center);
	}
	if ((thingB instanceof Boomdash) && (thingA.repelFromBoomdash !== undefined)) {
		thingA.repelFromBoomdash(thingB.center);
	}

	thingA.takeDamage(collisionStrength, true);
	thingB.takeDamage(collisionStrength, true);
	return true;
}
