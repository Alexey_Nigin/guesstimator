import {Graphics} from "pixi.js";
import {Polygon} from "detect-collisions";
import {round} from "../utils.js";
import {isVectorable, Vector} from "../i.js"; // ../vector.js
import {Random} from "../i.js";
import {logger} from "../i.js";
import {ConfigStructure, EitherOr, OptionalProperty, RequiredProperty} from "../i.js"; // ../validators.js
import {AI} from "../i.js";
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js";



// Range of axon lifespans, in seconds
const LIFESPAN_MIN = 2;
const LIFESPAN_MAX = 12;
// Length of a single-parent axon, in flares
const UNCAPPED_AXON_LENGTH = 16;



class AxonAI extends AI {
	// static CONFIG_STRUCTURE = new ConfigStructure([]);

	constructor(thing, config) {
		// CompassAI.CONFIG_STRUCTURE.test(config);

		super(thing);
	}

	move(dt, time) {
		this.thing.age += dt;

		let firstParent = this.thing.space.requestThingById(this.thing.firstParentId);
		let secondParent = (this.thing.variety !== "LASER")
			? this.thing.space.requestThingById(this.thing.secondParentId) : null;

		// Check if the axon needs to be removed
		switch (this.thing.variety) {
			case "MAZE": {
				// Remove the axon without transferring value
				let transferAborted = false;
				// One of the parents destroyed
				if ((firstParent === null) || (secondParent === null)) {
					transferAborted = true;
				}
				// At this point, the first parent should still have the value
				if (!transferAborted && !firstParent.value) logger.error("axon.js",
					"first parent lost value"
				);
				// Second parent already received value from another axon
				if (!transferAborted && secondParent.value) transferAborted = true;
				if (transferAborted) {
					if (firstParent !== null) firstParent.removeOutgoingAxon(false);
					this.thing.prepareToDie(false);
					return;
				}

				// Remove the axon and transfer value
				if (this.thing.age > this.thing.lifespan) {
					// The axon survived its lifespan
					firstParent.removeOutgoingAxon(true);
					secondParent.receiveValue();
					this.thing.prepareToDie(false);
					return;
				}
				break;
			};
			case "WALL": {
				if ((firstParent === null) || (secondParent === null)) {
					this.thing.prepareToDie(false);
					return;
				}
				break;
			};
			case "LASER": {
				if ((firstParent === null) || (this.thing.age > this.thing.lifespan)) {
					this.thing.prepareToDie(false);
					return;
				}
				break;
			}
		}

		if (this.thing.age >= 1.5) this.thing.ready = true;

		let startPos = Vector.sum(
			firstParent.pos,
			new Vector(this.thing.offsetFirst).rotateBy(firstParent.rot)
		);
		let endPos;
		if (this.thing.variety === "LASER") {
			endPos = Vector.sum(
				startPos,
				new Vector(UNCAPPED_AXON_LENGTH, 0).rotateBy(this.thing.angle)
			);
		} else {
			endPos = Vector.sum(
				secondParent.pos,
				new Vector(this.thing.offsetSecond).rotateBy(secondParent.rot)
			);
		}

		this.thing.len = Vector.diff(endPos, startPos).mag();
		// TODO: Get rid of radius
		this.thing.radius = this.thing.len / 2;
		this.thing.hitbox.setPoints([
			{x: - this.thing.len / 2, y: 0},
			{x:   this.thing.len / 2, y: 0}
		]);
		this.thing.pos = Vector.sum(startPos, endPos).scale(0.5);
		// vel remains (0, 0), for now - don't know what the best way to handle it is
		this.thing.rot = Vector.diff(endPos, startPos).dir();
	}
}



class AxonAnimation {
	constructor(thing) {
		this.thing = thing;

		this._line = new Graphics();

		this.thing.display.addChild(this._line);
	}

	update(dt, time) {
		this._line.clear();

		if (this.thing.ready) {
			this._line
				.moveTo(0, - this.thing.len / 2)
				.lineTo(0, this.thing.len / 2)
				.stroke({
					width: 0.04,
					color: 0xff0000,
					cap: "round"
				})
				.moveTo(0, - this.thing.len / 2)
				.lineTo(0, this.thing.len / 2)
				.stroke({
					width: 0.02,
					color: 0xffffff,
					cap: "round"
				});
		} else {
			this._line
				.moveTo(0, - this.thing.len / 2)
				.lineTo(0, this.thing.len / 2)
				.stroke({
					width: 0.02,
					color: 0xff0000,
					alpha: this.thing.age / 1.5,
					cap: "round"
				});
		}
	}
}



export class Axon extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the axon's first parent
		new RequiredProperty("firstParentId", v => Number.isFinite(v)),
		// Offset of the start of the axon relative to center of first parent
		// Defaults to (0, 0) if omitted.
		new OptionalProperty("offsetFirst", v => isVectorable(v)),
		new EitherOr([
			// Whether this axon is part of a wall
			// Must be true if specified (for towers), defaults to false if omitted (for
			// compasses).
			new OptionalProperty("isWall", v => (v === true)),
			// id of the axon's second parent
			new RequiredProperty("secondParentId", v => Number.isFinite(v)),
			// Offset of the end of the axon relative to center of second parent
			// Defaults to (0, 0) if omitted.
			new OptionalProperty("offsetSecond", v => isVectorable(v))
		], [
			// Absolute angle at which the axon originates from the first (and in this
			// case, only) parent
			// Not relative to parent's rot. Takes precedence over second parent.
			new RequiredProperty("angle", v => Number.isFinite(v))
		]),
		// Age of this axon in seconds, defaults to 0 if omitted
		new OptionalProperty("age", v => Number.isFinite(v) && v >= 0),
		// Lifespan of this axon, randomly chosen if omitted, deleted if isWall
		new OptionalProperty("lifespan", v => Number.isFinite(v) && v >= 0)
	]);

	// Create a new axon
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Axon.CONFIG_STRUCTURE.test(config);

		let variety;
		if (config.angle !== undefined) {
			variety = "LASER";
		} else if (config.isWall === true) {
			variety = "WALL";
		} else {
			variety = "MAZE";
		}

		// Fill in missing config fields
		config.name = "Axon";
		config.zIndex = (variety === "MAZE") ? Z_INDICES.NPC_CHILD : Z_INDICES.BOSS_CHILD;
		config.len = 1;
		config.hitbox = new Polygon({x: 0, y: 0}, [
			{x: -1, y: 0},
			{x:  1, y: 0}
		]);
		config.pos = new Vector();
		config.vel = new Vector();
		config.rot = 0;
		config.maxHealth = 12;
		config.type = THING_TYPES.ENERGY;
		config.faction = FACTIONS.DAMSEL;
		config.canDespawn = false;

		// Call parent constructor
		super(config);

		this.variety = variety;

		this.firstParentId = config.firstParentId;
		// Works even if the offsets are undefined!
		this.offsetFirst = new Vector(config.offsetFirst);
		if (this.variety === "LASER") {
			this.angle = config.angle;
		} else {
			this.secondParentId = config.secondParentId;
			this.offsetSecond = new Vector(config.offsetSecond);
		}
		this.age = config.age ?? 0;
		this.ready = false;
		if (this.variety !== "WALL") {
			this.lifespan = config.lifespan
							?? round(Random.uniform(LIFESPAN_MIN, LIFESPAN_MAX), 2);
		}

		this.ai = new AxonAI(this, {});
		this.animation = new AxonAnimation(this);
	}

	save() {
		let config = super.save();

		config.firstParentId = this.firstParentId;
		if (this.offsetFirst.x !== 0 || this.offsetFirst.y !== 0) {
			config.offsetFirst = this.offsetFirst.save();
		}
		if (this.variety === "LASER") {
			config.angle = round(this.angle);
		} else {
			if (this.variety === "WALL") config.isWall = true;
			config.secondParentId = this.secondParentId;
			if (this.offsetSecond.x !== 0 || this.offsetSecond.y !== 0) {
				config.offsetSecond = this.offsetSecond.save();
			}
		}
		if (this.age > 0) config.age = round(this.age);
		if (this.lifespan !== undefined) config.lifespan = round(this.lifespan);

		// No AI state

		return config;
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		// Nothing to do here - a player can't damage an axon directly.
		// TODO: Remove on collision with a station.
	}
}
