import {Graphics} from "pixi.js";
import {Polygon} from "detect-collisions";
import {Vector} from "../i.js"; // ../vector.js
import {sq} from "../i.js"; // ./dump.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js"; // ../validators.js
import {StraightLineAI} from "../i.js";
import {FACTIONS, Thing, THING_TYPES, Z_INDICES} from "../i.js";

const SPEED = 1;  // flares / second
const STANDARD_WAVELENGTH_SQ = sq(0.3) + sq(0.15);

class WaveAnimation {
	constructor(thing, length) {
		this.thing = thing;

		let wavelength = Math.sqrt(this.thing.wavelengthSq);
		this._body = new Graphics();
		this._body
			.moveTo(- wavelength / 2 + 0.01, 0)
			.lineTo(wavelength / 2 - 0.01, 0)
			.stroke({
				color: 0xe64e00,
				width: 0.02,
				cap: "round"
			})
			.moveTo(- wavelength / 2 + 0.005, 0)
			.lineTo(wavelength / 2 - 0.005, 0)
			.stroke({
				color: 0xffffff,
				width: 0.01,
				cap: "round"
			});
		this.thing.display.addChild(this._body);
	}

	update(dt, time) {}
}

export class Wave extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// pos of this wave, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// rot of this wave, in radians
		new RequiredProperty("rot", v => Number.isFinite(v)),
		// Square of the length of the wave
		// Defaults to correct value for compass if omitted.
		new OptionalProperty("wavelengthSq", v => (Number.isFinite(v) && v > 0))
		// Not explicitly listing health any more
	]);

	// Create a wave
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Wave.CONFIG_STRUCTURE.test(config);

		let wavelengthSq = config.wavelengthSq ?? STANDARD_WAVELENGTH_SQ;

		config.zIndex = Z_INDICES.NPC_CHILD;
		config.len = Math.sqrt(wavelengthSq);
		config.hitbox = new Polygon({x: 0, y: 0}, [
			{x: 0, y: - Math.sqrt(wavelengthSq) / 2},
			{x: 0, y: Math.sqrt(wavelengthSq) / 2}
		]);
		config.type = THING_TYPES.ENERGY;
		config.faction = FACTIONS.DAMSEL;
		config.canDespawn = true;
		config.despawnSafetyWindow = 0;
		config.vel = new Vector(SPEED, 0)
			.rotateBy(config.rot);
		config.maxHealth = 8;
		super(config);

		this.wavelengthSq = wavelengthSq;

		this.animation = new WaveAnimation(this)
		this.ai = new StraightLineAI(this, {});
	}

	// Take damage from an attack
	// damage is a number.
	takeDamage(damage) {
		// Nothing to do here - a player can't damage a wave directly.
		// TODO: Remove on collision with a station.
	}
}
