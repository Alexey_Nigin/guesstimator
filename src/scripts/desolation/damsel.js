// Distributed automated ...?

import {Container, Graphics, Sprite} from "pixi.js";
import {Polygon} from "detect-collisions";
import {createSmoothInterpolation, round} from "../utils.js";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ./vector.js
import {Random} from "../i.js";
import {logger} from "../i.js";
import {angleDiff, sq} from "../i.js";
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js"; // ./validators.js
import {AI, AIUtils} from "../i.js";
import {FACTIONS, STANDARD_PPF, Thing, THING_TYPES, Z_INDICES} from "../i.js"; // ./thing.js
import {COMPONENTS, DropList, ITEMS} from "../i.js";
import {Wave} from "../i.js";
import {Axon} from "../i.js";
import {ACHIEVEMENTS} from "../i.js";



const QUADRANTS = ["--", "-+", "+-", "++"];

const TOWER_OFFSETS = new Map([
	["--", new Vector(-1.5, -1)],
	["-+", new Vector(-1.5,  1)],
	["+-", new Vector( 1.5, -1)],
	["++", new Vector( 1.5,  1)]
]);

let _plateAngle = Math.atan(2);
const PLATE_INFO = new Map([
	["-+", {
		origin: new Vector(-0.25, 0.125),
		normal: Math.PI - _plateAngle,
		length: Math.hypot(0.3, 0.15)
	}],
	["+-", {
		origin: new Vector(0.25, -0.125),
		normal: - _plateAngle,
		length: Math.hypot(0.3, 0.15)
	}],
	["++", {
		origin: new Vector(0.15, 0.075),
		normal: _plateAngle,
		length: Math.hypot(0.5, 0.25)
	}]
]);

const AXON_INFO = [
	{
		start: "--",
		end: "-+",
		offsets: [
			{
				first:  new Vector(-0.1,  0.05),
				second: new Vector(-0.1, -0.05)
			},
			{
				first:  new Vector( 0.1,  0.05),
				second: new Vector( 0.1, -0.05)
			}
		]
	},
	{
		start: "--",
		end: "+-",
		offsets: [
			{
				first:  new Vector( 0.1, -0.05),
				second: new Vector(-0.1, -0.05)
			},
			{
				first:  new Vector( 0.1,  0.05),
				second: new Vector(-0.1,  0.05)
			}
		]
	},
	{
		start: "-+",
		end: "++",
		offsets: [
			{
				first:  new Vector( 0.1, -0.05),
				second: new Vector(-0.1, -0.05)
			},
			{
				first:  new Vector( 0.1,  0.05),
				second: new Vector(-0.1,  0.05)
			}
		]
	},
	{
		start: "+-",
		end: "++",
		offsets: [
			{
				first:  new Vector(-0.1,  0.05),
				second: new Vector(-0.1, -0.05)
			},
			{
				first:  new Vector( 0.1,  0.05),
				second: new Vector( 0.1, -0.05)
			}
		]
	}
];

// Time from noticing the player to the start of the first attack, s
const TIME_TO_FIRST_ATTACK = 5;
// Time between start of consecutive attacks, s
const TIME_BETWEEN_ATTACKS = 2.75;
// Lifespan of a laser, s
const LASER_LIFESPAN = 2;



class TowerAI extends AI {
	// static CONFIG_STRUCTURE = new ConfigStructure([]);

	constructor(thing, config) {
		// TowerAI.CONFIG_STRUCTURE.test(config);
		super(thing);

		this._charging = false;
	}

	move(dt, time) {
		let damsel = this.thing.space.requestThingById(this.thing.damselId);
		if (damsel === null) {
			this.thing.prepareToDie(false);
			return;
		}

		let offset = new Vector(TOWER_OFFSETS.get(this.thing.quadrant));
		let oldPos = this.thing.pos;
		this.thing.pos = Vector.sum(
			damsel.pos,
			offset.rotateBy(this.thing.space.compassDirection)
		);

		this._manageWaves(dt);
	}

	chargeWaves() {
		this._charging = true;
	}

	_manageWaves(dt) {
		if (!this._charging) return;

		this.thing.fractionCharged += dt / 1.5;
		this.thing.fractionCharged = Math.min(this.thing.fractionCharged, 1);

		if (this.thing.fractionCharged >= 1) {
			PLATE_INFO.forEach(plate => {
				let flippedOrigin = new Vector(plate.origin);
				let rotVector = new Vector(1, 0).rotateBy(plate.normal);
				if (this.thing.quadrant[0] == "-") {
					flippedOrigin.x *= -1;
					rotVector.x *= -1;
				}
				if (this.thing.quadrant[1] == "-") {
					flippedOrigin.y *= -1;
					rotVector.y *= -1;
				}
				this.thing.space.requestSpawn(Wave, {
					pos: flippedOrigin
						.rotateBy(this.thing.rot)
						.add(this.thing.pos),
					rot: rotVector.dir() + this.thing.rot,
					wavelengthSq: sq(plate.length)
				})
			});
			this.thing.fractionCharged = 0;
			this._charging = false;
		}
	}
}



class TowerAnimation {
	constructor(thing) {
		this.thing = thing;

		// Flip / rotate as needed for different towers
		this._flippy = new Container();
		if (thing.quadrant[0] === "-") this._flippy.scale.y = -1;
		if (thing.quadrant[1] === "-") this._flippy.scale.x = -1;
		thing.display.addChild(this._flippy);

		this._body = new Sprite(getTexture("images/desolation/tower.svg"));
		this._body.scale.set(1 / STANDARD_PPF);
		this._body.anchor.set(0.5, 0.5);

		this._plates = new Map();
		for (let plateName of ["-+", "+-", "++"]) {
			let plate = new Graphics();
			plate
				.moveTo(- PLATE_INFO.get(plateName).length / 2 + 0.01, -0.01)
				.lineTo(PLATE_INFO.get(plateName).length / 2 - 0.01, -0.01)
				.stroke({
					color: 0x652200,
					width: 0.02,
					cap: "round"
				});
			plate.rotation = PLATE_INFO.get(plateName).normal;
			plate.position.copyFrom(
				new Vector(PLATE_INFO.get(plateName).origin).rotateBy(- Math.PI / 2)
			);
			this._flippy.addChild(plate);
			this._plates.set(plateName, plate);
		}

		this._flippy.addChild(this._body);
	}

	update(dt, time) {
		let f = sq(this.thing.fractionCharged);
		let offset = 0.1 * 4 * f * (1 - f);

		this._plates.forEach((plate, plateName) => {
			let plateInfo = PLATE_INFO.get(plateName);
			plate.position.copyFrom(
				new Vector(offset, 0)
					.rotateBy(plateInfo.normal - Math.PI / 2)
					.add(new Vector(plateInfo.origin).rotateBy(- Math.PI / 2))
			);
		});
	}
}



const TOWER_DROPLIST = new DropList([
	[ITEMS.DENDRITIC_RECEIVER,  [2], [1]],
	[ITEMS.AFFINE_ADDER,        [1], [1]],
	[ITEMS.NONLINEAR_ACTIVATOR, [1], [1]],
	[ITEMS.AXONIC_EMITTER,      [3], [1]],
	[ITEMS.WAVE_PLATE,          [3], [1]]
]);



export class Tower extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// id of the damsel
		new RequiredProperty("damsel", v => Number.isFinite(v)),
		// Quadrant that this tower is in, one of QUADRANTS
		new RequiredProperty("quadrant", v => QUADRANTS.includes(v)),
		new OptionalProperty("fractionCharged",
			v => (Number.isFinite(v) && v >= 0 && v <= 1)
		)
	]);

	// Create a tower
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Tower.CONFIG_STRUCTURE.test(config);

		config.name = "Tower";
		config.zIndex = Z_INDICES.BOSS;
		config.len = 0.8;
		let hitboxPoints = [
			{x:   0.4, y: -0.05},
			{x:   0.1, y:  -0.2},
			// {x:     0, y: -0.15},  // Concavity was breaking stuff
			{x:  -0.1, y:  -0.2},
			{x:  -0.4, y: -0.05},
			// {x:  -0.3, y:     0},  // Concavity was breaking stuff
			{x:  -0.4, y:  0.05},
			{x:  -0.1, y:   0.2}
		];
		for (let point of hitboxPoints) {
			if (config.quadrant[0] === "-") point.x *= -1;
			if (config.quadrant[1] === "-") point.y *= -1;
		}
		config.hitbox = new Polygon({x: 0, y: 0}, hitboxPoints);
		config.pos = new Vector(5, 0);
		config.vel = new Vector();
		config.rot = config.space.compassDirection;
		config.maxHealth = 500;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.DAMSEL;
		config.canDespawn = false;
		config.showBossBar = true;
		super(config);

		this.damselId = config.damsel;
		this.quadrant = config.quadrant;

		this.fractionCharged = config.fractionCharged ?? 0;

		this.animation = new TowerAnimation(this);
		this.ai = new TowerAI(this, {});
	}

	// Save this tower into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();

		config.damsel = this.damselId;
		config.quadrant = this.quadrant;
		if (this.fractionCharged > 0) {
			config.fractionCharged = round(this.fractionCharged);
		}

		// let aiState = this.ai.save();

		return config;
	}

	// Take damage from an attack
	// Args:
	//   damage    - number
	//   dropItems - is an optional boolean (defaults to true if omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			this.prepareToDie(dropItems);
			// Do this last to avoid an infinite loop where two towers keep damaging each
			// other
			let damsel = this.space.requestThingById(this.damselId);
			if (damsel !== null) damsel.recordTowerFading(this.quadrant);
		}
	}

	prepareToDie(dropItems) {
		if (dropItems) {
			this.space.requestDrop(
				this.pos,
				this.radius,
				this.vel,
				TOWER_DROPLIST
			);
		}
		super.prepareToDie(dropItems);
	}

	// Start charging the waves
	chargeWaves() {
		this.ai.chargeWaves();
	}

	// Start firing the laser
	// Args:
	//   target - pos of the target to shoot
	fireLaser(target) {
		this.space.requestSpawn(Axon, {
			firstParentId: this.id,
			angle: Vector.diff(target, this.pos).dir(),
			lifespan: LASER_LIFESPAN
		});
	}
}



const IDLE_ORBIT_PERIOD = 2 * 60 * 60;
const IDLE_ORBIT_SPN = - 2 * Math.PI / IDLE_ORBIT_PERIOD;
const IDLE_ORBIT_START_ROT = Math.PI / 2;
// How long it takes the Damsel to return to idle orbit after the player leaves (seconds)
const TIME_TO_IDLE_ORBIT = 5 * 60;

const FLOWER_DROPLIST = new DropList([
	[ITEMS.FLOWER, [1], [1]]
], 0, [COMPONENTS.CORRELATION_MATRIX]);

function getIdleOrbitRot(time) {
	return IDLE_ORBIT_START_ROT + time * IDLE_ORBIT_SPN;
}



class DamselAI extends AI {
	// static CONFIG_STRUCTURE = new ConfigStructure([]);

	constructor(thing, config) {
		// DamselAI.CONFIG_STRUCTURE.test(config);
		super(thing);

		this._timeSincePlayerSeen = 0;

		this._lastAttacks = [null, null];
	}

	move(dt, time) {
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => entry.isPlayer,
			"DAMSEL-PLAYER"
		).filter(
			entry => (Vector.diff(entry.pos, this.thing.pos).mag() <= 20)
		);
		let playerPos = (tacticalReport.length === 0) ? 0 : tacticalReport[0].pos;
		let distToPlayer = (playerPos === null) ? Infinity
			: Vector.diff(playerPos, this.thing.pos).mag();

		if (distToPlayer < 6) this.thing.space.game.unicenter.ship.reportStat(
			"TIME_NEAR_DAMSEL", dt
		);

		if (distToPlayer < 10) {
			// Slow down to fight the player
			AIUtils.rotateBy(this.thing.orbit, 0, dt, 1);
			// Delete orbitTrajectory so it's recreated when the player leaves
			delete this.orbitTrajectory;
		} else {
			// Return to idle orbit
			if (this.orbitTrajectory === undefined) {
				this._createOrbitTrajectory(time);
			}
			AIUtils.setRot(this.thing.orbit, this.orbitTrajectory(time), dt);
		}
		AIUtils.setPos(this.thing, new Vector(300, 0).rotateBy(this.thing.orbit.rot));

		// The player is far away, reset health and fortifications
		if (tacticalReport.length === 0) {
			this._timeSincePlayerSeen += dt;
			if (this._timeSincePlayerSeen > 20) {
				this._removeAllTowers();
			}
			return;
		}
		this._timeSincePlayerSeen = 0;
		this._spawnTowers();

		this._orderAttacks(dt, playerPos, distToPlayer);
	}

	_spawnTowers() {
		if (this.thing.towers === null) {
			this.thing.towers = [];
			let towers = new Map();
			for (let quadrant of QUADRANTS) {
				let tower = this.thing.space.requestSpawn(Tower, {
					damsel: this.thing.id,
					quadrant: quadrant
				});
				this.thing.towers.push(tower.id);
				towers.set(quadrant, tower);
			}

			for (let axonEntry of AXON_INFO) {
				let firstParent = towers.get(axonEntry.start);
				let secondParent = towers.get(axonEntry.end);
				for (let offset of axonEntry.offsets) {
					this.thing.space.requestSpawn(Axon, {
						firstParentId: firstParent.id,
						offsetFirst: offset.first,
						secondParentId: secondParent.id,
						offsetSecond: offset.second,
						isWall: true
					});
				}
			}
		}
	}

	_removeAllTowers() {
		if (this.thing.towers == null) return;
		let towersByDistance = this.thing.towers
			.map(towerId => this.thing.space.requestThingById(towerId))
			.filter(tower => tower !== null)
			.forEach(tower => tower.prepareToDie(false));
		this.thing.towers = null;
	}

	_createOrbitTrajectory(time) {
		let eventualIdleRot = getIdleOrbitRot(time + TIME_TO_IDLE_ORBIT);
		eventualIdleRot = this.thing.orbit.rot
						+ angleDiff(eventualIdleRot, this.thing.orbit.rot);
		this.orbitTrajectory = createSmoothInterpolation(
			time,
			this.thing.orbit.rot,
			this.thing.orbit.spn,
			time + TIME_TO_IDLE_ORBIT,
			eventualIdleRot,
			IDLE_ORBIT_SPN
		);
	}

	_orderAttacks(dt, playerPos, distToPlayer) {
		// TODO: Reduce to ~5 when blaster gets a more reasonable range
		if (distToPlayer > 8) {
			this.thing.attackCooldown = TIME_BETWEEN_ATTACKS;
			return;
		}

		if (this.thing.towers === null) {
			logger.warning("damsel.js", "towers not initialized");
			return;
		}

		this.thing.attackCooldown -= dt;
		if (this.thing.attackCooldown > 0) return;

		// At this point, we definitely want to attack

		// Towers sorted from closest to furthest (from player)
		let towersByDistance = this.thing.towers
			.map(towerId => this.thing.space.requestThingById(towerId))
			.filter(tower => tower !== null)
			.sort((a, b) =>
				Vector.diff(playerPos, a.pos).mag() - Vector.diff(playerPos, b.pos).mag()
			);

		// Check if the last 2 attacks were the same
		let allWaves = (this._lastAttacks[0] === "W") && (this._lastAttacks[1] === "W");
		let allLasers = (this._lastAttacks[0] === "L") && (this._lastAttacks[1] === "L");

		// Don't use the same attack 3 times in a row
		let nextAttack = Random.element(["W", "L"]);
		if (allWaves) nextAttack = "L";
		if (allLasers) nextAttack = "W";
		if (towersByDistance.length <= 2) nextAttack = "*";

		if (nextAttack === "W" || nextAttack === "*") this._orderWaves();
		if (nextAttack === "L" || nextAttack === "*") {
			this._orderLaser(playerPos, towersByDistance);
		}

		this._lastAttacks.shift();
		this._lastAttacks.push(nextAttack);
		this.thing.attackCooldown += TIME_BETWEEN_ATTACKS;
	}

	// Assumes towers have been initialized
	_orderWaves() {
		for (let towerId of this.thing.towers) {
			let tower = this.thing.space.requestThingById(towerId);
			if (tower === null) continue;
			tower.chargeWaves();
		}
	}

	// Assumes towers have been initialized
	_orderLaser(playerPos, towersByDistance) {
		if (towersByDistance.length === 4) {
			// All towers unbroken - fire 1 laser
			towersByDistance[0].fireLaser(playerPos);
		} else {
			// Some towers broken - fire 2 lasers if possible
			if (towersByDistance.length >= 1) towersByDistance[0].fireLaser(playerPos);
			if (towersByDistance.length >= 2) towersByDistance[1].fireLaser(playerPos);
		}
	}
}



export class Damsel extends Thing {
	// I decided to stop listing health in every config structure
	static CONFIG_STRUCTURE = new ConfigStructure([
		// Where & how fast the Damsel is orbiting
		new OptionalProperty("orbitRot", v => Number.isFinite(v)),
		new OptionalProperty("orbitSpn", v => Number.isFinite(v)),
		// List of tower ids
		new OptionalProperty("towers",
			v => Array.isArray(v) && v.every(element => Number.isFinite(element))
		)
	]);

	// Create a Damsel
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Damsel.CONFIG_STRUCTURE.test(config);

		config.name = "DAMSEL";
		config.texture = "images/desolation/damsel.svg";
		config.zIndex = Z_INDICES.BOSS;
		config.len = 0.55;
		config.pos = new Vector(0, 300);
		config.vel = new Vector();
		config.rot = - Math.PI / 2;
		config.maxHealth = 50;
		config.type = THING_TYPES.ETHEREAL;
		config.faction = FACTIONS.DAMSEL;
		config.canDespawn = false;
		config.gpsIcon = "images/icons/gps/damsel.svg";
		config.showBossBar = true;
		super(config);

		// Make the orbit look kinda like a thing to reuse AIUtils
		this.orbit = {
			rot: config.orbitRot ?? IDLE_ORBIT_START_ROT,
			spn: config.orbitSpn ?? IDLE_ORBIT_SPN
		};
		this.towers = config.towers ?? null;
		this.attackCooldown = TIME_TO_FIRST_ATTACK;

		this.ai = new DamselAI(this, {});
	}

	// Save this damsel into a config object
	// See the comment on Thing.save() for more details.
	save() {
		let config = super.save();

		config.orbitRot = round(this.orbit.rot);
		config.orbitSpn = round(this.orbit.spn);
		if (this.towers !== null) {
			config.towers = [...this.towers];
		}
		// let aiState = this.ai.save();

		return config;
	}

	// Record that a tower has faded
	// Damages towers adjacent to quadrant by half their health, and drops a flower if all
	// towers are gone. Called by a tower right after it fades.
	//
	// Args:
	//   quadrant - one of QUADRANTS
	recordTowerFading(quadrant) {
		if (this.towers === null) {
			logger.warning("damsel.js", "attempting to damage nonexistent towers");
			return;
		}

		let anyTowersLeft = false;
		for (let towerId of this.towers) {
			let tower = this.space.requestThingById(towerId);
			if (tower === null || tower.state !== Thing.STATES.ALIVE) continue;

			anyTowersLeft = true;

			// Check that exactly one char of the quadrant matches
			let destQuadrant = tower.quadrant;
			if ((quadrant[0] === destQuadrant[0]) !== (quadrant[1] === destQuadrant[1])) {
				// Adjacent - inflict half of max health in damage
				tower.takeDamage(Math.ceil(tower.maxHealth / 2), true);
			}
		}

		if (!anyTowersLeft) {
			this.space.game.unicenter.ship.achievements.recordAchievement(
				ACHIEVEMENTS.DAMSEL
			);
			this.space.game.unicenter.ship.reportStat("DAMSEL_PHASE_1_COMPLETE", 1);
			// TODO: For some reason this.vel is a NaN vector here?
			this.space.requestDrop(
				this.pos,
				0.1,
				new Vector(0, 1.5),
				FLOWER_DROPLIST
			);
		}
	}
}
