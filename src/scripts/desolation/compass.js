import {Graphics, Sprite} from "pixi.js";
import {Polygon} from "detect-collisions";
import {getTexture} from "../assets.js";
import {Vector} from "../i.js"; // ../vector.js
import {Random} from "../i.js"; // ../random.js
import {sq} from "../i.js"; // ./dump.js
import {ConfigStructure, OptionalProperty, RequiredProperty} from "../i.js"; // ../validators.js
import {Density} from "../i.js"; // ../density.js
import {AI, AIUtils} from "../i.js";
import {FACTIONS, Thing, STANDARD_PPF, THING_TYPES, Z_INDICES} from "../i.js";
import {DropList, ITEMS} from "../i.js";
import {Wave} from "../i.js";
import {Axon} from "../i.js";
import {ACHIEVEMENTS} from "../i.js";

let _plateAngle = Math.atan(2);
let _plateCenterX = 0.60 / 4;
let _plateCenterY = 0.30 / 4;
const PLATE_INFO = new Map([
	["--", {
		origin: new Vector(- _plateCenterX, - _plateCenterY),
		normal: Math.PI + _plateAngle
	}],
	["-+", {
		origin: new Vector(- _plateCenterX, _plateCenterY),
		normal: Math.PI - _plateAngle
	}],
	["+-", {
		origin: new Vector(_plateCenterX, - _plateCenterY),
		normal: - _plateAngle
	}],
	["++", {
		origin: new Vector(_plateCenterX, _plateCenterY),
		normal: _plateAngle
	}]
]);

// Desired minimum distance between compasses, f
const DESIRED_DISTANCE = 1;
// Max speed of a compass, f/s
const MAX_SPEED = 0.2;
// Friction, 1/s
const FRICTION = 2;

// Distance at which the compass can notice the player, f
const DISTANCE_TO_NOTICE_PLAYER = 1.75;
// Time to notice the player and start charging the wave, s
const TIME_TO_NOTICE_PLAYER = 2;



class CompassAI extends AI {
	static CONFIG_STRUCTURE = new ConfigStructure([]);

	constructor(thing, config) {
		CompassAI.CONFIG_STRUCTURE.test(config);

		super(thing);

		this._timePlayerNearby = 0;
		this._charging = false;
		this._timeToAxonAttempt = 0;
	}

	move(dt, time) {
		let tacticalReport = this.thing.space.requestTacticalReport(
			entry => (entry.name === "Compass"),
			"COMPASS-REPEL"
		).filter(
			entry => (Vector.diff(entry.pos, this.thing.pos).mag() <= 1)
		);

		let acc = new Vector();
		for (let entry of tacticalReport) {
			acc.add(AIUtils.getRepelAcc(
				this.thing,
				entry,
				MAX_SPEED * FRICTION,
				DESIRED_DISTANCE
			));
		}
		// Apply upper bound to acceleration
		acc.norm(Math.max(acc.mag(), MAX_SPEED * FRICTION));

		AIUtils.translate(this.thing, acc, FRICTION, dt);

		this._manageWaves(dt, time);
		this._manageAxons(dt, time);
	}

	// Record that an axon-related action just happened
	// This activates a cooldown
	recordAxonAction() {
		this._timeToAxonAttempt = Random.uniform(0.5, 1);
	}

	_manageWaves(dt, time) {
		if (!this._charging) {
			let tacticalReport = this.thing.space.requestTacticalReport(
				entry => entry.isPlayer,
				"COMPASS-PLAYER"
			).filter(
				entry => (Vector.diff(entry.pos, this.thing.pos).mag()
								<= DISTANCE_TO_NOTICE_PLAYER)
			);
			let playerNearby = (tacticalReport.length > 0);

			if (playerNearby) {
				this._timePlayerNearby += dt;
			} else {
				this._timePlayerNearby = 0;
			}

			if (this._timePlayerNearby >= TIME_TO_NOTICE_PLAYER) {
				this._charging = true;
				this._timePlayerNearby = 0;
			}
			return;
		}

		this.thing.fractionCharged += dt / 1.5;
		this.thing.fractionCharged = Math.min(this.thing.fractionCharged, 1);

		if (this.thing.fractionCharged >= 1) {
			PLATE_INFO.forEach(plate =>
				this.thing.space.requestSpawn(Wave, {
					pos: new Vector(plate.origin)
						.rotateBy(this.thing.rot)
						.add(this.thing.pos),
					rot: plate.normal + this.thing.rot
				})
			);
			this.thing.fractionCharged = 0;
			this._charging = false;
		}
	}

	_manageAxons(dt, time) {
		this._timeToAxonAttempt -= dt;
		if ((this.thing.axonId === null) && (this._timeToAxonAttempt <= 0)) {
			if (this.thing.value) {
				let tacticalReport = this.thing.space.requestTacticalReport(
					entry => (entry.name === "Compass"),
					"COMPASS"
				).filter(
					entry => (
						!entry.thing.value
							&& Vector.diff(entry.pos, this.thing.pos).mag() <= 4
					)
				);

				if (tacticalReport.length > 0) {
					// Sort the tactical report from closest to furthest
					tacticalReport.sort((entryA, entryB) => {
						let distA = Vector.diff(entryA.pos, this.thing.pos).mag();
						entryA.dist = distA;
						let distB = Vector.diff(entryB.pos, this.thing.pos).mag();
						entryB.dist = distB;
						return distA - distB;
					});

					let weights;
					if (tacticalReport.length === 1) {
						weights = [1];
					} else if (tacticalReport.length === 2) {
						weights = [3, 2];
					} else {
						weights = [3, 2, 1];
					}
					let chosenEntry = tacticalReport[Random.weighted(weights)];

					let axon = this.thing.space.requestSpawn(Axon, {
						firstParentId: this.thing.id,
						secondParentId: chosenEntry.id
					});
					this.thing.axonId = axon.id;
				}
			}
			this._timeToAxonAttempt += 1;
		}
	}
}



class CompassAnimation {
	constructor(thing) {
		this.thing = thing;

		this._body = new Sprite(getTexture("images/desolation/compass.svg"));
		this._body.scale.set(1 / STANDARD_PPF);
		this._body.anchor.set(0.5, 0.5);

		this._plates = new Map();
		for (let plateName of ["--", "-+", "+-", "++"]) {
			let plate = new Sprite(getTexture("images/desolation/plate.svg"));
			plate.scale.set(1 / STANDARD_PPF);
			plate.anchor.set(0.5, 1);
			plate.rotation = PLATE_INFO.get(plateName).normal + Math.PI;
			plate.position.copyFrom(
				// Some signs may be wrong here but the compass is symmetric so it doesn't
				// matter. See Tower for better signs.
				new Vector(PLATE_INFO.get(plateName).origin).rotateBy(Math.PI / 2)
			);
			this.thing.display.addChild(plate);
			this._plates.set(plateName, plate);
		}

		this._valueDisplay = new Graphics();
		this._valueDisplay
			.circle(0, 0, 0.075)
			.fill(0xffffff);

		this.thing.display.addChild(this._body, this._valueDisplay);
	}

	update(dt, time) {
		let f = sq(this.thing.fractionCharged);
		let offset = 0.1 * 4 * f * (1 - f);

		this._plates.forEach((plate, plateName) => {
			let plateInfo = PLATE_INFO.get(plateName);
			plate.position.copyFrom(
				new Vector(offset, 0)
					.rotateBy(plateInfo.normal + Math.PI / 2)
					.add(new Vector(plateInfo.origin).rotateBy(Math.PI / 2))
			);
		});

		this._valueDisplay.visible = this.thing.value;
	}
}



const COMPASS_DROPLIST = new DropList([
	[ITEMS.DENDRITIC_RECEIVER,  [1, 2, 3, 4, 5], [0.1, 0.1, 0.05, 0.05, 0.02]],
	[ITEMS.AFFINE_ADDER,        [1],             [0.5]                       ],
	[ITEMS.NONLINEAR_ACTIVATOR, [1],             [0.4]                       ],
	[ITEMS.AXONIC_EMITTER,      [1],             [0.3]                       ],
	[ITEMS.WAVE_PLATE,          [1, 2, 3, 4],    [0.1, 0.1, 0.05, 0.02]      ]
], 1);



export class Compass extends Thing {
	static CONFIG_STRUCTURE = new ConfigStructure([
		// pos of this compass, a vector with values in flares
		new RequiredProperty("pos", v => (v instanceof Vector)),
		// vel of this compass, a vector with values in flares / second
		new OptionalProperty("vel", v => (v instanceof Vector)),
		// Health of this compass
		new OptionalProperty("health", v => (Number.isFinite(v) && v >= 0)),
	]);

	// Create a new compass
	// config is an object with the properties provided by the spawn request
	// enrichment, plus the properties specified in CONFIG_STRUCTURE above.
	constructor(config) {
		Compass.CONFIG_STRUCTURE.test(config);

		// Fill in missing config fields
		config.name = "Compass";
		config.zIndex = Z_INDICES.NPC;
		config.len = 0.60;
		config.hitbox = new Polygon({x: 0, y: 0}, [
			{x:  0.30, y:     0},
			{x:     0, y: -0.15},
			{x: -0.30, y:     0},
			{x:     0, y:  0.15}
		]);
		if (config.vel === undefined) config.vel = new Vector();
		config.rot = config.space.compassDirection;
		config.maxHealth = 32;
		config.type = THING_TYPES.SPACESHIP;
		config.faction = FACTIONS.DAMSEL;
		config.canDespawn = true;
		config.guesstimationWeight = 1;
		config.despawnSafetyWindow = 3;
		config.pool = Density.POOLS.STATIC;

		// Call parent constructor
		super(config);

		this.fractionCharged = 0;
		this.value = (Math.random() < 0.5);
		// id of the outgoing axon, or null if no axon
		this.axonId = null;

		this.ai = new CompassAI(this, {});
		this.animation = new CompassAnimation(this);
	}

	removeOutgoingAxon(valueTransferSucceeded) {
		this.axonId = null;
		if (valueTransferSucceeded) this.value = false;
		this.ai.recordAxonAction();
	}

	// Receive a value through an axon
	// Called by Axon AI.
	receiveValue() {
		if (this.value) logger.error("compass.js", "already has value, can't receive");
		this.value = true;
		this.ai.recordAxonAction();
	}

	// Take damage from an attack
	// Args:
	//   damage    - number
	//   dropItems - is an optional boolean (defaults to true if omitted).
	takeDamage(damage, dropItems) {
		if (this.health > damage) {
			this.health -= damage;
		} else {
			this.space.game.unicenter.ship.achievements.recordAchievement(
				ACHIEVEMENTS.COMPASS
			);
			this.prepareToDie(dropItems);
		}
	}

	prepareToDie(dropItems) {
		if (dropItems) {
			this.space.requestDrop(
				this.pos,
				this.radius,
				this.vel,
				COMPASS_DROPLIST
			);
		}
		super.prepareToDie(dropItems);
	}
};
