// Usage:
//     function tick(delta) {
//         ...
//         if (keyboard.isDown("THRUST")) {
//             ...
//         }
//         ...
//     }

import {logger} from "./i.js"; // ./logger.js

export class Keyboard {
	// The set of abstract keys
	// These keys are the abstraction that is exposed to the rest of the game,
	// they don't correspond 1:1 to the physical keys on the keyboard.
	static KEYS = new Set([
		"LEFT",
		"RIGHT",
		"UP",
		"DOWN",
		"UNICENTER",
		"RESPAWN",
		"FIRE",
		"DOCK",
		"HEAL",
		"BOOMDASH",
		"DEBUGGER",
		"DART_MOVE_LEFT",
		"DART_MOVE_RIGHT",
		"DART_TURN_LEFT",
		"DART_TURN_RIGHT",
		"DART_FIRE"
	]);

	// The map of key bindings
	// This is a map from uppercase keycodes (`event.key.toUpperCase()`) to
	// abstract keys (the KEYS set).
	// TODO: Make bindings configurable in some sort of menu?
	static _BINDINGS = new Map([
		["A",          "LEFT"           ],
		["ARROWLEFT",  "LEFT"           ],
		["D",          "RIGHT"          ],
		["ARROWRIGHT", "RIGHT"          ],
		["W",          "UP"             ],
		["ARROWUP",    "UP"             ],
		["S",          "DOWN"           ],
		["ARROWDOWN",  "DOWN"           ],
		["U",          "UNICENTER"      ],
		["R",          "RESPAWN"        ],
		[" ",          "FIRE"           ],
		["SPACEBAR",   "FIRE"           ], // <-- For compatibility with old browsers:
		["X",          "DOCK"           ], // https://mzl.la/3UqaoAM
		["E",          "HEAL"           ],
		["SHIFT",      "BOOMDASH"       ],
		["/",          "DEBUGGER"       ],
		["I",          "DART_MOVE_LEFT" ],
		["K",          "DART_MOVE_RIGHT"],
		["J",          "DART_TURN_LEFT" ],
		["L",          "DART_TURN_RIGHT"],
		["M",          "DART_FIRE"      ]
	]);

	// Create a new keyboard manager
	// Once you create a keyboard manager, never delete it; doing so results in
	// undefined behavior. Only create one keyboard manager per application;
	// creating more than one results in undefined behavior.
	constructor() {
		// Create a map storing the current state of the keyboard
		// This is a map from abstract keys (the KEYS set) to an object with the
		// following properties:
		//     pressed - boolean, true if the key pressed in the last tick
		//     held    - boolean, true if the key is currently held down
		// This map should always have an entry for each key in KEYS.
		this._state = new Map();
		Keyboard.KEYS.forEach(key => {
			this._state.set(key, {
				pressed: false,
				held:    false
			})
		});
		// Create property to track activity
		this.activeThisTick = false;
		// Create the control stack
		// Elements in this stack should be instances of Keyboard.Control; the
		// last element is topmost.
		this._controlStack = [];
		// Create a set for overrides that we deleted this tick
		this._deletedOverrides = new Set();
		// Add event listeners
		// The listeners use capturing propagation, which should make more sense
		// than bubbling propagation if I ever end up putting separate keyboard
		// event listeners on child elements. If I want to make safe deletion of
		// a keyboard manager possible, I should add a destructor that removes
		// these event listeners, but I haven't done that yet.
		window.addEventListener("keydown", this._downListener.bind(this), true);
		window.addEventListener("keyup", this._upListener.bind(this), true);
	}

	// Push a control to the control stack
	// Control is a valid Keyboard.Control.
	pushControl(control) {
		if (!((control instanceof Keyboard.Control) && control.valid)) {
			logger.error("keyboard.js", "invalid control");
			return;
		}
		if (this._controlStack.some(c => (c.name === control.name))) {
			logger.error("keyboard.js", `name "${control.name}" is already in use`);
			return;
		}

		this._controlStack.push(control);
	}

	// Pop a control from the control stack
	// name is the name of the control to pop. Note that this function only pops
	// the topmost control from the control stack, so it doesn't actually need
	// to know the name of the control; the name is only checked for catching
	// potential bugs.
	popControl(name) {
		let topControl = this._controlStack[this._controlStack.length - 1];
		if (topControl.name !== name) {
			logger.error("keyboard.js", `name mismatch on pop: "${name}"`
							+ ` !== "${topControl.name}"`);
			return;
		}

		// Add overrides from the control to deleted overrides
		topControl.overrides
			.forEach(this._deletedOverrides.add, this._deletedOverrides);

		this._controlStack.pop();
	}

	// Return true iff a control with name is in the control stack
	hasControl(name) {
		return this._controlStack.some(control => (control.name === name));
	}

	// Get actions for a given control for the current tick
	// name is the name of the control on the control stack. This function
	// returns a set of the action strings specified in the control.
	getActions(name) {
		// Honor overrides from controls that were deleted this tick, otherwise deleting
		// a control and checking an overwritten control in the same tick can lead to
		// weird behavior dependent on the order of operations.
		if (this._deletedOverrides.has(name)) return new Set();

		// Traverse the control stack from top to bottom to find the control of interest
		// and check if it is overridden.
		let control = null;
		for (let i = this._controlStack.length; i--;) {
			let c = this._controlStack[i];

			if (c.name === name) {
				// Found the control of interest, no need to check further
				control = c;
				break;
			}

			// If the control of interest is overridden, return no actions
			// Don't check overrides of controls that were just created: doing so can lead
			// to all sorts of weirdness because the control exists for only part of a
			// tick.
			if (!c.justCreated && c.overrides.has(name)) return new Set();
		}

		if (control === null) {
			logger.error("keyboard.js", `no control with name ${name}`);
			return new Set();
		}

		// Create a set for collecting the actions
		let collectedActions = new Set();

		for (let [key, status] of this._state) {
			// Check for the press action
			if ((control.actions.has(`.${key}`) && status.pressed)) {
				collectedActions.add(control.actions.get(`.${key}`));
			}

			// Check for the hold action
			if (control.actions.has(`_${key}`) && status.held) {
				collectedActions.add(control.actions.get(`_${key}`));
			}
		}

		return collectedActions;
	}

	// Clear tick-specific data
	// Call this method at the very end of every tick.
	clearStuff() {
		// Clear presses
		this._state.forEach((status, key, keyboard) => {
						keyboard.get(key).pressed = false
		});

		// Clear activity
		this.activeThisTick = false;

		// Clear deleted overrides
		this._deletedOverrides.clear();

		// Age controls
		this._controlStack.forEach(control => {
			control.justCreated = false;
		});
	}

	// Update this._state on keydown
	// This is an event listener passed to the window.
	_downListener(event) {
		// Convert keycode to uppercase to simplify dealing with SHIFT
		let keycode = event.key.toUpperCase();
		if (Keyboard._BINDINGS.has(keycode)) {
			let key = Keyboard._BINDINGS.get(keycode);
			// Was the key already held? If no, then it was just pressed
			let pressed = !this._state.get(key).held;
			// Use ||= in case the key gets 2+ keydown events in one tick
			this._state.get(key).pressed ||= pressed;
			this._state.get(key).held = true;

			if (pressed) this.activeThisTick = true;

		}
	}

	// Update this._state on keyup
	// This is an event listener passed to the window.
	_upListener(event) {
		// Convert keycode to uppercase to simplify dealing with SHIFT
		let keycode = event.key.toUpperCase();
		if (Keyboard._BINDINGS.has(keycode)) {
			let key = Keyboard._BINDINGS.get(keycode);
			this._state.get(key).held = false;

			this.activeThisTick = true;
		}
	}

	// TODO: Verify whether this note is still correct after recent code changes
	// This implementation is not perfect. It malfunctions if you press a lot of
	// special keys at the same time and then type several letters, and it also
	// malfunctions if you press two of the same key at the same time, like left
	// and right CTRLs. However, I think it's good enough for me.
}



Keyboard.Control = class {
	// Create a new keyboard control
	// Sample usage:
	//     new Keyboard.Control(
	//         "COOL_CONTROL",
	//         new Map([
	//             [".UP",   "DO_SOMETHING_ON_UP_PRESS" ],
	//             ["_DOWN", "DO_SOMETHING_ON_DOWN_HOLD"]
	//         ]),
	//         new Set(["OVERRIDDEN_CONTROL"])
	//     )
	constructor(name, actions, overrides) {
		this.valid = false;

		// Validate name
		if (!Keyboard.Control._isValidName(name)) {
			logger.error("keyboard.js", `name "${name}" has incorrect format`);
			return;
		}

		// Validate actions
		if (!(actions instanceof Map)) {
			logger.error("keyboard.js", "actions is not a Map");
			return;
		}
		for (let [key, action] of actions) {
			if (!((typeof key === "string") && [".", "_"].includes(key[0])
							&& Keyboard.KEYS.has(key.slice(1)))) {
				logger.error("keyboard.js", `invalid key "${key}"`);
				return;
			}
			if (!((typeof action === "string") && action.length > 0)) {
				logger.error("keyboard.js", `invalid action "${action}"`);
				return;
			}
		}

		// Validate overrides
		if (!(overrides instanceof Set)) {
			logger.error("keyboard.js", "overrides is not a Set");
			return;
		}
		for (let override of overrides) {
			if (!Keyboard.Control._isValidName(override)) {
				logger.error("keyboard.js",
								`override "${override}" has incorrect format`);
				return;
			}
		}

		// All validation checks passed
		this.valid = true;

		this.name = name;
		this.actions = actions;
		this.overrides = overrides;

		// Whether the control was created this tick
		this.justCreated = true;
	}

	// Return true iff name is a valid control name
	static _isValidName(name) {
		return /^[A-Z_]+$/.test(name);
	}
}



// Create an instance of Keyboard, to be used throughout the game
// TODO: Create an instance of Keyboard in some top-level class instead?
export let keyboard = new Keyboard();
