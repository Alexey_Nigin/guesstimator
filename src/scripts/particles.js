import {Container, Sprite} from "pixi.js";
import {getTexture} from "./assets.js";
import {Vector} from "./i.js"; // ./vector.js

export class Particles {
	// Types of particles
	static TYPES = {
		BEACONY_CALL: "BEACONY_CALL",
		BLASTER: "BLASTER",
		SIZY_BLASTER: "SIZY_BLASTER",
		SLEEP: "SLEEP"
	};

	// Info about each particle type
	static INFO = new Map([
		[
			Particles.TYPES.BEACONY_CALL,
			{
				texture: "images/particles/beacony-call.svg",
				len: 0.70,
				location: "BACK",
				// TODO: Actually put movements here
				movement: "ONE_AND_ONLY"
			}
		],
		[
			Particles.TYPES.BLASTER,
			{
				texture: "images/particles/blaster.svg",
				len: 0.05,
				location: "FRONT",
				// TODO: Actually put movements here
				movement: "ONE_AND_ONLY"
			}
		],
		[
			Particles.TYPES.SIZY_BLASTER,
			{
				texture: "images/particles/sizy-blaster.svg",
				len: 0.05,
				location: "FRONT",
				// TODO: Actually put movements here
				movement: "ONE_AND_ONLY"
			}
		],
		[
			Particles.TYPES.SLEEP,
			{
				texture: "images/particles/sleep.svg",
				len: 0.20,
				location: "FRONT",
				// TODO: Actually put movements here
				movement: "ONE_AND_ONLY"
			}
		]
	]);

	// Create a new particle manager
	// Create one per space, space is a reference to space. Once created, add
	// this.display to space.
	constructor(space) {
		this.space = space;

		this.displayBack = new Container();
		this.displayFront = new Container();
	}

	// Advance all particles by one frame
	// dt is the time since last update, in seconds. screenSize is a vector
	// containing the screen size, in pixels. pixelsPerFlare is a real number.
	update(dt, screenSize, pixelsPerFlare) {
		for (let i = this.displayBack.children.length; i--;) {
			this._updateParticle(this.displayBack.children[i], dt, screenSize,
							pixelsPerFlare);
		}
		for (let i = this.displayFront.children.length; i--;) {
			this._updateParticle(this.displayFront.children[i], dt, screenSize,
							pixelsPerFlare);
		}
	}

	// Add a splash of particles
	// type is one of Particles.TYPES. pos is the pos at which to spawn
	// particles. vel is the vel of the parent, or null to assume no vel of the parent.
	// number is the number of particles to spawn. parentId is the ID of the parent thing,
	// only needed for some particle types.
	// Particles disappear on their own, once you spawn them you can forget
	// about them.
	// TODO: Assign importance to particles and silently skip adding
	// low-importance particles if there are already too many particles present.
	add(type, pos, vel, number, parentId) {
		console.assert(Object.values(Particles.TYPES).includes(type));
		let typeInfo = Particles.INFO.get(type);

		for (let i = 0; i < number; ++i) {
			let particle = new Sprite(getTexture(typeInfo.texture));
			particle.anchor.set(0.5, 0.5);
			particle.type = type;
			particle.len = typeInfo.len;
			particle.movement = typeInfo.movement;
			particle.pos = new Vector(pos);
			particle.age = 0;

			switch (type) {
				case Particles.TYPES.BEACONY_CALL:
					particle.vel = new Vector(this.space.requestThingById(parentId).vel);
					particle.rot = 0;
					particle.parentId = parentId;
					break;
				case Particles.TYPES.BLASTER:
				case Particles.TYPES.SIZY_BLASTER:
					particle.vel = (vel === null) ? Vector.chooseLimited(1)
									: new Vector(vel);
					particle.rot = 0;
					break;
				case Particles.TYPES.SLEEP:
					particle.vel = (vel === null) ? new Vector() : new Vector(vel);
					particle.vel.sub(new Vector(0, 0.15));
					particle.rot = - Math.PI / 2;
					particle.phase = 2 * Math.PI * Math.random();
					break;
			}

			let correctDisplay = (typeInfo.location === "FRONT")
				? this.displayFront : this.displayBack;
			correctDisplay.addChild(particle);
		}
	}

	// Update a single particle
	// May delete the particle from the parent.
	_updateParticle(particle, dt, screenSize, pixelsPerFlare) {
		// Move the particle

		particle.age += dt;

		switch (particle.type) {
			case Particles.TYPES.BEACONY_CALL:
				let parent = this.space.requestThingById(particle.parentId);
				if (parent === null || particle.age > 1) {
					// Bye-bye particle
					particle.parent.removeChild(particle);
					return;
				}
				particle.pos.copyFrom(parent.pos);
				particle.vel.copyFrom(parent.vel);
				particle.len = 0 + 6 * (particle.age / 1);
				particle.alpha = Math.min(4 * (1 - particle.age), 1);
				break;
			case Particles.TYPES.BLASTER:
			case Particles.TYPES.SIZY_BLASTER:
				if (particle.age > 0.5) {
					// Bye-bye particle
					particle.parent.removeChild(particle);
					return;
				}
				particle.pos.add(Vector.scale(particle.vel, dt));
				particle.alpha = Math.min(4 * (0.5 - particle.age), 1);
				break;
			case Particles.TYPES.SLEEP:
				if (particle.age > 3) {
					// Bye-bye particle
					particle.parent.removeChild(particle);
					return;
				}
				particle.vel.add(new Vector(
					0.3 * Math.sin(Math.PI * particle.age + particle.phase),
					0
				).scale(dt));
				particle.pos.add(Vector.scale(particle.vel, dt));
				particle.alpha = Math.min(
					2 * particle.age,
					Math.min(4 * (3 - particle.age), 1)
				);
				break;
		}

		// Draw the particle

		// Switch from pos to position
		// pos is absolute, position is relative to the screen.
		let position = this.space.requestPosToPosition(particle.pos,
						pixelsPerFlare);

		let buffered_radius = pixelsPerFlare * particle.len;
		if (position.x + buffered_radius < 0
						|| position.x - buffered_radius > screenSize.x
						|| position.y + buffered_radius < 0
						|| position.y - buffered_radius > screenSize.y) {
			particle.visible = false;
			return;
		}

		particle.visible = true;
		// Rescale the particle so that its height is equal to len
		particle.scale.set(particle.len * pixelsPerFlare
						/ particle.texture.height);
		particle.position.copyFrom(position);

		// I draw images pointing up, but at rot 0 they should point right
		// (towards positive x), so add a 90 degree angle here.
		particle.rotation = particle.rot + Math.PI / 2;
	}
}
