import {Application, Assets} from "pixi.js";
import {Vector} from "./i.js"; // ./vector.js

// I need this code, but I don't know where to put it

export let DEBUG = false;

export const VERSION = "v0.13.0+";

export let G = new Application();

// Enable mipmapping for all textures by default
// settings.MIPMAP_TEXTURES = MIPMAP_MODES.ON;

export let MOUSE = new Vector();
// TODO This is a hack
export function setMousePosition(position) {
	MOUSE.copyFrom(position);
}

export function sq(x) {
	return x * x;
}

export function lerp(min, max, fraction) {
	return min + fraction * (max - min);
}

// Deep copy an object or an array
// This function silently discards any non-JSON-encodable data.
export function deepCopy(x) {
	return JSON.parse(JSON.stringify(x));
}

// Return a mod b
// The return value is always non-negative, unlike with %.
export function modulo(a, b) {
	return ((a % b) + b) % b;
}

// Return the alpha - beta difference in angles
// alpha, beta, and the return value are in radians
export function angleDiff(alpha, beta) {
	return modulo(alpha - beta + Math.PI, 2 * Math.PI) - Math.PI;
}
