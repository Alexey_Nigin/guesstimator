# Font assembly

We're using DejaVu fonts version 2.37

Then upload the `.ttf` to https://snowb.org, bump up spacing, set padding to 0, set
sharpness to 100%
