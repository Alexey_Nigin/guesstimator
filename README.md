# Project Guesstimator

Welcome to Project Guesstimator (name not final), an online space shooter with
old-school arcade mechanics and a massive open world!

## Credits

This game was created by Alexey Nigin, and is available under the
[Creative Commons Attribution-NonCommercial](https://creativecommons.org/licenses/by-nc/4.0/)
license. See [`LICENSE.txt`](LICENSE.txt) for legal stuff.

### Playtesters

The following amazing people have playtested this game:

* Ansh
* Cossette
* Damiya
* Deidre Mitchell
* Granddad
* Grandma
* Rachel
* Xavier

For privacy reasons, I only add people to the list above after getting their
explicit permission and preferred display name. If you playtested the game and
would like to be on this list, but I haven't asked you about this list yet, feel
free to speed up the process by sending me a note!

### Third-party libraries

Thank you to the these open-source libraries that made Project Guesstimator possible!

* [PixiJS](https://pixijs.com/), which powers all graphics in the game
* [detect-collisions](https://www.npmjs.com/package/detect-collisions), which is
  responsible for detecting all collisions between things
* [DejaVu fonts](https://github.com/dejavu-fonts/dejavu-fonts), a collection of beautiful
  and easy-to-read fonts used across the game

### Graphics

All graphics currently in the game were created in [Inkscape](https://inkscape.org/).

Graphics from [Kenney](https://www.kenney.nl/) helped me massively during the
early stages of development, though you won't see any of those graphics in the
current version of the game.

### Inspirations

Project Guesstimator takes a lot of inspiration from [Astroflux](https://astroflux.net/)
and [Asteroids](https://en.wikipedia.org/wiki/Asteroids_(video_game)).

### Extra thanks to

* Anastasia K. for suggesting the idea of Space Meatballs
* Michel Weststrate, whose
  [article](https://medium.com/visual-development/how-to-fix-nasty-circular-dependency-issues-once-and-for-all-in-javascript-typescript-a04c987cf0de)
  helped me organize the project's internal dependencies
* Taylor for helping me settle on a Petoskey stone as the texture for Cool Rock
