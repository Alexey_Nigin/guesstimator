const path = require('path');

module.exports = {
	// mode: 'production',
	mode: 'development',
	entry: {
		index: './src/scripts/app.js',
		test: './src/scripts/test/main.js'
	},
	devtool: 'source-map',
	devServer: {
		static: './dist',
		port: 35571
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist')
	},
	performance: {
		hints: false
	}
};
