# Trivia

## Items

Many items are based on real-world materials:

* Cool rock is based on a Petoskey stone.
* Aluminum ore is based on bauxite.
* Titanium ore is based on purified titanium dioxide.
* Platinum ore is based on sulfidic serpentinite.
* Yellow gem refining to polymers is inspired by amber.

Other items drew inspiration from all over the place:

* The red gem is based on the old placeholder "gem" item, the sprite for which
  was taken from a spritesheet by Kenney.nl.
* The central shapes of the yellow and green gems are based on an anecdote of an
  undergrad math class debating what shape is the best, with the two most common
  opinions being a circle and a triangle. Real crystalline lattices don't
  normally form circles.

## Things

* Space meatballs were created to commemorate the brown asteroid sprites from Kenney.nl
  that I used before drawing my own asteroid sprites.
